<?php include('header.php');?>
<?php $bikes =  $query_bike; ?>
<?php $bikes_color = array('White','Silver','Black','Gray','Blue','Red','Brown','Green','Gold','Orange','Violet','Yellow','Others');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s">Submit Your Vehicle</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Add Your Vehicle In Our Listings</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/post_bike" class="b-breadCumbs__page m-active">Submit a Vehicle</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
						<div class="b-infoBar__progress wow zoomInUp" data-wow-delay="0.5s">
							<div class="b-infoBar__progress-line clearfix">
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
									<div class="b-infoBar__progress-line-step-circle m-last">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<div class="b-submit">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
						<aside class="b-submit__aside">
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 1</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-motorcycle"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Add Your Vehicle</h4>
										<p>Select your vehicle &amp; add info</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 2</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-picture-o"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Photos &amp; Videos</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>
<!--							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-phone-square"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Contact Details</h4>
										<p>Choose vehicle specifications</p>
									</div>
								</div>
							</div>-->
							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-globe"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Submit &amp; Publish</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>
						</aside>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
						<div class="b-submit__main">
							<header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
								<h2 class="">Add Your Vehicle Details</h2>
							</header>
							<form class="s-submit clearfix" action="<?php echo base_url();?>index.php/Welcome/post_bike_requirment_step2" method="POST">
									<input type="hidden"  name="vehicle_type" value="bike"/>
								<div class="row">
									<div class="col-md-6 col-xs-12">
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Enter Registration Year <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[reg_year]" required>
														<option value="">Select Year </option>
													<?php 
                          $year=date("Y");
                          for($i=$year; $i>=1980; $i--) {
                           
                           ?>
                                                 <option value="<?php echo $i;?>"><?php echo $i;?></option>
												   <?php } ?>
												</select>
												 <span class="fa fa-bikeet-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
										    <label>Select Vehicle Model<span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[model_id]" id="bike_model" required>
												
												</select>
												<span class="fa fa-bikeet-down"></span>
											</div>
										    
											
										</div>
										
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Kilometers Driven <span id="warning_box">*</span></label>
											<div class='s-relative'>
												<input placeholder="Kilometers Driven" type="text"  name="vehicle_info[kilometer_driven]" maxlength="6" id="kms" required/>
												 
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
										<label>Price Expectation <span id="warning_box2">*</span></label>
										<input placeholder="e.g. 3,00,000" type="text" name="price" maxlength="6" id="price_expec">
									</div>										
										
									</div>
									<div class="col-md-6 col-xs-12">
									    
									
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select bike <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[bike_id]" id="select_bike" required>
													<option value="">Select bike</option>
													<?php foreach ($query_bike as $bikes_name) { ?>
														<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
												<?php	} ?>
												</select>
												<span class="fa fa-bikeet-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Variant <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[variant]" id="variant" required>
													
												</select>
												<span class="fa fa-bikeet-down"></span>
											</div>
										</div>
																				
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Color <span>*</span></label>
											<div class='s-relative color'>
												<select class="m-select" name="vehicle_info[color]" id="select_color" required>
													<option value="">Select Color</option>
													<?php foreach($bikes_color as $color) { ?>
													<option value="<?php echo $color;?>"><?php echo $color;?></option>
													<?php } ?>
												</select>
												<span class="fa fa-bikeet-down"></span>
											</div>
										</div>
										
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select City <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select single_select" name="vehicle_info[city]" required>
													<option value="" selected disabled="disabled">Select City</option>  
													<?php  foreach($query_cities as $cities){  ?>
                                                                <option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
                                                                
                                                                <?php } ?>
												</select>
												<span class="fa fa-bikeet-down"></span>
											</div>
										</div>
										
									</div>
								</div>
								<button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.5s">Save &amp; Next<span class="fa fa-angle-right"></span></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-submit-->


			<?php include('footer.php'); ?>
			
			<script>
			    $(document).ready(function(){
			
			   $(document).on("keyup","#kms",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box").html("*");
				   }
			   });
			   $(document).on("keyup","#price_expec",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box2").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box2").html("*");
				   }
			   });
				    $(document).on('change','#select_bike',function(){
					    var bike_id = $(this).val();
              $("#bike_model").html("<option>loading...</option>"); 
						$.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_model",{bike_id:bike_id},function(o){
						//console.log(o);
						    $("#bike_model").html(o);
						});
					});
					
				    $(document).on('change','#bike_model',function(){
					    var model_id = $(this).val();
               $("#variant").html("<option>loading...</option>"); 
						$.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_variant",{model_id:model_id},function(o){
						//console.log(o);
						    $("#variant").html(o);  
						});
					});	
					
					$(document).on("change",'#select_color',function(){
					    $color = $(this).val();
						//console.log($color);
						if($color=='Others'){
						 $('.color').html("<input placeholder='Enter color' type='text' name='vehicle_info[color]' required/>");
						}
					});
				});
			</script>