<?php include('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Used Listings</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query_bike);?> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/buyers_need" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

<!--		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="b-infoBar__compare wow zoomInUp" data-wow-delay="0.5s">
							<div class="dropdown">
								<a href="#" class="dropdown-toggle b-infoBar__compare-item" data-toggle='dropdown'><span class="fa fa-clock-o"></span>RECENTLY VIEWED<span class="fa fa-caret-down"></span></a>
								<ul class="dropdown-menu">
									<li><a href="detail.html">Item</a></li>
									<li><a href="detail.html">Item</a></li>
									<li><a href="detail.html">Item</a></li>
									<li><a href="detail.html">Item</a></li>
								</ul>
							</div>
							<a href="<?php echo base_url();?>index.php/Welcome/bike_compare" class="b-infoBar__compare-item"><span class="fa fa-compress"></span>COMPARE BIKES</a>
						</div>
					</div>
					<div class="col-lg-8 col-xs-12">
						<div class="b-infoBar__select">
							<form method="post" action="/">
								<div class="b-infoBar__select-one wow zoomInUp" data-wow-delay="0.5s">
									<span class="b-infoBar__select-one-title">SELECT VIEW</span>
									<a href="listingsTwo.html" class="m-list m-active"><span class="fa fa-list"></span></a>
									<a href="listTable.html" class="m-table"><span class="fa fa-table"></span></a>
								</div>
								<div class="b-infoBar__select-one wow zoomInUp" data-wow-delay="0.5s">
									<span class="b-infoBar__select-one-title">SHOW ON PAGE</span>
									<select name="select1" class="m-select">
										<option value="" selected="">10 items</option>
									</select>
									<span class="fa fa-caret-down"></span>
								</div>
								<div class="b-infoBar__select-one wow zoomInUp" data-wow-delay="0.5s">
									<span class="b-infoBar__select-one-title">SORT BY</span>
									<select name="select2" class="m-select">
										<option value="" selected="">Last Added</option>
									</select>
									<span class="fa fa-caret-down"></span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>b-infoBar-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
                                    <div id="filter_data">
					<div class="col-lg-8 col-sm-8 col-xs-12">
						<div class="b-items__cars" id="bike_filter">
                                                    <div class="b-items__cars">
							<?php  if(isset($query_car)) { ?>
							 <?php $car_option = array();?>
							<?php  foreach($query_car as $car_list) {  ?>

							<?php // var_dump($car_list);?>
                              <?php $car_info = json_decode($car_list->vehicle_info);?>
                              <?php $additional_info = json_decode($car_list->additional_info);?>
                              <?php $cars = $car_info->vehicle_info->car;?>
                             
                              
                              <?php array_push($car_option, $cars);?>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<img src="<?php echo base_url();?>media/270x230/dodge.jpg" alt='dodge'/>
<!--									<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>-->
									<span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
									<form method="post">
										<input type="checkbox" name="check1"/>
										<label for="check1" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
									</form>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_info->vehicle_info->car;?> <?php echo $car_info->vehicle_info->model;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $car_list->price;?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $car_info->vehicle_info->kilometer_driven;?> KM
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $car_info->vehicle_info->reg_year;?></a>
											<a href="#">Used</a>
											<a href="#">Color:-<?php echo $car_info->vehicle_info->color;?> </a>
											<a href="#"><?php echo $car_info->vehicle_info->fuel;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  } } ?>
							
						</div>
                                                    <div class="bike_details_after_filter">
		 		<?php 
                                           $bike_name='';
                                           $model_name= '';
                                           if(isset($query_bike)) { ?>
							 <?php $bike_option = array();?>
							<?php  foreach($query_bike as $bike_list) {  ?>

							<?php // var_dump($bike_list);?>
                              <?php $bike_info = json_decode($bike_list->vehicle_info);?>
                              <?php $additional_info = json_decode($bike_list->additional_info);?>
                              <?php $bikes = $bike_info->vehicle_info->bike_id;
                              
                               foreach($query_bike_details as $bike_details)
                                   {
                                   if($bike_details->id == $bike_info->vehicle_info->model_id)
                                       {
                                           $bike_name = $bike_details->bike_name;
                                           //var_dump($bike_details->bike_name);
                                           $model_name = $bike_details->model_name;
                                       }
                                   
                               }
                              //array_push($bike_option, $bikes);?>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<img src="<?php echo base_url();?>uploads/used_bikes/<?php echo $additional_info->additional_info->img[0];?>" alt='bike' height="230px" width="270px"/>
									<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>PHOTOS</a>
									<span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
									<form method="post">
										<input type="checkbox" name="check1" />
										<label for="check1" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
									</form>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name,' ',$model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $bike_list->price;?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $bike_info->vehicle_info->kilometer_driven;?> KMs
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $bike_info->vehicle_info->reg_year;?></a>
											
											<a href="#">Color:-<?php echo $bike_info->vehicle_info->color;?> </a>
                                                                                        <a href="#">City:-<?php echo $bike_info->vehicle_info->city;?></a>
<!--											<a href="#"><?php echo $bike_info->vehicle_info->fuel;?></a>-->
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  } } ?>
						    </div>	
						</div>
                                            
					
							
						<div class="b-items__pagination wow zoomInUp" data-wow-delay="0.5s">
							<div class="b-items__pagination-main">
								<a data-toggle="modal" data-target="#myModal" href="#" class="m-left"><span class="fa fa-angle-left"></span></a>
								<span class="m-active"><a href="#">1</a></span>
								<span><a href="#">2</a></span>
								<span><a href="#">3</a></span>
								<span><a href="#">4</a></span>
								<a href="#" class="m-right"><span class="fa fa-angle-right"></span></a>    
							</div>
						</div>

					</div>
                                    </div>    
                                    
					<div class="col-lg-3 col-sm-4 col-xs-12">
						<aside class="b-items__aside">
							<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">REFINE YOUR SEARCH</h2>
							<div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s">
								<form action="<?php echo base_url();?>index.php/Welcome/search_cars" method="post"> 
									<div class="b-items__aside-main-body">
										<div class="b-items__aside-main-body-item">
											<label>SELECT BY LOCATION	</label>
										
											<div>
												<select name="city" class="m-select" id="select_city">
													<option value="" selected="" disabled>Select City</option>
													<?php  if(isset($query_cities)) { ?>
													<?php foreach ($query_cities as $city_name) { ?>
														<option value="<?php echo $city_name->city_name;?>"><?php echo $city_name->city_name;?></option>
													<?php } } ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
										<div class="b-items__aside-main-body-item">
											<label>SELECT Price</label>
											<div id="price_tab">
												<select name="price" class="m-select" id="select_price">
                                                                                                    <option value="">Select Price</option>								<option value="50000">0-50000 Thousand</option>
                                                                                                    <option value="50000-100000">50000 Thousand - 1 lacs</option>
													<option value="1 lac & above">1 lacs & Above</option>
                                                   </select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
										<div class="b-items__aside-main-body-item">
											<label>SELECT TYPE</label>
											<div>
												<select name="price" class="m-select" id="select_kms">
                                                                                                    <option value="car">CAR</option>		
                                                                                                    <option value="bike">BIKE</option>
																									</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
									</div>
									<footer class="b-items__aside-main-footer">
<!--										<button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br />-->
										 
									</footer>
								</form>
							</div>
						
						</aside>
					</div>
				</div>
			</div>
		</section><!--b-items-->


		<?php include('footer.php');?>

<script>
		$(document).ready(function(){
				    $(document).on('change','#select_city',function(){
					    var city = $(this).val();
                                            $('#select_price').val("");
				$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_city",{city_id:city_id},function(o){
						//console.log(o);
						    $("#filter_data").html(o);
						});

			            });
                                 
                                    $(document).on('change','#select_price',function(){
					    var price = $(this).val();
                                            var city = $("#select_city").val();
                                            $("#type").val("");
                                            
					$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_city_price",{city:city,price:price},function(o){
						//console.log(o);
						    $("#filter_data").html(o);
						});
			            });
                                    
                                    $(document).on('change','#select_kms',function(){
                                            var kms = $(this).val(); 
					    var price = $("#select_price").val();
				            var model_id = $("#select_model").val();
                                            var bike_id = $("#select_bike").val();
                                            console.log(model_id,' ',bike_id);
					
                                        $.post("<?php echo base_url();?>index.php/Welcome/bike_kms_filter",{kms:kms,price:price,bike_id:bike_id,model_id:model_id},function(o){
                                           $(".bike_details_after_filter").html(o); 
                                        });
                                    
			            });
				});
			</script>