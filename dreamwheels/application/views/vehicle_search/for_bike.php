<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>">Bike Search</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>">
					<h3>Your search returned <span id="total_results"><?php echo count($query);?></span> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
				<a href="<?php echo base_url();?>index.php/Welcome/bike" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url().'index.php/Welcome/vehicle_search?'.http_build_query($_GET)?>" class="b-breadCumbs__page m-active">Available Bikes</a>
			</div>
		</div><!--b-breadCumbs-->

	        <section class="b-items s-shadow">
			<div class="container">
				<div class="row">
				<!--<div class="col-lg-9 col-sm-8 col-xs-12"> -->
					<div class="col-lg-11 col-sm-10 col-xs-12">
						<div class="b-items__cars">
						<table id="targetTable">
						<thead>
								<tr>
									<td>Bikes</td>
								</tr>
							</thead>
              <tbody  id="ajax-data">
			<?php  if(count($query)) {
                            
                            if(isset($_GET['vehicle_condition']) && $_GET['vehicle_condition']=='new')
                            {
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                            
                          foreach($query as $bike_list) {
                 
                 $bike_info = json_decode($bike_list->bike_info);
                          $bike_name = $bike_list->bike_name;
                          $model_name =$bike_list->model_name;
                          $variant_name =$bike_list->variant;
                              
                  $engine_transmission = json_decode($bike_list->engine_transmission);
                  $dimension = json_decode($bike_list->dimension);
?>
                             
                              							  <tr>
								<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>">
  <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $bike_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:240px;" style="height:280px;"/>
                  </a>
									
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name;?> <?php echo $model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $bike_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> 
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
										
											<a >Displacement: <?php echo $engine_transmission->displacement;?> </a>
                                                                                        <a >Power: <?php echo $engine_transmission->maximum_power;?> </a>
											<a >Tank:<?php echo $dimension->fuel_capacity;?> </a>
											<a >Kerb Weight:<?php echo $dimension->kerb_weight;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
						<?php  } 
                          
                            }
                          else if(isset($_GET['vehicle_condition']) && $_GET['vehicle_condition']=='used')
                          {
                                           
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                           foreach($query as $bike_list) {
                         
                          $bike_info = json_decode($bike_list->vehicle_info);
                          $bike_name = $bike_list->bike_name;
                          $model_name =$bike_list->model_name;
                          $variant_name =$bike_list->variant;
                              
                  $additional_info = json_decode($bike_list->additional_info);
                 ?>
							  
							  <tr>
							    <td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>"><img src="<?php render_item_image("uploads/used_bikes/".$additional_info->additional_info->img[0],'bike');?>" alt="<?php echo $bike_name; ?> available on dreamwheels.in" height="230px" width="270px"/></a>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name,' ',$model_name;?></h2>
										<span><?php echo inr($bike_list->price);?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $bike_info->vehicle_info->kilometer_driven;?> KMs
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $bike_info->vehicle_info->reg_year;?></a>
											
											<a href="#">Color:-<?php echo $bike_info->vehicle_info->color;?> </a>
                                            <a href="#">City:-<?php echo $bike_info->vehicle_info->city;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
							  </tr>
						<?php  } 
                                                     
                          } 
                        } else{
                          ?>
            
<!--                          <h3><center>No result found</center></h3>-->
                         <?php } ?>
                            </tbody>
						</table>	
						</div>
                                            <div class="ajax-load text-center " style="display:none;">
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>

					</div>
				
				</div>
			</div>
		</section><!--b-items-->
                