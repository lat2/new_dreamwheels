<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>">Car Search</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>">
					<h3>Your search returned <span id="total_results"><?php echo count($query);?></span> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url().'index.php/Welcome/vehicle_search?'.http_build_query($_GET)?>" class="b-breadCumbs__page m-active">Available Cars</a>
			</div>
		</div><!--b-breadCumbs-->

	        <section class="b-items s-shadow">
			<div class="container">
				<div class="row">
				<!--<div class="col-lg-9 col-sm-8 col-xs-12"> -->
					<div class="col-lg-11 col-sm-10 col-xs-12">
						<div class="b-items__cars">
						<table id="targetTable">
						<thead>
								<tr>
									<td>Cars</td>
								</tr>
							</thead>
              <tbody  id="ajax-data">
			<?php  if(count($query)) {
                            
                            if(isset($_GET['vehicle_condition']) && $_GET['vehicle_condition']=='new')
                            {
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $car_list) { 
                          $car_info = json_decode($car_list->car_info);
                           //print_r($car_info);
                          $car_name = @$car_list->car_name;
                          $model_name =@$car_list->model_name;
                          $variant_name =@$car_list->variant;
                              
                  $engine_transmission = json_decode($car_list->engine_transmission);
                 ?>
						<tr>
							<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>">
                              <img src="<?php render_item_image("uploads/car/".$car_info->images[0]);?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:240px;" style="height:280px;"/>
                  </a>
								
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h2>
										<span><?php  echo $car_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> KMPL
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
                                                                                    <a >Displacement: <?php echo $engine_transmission->displacement;?></a>
                                                                                    <a >Power: <?php echo $engine_transmission->maximum_power;?></a>
                                                                                    <a >Torque: <?php echo $engine_transmission->maximum_torque;?> </a>
											<a >Fuel: <?php echo $engine_transmission->fuel_type;?></a>
											<a >Gear: <?php echo $engine_transmission->gear_box;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
							<?php 
                          }
                            }
                          else if(isset($_GET['vehicle_condition']) && $_GET['vehicle_condition']=='used')
                          {
                                           
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $car_list) {
                         
                          $car_info = json_decode($car_list->vehicle_info);
                          $car_name = $car_list->car_name;
                          $model_name =$car_list->model_name;
                          $variant_name =$car_list->variant;
                              
                  $additional_info = json_decode($car_list->additional_info);
                 ?>
						<tr>
							<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>">
                              <img src="<?php render_item_image("uploads/used_cars/".$additional_info->additional_info->img[0]);?>" alt="<?php echo $car_name; ?> available on dreamwheels.in" style="width:240px;" style="height:280px;"/>
                  </a>
								
								</div>
									<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name;?></h2>
										<span><?php inr($car_list->price); ?> </span>
									</header>
									<p>
                    <?php  if(isset($additional_info->additional_info->comments)){echo $additional_info->additional_info->comments; }?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $car_info->vehicle_info->kilometer_driven;?> KM
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $car_info->vehicle_info->reg_year;?></a>
											<a href="#">Used</a>
											<a href="#">Color:-<?php echo $car_info->vehicle_info->color;?> </a>
											<a href="#"><?php echo $car_info->vehicle_info->fuel;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>
                                                
                                                
                          <?php                      
                          }
                                                     
                          } 
                        } else{
                          ?>
            
<!--                          <h3><center>No result found</center></h3>-->
                         <?php } ?>
                            </tbody>
						</table>	
						</div>
                                            <div class="ajax-load text-center " style="display:none;">
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>

					</div>
				
				</div>
			</div>
		</section><!--b-items-->
                