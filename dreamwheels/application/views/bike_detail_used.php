<?php include('header.php');?>
<?php 
                            $bike_name='';
                            $model_name= '';
                            if(isset($query)) { 
                                $bike_option = array();
                                foreach($query as $bike_list) {  
                                    // var_dump($bike_list);
                                    $bike_info = json_decode($bike_list->vehicle_info);
                                    $additional_info = json_decode($bike_list->additional_info);
                                    $user_info = json_decode($bike_list->user_info);
									//var_dump($user_info->name);?>   
                              <?php $bikes = $bike_info->vehicle_info->bike_id;
                              
                               foreach($query1 as $bike_details)
                                   {
                                   if($bike_details->id == $bike_info->vehicle_info->model_id)
                                       {
                                           $bike_name = $bike_details->bike_name;
                                           //var_dump($bike_details->bike_name);
                                           $model_name = $bike_details->model_name;
                                       }
                                   
                               }
                                           } }
                              //array_push($bike_option, $bikes);?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s"><?php echo $bike_name;?> <?php echo $model_name;?></h1>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
                            <a href="<?php echo base_url();?>index.php/Welcome/bike" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/used_bike_listing" class="b-breadCumbs__page">Used Bikes</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page"><?php echo $bike_name;?></a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active"><?php echo $model_name;?></a>
			</div>
		</div>
     
		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?php echo $bike_name;?> <?php echo $model_name;?> </h1>
								 
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="b-detail__head-price">
								<div class="b-detail__head-price-num"> <?php echo inr($bike_list->price);?></div>
<!--								<p>Included Taxes &amp; Checkup</p>-->
							</div>
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s">
									<div class="row m-smallPadding">
										<div class="col-xs-10">
											<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
												 <?php  foreach($additional_info->additional_info->img as $img1) { ?>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"></a>
													<img class="img-responsive center-block" src="<?php render_item_image("uploads/used_bikes/".$img1,'bike');?>" alt="<?php echo $bike_name; ?> available on dreamwheels.in" />
												</li>
				<?php	//var_dump( $img1);?>							
												<?php } ?>
												
											</ul>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical">
											<div class="b-detail__main-info-images-small" id="bx-pager">
												<?php  $i=0; foreach($additional_info->additional_info->img as $img) { ?>
												<a href="#" data-slide-index="<?php echo $i;?>" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src='<?php render_item_image("uploads/used_bikes/".$img,'bike');?>' alt="<?php echo $bike_name; ?> available on dreamwheels.in" />
												</a>
													<?php $i++; } ?>
												
											</div>
										</div>
									</div>
								</div>

								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#info1'>Seller's Comments			
									</div>
									<div id="info1">
										<p><?php echo $additional_info->additional_info->comments;?></p>
									</div>
                                                                  								</div>
							
							</div>
<!--                                                   <h2 class="border-heading-h2 margin-bottom-20" id="specs-overview-div"><?php echo $bike_info->bike,' ',$bike_info->model;?> Specifications</h2>-->
                                                   <div class="panel-group" id="accordion">
    <div class="panel panel-default">
<!--      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><b>Overview</b></a>
        </h4>
      </div>-->
<!--      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->front_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->rear_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Wheel Type</div></td><td><div class="engineright">Alloy</div></td></tr><tr><td><div class="engineleft enginetext">Starting</div></td><td><div class="engineright"><?php echo $engine_transmission->starting;?></div></td></tr><tr><td><div class="engineleft enginetext">Fuel Capacity</div></td><td><div class="engineright"><?php echo $dimension->fuel_capacity;?></div></td></tr><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Speed</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_speed;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?></div></td></tr></tbody></table>.
            </div>
          </div>
      </div>-->
    </div>
    <div class="panel panel-default">
<!--      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><b>Engine And Transmission</b></a>
        </h4>
      </div>-->
      <div id="collapse2" class="panel-collapse collapse">
<!--          <div class="panel-body"> 
              <div class="table-responsive">
            <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Power</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_power;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Torque</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_torque;?></div></td></tr><tr><td><div class="engineleft enginetext">Starting</div></td><td><div class="engineright"><?php echo $engine_transmission->starting;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Speed</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_speed;?></div></td></tr><tr><td><div class="engineleft enginetext">Cooling System</div></td><td><div class="engineright"><?php echo $engine_transmission->cooling_system;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?></div></td></tr><tr><td><div class="engineleft enginetext">Clutch</div></td><td><div class="engineright"><?php echo $engine_transmission->clutch;?></div></td></tr><tr><td><div class="engineleft enginetext">No. of Cylinders</div></td><td><div class="engineright"><?php echo $engine_transmission->no_of_cylinder;?></div></td></tr><tr><td><div class="engineleft enginetext">Drive Type</div></td><td><div class="engineright"><?php echo $engine_transmission->drive_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Valves (per cylinder)</div></td><td><div class="engineright"><?php echo $engine_transmission->valves_per_cylinder;?></div></td></tr><tr><td><div class="engineleft enginetext">Bore</div></td><td><div class="engineright"><?php echo $engine_transmission->bore;?></div></td></tr><tr><td><div class="engineleft enginetext">Stroke</div></td><td><div class="engineright"><?php echo $engine_transmission->stroke;?></div></td></tr><tr><td><div class="engineleft enginetext">Supply System</div></td><td><div class="engineright"><?php echo $engine_transmission->supply_system;?></div></td></tr><tr><td><div class="engineleft enginetext">Transmission Type</div></td><td><div class="engineright"><?php echo $engine_transmission->transmission_type;?></div></td></tr></tbody></table>
              </div> 
          </div>-->
      </div>
    </div>
<!--    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><b>Tyres And Brakes</b></a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Tyre Size</div></td><td><div class="engineright"><p>	<strong>Front :-</strong><?php echo $tyres_brakes->tyre_size_front;?>,</p><p>	<strong>Rear :-</strong><?php echo $tyres_brakes->tyre_size_rear;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Tyre Type</div></td><td><div class="engineright"><?php echo $tyres_brakes->tyre_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheel Size</div></td><td><div class="engineright"><p>	<strong>Front :-</strong><?php echo $tyres_brakes->wheel_size_front;?>,</p><p>	<strong>Rear :-</strong><?php echo $tyres_brakes->wheel_size_rear;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->front_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->rear_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake Diameter</div></td><td><div class="engineright"><?php echo $tyres_brakes->front_brake_diameter;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake Diameter</div></td><td><div class="engineright"><?php echo $tyres_brakes->rear_brake_diameter;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>-->
                                                        
<!--    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><b>Features And Safety</b></a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse"> 
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Speedometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->speedometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Tachometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->tachometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Odometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->odometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Tripmeter</div></td><td><div class="engineright"><?php echo $feature_safety->tripmeter;?></div></td></tr><tr><td><div class="engineleft enginetext">ABS</div></td><td><div class="engineright"><?php echo $feature_safety->abs;?></div></td></tr><tr><td><div class="engineleft enginetext">CBS</div></td><td><div class="engineright"><?php echo $feature_safety->cbs;?></div></td></tr><tr><td><div class="engineleft enginetext">EBS</div></td><td><div class="engineright"><?php echo $feature_safety->ebs;?></div></td></tr><tr><td><div class="engineleft enginetext">Seat Type</div></td><td><div class="engineright"><?php echo $feature_safety->seat_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Console</div></td><td><div class="engineright"><?php echo $feature_safety->console;?></div></td></tr><tr><td><div class="engineleft enginetext">Clock</div></td><td><div class="engineright"><?php echo $feature_safety->clock;?></div></td></tr><tr><td><div class="engineleft enginetext">Traction Control</div></td><td><div class="engineright"><?php echo $feature_safety->traction_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Cruise Control</div></td><td><div class="engineright"><?php echo $feature_safety->cruise_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Navigation</div></td><td><div class="engineright"><?php echo $feature_safety->navigation;?></div></td></tr><tr><td><div class="engineleft enginetext">Quick Shifter</div></td><td><div class="engineright"><?php echo $feature_safety->quick_shifter;?></div></td></tr><tr><td><div class="engineleft enginetext">Launch Control</div></td><td><div class="engineright"><?php echo $feature_safety->launch_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Power Modes</div></td><td><div class="engineright"><?php echo $feature_safety->power_modes;?></div></td></tr><tr><td><div class="engineleft enginetext">Adjustable Windscreen</div></td><td><div class="engineright"><?php echo $feature_safety->adjustable_wind_screen;?></div></td></tr><tr><td><div class="engineleft enginetext">Mobile Connectivity</div></td><td><div class="engineright"><?php echo $feature_safety->mobile_connectivity;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>  -->
                                                        
<!--    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><b>Chasis And Suspension</b></a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Chassis/Frame</div></td><td><div class="engineright"><?php echo $chasis_suspension->chasis_frame;?></div></td></tr><tr><td><div class="engineleft enginetext">Front Suspension</div></td><td><div class="engineright"><?php echo $chasis_suspension->front_suspension;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Suspension</div></td><td><div class="engineright"><?php echo $chasis_suspension->rear_suspension;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>-->
                                                        
<!--    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><b>Dimensions</b></a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Length</div></td><td><div class="engineright"><?php echo $dimension->length;?></div></td></tr><tr><td><div class="engineleft enginetext">Height</div></td><td><div class="engineright"><?php echo $dimension->height;?></div></td></tr><tr><td><div class="engineleft enginetext">Width</div></td><td><div class="engineright"><?php echo $dimension->width;?></div></td></tr><tr><td><div class="engineleft enginetext">Ground Clearance</div></td><td><div class="engineright"><?php echo $dimension->ground_clearance;?></div></td></tr><tr><td><div class="engineleft enginetext">Saddle Height</div></td><td><div class="engineright"><?php echo $dimension->saddle_height;?></div></td></tr><tr><td><div class="engineleft enginetext">Kerb Weight</div></td><td><div class="engineright"><?php echo $dimension->kerb_weight;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheelbase</div></td><td><div class="engineright"><?php echo $dimension->wheel_base;?></div></td></tr><tr><td><div class="engineleft enginetext">Fuel Capacity</div></td><td><div class="engineright"><?php echo $dimension->fuel_capacity;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>-->
                                                        
<!--    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><b>Electricals</b></a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Head Lamp</div></td><td><div class="engineright"><?php echo $electricals->head_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Tail Lamp</div></td><td><div class="engineright"><?php echo $electricals->tail_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Turn Signal Lamp</div></td><td><div class="engineright"><?php echo $electricals->turn_signal_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Pass Switch</div></td><td><div class="engineright"><?php echo $electricals->pass_switch;?></div></td></tr><tr><td><div class="engineleft enginetext">LED Tail Light</div></td><td><div class="engineright"><?php echo $electricals->led_tail_light;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>                                                    -->
  </div>
                                                
                                                    
						</div>
                                            
					<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Description</h2>
                                                                        <div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Model Year</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $bike_info->vehicle_info->reg_year;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Kilometers Driven</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $bike_info->vehicle_info->kilometer_driven;?> KMs</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Color</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $bike_info->vehicle_info->color;?></p>
										</div>
									</div>
									
<!--									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Torque</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_torque;?></p>
										</div>
									</div>
                                                                        
                                                                        <div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Speed</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_speed;?></p>
										</div>
									</div>-->
									
<!--									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Gear</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->gear_box;?></p>
										</div>
									</div>-->
									
									 
								</div>
								
								<div class="b-detail__main-aside-about wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Seller Details</h2>
                  <div class="b-detail__main-aside-about-call" style="padding: 0.1px !important">
										<center><h1>Like this Bike? </h1></center>
									</div>
									<div class="b-detail__main-aside-about-seller">
										<p id="para2">Confirm your details and contact seller directly.</p>
									</div>
									<div class="b-detail__main-aside-about-form">
										<div class="b-detail__main-aside-about-form-links">
											<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#form1'>General Enquiry</a>
											 
										</div>
                                                                            
										<form action="#"><div id="buyer_info">
                                                                                        <input type="hidden" placeholder="YOUR NAME" value="<?php echo $bike_list->id;?>" id="vehicle_id" name="vehicle_id"/>
											<input type="text" placeholder="YOUR NAME" value="" name="name" id="name" required/>
											<input type="hidden" placeholder="EMAIL ADDRESS" value="" name="email" id="email2" required/>
											<input type="tel" placeholder="PHONE NO." value="" name="phone" id="phone" required/>
<!--											<textarea name="text" placeholder="message"></textarea>-->
											
											<button id="seller_details" class="btn m-btn">GET SELLER DETAILS<span class="fa fa-angle-right"></span></button>
                                                                                        </div>
                                                                                    
                                                                                    <div id="seller_info" style="display:none">
											<input type="text" placeholder="YOUR NAME" value="" id="seller_name" readonly/>
											<input type="email" placeholder="EMAIL ADDRESS" value="" id="seller_email" readonly/>
											<input type="tel" placeholder="PHONE NO." value="" id="seller_phone" readonly/>
<!--											<textarea name="text" placeholder="message"></textarea>-->
											
                                                                                        </div>
										</form>
										 
									</div>
								</div>
								</aside>
								</div>
							
						</div>
					</div>
				</div>
			</div>
                      
			</div>
		</section><!--b-detail-->

		<?php include('footer.php');?> 

<script>
    $(document).ready(function(){
        $(document).on("click",'#seller_details',function(){
            var buyer_name=$("#name").val();
            var buyer_email=$("#email2").val();
            var buyer_phone=$("#phone").val();
            var vehicle_id = $("#vehicle_id").val();
            $.post("<?php echo base_url();?>index.php/Welcome/insert_buyer_details?user_name=<?php echo $user_info->name; ?>&user_phone=<?php echo $user_info->phone; ?>",{vehicle_id:vehicle_id,buyer_name:buyer_name,buyer_email:buyer_email,buyer_phone:buyer_phone},function(o){
                event.preventDefault();
                 });
                 $("#seller_info").show();
                 $("#buyer_info").hide();
               $("#para1").html("SELLER DETAILS");
               $("#para2").html("You can contact the seller on the given details.");
               $("#seller_name").val("NAME:- <?php echo $user_info->name; ?>");
               $("#seller_email").val("EMAIL:- <?php echo $user_info->email; ?>");
               $("#seller_phone").val("PHONE:- <?php echo $user_info->phone; ?>");
               event.preventDefault();
           
        });  
    });                    
</script>