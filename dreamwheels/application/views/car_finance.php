<?php include('header.php');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.7s">Car Finance</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s">
					<h3>The Largest Auto Dealer Online</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/car_finance" class="b-breadCumbs__page m-active">About Us</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-best">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-xs-12">
						<div class="b-best__info">
<!--							<header class="s-lineDownLeft b-best__info-head">
								<h2 class="wow zoomInUp" data-wow-delay="0.5s">About Us</h2>
							</header>
							-->
                            <h4 class="wow zoomInUp" data-wow-delay="0.5s">How car finance works and why you need it
                            </h4>                            

                            <p class="wow zoomInUp" data-wow-delay="0.5s">Nowadays it is common to get finance for almost everything. Banks are giving home loans, personal loans and even business loans, so why not car loan? Well, it is just as easy to get finance on your car as it is with any other thing.</p>

                            

                            <h4 class="wow zoomInUp" data-wow-delay="0.5s">What is car finance?
                            </h4>
                            
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> We all live in a world where we are surrounded by thousands of products. Of course, we want to put our hands on everything we can think of but monetary constraints always hold us down. </p> 
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> Coming onto the question of insurance, there are many reasons why you should get your car insured. It is mandatory for every car plying on the roads to have a valid insurance otherwise they can be seized. </p> 
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> This is particularly true for the world of cars. People looking to buy their dream vehicles are helpless as dishing out lakhs of rupees at once is not possible for everyone. It is now that car finance comes into play and helps you realize your dreams. </p> 
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> By availing finance options, you can buy the car that you’ve been dreaming of by just paying a percentage of the total price. The remaining price is paid for by the banks or other financial institutes who then recover the amount plus some interest on it via equated monthly installments or EMI. </p> 

                              <h4 class="wow zoomInUp" data-wow-delay="0.5s">Benefits of car finance
                            </h4> 

                            <p class="wow zoomInUp" data-wow-delay="0.5s">Even though buying car on finance is easy and lucrative, many people shy away because they don’t want debt. Car finance is just like the other loan. You have to pay back in EMIs and this makes some people wary of whether they should go for finance options or not.</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s">However, here we are listing down some of the reasons why going for finance is the smart thing to do while you’re buying your vehicle:</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s">The age of digital markets has made securing car loans pretty easy. You can apply for a loan on a bank’s website and be approved within days. There are also options for you to secure loans on the spot from vehicle dealerships, subject to meeting their eligibility requirements.</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s">So, what are you waiting for? Plan for your dream car today and let the banks worry about finances as you own and drive around the car you always wanted.</p>

                            <h6 class="wow zoomInUp" data-wow-delay="0.5s">Helps you keep money liquid:
                            </h6> 

							<p class="wow zoomInUp" data-wow-delay="0.5s">Now, this point is a little different. While you might have the entire amount required to buy a vehicle, I’d still suggest a loan because it is better to keep liquid money with you. Putting all your cash behind the vehicle is not a good choice so you must stick with finance.</p>
							<!--<a href="article.html" class="btn m-btn m-readMore wow zoomInUp" data-wow-delay="0.5s">view listings<span class="fa fa-angle-right"></span></a>-->
						</div>
					</div>
					<div class="col-sm-4 col-xs-12">
                                            <p><img src="<?php echo base_url();?>images/finance.jpg" width="350"></p> 
						
                                            <p class="wow zoomInUp" data-wow-delay="0.5s"><b>You can buy the car you want:</b> The first and foremost benefit is that you can buy the car that you want. Of course, you cannot go overboard as the lending limit depends on your credit score and yearly income, but you can buy anything that falls in your range without having to worry about the entire cost of the vehicle at the time. These days, it is quite easy to secure vehicle finance so you can just walk in to a bank and drive out with your brand new car.</p>
					</div>
				</div>
			</div>
		</section><!--b-best-->

		
		<?php include('footer.php');?>