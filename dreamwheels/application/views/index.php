<?php require('header.php'); ?>
<section class="b-slider"> 
			<div id="carousel" class="slide carousel carousel-fade">
				<div class="carousel-inner">
					<div class="item next left">
						<img src="<?php echo base_url(); ?>media/main-slider/1.jpg" alt="sliderImg">
						
					</div>
					<div class="item active left">
						<img src="<?php echo base_url(); ?>media/main-slider/2.jpg" alt="sliderImg">
						
					</div>
					<div class="item">
						<img src="<?php echo base_url(); ?>media/main-slider/3.jpg" alt="sliderImg">
						
					</div>
                                       <div class="container">
      <div class="carousel-caption b-slider__info">
       <h1 class="form-title">Choose vehicle</h1>

       <div class="form-group" id="bike-car-icon">
        <div class="col-sm-2 col-sm-offset-3" style="width: 112px;"> 
         <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g style="fill: #f41818;">
	 <path d="M258.3,343.6H277v41.2h-25.2l7.8-40.8L258.3,343.6L258.3,343.6z M295.4,343.6h39.4c18.3,1.8,33,8.7,47.2,19.7
	       c-1.4,0.5-2.7,1.4-4.1,2.3v0.5c-3.7,3.7-5.5,13.3-5.5,18.8h-76.9V343.6L295.4,343.6z"></path>
	 <path d="M107.6,476.4c-20.6,0-46.2-7.8-47.6-8.2l-2.7-1.4v-39.8l2.3-1.4c1.8-1.8,6.9-7.3,6.9-11v-7.8c0.5-5.5,0.5-11.9,0.9-17.4
	       c0-3.7,6-11,11.9-13.3l1.8-0.5h146.5l7.3-38.9c1.4-6.9,6.9-11.4,14.2-11.4h86.5c27.5,2.3,43.5,15.6,61.4,29.8
	       c8.2,6.4,16.5,15.1,26.1,21.1c4.1,0.5,10.5,0.5,17.9,0.9c43.5,1.8,83.3,4.6,87.5,24.3l4.1,34.8c2.7,1.8,6.4,5.5,6.4,10.5v15.1
	       c0,4.1-0.5,6.4-4.1,9.2c-6.9,4.1-14.7,6-30.7,6h-6.4v-9.2h6.9c13.7,0,20.1-0.9,25.2-4.1c0,0,0,0.5,0-1.4V447c0-1.4-2.3-2.7-3.2-3.2
	       l-2.7-0.9l-4.6-39.4c-3.2-13.3-54-15.1-78.3-16c-8.2-0.5-15.1-0.5-19.7-0.9l-1.8-0.5c-10.5-6.4-19.7-16-27.9-22.9
	       c-16.9-13.7-31.6-25.6-55.4-27.5h-87.5c-2.3,0-4.1,0.9-4.6,3.2l-8.7,47.2H81.5c-2.7,1.4-5,4.1-5.5,5c0,4.6-0.5,11.4-0.5,16.9v7.3
	       c0,6.9-6.4,13.3-9.2,16v28.4c7.3,2.7,26.6,8.7,41.7,8.2h8.7v9.2h-8.2C108.1,476.4,108.1,476.4,107.6,476.4L107.6,476.4z
	       M423.6,476.4H185.5v-9.2h238.1V476.4L423.6,476.4z"></path>
	 <polygon points="529.3,444.3 523.4,407.7 515.1,407.7 515.1,444.3 	"></polygon>
	 <polygon points="327.4,398.5 304.5,398.5 304.5,393.9 327.4,393.9 	"></polygon>
	 <path d="M70.5,412.2h5c9.6,0,22.4-5.5,22.9-14.2v-8.7H72.3C72.3,397.6,71,406.3,70.5,412.2L70.5,412.2z"></path>
	 <path d="M381,368.7L381,368.7c-2.7,2.7-4.1,12.4-4.1,16c0,1.4,0.5,2.3,1.4,3.2c0.9,0.9,1.8,1.4,3.2,1.4H392c11.4,0-1.8-20.1-6-21.5
	       C384.2,366.9,382.4,367.4,381,368.7L381,368.7z"></path>
	 <polygon points="222.1,443.4 222.1,453.5 394.3,453.5 	"></polygon>
	 <path d="M405.2,471.8h-4.6v-12.4c0-33.4,25.6-60.4,59.1-60.4s60,27,60,60.4v12.4h-4.6v-13.3c0-29.8-25.6-53.1-55.4-53.1
	       c-29.8,0-54.5,24.3-54.5,54V471.8L405.2,471.8z"></path>
	 <path d="M460.7,457.1c-6,0-10.5,4.6-10.5,10.5s4.6,10.5,10.5,10.5c6,0,10.5-4.6,10.5-10.5S466.6,457.1,460.7,457.1L460.7,457.1z"></path>
	 <path d="M210.2,471.8h-6.4v-12.4c0-29.8-23.8-54-53.6-54s-51.7,24.3-51.7,54v12.4h-9.2V459c0-33.4,27-60,60.4-60
	       s62.7,25.6,62.7,59.1L210.2,471.8L210.2,471.8z"></path>
	 <path d="M460.7,413.2L460.7,413.2c12.8,0,24.3,5,32.5,13.7h-0.9c8.2,8.2,13.7,19.7,13.7,32.5v14.2v3.2h-4.6H500
	       c-2.3,7.8-6.9,15.1-12.8,20.1c-6.9,6-16,11.4-26.1,11.4s-19.2-3.7-26.1-9.6V497c-6-5-10.5-12.4-12.8-20.1h-2.3h-5v-4.6v-12.4
	       c0-12.8,5-24.3,13.7-32.5h0.5C436.8,418.2,448.3,413.2,460.7,413.2L460.7,413.2L460.7,413.2z M460.7,440.6c-14.7,0-27,11.9-27,27
	       c0,14.7,11.9,27,27,27c14.7,0,27-11.9,27-27C487.7,452.5,475.8,440.6,460.7,440.6L460.7,440.6L460.7,440.6z M152,440.6
	       c-14.7,0-27,11.9-27,27c0,14.7,11.9,27,27,27c14.7,0,27-11.9,27-27C179,452.5,166.7,440.6,152,440.6L152,440.6L152,440.6z
	       M152,413.2L152,413.2c12.8,0,24.3,5,32.5,13.7h0.9c8.2,8.2,13.7,19.7,13.7,32.5v14.2v3.2h-4.6h-3.7c-2.3,7.8-6.9,15.1-12.8,20.1
	       c-6.9,6-16,11.4-26.1,11.4c-10.1,0-19.2-5.5-26.1-11.4c-6-5-10.5-12.4-12.8-20.1h-2.3h-3.2v-4.6v-12.4c0-12.8,5-24.3,13.7-32.5
	       C129.6,418.2,139.2,413.2,152,413.2L152,413.2z"></path>
	 <path d="M152,457.1c-6,0-10.5,4.6-10.5,10.5s4.6,10.5,10.5,10.5c6,0,10.5-4.6,10.5-10.5S158,457.1,152,457.1L152,457.1z"></path>
	</g>
         </svg></div>
        <div class="col-sm-2" style="margin-top: -12px;">
         <a href="<?php echo base_url(); ?>index.php/Welcome/bike">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 400.261 400.261" style="enable-background:new 0 0 400.261 400.261;" xml:space="preserve" width="75px" height="55px">
	 <path d="M343.653,167.446c0.103-0.514,0.161-1.039,0.161-1.569v-41.77c0-30.715-30.281-44.692-46.293-47.792  c-2.344-0.456-4.77,0.163-6.611,1.683c-1.842,1.52-2.909,3.783-2.909,6.171v36.235c-4.711-2.728-10.173-4.298-16-4.298h-55.542  c-17.645,0-32,14.332-32,31.948c0,0.066,0.005,0.132,0.005,0.198h-40.447c-1.705,0-7.512-0.353-10.902-4.884l-22.57-32.095  c-1.498-2.13-3.939-3.398-6.544-3.398H30.469c-7.322,0-13.027,2.98-15.652,8.176c-2.615,5.178-1.644,11.512,2.663,17.388  l27.459,38.624C18.352,185.106,0,212.447,0,244.001c0,44.162,35.929,80.091,80.091,80.091c32.877,0,61.177-19.928,73.493-48.337  h79.254c4.106,0,8.508-0.905,12.78-2.479c11.875,30.348,41.274,50.816,74.552,50.816c44.162,0,80.091-35.929,80.091-80.091  C400.261,208.504,376.669,177.591,343.653,167.446z M216.458,132.107H272c8.822,0,16,7.154,16,15.948  c0,8.794-7.178,15.948-16,15.948h-55.542c-8.822,0-16-7.154-16-15.948C200.458,139.262,207.636,132.107,216.458,132.107z   M80.091,308.092c-35.34,0-64.091-28.751-64.091-64.091c0-35.334,28.751-64.081,64.091-64.081c14.694,0,28.239,4.985,39.06,13.337  l-11.589,11.683c-7.991-5.645-17.502-8.7-27.476-8.7c-26.34,0-47.769,21.427-47.769,47.764c0,26.34,21.429,47.769,47.769,47.769  c26.334,0,47.758-21.429,47.758-47.769c0-4.418-3.582-8-8-8s-8,3.582-8,8c0,17.517-14.247,31.769-31.758,31.769  c-17.517,0-31.769-14.251-31.769-31.769c0-17.515,14.251-31.764,31.769-31.764c5.698,0,11.17,1.499,15.969,4.301l-21.654,21.832  c-3.112,3.137-3.091,8.202,0.046,11.313c1.56,1.548,3.597,2.32,5.634,2.32c2.058,0,4.116-0.79,5.68-2.366l44.746-45.112  c8.542,10.889,13.655,24.591,13.655,39.474C144.162,279.341,115.42,308.092,80.091,308.092z M158.598,259.755  c1.02-5.096,1.564-10.362,1.564-15.754c0-44.157-35.92-80.081-80.071-80.081c-6.754,0-13.312,0.849-19.582,2.431l-30.024-42.231  c-0.027-0.038-0.054-0.075-0.081-0.112c-0.032-0.044-0.063-0.087-0.094-0.129c0.052,0,0.105-0.001,0.16-0.001h69.376l20.239,28.78  c0.035,0.049,0.069,0.097,0.105,0.146c7.834,10.625,20.218,11.451,23.827,11.451h44.876c5.573,9.417,15.84,15.75,27.566,15.75H272  c17.645,0,32-14.332,32-31.948c0-1.002-0.052-1.992-0.143-2.97c0.091-0.482,0.143-0.978,0.143-1.486V95.032  c9.853,4.008,23.813,12.595,23.813,29.076v15.948h-7.446c-4.418,0-8,3.582-8,8s3.582,8,8,8h7.446v6.827l-3.792,4.355  c-0.015,0.017-0.03,0.034-0.044,0.051l-35.985,41.336c-2.749,2.505-5.203,5.327-7.315,8.402L252.3,249.624  c-0.142,0.163-0.277,0.332-0.405,0.505c-3.714,5.038-12.797,9.625-19.056,9.625H158.598z M292.828,227.685l21.659,21.94  c1.565,1.585,3.629,2.38,5.694,2.38c2.03,0,4.061-0.768,5.62-2.307c3.145-3.104,3.177-8.169,0.073-11.313l-21.657-21.939  c4.747-2.774,10.262-4.373,16.146-4.373c17.712,0,32.122,14.407,32.122,32.116c0,17.712-14.41,32.122-32.122,32.122  c-17.706,0-32.111-14.41-32.111-32.122C288.25,238.159,289.924,232.512,292.828,227.685z M320.17,308.092  c-27.483,0-51.662-17.44-60.528-43.01c1.862-1.61,3.534-3.343,4.92-5.176l8.124-9.332c3.133,23.527,23.311,41.739,47.675,41.739  c26.535,0,48.122-21.587,48.122-48.122c0-26.531-21.587-48.116-48.122-48.116c-0.079,0-0.156,0.006-0.235,0.006l12.927-14.849  c29.47,6.02,51.208,32.282,51.208,62.77C384.261,279.341,355.51,308.092,320.17,308.092z" fill="#c7c7c7"></path>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	 <g>
	 </g>
	</svg>
         </a>
        </div>
       </div>
       <div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#cars" class="btn btn-default active" data-toggle="tab" id="new">
         <input type="radio" />New Cars
        </a>
        <a href="#cars" class="btn btn-default" data-toggle="tab" id="used">
         <input type="radio" />Used Cars
        </a>
        <input type="hidden" id="vehicle_condition" value="new">
       </div>
       <div class="tab-content">
        <div class="tab-pane active" id="cars"><div class="form-group col-xs-offset-2" style="padding-top: 10px;"> 
	<label class="radio-inline">
	 <input type="radio" name="optradio" value="by_budget" checked="checked" class="radion-opt search_flow">By Budget
	</label>
	<label class="radio-inline">
	 <input type="radio" name="optradio" value="by_brand" class="radion-opt search_flow">By Brand
	</label></div>
         <div class="mobile-option" id="by_budget">
	<div class="form-group">
	 <div class="col-sm-12 col-xs-12">	
	 <select name="newCarBrandSelect" id="newCarBrandSelect" class="budget form-control">
	  <option value="default">-Select Budget-</option>
          <option value="low_to_high">Low to High Budget</option>
          <option value="high_to_low">High to Low Budget</option>
	  <option value="1-lakh-5-lakh">1 Lakh - 5 Lakh</option>
	  <option value="5-lakh-10-lakh">5 Lakh - 10 Lakh</option>
	  <option value="10-lakh-20-lakh">10 Lakh - 20 Lakh</option>
	  <option value="20-lakh-50-lakh">20 Lakh - 50 Lakh</option>
	  <option value="50-lakh-1-crore">50 Lakh - 1 Crore</option>
	  <option value="above-1-crore">Above 1 Crore</option>
	 </select>
	 </div>
	</div>
	<div class="form-group">
	<div class="col-sm-12 col-xs-12">
	 <select name="newCarBrandSelect" id="newCarBrandSelect" class="vehicle_type form-control">
	  <option value="default">-Select Vehicle Types-</option>
          <option value="all_type">All Types</option>
	  <option value="hatchback">Hatchback</option>
	  <option value="sedan">Sedans</option>
	  <option value="suv">SUV</option>
	  <option value="convertible">Convertibles</option>
	  <option value="coupe">Coupe</option>
	 </select>
	 </div>
	</div>
	</div>
        <div class="mobile-option" style="display: none;"  id="by_brand">
	 <div class="form-group budget car_brand" >
	  <div class="col-sm-12 col-xs-12">
             <?php if(isset($query_car)) { ?>

                    <select class="car-budget brand select_car form-control" name="newCarBrandSelect" id="newCarBrandSelect">
                  <option value="default">-Select Brand-</option>
                
                 
                <?php foreach ($query_car as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
				</div>
	 </div>

	 <div class="form-group car_brand">
	 <div class="col-sm-12 col-xs-12">
	  <select name="newCarBrandSelect" id="newCarBrandSelect" class="model form-control">
	   <option value="default">-Select Model-</option>
	   
	  </select>
	  </div>
	 </div>
	</div>
         </div>
        </div>

        <div class="form-group">
      <div class="col-sm-12 col-xs-12">
          <button class="btn btn-danger btn-lg center-block" id="search_button" style="margin-top: 15px;"><i class="fa fa-search"></i> Search</button>
      </div>
	</div>
       </div>


      </div>
                                    <div class="carousel-caption b-slider__info2" style="box-shadow:none;width: 24% !important">
	      <form class="b-blog__aside-search wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>" action="<?php echo base_url();?>index.php/Welcome/vehicle_search" method="GET" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
								<div>
                                                                    <input type="text" name="search" value="" placeholder="Search Cars or Brands eg. Verna, or Hyundai" style="border-radius:0px !important">
									<button type="submit"><span class="fa fa-search"></span></button>
									
									<input type="hidden" name="type" value="">
								</div>        
							</form>
	  </div>
                                    						
				</div>
				<a class="carousel-control right" href="#carousel" data-slide="next">
					<span class="fa fa-angle-right m-control-right"></span>
				</a>
				<a class="carousel-control left" href="#carousel" data-slide="prev">
					<span class="fa fa-angle-left m-control-left"></span>
				</a>
			</div>
		</section>

<section class="b-search" >
 <div class="container">
  <form action="<?php echo base_url(); ?>index.php/Welcome/vehicle_search" method="GET" class="b-search__main">
    <div class="b-search__main-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
     <h2>Unsure which vehicle you are looking for? Find it here.</h2>
    </div>
    <div class="b-search__main-type wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
     <div class="col-xs-12 col-md-2 s-noLeftPadding">
      <h4>I am Looking for</h4>
     </div>
     <div class="col-xs-12 col-md-10">
      <div class="row">
       
       <div class="col-xs-2">
	   <a href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=car&vehicle_budget=default&vehicle_type=suv&vehicle_condition=new&vehicle_brand=default&vehicle_model=default">
        <!--<input id="type2" type="radio" name="type" />-->
        <label for="type2" class="b-search__main-type-svg"  data="suv">
         <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	    viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g>
	 <path d="M199.2,342.6h95.7c23.8,2.3,47.2,17.4,68.7,31.1c-0.5,0.5-0.9,0.9-1.4,1.4l0,0c-3.7,3.2-5.5,7.8-5.5,12.4
	       c0,0.5,0,0.9,0,1.4l-218.4-8.2C161.2,361.9,174.9,342.6,199.2,342.6L199.2,342.6z"/>
	 <path d="M115.4,475.4c-21.5,0-33.4-11.4-33.9-11.9l-1.4-1.4v-28.4l0.5-0.9c0.9-1.8,3.2-7.3,3.2-11.9c0-4.1,0.5-10.5,0.5-16.9v-11
	       c0.5-7.3,27.5-36.6,36.6-44.9v-9.6l4.6-2.7c18.8-4.1,55.4-6.4,80.6-6.4h89.3c33.4,2.7,64.6,22.9,92,40.3c5,3.2,9.6,7.8,14.2,10.5
	       c2.3,0,4.6,0.5,8.2,0.9c51.7,5,100.3,11.9,104.9,32.1v62.3l-3.7,2.7c-6.9,1.4-13.3,2.3-27.9,2.3h-13.7v-9.2h14.2
	       c11.9,0,17.9-0.5,22.4-1.4v-56.3c-2.3-9.6-34.8-17.9-96.2-23.8c-3.7-0.5-6.9-0.5-9.6-0.9l-1.8-0.5c-5-3.2-10.1-8.2-15.6-11.4
	       c-27.9-17.9-56.8-36.2-87.5-38.5h-89.7c-22.9,0-55.9,1.8-75.1,5.5v8.2l-4.1,1.8c-4.6,2.7-30.7,34.3-32.5,40.3v10.5
	       c0,6.4-0.9,12.8-0.9,16.9c0,6.4-1.8,12.8-3.7,15.6v19.7c3.2,2.7,12.4,10.5,26.6,10.1h5.5v9.2L115.4,475.4
	       C114.5,475.4,115.9,475.4,115.4,475.4L115.4,475.4z M396.1,475.4H194.6v-9.2h201.5V475.4L396.1,475.4z"/>
	 <path d="M365.4,378.4L365.4,378.4c-2.7,2.7-4.1,6-4.1,9.6c0,1.4,0.5,2.7,1.4,3.7c0.9,0.9,1.8,1.4,3.2,1.4h10.5
	       c1.8,0,3.2-0.9,4.1-2.3c0.9-1.4,0.9-3.7,0-5c-1.8-3.2-5-6.4-10.1-8.2C368.6,376.5,366.8,377,365.4,378.4L365.4,378.4z"/>
	 <path d="M367.2,390.7c-0.9,0-1.8-0.5-2.7-1.4l-6-8.2c-0.9-1.4-0.9-3.2,0.9-4.6c1.4-0.9,3.2-0.9,4.6,0.9l6,8.2
	       c0.9,1.4,0.9,3.2-0.9,4.6C368.2,390.3,367.7,390.7,367.2,390.7L367.2,390.7z"/>
	 <path d="M90.2,406.7h8.2c7.8,0,17.9-5.5,18.3-12.4v-6H92.5C92.5,394.8,90.2,400.3,90.2,406.7L90.2,406.7z"/>
	 <path d="M478.1,402.2l11,20.1c3.7-2.3,9.2-4.1,13.3-4.1l0,0l0,0l0,0l0,0l0,0l0,0c2.7,0,5.5-1.8,8.7-1.8c0-8.2,0.9-8.7-6.4-14.2
	       H478.1L478.1,402.2z"/>
	 <polygon points="299.9,406.7 277,406.7 277,402.2 299.9,402.2 	"/>
	 <polygon points="194.6,402.2 171.7,402.2 171.7,397.6 194.6,397.6 	"/>
	 <polygon points="222.1,450.7 222.1,457.1 371.4,457.1 	"/>
	 <path d="M508.7,436.1c-6.9,0-10.1,0-16.9,0c-3.7,3.7-3.7,8.2-3.7,12.8c6.9,0,13.7,0,20.6,0C508.7,444.8,508.7,440.2,508.7,436.1
	       L508.7,436.1z"/>
	 <path d="M488.6,457.1c-1.4-2.7-1.4-6.4-1.4-9.2c6.9,0,7.3,0.5,14.2,0.5v8.7H488.6L488.6,457.1z"/>
	 <path d="M138.3,347.7l-44.4,38.9v-8.7c9.2-12.4,28.4-33.4,32.1-35.7L138.3,347.7L138.3,347.7z"/>
	 <path d="M126.8,500.2c-6-5-10.5-12.4-12.8-20.1h-2.3h-3.7v-4.6v-16c0-12.8,5-24.3,13.7-32.5c8.2-8.2,18.8-13.7,31.6-13.7l0,0
	       c12.8,0,24.3,5,32.5,13.7h0.5c8.2,8.2,13.7,19.7,13.7,32.5v14.2v6.9h-4.6h-2.7c-2.3,7.8-6.9,15.1-12.8,20.1c-6.9,6-16,7.8-26.1,7.8
	       s-19.2-3.7-26.1-9.6v1.4H126.8z M427.2,440.6c-14.7,0-27,11.9-27,27c0,14.7,11.9,27,27,27c14.7,0,27-11.9,27-27
	       C453.8,452.5,441.9,440.6,427.2,440.6L427.2,440.6L427.2,440.6z M152.9,440.6c-14.7,0-27,11.9-27,27c0,14.7,11.9,27,27,27
	       c14.7,0,27-11.9,27-27C179.5,452.5,167.6,440.6,152.9,440.6L152.9,440.6L152.9,440.6z M427.2,413.2L427.2,413.2
	       c12.8,0,24.3,5,32.5,13.7h0.9c8.2,8.2,13.7,19.7,13.7,32.5v14.2v6.9h-4.6h-3.2c-2.3,7.8-6.9,15.1-12.8,20.1c-6.9,6-16,7.8-26.1,7.8
	       s-19.2-3.7-26.1-9.6v1.8c-6-5-10.5-12.4-12.8-20.1h-2.3h-3.7v-4.6v-16c0-12.8,5-24.3,13.7-32.5
	       C404.3,418.2,414.4,413.2,427.2,413.2L427.2,413.2L427.2,413.2z M427.2,457.1c6,0,10.5,4.6,10.5,10.5s-4.6,10.5-10.5,10.5
	       c-6,0-10.5-4.6-10.5-10.5S421.3,457.1,427.2,457.1L427.2,457.1L427.2,457.1z M152.9,457.1c-6,0-10.5,4.6-10.5,10.5
	       s4.6,10.5,10.5,10.5c6,0,10.5-4.6,10.5-10.5C163,461.7,158.4,457.1,152.9,457.1L152.9,457.1z"/>
	</g>
         </svg>
        </label>
        <h5><label for="type2" class="vehicle_type">Suv</label></h5>
		</a>
       </div>
       <div class="col-xs-2">
        <a href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=car&vehicle_budget=default&vehicle_type=coupe&vehicle_condition=new&vehicle_brand=default&vehicle_model=default">
        <!--<input id="type3" type="radio" name="type" />-->
        <label for="type3" class="b-search__main-type-svg" data="coupe">
         <svg  version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	     viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g>
	 <path d="M198.7,492.4v-9.2h21.1c65.9,0,134.2-0.5,198.7,0v9.2c-64.6,0-132.8,0-198.7,0H198.7L198.7,492.4z M152.9,493.8
	       c-20.6-2.7-57.2-13.7-69.1-22.4l-4.1-1.4v-25.6l1.4-1.4c1.8-1.8,6-8.2,6-12.8l4.1-18.3L86.5,393l22,2.7h37.5
	       c3.7-1.4,8.7-3.7,14.2-6c29.3-11.9,56.8-21.5,68.7-21.5h78.8c1.4,0,2.3,0,3.7,0.5c23.8,6.9,45.3,18.8,70.5,33.4h0.5
	       c70.1,2.7,111.3,8.2,125.5,16.5l2.3,1.4v14.7l5.5,4.6v2.3c0,17.9-7.8,25.2-12.4,29.3c0,0.5-0.5,0.9,0,0.9c1.4,2.7,2.3,5,2.3,7.8
	       v3.2l-5,1.4c-11,4.6-22.4,7.3-36.2,8.7v-9.6c11-0.9,21.1-3.2,30.2-6.4c0-0.5,0.9-0.5,0.9-0.9c-0.9-2.3-1.8-4.1-1.8-6v-2.3l0.9-0.9
	       c0.9-0.9,1.4-1.4,2.3-2.3c4.1-3.7,9.2-7.8,9.6-20.1l-5.5-4.6v-16c-11-5-40.3-11-118.1-14.2h-3.7l-0.9-0.5
	       c-24.7-14.7-46.2-24.7-69.1-31.1c-0.5,0-0.9,0-0.9,0H229c-10.5,0-44.9,13.7-65,22c-6.4,2.3-11.4,4.1-15.6,5.5l-1.4,0.5h-39.4h-8.2
	       l1.4,5.5l-4.6,20.6c0,6.4-5,13.7-7.8,16.5v16.9c12.4,7.3,46.2,16.9,64.1,19.2v9.2H152.9z"/>
	 <path d="M243.6,385.2c0.5,8.2,1.4,16,1.8,24.3c-12.4-2.7-25.2-5.5-37.5-8.7C223,390.7,225.7,385.2,243.6,385.2L243.6,385.2
	       L243.6,385.2z M253.2,385.2c17.9,0,36.2,0,54,0c12.8,3.7,25.2,9.2,37.5,15.1l-0.5,0.5l0,0c-2.7,2.7-4.6,6.4-5.5,10.1
	       c-9.2,0-19.7,0-30.7,0c-17.4,0-35.3,0-52.7,0C254.6,402.6,254.1,393.9,253.2,385.2L253.2,385.2z"/>
	 <path d="M424.9,503.4c-4.6-2.3-8.2-6-11.4-10.1l-3.2-0.5c-1.8-0.5-5-0.9-6.9-0.9l-3.7-0.9v-3.7V475c0-11.9,2.7-22.4,10.5-30.2h1.4
	       c6.4-6.4,16-10.5,30.2-10.5c13.7,0,23.4,4.1,30.2,10.5l0,0c7.8,7.8,10.5,18.3,10.5,30.2v7.8v4.1l-4.1,0.5c-1.4,0.5-2.7,0.5-4.1,0.9
	       l0,0l-1.4,0.5c-2.7,5.5-6.4,10.1-11.4,13.7c-5.5,3.7-12.4,6-19.7,6C435.9,507.9,430,506.1,424.9,503.4L424.9,503.4L424.9,503.4
	       L424.9,503.4z M441.9,449.3c-13.3,0-23.8,10.5-23.8,23.8s10.5,23.8,23.8,23.8c13.3,0,23.8-10.5,23.8-23.8
	       C465.7,460.3,455.2,449.3,441.9,449.3L441.9,449.3L441.9,449.3z M174.5,449.3c-13.3,0-23.8,10.5-23.8,23.8s10.5,23.8,23.8,23.8
	       c13.3,0,23.8-10.5,23.8-23.8C198.3,460.3,187.7,449.3,174.5,449.3L174.5,449.3L174.5,449.3z M145.2,444.3h-0.5
	       c6.4-6.4,16-10.5,30.2-10.5c13.7,0,23.4,4.1,30.2,10.5h1.8c7.8,7.8,10.5,18.3,10.5,30.2v15.1v2.7h-4.6h-11.4
	       c-2.7,3.7-6.4,6.9-11,9.2c-5,2.7-10.5,6-16,6c-6,0-11.9-1.4-16.5-4.6c-4.6-2.3-8.2-6-11.4-10.1l-3.2-0.5c-1.8-0.5-3.2-0.9-5-1.4
	       l-3.7-0.9v-3.7v-12.4C134.6,462.6,137.4,452.1,145.2,444.3L145.2,444.3z"/>
	 <path d="M174.5,464.9c-5,0-8.7,3.7-8.7,8.7c0,4.6,3.7,8.7,8.7,8.7c4.6,0,8.7-4.1,8.7-8.7C183.2,468.6,179,464.9,174.5,464.9
	       L174.5,464.9z"/>
	 <polygon points="281.2,423.7 258.3,423.7 258.3,419.1 281.2,419.1 	"/>
	 <polygon points="230.8,465.4 230.8,474.1 388.8,474.1 	"/>
	 <path d="M441.9,464.9c-4.6,0-8.7,3.7-8.7,8.7c0,4.6,3.7,8.7,8.7,8.7s8.7-4.1,8.7-8.7C450.6,468.6,446.5,464.9,441.9,464.9
	       L441.9,464.9z"/>
	 <path d="M464.3,396.2h-49.9l-21.1,9.2v2.3c21.5,1.4,47.6,3.2,71,6V396.2L464.3,396.2z"/>
	 <path d="M347.1,404.5L347.1,404.5c-2.7,2.7-4.1,6-4.1,9.6c0,1.4,0.5,2.7,1.4,4.1c0.9,0.9,1.8,1.4,3.2,1.4h10.5
	       c1.8,0,3.2-0.9,4.1-2.3c0.9-1.4,0.9-3.7,0-5c-1.8-3.2-5-6.4-10.1-8.2C350.3,402.6,348.5,403.1,347.1,404.5L347.1,404.5z"/>
	 <path d="M355.8,423.7c-0.9,0-1.8-0.5-2.7-1.4l-6-8.2c-0.9-1.4-0.9-3.2,0.9-4.6c1.4-0.9,3.2-0.9,4.6,0.5l6,8.2
	       c0.9,1.4,0.9,3.2-0.5,4.6C357.2,423.2,356.7,423.7,355.8,423.7L355.8,423.7z"/>
	</g>
         </svg>
        </label>
        <h5><label for="type3" class="vehicle_type">Coupe</label></h5>
		</a>
       </div>
       <div class="col-xs-2">
        <a href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=car&vehicle_budget=default&vehicle_type=convertible&vehicle_condition=new&vehicle_brand=default&vehicle_model=default">
        <!--<input id="type4" type="radio" name="type" />-->
        <label for="type4" class="b-search__main-type-svg" data="convertible">
         <svg version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	    viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g>
	 <path d="M249.1,378.4h-0.9c-4.1,0.5-6,4.1-7.3,8.2l-2.3,8.7c-4.1,12.8,19.2,12.8,18.8,1.8v-11.4
	       C256.9,381.1,253.7,377.9,249.1,378.4L249.1,378.4z"/>
	 <path d="M142,488.3c-21.1,0-41.7-14.2-42.6-14.7l-2.3-1.4v-32.1l2.3-1.4c0.9-1.8,2.3-7.3,2.3-11.9v-30.2l58.6-3.7
	       c25.2-1.4,40.8,1.4,58.2,4.1c9.6,1.4,19.7,3.2,31.6,4.1h95.2c5.5,0,11-0.9,16.9-1.4c6-0.9,11.9-1.8,18.3-1.8
	       c0.9,0,38.9,4.1,69.6,11.9c33,8.2,44,21.5,45.3,27.5l2.7,47.2l-4.1,0.9c-7.8,1.8-14.2,2.7-31.1,2.7h-18.3v-9.2h16
	       c12.8,0,19.7-0.5,25.2-1.8v-2.7v-35.7c-0.5-1.4-9.6-11.9-38-19.2c-29.8-7.8-67.3-11.9-67.8-11.9c-5,0-10.5,0.5-16.5,1.4
	       c-6,0.9-11.9,1.8-18.3,1.8h-95.7c-12.8-0.9-22.9-2.7-33-4.1c-16.5-2.7-31.6-4.6-55.9-3.7l-49.9,2.7v21.5c0,6.4-3.2,12.8-4.6,15.6
	       v23.8c6,3.7,22.9,12.4,38,11.9h12.4v9.2h-14.2C142,488.3,142.4,488.3,142,488.3L142,488.3z M394.7,488.3H207v-9.2h187.7V488.3
	       L394.7,488.3z"/>
	 <path d="M106.2,415.9c0,4.6-1.8,9.6-1.8,12.4h6.4c7.8,0,17.9-6,18.3-12.4v-6h-22.9V415.9L106.2,415.9L106.2,415.9z"/>
	 <path d="M458.8,433.3l9.2,16.5c3.2-1.8,7.3-2.7,11-2.7l0,0l0,0l0,0l0,0l0,0l0,0c4.6,0,9.6,0,14.2,0v-0.9c-0.5-1.4-5-9.2-9.6-12.8
	       C475.3,433.3,467.1,433.3,458.8,433.3L458.8,433.3z"/>
	 <path d="M213.4,488.3c-1.8,3.7-4.1,6.9-6.9,9.6l0,0c-6.4,6.4-14.7,10.1-24.3,10.1s-17.9-4.1-24.3-10.1c-2.7-2.7-5-6-6.9-9.6h-4.6
	       h-3.7v-4.6v-12.4c0-11,4.6-21.5,11.9-28.8c7.3-7.3,16.5-11.9,27.5-11.9l0,0c11,0,21.1,4.1,28.8,11.4l0,0h2.3
	       c7.3,7.3,11.9,17.4,11.9,28.8v12.4v4.6h-4.6h-7.3V488.3L213.4,488.3z M420.4,449.3c-13.3,0-23.8,10.5-23.8,23.8
	       s10.5,23.8,23.8,23.8c13.3,0,23.8-10.5,23.8-23.8C444.6,460.3,433.6,449.3,420.4,449.3L420.4,449.3L420.4,449.3z M182.2,449.3
	       c-13.3,0-23.8,10.5-23.8,23.8S169,497,182.2,497s23.8-10.5,23.8-23.8C206.5,460.3,195.5,449.3,182.2,449.3L182.2,449.3L182.2,449.3
	       z M420.4,431L420.4,431c11,0,21.1,4.1,28.8,11.4l0,0h2.3c7.3,7.3,11.9,17.4,11.9,28.8v12.4v4.6h-4.6h-7.3c-1.8,3.7-4.1,6.9-6.9,9.6
	       c-6.4,6.4-14.7,10.1-24.3,10.1c-9.6,0-17.9-3.7-24.3-10.1l0,0c-2.7-2.7-5-6-6.9-9.6h-4.6H381v-4.6v-12.4c0-11,4.6-21.1,11.9-28.8
	       H392l0,0C399.3,435.6,409.4,431,420.4,431L420.4,431z"/>
	 <path d="M420.4,464.9c-4.6,0-8.7,3.7-8.7,8.7c0,4.6,4.1,8.7,8.7,8.7s8.7-3.7,8.7-8.7C429.1,468.6,425.4,464.9,420.4,464.9
	       L420.4,464.9z"/>
	 <polygon points="266.5,424.2 243.6,424.2 243.6,419.6 266.5,419.6 	"/>
	 <path d="M182.2,464.9c-5,0-8.7,3.7-8.7,8.7c0,4.6,3.7,8.7,8.7,8.7c4.6,0,8.7-3.7,8.7-8.7C190.9,468.6,187.3,464.9,182.2,464.9
	       L182.2,464.9z"/>
	 <polygon points="234.4,464 234.4,469.9 367.7,469.9 	"/>
	 <polygon points="490.9,469.9 481.7,469.9 481.7,465.4 490.9,465.4 	"/>
	 <path d="M483.1,460.8h-6.9c-1.8,0-3.2,1.8-3.2,3.7v2.3c0,1.8,1.4,3.2,3.2,3.2h6.9c1.8,0,3.2-1.4,3.2-3.2v-2.3
	       C486.3,462.2,484.9,460.8,483.1,460.8L483.1,460.8z"/>
	 <path d="M101.7,465.4v13.3h36.6v-0.9C123.6,476.4,107.6,469,101.7,465.4L101.7,465.4z"/>
	 <path d="M185,376.1L185,376.1c-4.1,0.5-6,4.1-6.9,8.2l-2.7,8.7c-0.5,0.9-0.5,1.4-0.5,1.8c6.4,0.5,11.9,0.9,18.3,1.4
	       c0-0.5,0-0.9,0-1.4v-11.4C192.8,378.8,189.6,375.6,185,376.1L185,376.1z"/>
	 <path d="M383.3,398.5c0,0-46.7-28.4-65.9-34.3c-2.3-0.9-4.6-1.4-6.9-1.8l0,0c-2.3-0.5-5-0.9-7.8-1.4l-1.8,9.2
	       c24.3,6,33.9,21.1,54.9,33.9c3.2,1.8,19.2,0.9,22.4,2.7L383.3,398.5L383.3,398.5z"/>
	 <path d="M337.5,400.8L337.5,400.8c-2.7,2.7-4.1,6-4.1,9.6c0,1.4,0.5,2.3,1.4,3.2c0.9,0.9,1.8,1.4,3.2,1.4H348
	       c1.8,0,3.2-0.9,4.1-2.3c0.9-1.4,0.9-3.2,0-4.6c-1.8-3.2-5-6.4-10.1-8.2C340.2,399,338.4,399.4,337.5,400.8L337.5,400.8z"/>
	 <path d="M345.7,412.7c-0.9,0-1.8-0.5-2.7-1.4l-6-8.2c-0.9-1.4-0.9-3.2,0.5-4.6c1.4-0.9,3.2-0.9,4.6,0.9l6,8.2
	       c0.9,1.4,0.9,3.2-0.9,4.6C347.1,412.7,346.6,412.7,345.7,412.7L345.7,412.7z"/>
	</g>
         </svg>
        </label>
        <h5><label for="type4" class="vehicle_type">Convertible</label></h5>
		</a>
       </div>
       <div class="col-xs-2">
        <a href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=car&vehicle_budget=default&vehicle_type=sedan&vehicle_condition=new&vehicle_brand=default&vehicle_model=default">
        <!--<input id="type5" type="radio" name="type" />-->
        <label for="type5" class="b-search__main-type-svg" data="sedan">
         <svg version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	    viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g>
	 <path d="M207.9,399.4c-9.2,0-34.8-2.3-48.1-9.6c20.6-11,57.2-23.8,76-23.8c16.5,0,36.6,0,52.7,0c25.2,0,46.2,9.2,66.9,20.6
	       c-3.2,3.2-5,6.4-5.5,11.4c0,0.5,0,0.9,0,1.4H207.9L207.9,399.4z"/>
	 <path d="M134.6,486.4c-21.1,0-45.3-12.4-46.7-12.8l-2.3-1.4v-32.1l-1.4-0.9c0.9-1.8,6-7.3,6-11.9v-29.8l17.9-4.1
	       c24.3-11.4,90.7-34.3,134.6-34.3h54.9c12.4,0.9,22.9,3.2,32.1,5.5c17.9,5.5,58.6,30.2,65,34.3c6.4,0.9,40.3,5,67.8,11.9
	       c33,8.2,44,21.5,45.3,27.5l2.7,47.2l-4.1-0.9c-7.8,1.8-14.2,2.7-31.1,2.7h-18.8v-9.2h13.7c12.8,0,22-0.5,27.5-1.8v-0.9v-35.7
	       c-0.5-1.4-9.6-11.9-38-19.2c-29.8-7.8-66.9-11.9-67.3-11.9l-1.8-0.5c-0.5-0.5-45.8-28.8-64.1-34.3c-8.2-2.3-18.3-4.1-29.3-5.5h-54
	       c-42.6,0-104.4,22.9-127.8,33.9l-4.6,1.4l-11,2.3v22c0,6.4-3.2,12.8-4.6,15.6v22.9c6,3.7,22.9,12.4,38,11.9h12.4v9.2h-12.4
	       C132.8,486.4,135.1,486.4,134.6,486.4L134.6,486.4z M402,486.4H200.6v-9.2H402V486.4L402,486.4z"/>
	 <path d="M99.8,414.1c0,4.6-2.3,9.6-2.7,12.4h6.9c7.8,0,17.9-6,18.3-12.4v-6H99.4L99.8,414.1L99.8,414.1L99.8,414.1z"/>
	 <path d="M471.6,433.3l9.2,16.5c2.7-1.8,7.3-2.7,11-2.7l0,0l0,0l0,0l0,0l0,0l0,0c4.6,0,9.6,0,14.2,0v-0.9c-0.5-1.4-5-9.2-9.6-12.8
	       C487.7,433.3,479.9,433.3,471.6,433.3L471.6,433.3z"/>
	 <polygon points="310.5,426.9 287.6,426.9 287.6,422.3 310.5,422.3 	"/>
	 <polygon points="214.3,426.9 191.4,426.9 191.4,422.3 214.3,422.3 	"/>
	 <polygon points="232.6,459.9 232.6,468.1 381.9,468.1 	"/>
	 <polygon points="502.8,468.1 493.6,468.1 493.6,463.5 502.8,463.5 	"/>
	 <path d="M494.5,463.5h-6.9c-1.8,0-3.2-0.9-3.2,0.9v4.6c0,1.8,1.4,3.2,3.2,3.2h6.9c1.8,0,3.2-1.4,3.2-3.2v-2.3
	       C498.2,464.9,496.8,463.5,494.5,463.5L494.5,463.5z"/>
	 <path d="M359,396.2L359,396.2c-2.7,2.7-4.1,6-4.1,9.6c0,1.4,0.5,0.5,1.4,1.4c0.9,0.9,1.8,1.4,3.2,1.4H370c1.8,0,3.2-0.9,4.1-2.3
	       c0.9-1.4,0.9-0.9,0-2.7c-1.8-3.2-5-6.4-10.1-8.2C362.2,394.4,360.4,394.8,359,396.2L359,396.2z"/>
	 <path d="M367.7,415.5c-0.9,0-1.8-0.5-2.7-1.4l-6-8.2c-0.9-1.4-0.9-3.2,0.9-4.6c1.4-0.9,3.2-0.9,4.6,0.9l6,8.2
	       c0.9,1.4,0.9,3.2-0.9,4.6C369.1,415,368.6,415.5,367.7,415.5L367.7,415.5z"/>
	 <path d="M95.2,463.5v13.3h35.3l-0.5-0.9C115.4,474.5,101.2,467.2,95.2,463.5L95.2,463.5z"/>
	 <path d="M175.4,431L175.4,431c11,0,21.1,4.6,28.8,11.9l0,0h-1.4c7.3,7.3,11.9,17.4,11.9,28.8v11.9v2.7h-4.6h-8.7
	       c2.7-4.6,4.1-9.6,4.1-14.7c0-16-13.3-27.5-29.3-27.5s-29.3,11-29.3,27.5c0,5.5,1.4,10.5,4.1,14.7h-10.5h-3.2v-4.6v-10.5
	       c0-11,4.6-21.1,11.9-28.8h-1.8l0,0C153.9,435.6,163.9,431,175.4,431L175.4,431z"/>
	 <path d="M151.1,449.3c6.4-6.4,14.7-10.1,24.3-10.1c9.6,0,17.9,3.7,24.3,10.1l0,0c6.4,6.4,10.1,14.7,10.1,24.3
	       c0,9.6-3.7,17.9-10.1,24.3l0,0c-6.4,6.4-14.7,10.1-24.3,10.1c-9.6,0-17.9-3.7-24.3-10.1c-6.4-6.4-10.1-14.7-10.1-24.3
	       C141,464,144.7,455.3,151.1,449.3L151.1,449.3L151.1,449.3L151.1,449.3z M175.4,449.3c-13.3,0-23.8,10.5-23.8,23.8
	       s10.5,23.8,23.8,23.8c13.3,0,23.8-10.5,23.8-23.8C199.2,460.3,188.7,449.3,175.4,449.3L175.4,449.3z"/>
	 <path d="M175.4,464.9c-5,0-8.7,4.1-8.7,8.7c0,5,3.7,8.7,8.7,8.7c4.6,0,8.7-3.7,8.7-8.7C184.1,468.6,180,464.9,175.4,464.9
	       L175.4,464.9z"/>
	 <path d="M432.7,431L432.7,431c11,0,21.1,4.6,28.8,11.9l0,0h1.8c7.3,7.3,11.9,17.4,11.9,28.8v11.9v2.7h-4.6h-11.9
	       c2.7-4.6,4.1-9.6,4.1-14.7c0-16-13.3-27.5-29.3-27.5s-29.3,11-29.3,27.5c0,5.5,1.4,10.5,4.1,14.7h-10.5h-4.6v-4.6v-10.5
	       c0-11,4.6-21.1,11.9-28.8h-0.5l0,0C411.7,435.6,421.7,431,432.7,431L432.7,431z"/>
	 <path d="M408.5,449.3c6.4-6.4,14.7-10.1,24.3-10.1c9.6,0,17.9,3.7,24.3,10.1l0,0c6.4,6.4,10.1,14.7,10.1,24.3
	       c0,9.6-3.7,17.9-10.1,24.3l0,0c-6.4,6.4-14.7,10.1-24.3,10.1c-9.6,0-17.9-3.7-24.3-10.1c-6.4-6.4-10.1-14.7-10.1-24.3
	       C398.8,464,402.5,455.3,408.5,449.3L408.5,449.3L408.5,449.3L408.5,449.3z M432.7,449.3c-13.3,0-23.8,10.5-23.8,23.8
	       s10.5,23.8,23.8,23.8c13.3,0,23.8-10.5,23.8-23.8C457,460.3,446,449.3,432.7,449.3L432.7,449.3z"/>
	 <path d="M432.7,464.9c-4.6,0-8.7,4.1-8.7,8.7c0,5,3.7,8.7,8.7,8.7c4.6,0,8.7-3.7,8.7-8.7C441.9,468.6,437.8,464.9,432.7,464.9
	       L432.7,464.9z"/>
	</g>
         </svg>
        </label>
        <h5><label for="type5" class="vehicle_type">Sedan</label></h5>
		</a>
       </div>
       <div class="col-xs-2">
        <a href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=car&vehicle_budget=default&vehicle_type=hatchback&vehicle_condition=new&vehicle_brand=default&vehicle_model=default">
        <!--<input id="type6" type="radio" name="type" />-->
        <label for="type6" class="b-search__main-type-svg" data="minicar">
         <svg version="1.1" id="Layer_6" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	    viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
	<g>
	 <path d="M367.7,491.9h-142v-9.2h142V491.9L367.7,491.9z M408.9,490.1v-9.2c8.2,0,14.2-0.9,18.3-1.4V448c-2.7-11-39.8-27.5-54-27.5
	       l-0.9-1.4h-1.4h-5c-27.9-16.9-49.9-29.3-82-32.1h-25.6c-19.7,0-35.7,1.4-48.1,4.6l1.8,0.5l-4.1,2.7c-13.3,8.2-36.2,28.8-34.3,39.8
	       c0,6.9-4.6,11.9-7.3,14.2v22.9c1.4,2.3,5.5,7.8,14.7,9.2l-2.3,9.2c-16-2.7-21.1-14.7-21.1-15.1l-0.5-1.8v-29.3l2.3-1.4
	       c1.8-0.9,4.1-4.6,4.1-7.3c-2.3-16.5,22.4-37.1,35.7-46.2l-3.7-5.5l6,0.9c14.2-4.6,33-6.9,56.8-6.9h26.1
	       c34.8,3.2,58.6,17.9,86.5,34.8l2.7-1.4c16.5,0.9,58.2,18.3,62.3,36.2l2.7,40.3h-6.4C425.9,488.7,420.4,489.6,408.9,490.1
	       L408.9,490.1z"/>
	 <path d="M202.4,442.9L202.4,442.9c9.2,0,17.9,3.7,24.3,10.1h-1.4c6,6,10.1,14.7,10.1,24.3v8.7v6.4h-4.6H229c-1.4,3.2-3.7,6.4-6,8.7
	       v-1.4l0,0c-5,5-12.4,8.7-20.6,8.7c-7.8,0-15.1-3.2-20.6-8.7l0,0l0,0c-2.7-2.7-5-6.4-6.4-10.1c-1.4-0.5-5-0.9-6-1.4l-2.7-1.4v-3.2
	       v-6.4c0-9.2,3.7-17.9,10.1-24.3h2.3C184.5,447,192.8,442.9,202.4,442.9L202.4,442.9L202.4,442.9z M388.3,459
	       c-11,0-19.7,8.7-19.7,19.7c0,11,8.7,19.7,19.7,19.7c11,0,19.7-8.7,19.7-19.7C408,468.1,399.3,459,388.3,459L388.3,459L388.3,459z
	       M202.4,459c-11,0-19.7,8.7-19.7,19.7c0,11,8.7,19.7,19.7,19.7s19.7-8.7,19.7-19.7C222.1,468.1,213.4,459,202.4,459L202.4,459
	       L202.4,459z M388.3,442.9L388.3,442.9c9.2,0,17.9,3.7,24.3,10.1l0,0c6,6,10.1,14.7,10.1,24.3v8.2v6.4h-4.6c-0.9,0-1.8,0-3.2,0
	       c-1.4,3.2-3.7,4.6-6,7.3l0,0l0,0c-5,5-12.4,8.7-20.6,8.7c-7.8,0-15.1-3.2-20.6-8.7l0,0v1.4c-2.7-2.7-4.6-5.5-6-8.7H359h-5v-4.6
	       v-10.1c0-9.2,3.7-17.9,10.1-24.3h0.5C370.4,447,378.7,442.9,388.3,442.9L388.3,442.9z"/>
	 <path d="M205.1,393.9c-9.2,5-23.4,12.8-30.2,20.1h16l21.5-22.9L205.1,393.9L205.1,393.9z"/>
	 <path d="M212,416.4c14.7-16.5,24.7-25.2,37.1-25.2h34.8c20.6,1.8,37.5,8.7,54.5,17.9c-0.5,0-0.5-0.5-0.9,0l0,0
	       c-3.2,3.2-5,6.9-5,11.4c0,0.9,0,1.4,0,2.3L212,416.4L212,416.4z"/>
	 <path d="M166.2,425.5v0.9v11h13.3c0,0,8.2-9.6,7.8-15.1C186.8,421.9,177.7,422.3,166.2,425.5L166.2,425.5z"/>
	 <path d="M202.4,471.8c-4.1,0-7.3,3.2-7.3,7.3c0,4.1,3.2,7.3,7.3,7.3c4.1,0,7.3-3.2,7.3-7.3C209.3,475,206.1,471.8,202.4,471.8
	       L202.4,471.8z"/>
	 <path d="M388.3,471.8c-4.1,0-7.3,3.2-7.3,7.3c0,4.1,3.2,7.3,7.3,7.3c4.1,0,7.3-3.2,7.3-7.3C395.6,475,392.4,471.8,388.3,471.8
	       L388.3,471.8z"/>
	 <polygon points="285.3,432.4 271.5,432.4 271.5,427.8 285.3,427.8 	"/>
	 <path d="M339.8,411.3L339.8,411.3c-2.7,2.7-4.1,6-4.1,9.6c0,1.4,0.5,4.6,1.4,5.5c0.9,0.9,1.8,1.4,3.2,1.4h10.5
	       c1.8,0,3.2-0.9,4.1-2.3c0.9-1.4,0.9-5.5,0-6.9c-1.8-3.2-5-6.4-10.1-8.2C343,409.5,341.1,410,339.8,411.3L339.8,411.3z"/>
	 <path d="M348.5,430.6c-0.9,0-1.8-0.5-2.7-1.4l-6-8.2c-0.9-1.4-0.9-3.2,0.9-4.6c1.4-0.9,3.2-0.9,4.6,0.9l6,8.2
	       c0.9,1.4,0.9,3.2-0.9,4.6C349.8,430.1,348.9,430.6,348.5,430.6L348.5,430.6z"/>
	 <polygon points="225.7,427.8 212,427.8 212,423.2 225.7,423.2 	"/>
	 <path d="M397,423.2c11.9,21.1,16,26.6,31.6,26.6v0.5l0,0l0,0l0,0l0,0h0.5h1.8v-2.7c-1.8-8.7-16-17.9-30.7-24.3H397L397,423.2z"/>
	</g>
         </svg>
        </label>
        <h5><label for="type6" class="vehicle_type">Hatchback</label></h5>
		</a>
       </div>
      </div>
     </div>
    </div>

    <div class="b-search__main-form wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
     <div class="row">
      <div class="col-md-12">
       <div class="m-firstSelects">
        <div class="col-md-3 col-sm-3 col-xs-3">
            
          <input type="hidden" name="search_flow" value='by_brand'> 
          <input type='hidden' name='vehicle' value='car'/>
          <input type='hidden' name='vehicle_type' value='default'/>
          <input type='hidden' name='vehicle_condition' value='new'/>
          
         <select name="vehicle_brand" id="select_car">
	<option value="default">Select a car</option>
	<?php if(isset($query_car)) { 
                foreach ($query_car as $car_name) { ?>
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php }  } ?>
         </select>
         <span class="fa fa-caret-down"></span>
         <p>e.g : Maruti , Hyundai , Tata </p>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
         <select name="vehicle_model" id="select_model">
	<option value="default">Choose car model</option>
         </select>
         <span class="fa fa-caret-down"></span>
         <p>e.g: Tiago (Tata)</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
         <select name="vehicle_fuel_type">
	<option value="default">Fuel type</option>
        <option value="all_type">All type</option>
<?php foreach ($fuel_type as $fuel) { ?>

        <option value="<?php echo $fuel; ?>"><?php echo strtoupper($fuel); ?></option>

<?php } ?>
         </select>
         <span class="fa fa-caret-down"></span>
         <p>e.g:  CNG,Diesel,Petrol</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
       <select name="vehicle_budget">
         <option value="default">Price range</option>
         <option value="low_to_high">Low to High Budget</option>
         <option value="high_to_low">High to Low Budget</option>
         <option value="1-lakh-5-lakh">1 Lakh - 5 Lakh</option>
	  <option value="5-lakh-10-lakh">5 Lakh - 10 Lakh</option>
	  <option value="10-lakh-20-lakh">10 Lakh - 20 Lakh</option>
	  <option value="20-lakh-50-lakh">20 Lakh - 50 Lakh</option>
	  <option value="50-lakh-1-crore">50 Lakh - 1 Crore</option>
	  <option value="above-1-crore">Above 1 Crore</option>
        </select>
        <p>e.g: 2-5 Lakh , 10+ Lakh</p>
     
       </div>
    
          
   <div class="col-md-2 col-sm-2 col-xs-2">
  
        <div class="b-search__main-form-submit">
        <button type="submit" class="btn m-btn">Search <span class="fa fa-search"></span></button>
       </div>
     
     
     </div>
        
         
       
       </div>
      </div>
   
    </div>
      </div>
  </form>
 </div>
</section><!--b-search-->

 <?php if(count($featured)) { ?>
<section class="b-featured">
 <div class="container">
  <h2 class="s-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">Featured Cars</h2>
  <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
 
    <?php foreach ($featured as $vehicle_data) { ?>

  <?php $car_info2 = json_decode($vehicle_data->car_info);
        $additional_info = json_decode($vehicle_data->engine_transmission);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
        ?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid;?>">
   <img src="<?php render_item_image("uploads/cars/".$car_info2->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   <span class="m-premium">Featured</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $additional_info->mileage; ?></span>
        <span class="more-info">Max Power:<?php echo $additional_info->maximum_power; ?></span>
        <span class="more-info">Fuel Type:<?php echo $additional_info->fuel_type; ?></span>
       </div>
      </div>
     </div>
   <?php } ?>

  </div>
 </div>
</section>
<?php } ?><!--b-featured-->


<!--used Cars-->
 <?php if(count($used)) { ?>
<section class="b-featured">
 <div class="container">
  <h2 class="s-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">Used Cars</h2>
  <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php foreach ($used as $vehicle_data) { ?>

  <?php $car_info2 = json_decode($vehicle_data->vehicle_info);
        $additional_info = json_decode($vehicle_data->additional_info);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
        ?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $vehicle_data->ckid;?>">
   <img src="<?php render_item_image("uploads/used_cars/".$additional_info->additional_info->img[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   <span class="m-premium">Ready for sale</span>
    </a>
   
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/used_car_details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Year:<?php echo $car_info2->vehicle_info->reg_year; ?></span>
        <span class="more-info">Color:<?php echo $car_info2->vehicle_info->color; ?></span>
        <span class="more-info">Fuel Type:<?php echo $car_info2->vehicle_info->fuel; ?></span>
       </div>
      </div>
     </div>
   <?php } ?>

  </div>
 </div>
</section>
<?php } ?><!--b-used-->


<!--Latest cars-->

 <?php if(count($latest)) { ?>
<section class="b-featured">
 <div class="container">
  <h2 class="s-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">Latest Cars</h2>
  <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
 
    <?php foreach ($latest as $vehicle_data) { ?>

  <?php $car_info2 = json_decode($vehicle_data->car_info);
        $additional_info = json_decode($vehicle_data->engine_transmission);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
        ?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid;?>">
   <img src="<?php render_item_image("uploads/cars/".$car_info2->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
    <span class="m-leasing">Latest</span>
                </a>
   
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $additional_info->mileage; ?></span>
        <span class="more-info">Max Power:<?php echo $additional_info->maximum_power; ?></span>
        <span class="more-info">Fuel Type:<?php echo $additional_info->fuel_type; ?></span>
       </div>
      </div>
     </div>
   <?php } ?>

  </div>
 </div>
</section>
<?php } ?><!--b-latest-->

 <?php if(count($upcoming)) { ?>
<section class="b-featured">
 <div class="container">
  <h2 class="s-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">Upcoming Cars</h2>
  <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
 
    <?php foreach ($upcoming as $vehicle_data) { ?>

  <?php $car_info2 = json_decode($vehicle_data->car_info);
        $additional_info = json_decode($vehicle_data->engine_transmission);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
        ?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid;?>">
   <img src="<?php render_item_image("uploads/cars/".$car_info2->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   <span class="m-leasing">Upcoming</span>
                </a>
          
     
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $additional_info->mileage; ?></span>
        <span class="more-info">Max Power:<?php echo $additional_info->maximum_power; ?></span>
        <span class="more-info">Fuel Type:<?php echo $additional_info->fuel_type; ?></span>
       </div>
      </div>
     </div>
   <?php } ?>

  </div>
 </div>
</section>
<?php } ?><!--b-upcoming-->


 <?php if(count($recommended)) { ?>
<section class="b-featured">
 <div class="container">
  <h2 class="s-title wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">Recommended Cars</h2>
  <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
 
    <?php foreach ($recommended as $vehicle_data) { ?>

  <?php $car_info2 = json_decode($vehicle_data->car_info);
        $additional_info = json_decode($vehicle_data->engine_transmission);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
        ?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid;?>">
   <img src="<?php render_item_image("uploads/cars/".$car_info2->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   <span class="m-leasing">Recommended</span>
                </a>
       
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $additional_info->mileage; ?></span>
        <span class="more-info">Max Power:<?php echo $additional_info->maximum_power; ?></span>
        <span class="more-info">Fuel Type:<?php echo $additional_info->fuel_type; ?></span>
       </div>
      </div>
     </div>
   <?php } ?>

  </div>
 </div>
</section>
<?php } ?><!--b-recommended-->


<section class="b-asks">
 <div class="container">
  <div class="row">
   <div class="col-md-6 col-sm-10 col-sm-offset-1 col-md-offset-0 col-xs-12"><a href="<?php echo base_url(); ?>index.php/Welcome/new_car" style="text-decoration: none;">
     <div class="b-asks__first wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
      <div class="b-asks__first-circle">
       <span class="fa fa-car"></span>
      </div>
      <div class="b-asks__first-info">
       <h2>Are you looking for a car?</h2>
       <p>Search our inventory with thousands of cars & More 
        cars are adding on daily basis</p>
      </div>
      <div class="b-asks__first-arrow">
       <span class="fa fa-angle-right"></span>
      </div>
     </div>
    </a>
   </div>


   <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-12 col-md-offset-0">
    <a href="<?php echo base_url(); ?>index.php/Welcome/post_car" style="text-decoration: none;">
     <div class="b-asks__first m-second wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
      <div class="b-asks__first-circle">
       <span class="fa fa-inr"></span>
      </div>
      <div class="b-asks__first-info">
       <h2>Do you want to sell a car?</h2>
       <p>Just fill in few details and sell your car without paying any listing fee.</p>  
      </div>
      <div class="b-asks__first-arrow">
       <span class="fa fa-angle-right"></span>
      </div>
     </div>
    </a>
   </div>
   <div class="col-xs-12">
    <p class="b-asks__call wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Need Help ? Call Us : <span>+91-9044 988 899</span></p>
   </div>
  </div>
 </div>
</section><!--b-asks-->

<section class="b-auto">
 <div class="container">
  <h5 class="s-titleBg wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Giving our Customers best deals</h5><br />
  <h2 class="s-title wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Best Offer from Dreamwheels</h2>
  <div class="row">

   <div class="col-md-12">
    <div class="b-auto__main">
     <div class="col-md-12">
      <a href="#"  class="b-auto__main-toggle s-lineDownCenter m-active j-tab wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100" data-to="#first">Upcoming Cars</a>
      <a href="#" class="b-auto__main-toggle j-tab wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100" data-to="#second">Recommended Cars</a>
     </div>
     <div class="clearfix"></div>
     <div class="row m-margin" id="first">
         <!--   Start of Recomended cars -->
      <?php if (count($upcoming)) { ?>
       <?php foreach ($upcoming as $vehicle_data) { 
            $car_upcoming = json_decode($vehicle_data->car_info);
            $additional_info = json_decode($vehicle_data->engine_transmission);
  
                          $car_name = $vehicle_data->car_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = ""; ?>
         
        <div class="col-md-4 col-sm-6 col-xs-12">
         <div class="b-auto__main-item wow slideInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
          
               
               
      
             <img class="img-responsive" src="<?php render_item_image("uploads/car/".$car_upcoming->images[0],'car');?>" alt="mers"alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:270px; height:150px;"/>

          <div class="b-world__item-val">

          </div>
          <h2><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->ckid; ?>"><?php echo $car_name,' ',$model_name; ?> </a></h2>
          <div class="b-auto__main-item-info">
           <span class="m-price">
  	<i class="fa fa-inr" aria-hidden="true"></i> <?php echo $car_upcoming->price; ?>
           </span>

          </div>
          <div class="b-featured__item-links m-auto">

  <?php $date = $car_upcoming->upcoming_date; ?>

           Launch Date : <?php $date = date_create($date);
  echo date_format($date, 'd M Y');
  ?>


          </div>
         </div>
        </div>
       <?php } ?>
  
<?php } ?>
     </div>

     <div class="row m-margin" id="second">
      <?php if (isset($recommended)) { ?>
       <?php // var_dump($upcoming); ?>
       <?php foreach ($recommended as $recommended_cars) { ?>

  <?php $car_recommend = json_decode($recommended_cars->car_info);          
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
       
                $model_id = $car_upcoming->model;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  } ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
         <div class="b-auto__main-item wow slideInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
             
          <img class="img-responsive" src="<?php render_item_image("uploads/car/".$car_recommend->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:270px; height:150px;"/>
          <div class="b-world__item-val">
  	        <!--<span class="b-world__item-val-title">REGISTERED <span>2014</span></span>-->
          </div>
          <h2><a href="detail.html"><?php echo $car_name,' ',$model_name; ?> </a></h2>
          <div class="b-auto__main-item-info">
           <span class="m-price">
  	<i class="fa fa-inr" aria-hidden="true"></i> &nbsp; <?php echo $car_recommend->price; ?>
           </span>
           <span class="m-number">
  	         <!--<span class="fa fa-tachometer"></span>35,000 KM
           </span>-->
          </div>
          <div class="b-featured__item-links m-auto">
<!--           <span class="more-info">BHP:<?php echo $car_recommend->bhp; ?></span>
           <span class="more-info">Gear:<?php echo $car_recommend->gear; ?></span>
           <span class="more-info">Seat:<?php echo $car_recommend->seat; ?></span>
           <span class="more-info">Fuel Type:<?php echo $car_recommend->fuel; ?></span>-->
          </div>
         </div>
        </div>
 <?php }
} ?>

     </div>
    </div>
   </div>
  </div>
</section><!--b-auto-->






<script type="text/javascript">
    $(document).ready(function(){

  $(document).on("change",".select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car_id},function(o){
           
           $(".model").html(o);
           });
      });
      $(document).on("change","#select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car_id},function(o){
           
           $("#select_model").html(o);
           //$("#model").hide();
           });
      });  
 $(document).on('click', '#used', function () {
  $("#vehicle_condition").val('used');
  //alert($("#bikes_type").val());
 });
 $(document).on('click', '#new', function () {
  $("#vehicle_condition").val('new');
 });
 

var vehicle,vehicle_type,vehicle_budget,vehicle_brand,vehicle_model,vehicle_condition,search_flow;
 search_flow=$(".search_flow").val();
  $(document).on('click','.search_flow',function () {
       $("#by_budget,#by_brand").hide();
        search_flow=$(this).val();
        //console.log(search_flow);
        $("#"+search_flow).show();

 });
 
 $(document).on("click","#search_button",function(){
     vehicle_condition = $("#vehicle_condition").val();
     vehicle_budget=$(".budget").find("option:selected").val();
     vehicle_type=$(".vehicle_type").find("option:selected").val();
     vehicle_brand=$(".brand").find("option:selected").val();
     vehicle_model=$(".model").find("option:selected").val();
      
     var page = "<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow="+search_flow+"&vehicle=car&vehicle_budget=" +vehicle_budget + "&vehicle_type=" +vehicle_type+"&vehicle_condition="+vehicle_condition+"&vehicle_brand="+vehicle_brand+"&vehicle_model="+vehicle_model;
window.location.href=page;
 });
 

    });
</script>
<?php include('footer.php'); ?>