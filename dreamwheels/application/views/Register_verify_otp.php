<?php include('header.php'); ?>
<style>
 .lower_case{
  text-transform: none!important;
 }
</style>

<section class="b-pageHeader">
 <div class="container">
 <h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Verify your mobile no.</h1>

 </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
 <div class="container">
<a href="<?php echo base_url(); ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span>
<a href="<?php echo base_url(); ?>index.php/Welcome/Register" class="b-breadCumbs__page m-active">Register</a><span class="fa fa-angle-right"></span>
<a href="#" class="b-breadCumbs__page m-active">Verify OTP</a>
 </div>
</div><!--b-breadCumbs-->


<section class="b-contacts s-shadow">
 <div class="container"> 
  <div class="row">
   <div class="col-xs-12">
    <div class="b-contacts__form">
       <?php if($this->session->get_userdata("otp_op")['otp_op'])
{ ?>
      <div class="alert alert-success">

      <?php echo $this->session->get_userdata("otp_op")['otp_op_msg']; ?>
      </div>
    
     <?php } $this->session->set_userdata("otp_op")['otp_op']=FALSE; ?>
     <header class="b-contacts__form-header s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
      <h2 class="s-titleDet">Please enter your One Time Password (OTP)</h2> 
     </header>
     <p class=" wow zoomInUp" data-wow-delay="0.5s">You are just one step behind to be a DreamWheelers.</p>
     <div id="success"></div>




     <form id="contactForm" action="<?php echo base_url(); ?>index.php/User/verify_otp" class="s-form wow zoomInUp" data-wow-delay="0.5s" method="post">
      <div class="form-group">
       <div class="col-xs-6">
        <input type="text" placeholder="OTP" name="otp" id="otp" class="lower_case bg-success" required/>
       </div>
      </div>
      <div class="form-group">
      <div class="col-xs-6"> 
      <button type="submit" class="btn m-btn" style="margin:0px;">Verify <span class="fa fa-angle-right"></span></button>
      </div>
      </div>
 
<div class="form-group">
   <div class="col-md-12" style="margin-top: 20px;"> 
   <hr>
   <center><span style="color:#f76d2b">Still Don't Get OTP ?</span> <a href="<?php echo base_url(); ?>index.php/Welcome/Register">Resend Now</a>
   <br>
  <img src="<?php echo base_url(); ?>images/logo.png" style="margin-top:5px;">
   </center>
   </div>
   </div>
 
     </form>

   
     
    </div>
   </div>
  </div>
 </div>

</section><!--b-contacts-->


<!--Main-->   
<?php include('footer.php'); ?>

<script>
    $(document).ready(function(){
        $(document).on("blur","#user-email",function(){
            var email = $(this).val();
            $.post("<?php echo base_url();?>index.php/User/email_validate",{email:email},function(o){
                  //console.log(o);
                  if(o=="Match"){
                      alert("Email already exist !!!");
                      $("#user-email").val("");
        }
            
            });
        });
        $(document).on("blur","#user-phone",function(){
            var phone = $(this).val();
            $.post("<?php echo base_url();?>index.php/User/phone_validate",{phone:phone},function(o){
                  //console.log(o);
                  if(o=="Match"){
                      alert("Phone Number already exist !!!");
                      $("#user-phone").val("");
        }
            
            });
        });
        
//        $("#contactForm").submit(function () {
//            
//            var phone = $("#user-phone").val();
//            var email = $("#user-email").val();
//            var name = $("#user-name").val();
//            var password = $("#user-password").val();
//            var city = $("#city").val();
//            alert(city);
//                  if(phone=="" && email=="" && name=="" && password=="" && city==""){
//                      alert("Please fill all the fields !!!");
//                     event.preventDefault();
//        }
//            
//           
//        });
    });
</script>    