<?php 
        //var_dump($cars);
        //var_dump($bikes);
?>

<div class="col-md-3 col-xs-6">
						<div class="b-info__latest">
							<h3>Latest cars</h3>
                                                    <?php  if(isset($cars)) {
                                                            $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                                                            
                                                            ?>
							 <?php $car_option = array();?>
							<?php  foreach($cars as $car_list) { 
                                                 $car_info = json_decode($car_list->car_info);
                                                             $model_id = $car_info->model;
                  $variant_id = $car_info->variant;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
                  foreach ($query_variant as $variant) {
                  
                      if($variant->id==$variant_id){
                          $variant_name = $variant->variant;
                       }                      
                  }
                              
                  $engine_transmission = json_decode($car_list->engine_transmission);
                 ?>   
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-audi"><a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>"><img class="img-responsive" src="<?php render_item_image("uploads/car/".$car_info->images[0],'car');?>" alt="<?php echo $car_name." ".$model_name; ?> available on dreamwheels.in" style="width:80px; height:65px;"/></a></div>
                                                                
								<div class="b-info__latest-article-info">
									<h6><a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>"><?php echo $car_name,' ',$model_name;?></a></h6>
                                                                        <p><i class="fa fa-inr"></i> <?php echo $car_info->price;?></p>
								</div>
							</div>
                                                    <?php  } } ?>    

						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__latest">
							<h3>Latest bikes</h3>
                                                        <?php  
                                                        
                                                              $i=0;
                                                        foreach($bikes as $bike_list) {
                                         $bike_id=$bike_list->bike_id;
			                        $model_id=$bike_list->model_id;
									
						 foreach($query_model as $model_name){
						    if($model_id==$model_name->id){
							        $bike_name = $model_name->bike_name;
                                           //var_dump($model_name->model_name);
                                    $model = $model_name->model_name;
							}
							
						 }
							?>
                                    
							<?php // var_dump($bike_list);?>
                              <?php $bike_info = json_decode($bike_list->bike_info); ?>
                                                        
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-audi"><a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>"><img class="img-responsive" src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $bike_name." ".$model; ?> available on dreamwheels.in" style="width:80px; height:65px;"/></a></div>
								<div class="b-info__latest-article-info">
									<h6><a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>"><?php echo $bike_name,' ',$model;?></a></h6>
									<p><p><i class="fa fa-inr"></i> <?php echo $bike_info->price;?></p></p>
								</div>
							</div>
                                                        
                                                        <?php  } ?>
                                                        
<!--							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-audiSpyder"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="#">AUDI R8 SPYDER V-8</a></h6>
									<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
								</div>
							</div>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-aston"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="#">ASTON MARTIN VANTAGE</a></h6>
									<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
								</div>
							</div>-->
						</div>
					</div>