<?php include('header.php'); ?>
<style>
 .lower_case{
  text-transform: none!important;
 }
</style>

<section class="b-pageHeader">
 <div class="container">
  <h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Recover your password</h1>

 </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
 <div class="container">
  <a href="<?php echo base_url(); ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>index.php/Welcome/Register" class="b-breadCumbs__page m-active">Password recovery</a>
 </div>
</div><!--b-breadCumbs-->


<section class="b-contacts s-shadow">
 <div class="container"> 
  <div class="row">
   <div class="col-xs-12">
    <div class="b-contacts__form">
     <?php if(isset($this->session->get_userdata("recovery_op")['recovery_op']))
{ ?>
      <div class="alert alert-success">

      <?php echo $this->session->get_userdata("recovery_op")['recovery_op_msg']; ?>
      </div>
    
     <?php } $this->session->set_userdata("recovery_op")['recovery_op']=FALSE; ?>
     <header class="b-contacts__form-header s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
      <h2 class="s-titleDet">Please use only registered email-id or mobile number for password recovery</h2> 
     </header>
     <p class=" wow zoomInUp" data-wow-delay="0.5s"></p>
     <div id="success"></div>




     <form id="passwordRecoveryForm" action="<?php echo base_url(); ?>index.php/User/password_recovery" class="s-form wow zoomInUp" data-wow-delay="0.5s" method="post">
      <div class="form-group">
       <div class="col-xs-6">
        <input type="text" placeholder="Registered Mobile No. or Email-ID" name="mobile_email" id="mobile_email" class="lower_case bg-success" required/>
       </div>
       <div class="col-xs-6">
           <button type="submit" class="btn m-btn" style="margin:0px">Submit<span class="fa fa-angle-right"></span></button>
       </div>
      </div>
     
     </form>

     
    </div>
   </div>
  </div>
 </div>

</section><!--b-contacts-->


<!--Main-->   
<?php include('footer.php'); ?>

<script>
    $(document).ready(function(){
 
        $("#passwordRecoveryForm").submit(function () {
            
            var mobile_email = $("#mobile_email").val();
                  if(mobile_email==""){
                      alert("Mobile/Email required !!!");
                     event.preventDefault();
        }
            
           
        });
    });
</script>    