<div class="col-lg-8 col-sm-8 col-xs-12">
						<div class="b-items__cars" id="bike_filter">
                                                    <div class="b-items__cars">
							<?php  if(isset($query_car)) { ?>
							 <?php $car_option = array();?>
							<?php  foreach($query_car as $car_list) {  ?>

							<?php // var_dump($car_list);?>
                              <?php $car_info = json_decode($car_list->vehicle_info);?>
                              <?php $additional_info = json_decode($car_list->additional_info);?>
                              <?php $cars = $car_info->vehicle_info->car;?>
                             
                              
                              <?php array_push($car_option, $cars);?>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<img src="<?php echo base_url();?>media/270x230/dodge.jpg" alt='dodge'/>
<!--									<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>-->
									<span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
									<form method="post">
										<input type="checkbox" name="check1"/>
										<label for="check1" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
									</form>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_info->vehicle_info->car;?> <?php echo $car_info->vehicle_info->model;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $car_list->price;?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $car_info->vehicle_info->kilometer_driven;?> KM
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $car_info->vehicle_info->reg_year;?></a>
											<a href="#">Used</a>
											<a href="#">Color:-<?php echo $car_info->vehicle_info->color;?> </a>
											<a href="#"><?php echo $car_info->vehicle_info->fuel;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  } } ?>
							
						</div>
                                                    <div class="bike_details_after_filter">
		 		<?php 
                                           $bike_name='';
                                           $model_name= '';
                                           if(isset($query_bike)) { ?>
							 <?php $bike_option = array();?>
							<?php  foreach($query_bike as $bike_list) {  ?>

							<?php // var_dump($bike_list);?>
                              <?php $bike_info = json_decode($bike_list->vehicle_info);?>
                              <?php $additional_info = json_decode($bike_list->additional_info);?>
                              <?php $bikes = $bike_info->vehicle_info->bike_id;
                              
                               foreach($query_bike_details as $bike_details)
                                   {
                                   if($bike_details->id == $bike_info->vehicle_info->model_id)
                                       {
                                           $bike_name = $bike_details->bike_name;
                                           //var_dump($bike_details->bike_name);
                                           $model_name = $bike_details->model_name;
                                       }
                                   
                               }
                              //array_push($bike_option, $bikes);?>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<img src="<?php echo base_url();?>uploads/used_bikes/<?php echo $additional_info->additional_info->img[0];?>" alt='bike' height="230px" width="270px"/>
									<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>PHOTOS</a>
									<span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
									<form method="post">
										<input type="checkbox" name="check1" />
										<label for="check1" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
									</form>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name,' ',$model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $bike_list->price;?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $bike_info->vehicle_info->kilometer_driven;?> KMs
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $bike_info->vehicle_info->reg_year;?></a>
											
											<a href="#">Color:-<?php echo $bike_info->vehicle_info->color;?> </a>
                                                                                        <a href="#">City:-<?php echo $bike_info->vehicle_info->city;?></a>
<!--											<a href="#"><?php echo $bike_info->vehicle_info->fuel;?></a>-->
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  } } ?>
						    </div>	
						</div>
                                            
					
							
						<div class="b-items__pagination wow zoomInUp" data-wow-delay="0.5s">
							<div class="b-items__pagination-main">
								<a data-toggle="modal" data-target="#myModal" href="#" class="m-left"><span class="fa fa-angle-left"></span></a>
								<span class="m-active"><a href="#">1</a></span>
								<span><a href="#">2</a></span>
								<span><a href="#">3</a></span>
								<span><a href="#">4</a></span>
								<a href="#" class="m-right"><span class="fa fa-angle-right"></span></a>    
							</div>
						</div>

					</div>