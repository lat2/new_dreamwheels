<?php include('header.php'); ?>
<style>
 .lower_case{
  text-transform: none!important;
 }
</style>

<section class="b-pageHeader">
 <div class="container">
  <h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Registration form</h1>

 </div>
</section><!--b-pageHeader-->

<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
 <div class="container">
  <a href="<?php echo base_url(); ?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url(); ?>index.php/Welcome/Register" class="b-breadCumbs__page m-active">Register</a>
 </div>
</div><!--b-breadCumbs-->


<section class="b-contacts s-shadow">
 <div class="container"> 
  <div class="row">
   <div class="col-xs-12">
    <div class="b-contacts__form">
     <?php if (isset($_GET['msg'])) { ?>
      <div class="alert alert-success">

       <strong>Success!</strong> You are successfully Registered.
      </div>
     <?php } ?>
     <header class="b-contacts__form-header s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
      <h2 class="s-titleDet">Just fill in few details and sell your car without paying any listing fee</h2> 
     </header>
     <p class=" wow zoomInUp" data-wow-delay="0.5s">Enter the details for the registration.</p>
     <div id="success"></div>




     <form id="contactForm" action="<?php echo base_url(); ?>index.php/User/register_user" class="s-form wow zoomInUp" data-wow-delay="0.5s" method="post">
      <div class="form-group">
       <div class="col-xs-6">
        <input type="text" placeholder="name" name="user-name" id="user-name" class="lower_case bg-success" required/>
       </div>
       <div class="col-xs-6">
        <input type="text" placeholder="email" name="user-email" id="user-email" class="lower_case" required/>
       </div>
      </div>
      <div class="form-group">
       <div class="col-xs-6">
        <input type="password" placeholder="password" name="password" id="user-password"/>
       </div>
       <div class="col-xs-6">
        <input type="text" placeholder="phone number" name="user-phone" id="user-phone" required/>
       </div>
      </div>
      <div class="form-group">
       <div class="col-xs-6">
        <select class="m-select" placeholder="Select city" name="user_info[city]" id="city" required>
         <option value="">Select City</option>
         <?php foreach ($query as $cities) { ?>
          <option value="<?php echo $cities->city_id; ?>"><?php echo $cities->city_name; ?></option>

         <?php } ?>
        </select>
       </div>
       <div class="col-xs-6">

       </div>
      </div>
      <br>
<!--								<textarea id="user-message" name="user_info[user_address]" placeholder="ADDRESS"></textarea>-->
      <div class="form-group">
       <button type="submit" class="btn m-btn" style="margin:0px">REGISTER NOW<span class="fa fa-angle-right"></span></button>
      </div>
     </form>

     
    </div>
   </div>
  </div>
 </div>

</section><!--b-contacts-->


<!--Main-->   
<?php include('footer.php'); ?>

<script>
    $(document).ready(function(){
        $(document).on("blur","#user-email",function(){
            var email = $(this).val();
            $.post("<?php echo base_url();?>index.php/User/email_validate",{email:email},function(o){
                  //console.log(o);
                  if(o=="Match"){
                      alert("Email already exist !!!");
                      $("#user-email").val("");
        }
            
            });
        });
        $(document).on("blur","#user-phone",function(){
            var phone = $(this).val();
            $.post("<?php echo base_url();?>index.php/User/phone_validate",{phone:phone},function(o){
                  //console.log(o);
                  if(o=="Match"){
                      alert("Phone Number already exist !!!");
                      $("#user-phone").val("");
        }
            
            });
        });
        
//        $("#contactForm").submit(function () {
//            
//            var phone = $("#user-phone").val();
//            var email = $("#user-email").val();
//            var name = $("#user-name").val();
//            var password = $("#user-password").val();
//            var city = $("#city").val();
//            alert(city);
//                  if(phone=="" && email=="" && name=="" && password=="" && city==""){
//                      alert("Please fill all the fields !!!");
//                     event.preventDefault();
//        }
//            
//           
//        });
    });
</script>    