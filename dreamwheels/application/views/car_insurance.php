<?php include('header.php');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.7s">Car Insurance</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s">
					<h3>The Largest Auto Dealer Online</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/car_insurance" class="b-breadCumbs__page m-active">Car Insurance</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-best">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="b-best__info">
<!--							<header class="s-lineDownLeft b-best__info-head">
								<h2 class="wow zoomInUp" data-wow-delay="0.5s">About Us</h2>
							</header>
							-->
                            <p class="wow zoomInUp" data-wow-delay="0.5s"><img src="<?php echo base_url();?>images/car-banner.jpg" alt="Car insurance online, motor car insurance, Sell used Cars in Delhi , insurance renewal, cheap car insurance, compare car insurance, Sell used cars in Bangalore" width="100%" align="right"></p>                         

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Driving can be a risky job. You never know who you might run into or who might run into you. Because of this, getting your car insured becomes a necessity. With over 100,000 people being killed due to road accidents each year, it is indeed very important to be insured.</p>

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Reasons for getting your car insured:</p>

<p class="wow zoomInUp" data-wow-delay="0.5s">Coming onto the question of insurance, there are many reasons why you should get your car insured. It is mandatory for every car plying on the roads to have a valid insurance otherwise they can be seized.</p>

                            

                            <center><h3>Here are a few reasons why you have to get your car insured:</h3>		 </center>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="border:1px solid #bcbcbc;padding:10px;height:500px;text-align:justify;float:left;">
		
		<center><img src="<?php echo base_url();?>images/law.png" align="center"></center>
			<center><h3>LAW</h3>		 </center>
			<p>Yes, it is right. According to government rules, it is mandatory for a car to be insured in order to ply on the roads. So, like it or not, you need your car to be insured at all times.</p>
	</div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="border:1px solid #bcbcbc;padding:10px;height:500px;text-align:justify;float:left;">
		
		<center><img src="<?php echo base_url();?>images/save-money.png" align="center" alt="Car insurance online, motor car insurance, Sell used Cars in Delhi , insurance renewal, cheap car insurance, compare car insurance, Sell used cars in Bangalore"></center>
			<center><h3> SAVES YOUR MONEY</h3>		 </center>
			<p>Imagine this; someone rams into your car and you’re left with some pretty bad dents. In such a case, it will probably take a lot of money to get your car repaired. If you’re insured, then you can file a claim and your repairing charges will be negligible. Thus, keeping your car insured makes sure that you’re saving a lot of money on car repairs.</p>
	</div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="border:1px solid #bcbcbc;padding:10px;height:500px;text-align:justify;float:left;">
		
		<center><img src="<?php echo base_url();?>images/liablity.png" align="center"></center>
			<center><h3>REDUCES LIABILITY</h3>		 </center>
			<p> Now imagine this; you cause an accident wherein you damage somebody else’s car or cause some injury to another person. They’ll ask you to pay up right? Well, having comprehensive car insurance entitles you to claim for third party damages and the insurance company will make sure that the other party is paid for the damages caused. This greatly reduces your liability and makes you immune from monetary loss.</p>
	</div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="border:1px solid #bcbcbc;padding:10px;height:500px;text-align:justify;float:left;">
		
		<center><img src="<?php echo base_url();?>images/peace.png" align="center"></center>
			<center><h3>PEACE OF MIND</h3>		 </center>
			<p>Last but not the least, car insurance is very important because it brings peace of mind. How? Well, if you read the three points above, you will know the exact reason. If your car is insured, then your car will never be seized (unless you’re short of other required documents). Having insurance will save you money in case of car damage and reduce your liability in case of an accident caused by you. In all these cases, you’re free from hassles and can have peace of mind as you drive on the roads.</p>
	</div>
                            
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> So, whether it is your new car just a second hand one, make sure that you have it insured. The digital age has made insurance pretty easy with websites providing for all services required. Just go through some quotations, find out the one that suits your needs, call their representative and have them deliver your insurance papers at home and via email. </p> 
                             <p class="wow zoomInUp" data-wow-delay="0.5s"> Get your car insured today so that you can get on the roads with nothing to fear. Drive peacefully and safely for a pleasant commute every time you step out. </p> 						<!--<a href="article.html" class="btn m-btn m-readMore wow zoomInUp" data-wow-delay="0.5s">view listings<span class="fa fa-angle-right"></span></a>-->
						</div>
					</div>
<!--					<div class="col-sm-4 col-xs-12">
                                            <p><img src="<?php echo base_url();?>images/finance.jpg" width="350"></p> 
						
                                            <p class="wow zoomInUp" data-wow-delay="0.5s"><b>You can buy the car you want:</b> The first and foremost benefit is that you can buy the car that you want. Of course, you cannot go overboard as the lending limit depends on your credit score and yearly income, but you can buy anything that falls in your range without having to worry about the entire cost of the vehicle at the time. These days, it is quite easy to secure vehicle finance so you can just walk in to a bank and drive out with your brand new car.</p>
					</div>-->
				</div>
			</div>
		</section><!--b-best-->

		
		<?php include('footer.php');?>