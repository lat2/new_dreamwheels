<?php include('header.php');?>
   <section class="b-pageHeader">
            <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInLeft;">EMI Calculator</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInRight;">
                    <h3>EMI Calculator</h3>
                </div>
            </div>
        </section><!--b-pageHeader-->

        <div class="b-breadCumbs s-shadow">
            <div class="container wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                <a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active">EMI Calculator</a>
            </div>
        </div><!--b-breadCumbs-->

         

        <section class="b-compare s-shadow">
            <div class="container">
                <div class="b-compare__images">
                    <div class="row">
                    <p>It is very easy to calculate the EMI for your car loan. You will get EMI as soon as you enter the required loan amount and the interest rate. Installment in EMI calculator is calculated on reducing balance. As per the rules of financing institutions, processing fee or possible charges may be applicable which are not shown in the EMI we calculate.</[>
                        <br><br>
                        <div class="col-md-3">
                        <select class="form-control" id="select_car">
                        <option value="" selected="" disabled>Select Car</option>
                        <?php if(isset($car_list)) { ?>
                        <?php foreach($car_list as $cars){ ?>
                        <option value=<?php echo $cars->cars;?>><?php echo trim($cars->cars,'"');?></option>
                        <?php } } ?>
                        </select>
                        </div>
                        <div class="col-md-3">
                        <select class="form-control" id="model">
                        <option value="">Select Model</option>
                       
                        </select>
                        </div>
                        <div class="col-md-3">
                        <select class="form-control" id="select_variant">
                        <option>Select Variant</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <input type="hidden" id="price_val" name="price" value="">
                 <div class="col-md-9">
                 <div class="panel panel-default">
<div class="panel-heading">Car Name,Model Name,Variant Name</div>
    <div class="panel-body" style="display:none;"><b>1 ) Down Payment you will make</b><p class="pull-right" id="down"><b>0</b></p></div>
     <div class="panel-body" style="display:none;"><div id="slider3"> <div id="custom-handle" class="ui-slider-handle"></div></div></div>
     <div class="panel-body"><b>1 ) Down Payment you will make</b><p class="pull-right"><b><output></output></b></p></div>
     <div class="panel-body"> <input type="range" value="0" min="0" max="1500" data-rangeslider></div>
    <div class="panel-body"><b>2 ) Bank Interest Rate(%)</b><p class="pull-right" id="rate"><b>0</b></p></div>
    <div class="panel-body"><div id="slider1"> <div id="custom-handle" class="ui-slider-handle"></div></div></div>
    <div class="panel-body"><b>3 ) Loan Period(Months)</b><p class="pull-right" id="period"><b>0</b></p></div>
    <div class="panel-body"><div id="slider4"> <div id="custom-handle" class="ui-slider-handle"></div></div></div>
    <div class="panel-body" style="text-align:center;"><b>EMI per month:<p id="emi_total"> <i class="fa fa-inr" aria-hidden="true"></i> 0</p> </b></div>
    <div class="panel-body" style="text-align:center;">Total Loan Amount: <b><span id="total_loan"> <i class="fa fa-inr" aria-hidden="true"></i> 0</span> </b> / Payable Amount <b><span id="total_emi"><i class="fa fa-inr" aria-hidden="true"></i> 0</span></b></div>
  </div>
  </div>
                
        </section><!--b-compare-->

        

        <div class="b-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-xs-6">
                        <aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                            <article class="b-info__aside-article">
                                <h3>OPENING HOURS</h3>
                                <div class="b-info__aside-article-item">
                                    <h6>Sales Department</h6>
                                    <p>Mon-Sat : 8:00am - 5:00pm<br>
                                        Sunday is closed</p>
                                </div>
                                <div class="b-info__aside-article-item">
                                    <h6>Service Department</h6>
                                    <p>Mon-Sat : 8:00am - 5:00pm<br>
                                        Sunday is closed</p>
                                </div>
                            </article>
                            <article class="b-info__aside-article">
                                <h3>About us</h3>
                                <p>Vestibulum varius od lio eget conseq
                                    uat blandit, lorem auglue comm lodo 
                                    nisl non ultricies lectus nibh mas lsa 
                                    Duis scelerisque aliquet. Ante donec
                                    libero pede porttitor dacu msan esct
                                    venenatis quis.</p>
                            </article>
                            <a href="about.html" class="btn m-btn">Read More<span class="fa fa-angle-right"></span></a>
                        </aside>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="b-info__latest">
                            <h3>LATEST AUTOS</h3>
                            <div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__latest-article-photo m-audi"></div>
                                <div class="b-info__latest-article-info">
                                    <h6><a href="detail.html">MERCEDES-AMG GT S</a></h6>
                                    <p><span class="fa fa-tachometer"></span> 35,000 KM</p>
                                </div>
                            </div>
                            <div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__latest-article-photo m-audiSpyder"></div>
                                <div class="b-info__latest-article-info">
                                    <h6><a href="#">AUDI R8 SPYDER V-8</a></h6>
                                    <p><span class="fa fa-tachometer"></span> 35,000 KM</p>
                                </div>
                            </div>
                            <div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__latest-article-photo m-aston"></div>
                                <div class="b-info__latest-article-info">
                                    <h6><a href="#">ASTON MARTIN VANTAGE</a></h6>
                                    <p><span class="fa fa-tachometer"></span> 35,000 KM</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="b-info__twitter">
                            <h3>from twitter</h3>
                            <div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
                                <div class="b-info__twitter-article-content">
                                    <p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
                                    <span>20 minutes ago</span>
                                </div>
                            </div>
                            <div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
                                <div class="b-info__twitter-article-content">
                                    <p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
                                    <span>20 minutes ago</span>
                                </div>
                            </div>
                            <div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                                <div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
                                <div class="b-info__twitter-article-content">
                                    <p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
                                    <span>20 minutes ago</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                            <p>contact us</p>
                            <div class="b-info__contacts-item">
                                <span class="fa fa-map-marker"></span>
                                <em>202 W 7th St, Suite 233 Los Angeles,
                                    California 90014 USA</em>
                            </div>
                            <div class="b-info__contacts-item">
                                <span class="fa fa-phone"></span>
                                <em>Phone:  1-800- 624-5462</em>
                            </div>
                            <div class="b-info__contacts-item">
                                <span class="fa fa-fax"></span>
                                <em>FAX:  1-800- 624-5462</em>
                            </div>
                            <div class="b-info__contacts-item">
                                <span class="fa fa-envelope"></span>
                                <em>Email:  info@domain.com</em>
                            </div>
                        </address>
                        <address class="b-info__map">
                            <a href="contacts.html">Open Location Map</a>
                        </address>
                    </div>
                </div>
            </div>
        </div><!--b-info-->

   
   
   
<script type="text/javascript">
             $(document).on("change","#select_variant",function(){

                 var model = $('#model option:selected').val();
                 var car = $('#select_car option:selected').val();
                 var variant =  $('#select_variant option:selected').val();
                  $.post("<?php echo base_url();?>index.php/Welcome/select_price",{car_name:car,model_name:model,variant_name:variant},function(o){
                    console.log(o);
                 $("#price_val").val(o);
                  });

              });
        </script>

        
   <script type="text/javascript">

  $(document).on("change","#select_car",function(){
   var car = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_model",{car_name:car},function(o){
          // console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
  </script>

  <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var car = $('#select_car option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{car_name:car,model_name:model},function(o){
          // console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 


 <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider3" ).slider({
        min: 10000,
      max: 1000000,
        step: 1000,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#down').text(ui.value);
      }
    });
  } );
  </script>
  <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider1" ).slider({
        min: 8,
      max: 26,
        step: 0.1,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#rate').text(ui.value);
        emi_calculate();
      }
    });
  } );
  </script>
  <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider2" ).slider({
        min: 10000,
      max: 1000000,
        step: 1000,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#down2').text(ui.value);
      }
    });
  } );
  </script>
   <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider4" ).slider({
        min: 12,
      max: 108,
        step: 1,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#period').text(ui.value);
        emi_calculate();
      }
    });
  } );
  </script>


    <script>
    $(function() {

        var $document = $(document);
        var selector = '[data-rangeslider]';
        var $element = $(selector);

        // For ie8 support
        var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

        // Example functionality to demonstrate a value feedback
        function valueOutput(element) {
            var value = element.value;
            var output = element.parentNode.getElementsByTagName('output')[0] || element.parentNode.parentNode.getElementsByTagName('output')[0];
            output[textContent] = value;
        }

        $document.on('input', 'input[type="range"], ' + selector, function(e) {
            valueOutput(e.target);
        });

      

        // Basic rangeslider initialization
        $element.rangeslider({

            // Deactivate the feature detection
            polyfill: false,

            // Callback function
            onInit: function() {
                valueOutput(this.$element[0]);
            },

            // Callback function
            onSlide: function(position, value) {
                console.log('onSlide');
                console.log('position: ' + position, 'value: ' + value);
            },

            // Callback function
            onSlideEnd: function(position, value) {
                console.log('onSlideEnd');
                console.log('position: ' + position, 'value: ' + value);
            }
        });

    });
    </script>
      <script>
  function emi_calculate(){
  var down_payment=0,rate=0,period=0,price=0,princ=0,term=0,intr=0;
  down_payment = down_payment+Number($('#down5').text());
  rate = Number($('#rate').text())+Number(rate);
  period = Number($('#period').text())+period;
  price = Number($('#price_val').val())+price;
  princ = Number(price)-Number(down_payment);
   term  = Number(period);
   intr   = Number(rate / 1200);
 if(rate >0 && term >0 && price>0){
   console.log("down pay: "+down_payment+" rate "+rate+" period "+period+" price "+price+" princ "+princ+"  term "+ term+" intr "+intr);
 emi = princ*intr/(1-(Math.pow(1/(1+intr),term)));
 emi_total = Number(Math.floor(emi));
 console.log("emi: "+emi+" emi_total "+emi_total);
 $('#emi_total').html('<i class="fa fa-inr" aria-hidden="true"></i> '+emi_total);
 $('#total_loan').html(princ);
 var total_emi = Number(emi_total) * Number(period);
  $('#total_emi').html(total_emi);
  }
}
  </script>
         <?php include('footer.php');?>