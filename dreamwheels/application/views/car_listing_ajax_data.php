	<?php  if(isset($query)) {
                         
      $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $car_list) {
                         
                          $car_info = json_decode($car_list->vehicle_info);
                          $car_name = $car_list->car_name;
                          $model_name =$car_list->model_name;
                          $variant_name =$car_list->variant;
                              
                  $additional_info = json_decode($car_list->additional_info);
                 ?>
						<tr>
							<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>">
                              <img src="<?php render_item_image("uploads/used_cars/".$additional_info->additional_info->img[0]);?>" alt='dodge' style="width:240px;" style="height:280px;"/>
                  </a>
								
								</div>
									<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $car_list->price;?></span>
									</header>
									<p>
                    <?php  if(isset($additional_info->additional_info->comments)){echo $additional_info->additional_info->comments; }?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $car_info->vehicle_info->kilometer_driven;?> KM
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $car_info->vehicle_info->reg_year;?></a>
											<a href="#">Used</a>
											<a href="#">Color:-<?php echo $car_info->vehicle_info->color;?> </a>
											<a href="#"><?php echo $car_info->vehicle_info->fuel;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>
						<?php  } ?>
            
                             
              <?php }else{ ?>
                          <h3><center>No more results</center></h3>
                         <?php } ?>