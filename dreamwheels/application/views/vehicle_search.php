<?php include('header.php');?>

                <?php if(isset($_GET['vehicle']) && $_GET['vehicle']=='car'){
                    include('vehicle_search/for_car.php');
                    }else if(isset($_GET['vehicle']) && $_GET['vehicle']=='bike'){ 
                        
                     include('vehicle_search/for_bike.php');   
                    }else{
                    
                    echo "Invalid Search Input :)";
                }?>

<?php include('footer.php');?>
		
<script>
	$(document).ready(function(){
			$("#targetTable").DataTable({
     "bPaginate": false,
     "bInfo": false
});
var page = 1;
var loadmore=true;
var rows=$("#ajax-data tr").length;
var waiting_queue=false;
$(window).scroll(function() {

    if($(window).scrollTop() >= $("#targetTable").height()) {
           if(loadmore && !waiting_queue)
           {
page++;
waiting_queue=true;
console.log("Page No",page);
loadMoreData(page); 

           }  
    }
});

	function loadMoreData(page){
	  $.ajax(
	        {
                    url: "?page="+page+"&<?php echo http_build_query($_GET);?>" ,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#ajax-data").append(data);
              if($("#ajax-data tr").length>rows){rows=$("#ajax-data tr").length;waiting_queue=false;}
              else{ loadmore=false; }
              $("#total_results").html($("#ajax-data tr").length);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
	});
</script>	

