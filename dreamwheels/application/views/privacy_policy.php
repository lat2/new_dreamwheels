<?php include('header.php');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.7s">Privacy Policy</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s">
					<h3>The Largest Auto Dealer Online</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/policy" class="b-breadCumbs__page m-active">Privacy Policy</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-best">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="b-best__info">
							<header class="s-lineDownLeft b-best__info-head">
								<h2 class="wow zoomInUp" data-wow-delay="0.5s">Privacy Policy</h2>
							</header>
							
                         

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Privacy is the state of being free from public disturbance as it's a private information of a person. We, at Dream wheels, value our customers and provide surety to protect your privacy. And never disclose personal information against any untrusted third party for any marketing purposes without your consent. We safeguard your data with confidentiality and integrity. You may learn here described policy which shows how we get your information and where to use those with our services by maintaining your privacy.</p>
                            
                            <h4>Information collection:</h4>
                            
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Generally, when you are browsing the site you are anonymous to us.We gather required personal contact and identity information about you from various sources. Our goal of using and collecting your information like: name, email-ids and other is different for each transaction.</p>
                            
                            <h4>Where your information will be disclosed:</h4>
                            
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> According to this, we may share your data for promotional activities and with our associated companies like: financial service providers, leasing and insurance companies, certified realtors, transaction service providers regarding payments, auto repair and data processing companies, and so on. All these disclosures even raise benefits to our customers by exploring more accessing rights, fluently operate our business services, to inform you about our new services, promotions, campaigns, etc. Besides this, we never share individual information to unofficial or unauthenticated third parties or sources. We reserve limitations and restrictions as per law, so they won't be able to access this information.</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s">Some confidential search engines as a third party vendor, uses the cookie facility to serve advertisement on our sites.</p>
                            
                       			        </div>
					</div>
<!--					<div class="col-sm-4 col-xs-12">
                                            <p><img src="<?php echo base_url();?>images/finance.jpg" width="350"></p> 
						
                                            <p class="wow zoomInUp" data-wow-delay="0.5s"><b>You can buy the car you want:</b> The first and foremost benefit is that you can buy the car that you want. Of course, you cannot go overboard as the lending limit depends on your credit score and yearly income, but you can buy anything that falls in your range without having to worry about the entire cost of the vehicle at the time. These days, it is quite easy to secure vehicle finance so you can just walk in to a bank and drive out with your brand new car.</p>
					</div>-->
				</div>
			</div>
		</section><!--b-best-->

		
		<?php include('footer.php');?>