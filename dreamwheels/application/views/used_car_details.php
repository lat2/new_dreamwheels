<?php include('header.php');?>

<?php if(isset($query)) { 
       $car_name="";
	   $model_name="";
?>
       <?php foreach ($query as $car) { ?>
        <?php $car_info = json_decode($car->vehicle_info);?>
        <?php  $user_info = json_decode($car->user_info);?>
         <?php  $additional_info = json_decode($car->additional_info);
		        $model_id = $car_info->vehicle_info->model;
                  $variant_id = $car_info->vehicle_info->varient;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
		 ?>
    
 <?php } } ;?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s"><?php echo $car_name,' ',$model_name;?></h1>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/car_listing" class="b-breadCumbs__page">Used Cars</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active"><?php echo $car_name,' ',$model_name;?></a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row wow zoomInUp" data-wow-delay="0.5s">
					<div class="col-xs-3">
						<div class="b-infoBar__premium">Used Car Details</div>
					</div>
					<div class="col-xs-9">
<!--						<div class="b-infoBar__btns">
							<a href="#" class="btn m-btn m-infoBtn">SHARE THIS VEHICLE<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">ADD TO FAVOURITES<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">PRINT THIS PAGE<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">DOWNLOAD MANUAL<span class="fa fa-angle-right"></span></a>
						</div>-->
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->
       
		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?php echo $car_name,' ',$model_name;?></h1>
								 
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="b-detail__head-price">
              <div class="b-detail__head-price-num" style="padding:2px !important ;"><?php inr($car->price); ?></div>
								
							</div>
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s">
									<div class="row m-smallPadding">
										<div class="col-xs-10">
											<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
												 <?php   foreach($additional_info->additional_info->img as $img1) { ?>
												<li class="s-relative">                                        
<!--													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"></a>-->
                           <img class="img-responsive center-block" src="<?php render_item_image("uploads/used_cars/".$img1);?>" alt="<?php echo $car_name; ?> available on dreamwheels.in"/>
												</li>
												
												<?php } ?>
												
											</ul>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical">
											<div class="b-detail__main-info-images-small" id="bx-pager">
												<?php  $i=0; foreach($additional_info->additional_info->img as $img) { ?>
												<a href="#" data-slide-index="<?php echo $i;?>" class="b-detail__main-info-images-small-one">
<img class="img-responsive center-block" src="<?php render_item_image("uploads/used_cars/".$img);?>" alt="<?php echo $car_name; ?> available on dreamwheels.in"/>
												</a>
													<?php $i++; } ?>
																							
											</div>
										</div>
									</div>
								</div>

								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#info1'>SELLER'S COMMENTS</a>
										
									</div>
									<div id="info1">
                                                                            <p><?php echo (isset($additional_info->comment))?$additional_info->comment:'';?></p>
									</div>
									
									
									
								</div>
							
							</div>
						</div>
						<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Description</h2>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Car</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $car_name;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Model</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $model_name;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Kms Driven </h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $car_info->vehicle_info->kilometer_driven;?> kmpl</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Color</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $car_info->vehicle_info->color;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Registration Year</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $car_info->vehicle_info->reg_year;?></p>
										</div>
									</div>
									
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Fuel Type</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $car_info->vehicle_info->fuel;?></p>
										</div>
									</div>
									 
								</div>
								
								<div class="b-detail__main-aside-about wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Seller Details</h2>
                  <div class="b-detail__main-aside-about-call" style="padding: 0.1px !important">
										<center><h1>Like this Car? </h1></center>
									</div>
									<div class="b-detail__main-aside-about-seller">
										<p id="para2">Confirm your details and contact seller directly.</p>
									</div>
									<div class="b-detail__main-aside-about-form">
										<div class="b-detail__main-aside-about-form-links">
											<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#form1'>General Enquiry</a>
											 
										</div>
                                                                            
										<form action="#"><div id="buyer_info">
                                                                                        <input type="hidden" placeholder="YOUR NAME" value="<?php echo $car->id;?>" id="vehicle_id" name="vehicle_id"/>
											<input type="text" placeholder="YOUR NAME" value="" name="name" id="name" required/>
											<input type="hidden" placeholder="EMAIL ADDRESS" value="noemail@dreamwheels.in" name="email" id="email2" required/>
											<input type="tel" placeholder="PHONE NO." value="" name="phone" id="phone" required/>
<!--											<textarea name="text" placeholder="message"></textarea>-->
											
											<button id="seller_details" class="btn m-btn">Get Seller Details<span class="fa fa-angle-right"></span></button>
                                                                                        </div>
                                                                                    
                                                                                    <div id="seller_info" style="display:none">
											<input type="text" placeholder="YOUR NAME" value="" id="seller_name" readonly/>
										<!--<input type="email" placeholder="EMAIL ADDRESS" value="" id="seller_email" readonly/>-->
											<input type="tel" placeholder="PHONE NO." value="" id="seller_phone" readonly/>
<!--											<textarea name="text" placeholder="message"></textarea>-->
											
                                                                                        </div>
										</form>
										 
									</div>
								</div>
								
								</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-detail-->

		<?php include('footer.php');?>
<script>
    $(document).ready(function(){
        $(document).on("click",'#seller_details',function(){
            var buyer_name=$("#name").val();
            var buyer_email=$("#email2").val();
            var buyer_phone=$("#phone").val();
            var vehicle_id = $("#vehicle_id").val();
            $.post("<?php echo base_url();?>index.php/Welcome/insert_buyer_details?user_name=<?php echo $user_info->name; ?>&user_phone=<?php echo $user_info->phone; ?>",{vehicle_id:vehicle_id,buyer_name:buyer_name,buyer_email:buyer_email,buyer_phone:buyer_phone},function(o){
                event.preventDefault();
	      console.log(o);
                 });
                 $("#seller_info").show();
                 $("#buyer_info").hide();
               $("#para1").html("SELLER DETAILS");
               $("#para2").html("You can contact the seller on the given details.");
               $("#seller_name").val("NAME:- <?php echo $user_info->name; ?>");
               $("#seller_phone").val("PHONE:- <?php echo $user_info->phone; ?>");
               event.preventDefault();
           
        });  
    });                    
</script>