<?php include('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
			<div class="wow zoomInLeft" data-wow-delay="0.5s">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Bike Listings</h1>
			</div>	
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query);?> results for bikes.</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/new_launch_bike" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
				 <!--   <div class="col-lg-9 col-sm-8 col-xs-12">-->
					<div class="col-lg-11 col-sm-10 col-xs-12">
						<div class="b-items__cars">
												<table id="targetTable">
							<thead>
								<tr><td>Bikes listings</td></tr>
							</thead>
              
              <tbody  id="ajax-data">
							<?php  if(isset($query)) { 
                          $bike_name = "";
                          $model_name = "";
                          $variant_name = "";
                foreach($query as $bike_list) {
                 
                 $bike_info = json_decode($bike_list->bike_info);
                          $bike_name = $bike_list->bike_name;
                          $model_name =$bike_list->model_name;
                          $variant_name =$bike_list->variant;
                              
                  $engine_transmission = json_decode($bike_list->engine_transmission);
                  $dimension = json_decode($bike_list->dimension);
?>
                              							  <tr>
								<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>">  <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt='dodge' style="width:240px;" style="height:280px;"/></a>
                  
                  
                  
									
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name;?> <?php echo $model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $bike_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> 
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
										
											<a >Displacement: <?php echo $engine_transmission->displacement;?> </a>
                                                                                        <a >Power: <?php echo $engine_transmission->maximum_power;?> </a>
											<a >Tank:<?php echo $dimension->fuel_capacity;?> </a>
											<a >Kerb Weight:<?php echo $dimension->kerb_weight;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
			<?php  } ?>
              </tbody>
						</table>	
                             
              <?php }else{ ?>
                          <h3><center>No result found</center></h3>
                         <?php } ?>
						</div>

		<div class="ajax-load text-center " >
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>
					</div>
					
						
						</aside>
					</div>
				</div>
			</div>
		</section><!--b-items-->

		      
		<?php include('footer.php');?>
<script>
	$(document).ready(function(){
	  $("#targetTable").DataTable();
var page = 1;
var loadmore=true;
var rows=$("#ajax-data tr").length;
var waiting_queue=false;
$(window).scroll(function() {

    if($(window).scrollTop() >= $("#targetTable").height()) {
           if(loadmore && !waiting_queue)
           {
page++;
waiting_queue=true;
console.log("Page No",page);
loadMoreData(page); 

           }  
    }
});

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#ajax-data").append(data);
              if($("#ajax-data tr").length>rows){rows=$("#ajax-data tr").length;waiting_queue=false;}
              else{ loadmore=false; }
              $("#total_results").html($("#ajax-data tr").length);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
	});
</script>	
