<?php include('include/header.php');?>
  <aside class="main-sidebar">
<?php include('include/sidebar.php');?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bike List
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Bike</a></li>
        <li class="active"> Bike List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Added Bikes</h3>
            </div>
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Deleted Successfully..!!
              </div>
              <?php }else if(isset($_GET['msg2'])){ ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Updated Successfully..!!
              </div>
              <?php } ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th> 
                  <th>Bike</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php if(isset($query)) {
                      $i=1;?>
                  <?php foreach ($query as $bikes) {
                         $bike_id=$bikes['bike_id'];
			             $model_id=$bikes['model_id'];
			             $variant_id=$bikes['variant_id'];
						 foreach($query_model as $model_name){
						    if($model_id==$model_name->id){
							        $bike_name = $model_name->bike_name;
                                           //var_dump($model_name->model_name);
                                    $model = $model_name->model_name;
							}
							
						 }
						  foreach($query_variant as $variant_name){
						    
							if($variant_id==$variant_name->id){
							   $variant=$variant_name->variant;
							}
							
						 }
                                                 if($bikes['status']==1){
                             $status='Enabled';
                             $status_btn = "label label-success";
                         }else{
                             $status='Disabled';
                             $status_btn = "label label-danger";
                         }
				  ?>
                  <?php $json_bike = json_decode($bikes['bike_info']);?>
                <tr>  
                  <td><?php echo $i++;?></td>   
                  <td><?php echo $bike_name;?>&nbsp;<?php echo $model;?> - <?php echo $variant;?></td>
                  <td><?php echo ucwords($json_bike->bike_type);?></td>
                  <td><?php echo $json_bike->price;?></td>
                  <td><span class="label label-success"><?php echo ucwords($json_bike->category);?></span></td>
                  <td><span class="<?php echo $status_btn;?>"><?php echo $status;?></span></td>
                  <td><a href="#" id="enable" data-id="<?php echo $bikes['id'];?>">Enable</a> | <a href="#" data-id="<?php echo $bikes['id'];?>"  id="disable">Disable</a> | <a href="edit_bike?id=<?php echo $bikes['id'];?>"><i class="fa fa-fw fa-edit"></i></a> | <a href="clone_bike?id=<?php echo $bikes['id'];?>"><i class="fa fa-fw fa-copy"></i></a> | <a href="#" id="del_btn" bike_id="<?php echo $bikes['id'];?>" data-toggle="modal" data-target="#modal-default"><i class="fa fa-fw fa-remove"></i></a></td>
                </tr>
                <?php } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Bike</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? Want to delete this</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="delete_bike?id=<?php echo $bikes['id'];?>" id="del_bike"><button type="button" class="btn btn-primary">Yes</button></a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- jQuery 3 -->
<?php include('include/footer.php');?>

  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
  
   $(document).on("click","#enable",function(){
      var status = 1;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_bike",{status:status,id:id},function(){
          location.reload();
      });
  });
  $(document).on("click","#disable",function(){
      var status = 0;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_bike",{status:status,id:id},function(){
          location.reload();
      });
  });
  
  $(document).on("click","#del_btn",function(){
		var id = $(this).attr("bike_id");
		$('#del_bike').attr("href","delete_bike?id="+id);
  });
</script>