<?php include('include/header.php');?>
  <aside class="main-sidebar">
<?php include('include/sidebar.php');?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Car List
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Car</a></li>
        <li class="active"> Car List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Added Cars</h3>
            </div>
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $_GET['msg']; ?>
              </div>
              <?php }else if(isset($_GET['msg2'])) { ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Updated Successfully..!!
              </div>
              <?php } ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th> 
                  <th>Car</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Category</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php if(isset($query)) { 
                      $i=1;$car_name = "";
                          $model_name = "";
                          $variant_name = "";?>
                  <?php foreach ($query as $cars) { ?>
                  <?php $json_car = json_decode($cars['car_info']);
                  $model_id = $json_car->model;
                 @ $variant_id = $json_car->variant;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
                  foreach ($query_variant as $variant) {
                  
                      if($variant->id==$variant_id){
                          $variant_name = $variant->variant;
                       }                      
                  }
                        if($cars['status']==1){
                             $status='Enabled';
                             $status_btn = "label label-success";
                         }else{
                             $status='Disabled';
                             $status_btn = "label label-danger";
                         }
                  ?>
                <tr>
                  <td><?php echo $i++;?></td>  
                  <td><?php echo $car_name;?>&nbsp;<?php echo $model_name;?> - <?php echo $variant_name;?></td>
                  <td><?php echo ucwords($json_car->car_type);?></td>
                  <td><?php echo $json_car->price;?></td>
                  <td><span class="label label-success"><?php echo ucwords($json_car->category);?></span></td>
                  <td><span class="<?php echo $status_btn;?>"><?php echo $status;?></span></td>
                  <td><a href="#" id="enable" data-id="<?php echo $cars['id'];?>">Enable</a> | <a href="#" data-id="<?php echo $cars['id'];?>"  id="disable">Disable</a> | <a href="edit_car?id=<?php echo $cars['id'];?>"><i class="fa fa-fw fa-edit"></i></a> | <a href="#" data-toggle="modal" data-target="#modal-default"><i class="fa fa-fw fa-remove"></i></a> | <a href="clone_car?id=<?php echo $cars['id'];?>"><i class="fa fa-fw fa-copy"></i></a></td>
                </tr>
                <?php } } ?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? Want to delete this</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="delete_car?id=<?php echo $cars['id'];?>"><button type="button" class="btn btn-primary">Yes</button></a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- jQuery 3 -->
<?php include('include/footer.php');?>

  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
  
    $(document).on("click","#enable",function(){
      var status = 1;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_car",{status:status,id:id},function(){
          location.reload();
      });
  });
  $(document).on("click","#disable",function(){
      var status = 0;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_car",{status:status,id:id},function(){
          location.reload();
      });
  });
</script>