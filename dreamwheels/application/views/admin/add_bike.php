<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your bike has been successfully added.
  </div>
        <?php  } ?>
            <form name="form" action="<?php echo base_url();?>index.php/Admin/add_new_bike" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Enter Bike Name</label>
                 
                
                   <?php if(isset($query_name)) { ?>

                    <select class="form-control select2"  name="bike_id" style="width: 100%;" id="select_bike">
              <option value="">Select bike</option>
													<?php foreach ($query_name as $bikes_name) { ?>
														<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
												<?php	} ?>
                </select>
                <?php  } ?>
                </div>
                <div class="col-md-3" id="show_model">
                  <label for="exampleInputPassword1">Model</label>
                  <select class="form-control select2"  name="model_id" style="width: 100%;" id="model">
                  <option value="">Select </option>
                  
                 
                </select>
                </div>
                 <div class="col-md-3" id="show_model">
                 <label for="exampleInputEmail1"> Variant</label>
                 <select class="form-control select2" name="variant_id" style="width: 100%!important;" id="select_variant">
                  <option value="">Select </option>
                  </select>
                 </div>
                  <div class="col-md-3" id="car_type">
                 <label for="exampleInputEmail1"> Bike Type</label>
                 <select class="form-control select2" name="bike_info[bike_type]" style="width: 100%!important;" id="car_type"> 
                  <option value="">Select </option>
                  <option value="bikes">Bikes</option>
                  <option value="scooter">Scooter </option>
                  <option value="tourer">Tourer </option>
                  <option value="sport">Sports </option>
                  <option value="cruiser">Cruiser </option>
                  <option value="cafe racer">Cafe Racer </option>
              </select>
                 </div>
              </div>
              <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" name="bike_info[price]" class="form-control" id="price" placeholder="Enter Ex Showroom Price" required>
                </div>
              
              </div>
                  
               <div style="clear: both;">
                 <div class="panel-body" style="font-size: 18px;">Engine & Transmission</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Type</label>
                    <input class="form-control"  name="engine_transmission[engine_type]" style="width: 100%;" placeholder="e.g.,Air Cooled, 4 Stroke">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Displacement</label>
                    <input class="form-control"  name="engine_transmission[displacement]" style="width: 100%;" placeholder="In cc">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Mileage</label>
                    <input class="form-control"  name="engine_transmission[mileage]" style="width: 100%;" placeholder="kmpl">
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Power</label>
                    <input class="form-control"  name="engine_transmission[maximum_power]" style="width: 100%;" placeholder="e.g.,10 Ps @ 400 rpm">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Torque</label>
                    <input class="form-control"  name="engine_transmission[maximum_torque]" style="width: 100%;" placeholder="e.g.,10.3 nm @ 5500 rpm">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Starting</label>
                    <input class="form-control"  name="engine_transmission[starting]" style="width: 100%;" placeholder="Kick or Self start">
                </div>
                   
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Speed</label>
                    <input class="form-control"  name="engine_transmission[maximum_speed]" style="width: 100%;" placeholder="In kmph">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Cooling System</label>
                    <input class="form-control"  name="engine_transmission[cooling_system]" style="width: 100%;" placeholder="e.g.,air cooled">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Box</label>
                    <input class="form-control"  name="engine_transmission[gear_box]" style="width: 100%;" placeholder="e.g.,4 speed or CVT">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clutch</label>
                    <input class="form-control"  name="engine_transmission[clutch]" style="width: 100%;" placeholder="e.g.,wet, multi plate">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">No of Cylinders</label>
                    <select class="form-control"  name="engine_transmission[no_of_cylinder]" style="width: 100%;">
                      <?php for ($i=1; $i < 6; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Drive Type</label>
                    <input class="form-control"  name="engine_transmission[drive_type]" style="width: 100%;" placeholder="e.g., chain, belt or shaft">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Valves Per Cylinder</label>
                     <select class="form-control"  name="engine_transmission[valves_per_cylinder]" style="width: 100%;">
                      <?php for ($i=1; $i < 6; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Bore</label>
                    <input class="form-control"  name="engine_transmission[bore]" style="width: 100%;" placeholder="e.g., 52.4 mm">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Stroke</label>
                    <input class="form-control"  name="engine_transmission[stroke]" style="width: 100%;" placeholder="e.g., 57.8 mm">
                   
                </div>
                   
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Supply System</label>
                    <input class="form-control"  name="engine_transmission[supply_system]" style="width: 100%;" placeholder="e.g., Carbuerator, FI">
                   
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Transmission Type</label>
                    <input class="form-control"  name="engine_transmission[transmission_type]" style="width: 100%;" placeholder="e.g., Manual">
                   
                </div>   
               
              </div>
                 
                 <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Tyres & Brakes</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size Front</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size_front]" style="width: 100%;" placeholder="e.g., 80/100-18">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size Rear</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size_rear]" style="width: 100%;" placeholder="e.g., 80/100-18">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Type</label>
                    <input class="form-control"  name="tyres_brakes[tyre_type]" style="width: 100%;" placeholder="e.g., tubeless">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Radial tyres</label>
                    <select class="form-control"  name="tyres_brakes[radial_tyres]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="No">No</option>
                </select>
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size Front</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size_front]" style="width: 100%;" placeholder="Enter the values in inches">
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size Rear</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size_rear]" style="width: 100%;" placeholder="Enter the values in inches">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake</label>
                     <select class="form-control"  name="tyres_brakes[front_break]" style="width: 100%;">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake</label>
                     <select class="form-control"  name="tyres_brakes[rear_break]" style="width: 100%;">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake Diameter</label>
                    <input class="form-control"  name="tyres_brakes[front_brake_diameter]" style="width: 100%;" placeholder="Enter the values in mm">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake Diameter</label>
                    <input class="form-control"  name="tyres_brakes[rear_brake_diameter]" style="width: 100%;" placeholder="Enter the values in mm">
                </div>   
                     
               
              </div>
               </div>     
                 
            <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Features & Safety</div>
  
               <div class="form-group">
                   

               <div class="col-md-3">
                  <label for="exampleInputPassword1">Speedometer</label>
                     <select class="form-control"  name="feature_safety[speedometer]" style="width: 100%;">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div>
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Odometer</label>
                     <select class="form-control"  name="feature_safety[odometer]" style="width: 100%;">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tachometer</label>
                     <select class="form-control"  name="feature_safety[tachometer]" style="width: 100%;">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tripmeter</label>
                     <select class="form-control"  name="feature_safety[tripmeter]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">ABS</label>
                    <select class="form-control"  name="feature_safety[abs]" style="width: 100%;">
                   <option value="no">No</option>
				   <option value="yes">Yes</option>
                    
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">CBS</label>
                    <select class="form-control"  name="feature_safety[cbs]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">EBS</label>
                    <select class="form-control"  name="feature_safety[ebs]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Hook</label>
                    <select class="form-control"  name="feature_safety[seat_hook]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                    
                </select>
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Type</label>
                    <input class="form-control"  name="feature_safety[seat_type]" style="width: 100%;" placeholder="e.g., single or split">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Console</label>
                    <select class="form-control"  name="feature_safety[console]" style="width: 100%;">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clock</label>
                    <select class="form-control"  name="feature_safety[clock]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                    
                </select>
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">UnderSeat Storage</label>
                    <select class="form-control"  name="feature_safety[underseat_storage]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                    
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Traction Control</label>
                     <select class="form-control"  name="feature_safety[traction_control]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div> 
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Cruise Control</label>
                     <select class="form-control"  name="feature_safety[cruise_control]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Navigation</label>
                     <select class="form-control"  name="feature_safety[navigation]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Quick Shifter</label>
                     <select class="form-control"  name="feature_safety[quick_shifter]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>   
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Launch Control</label>
                     <select class="form-control"  name="feature_safety[launch_control]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>   
                 
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Modes</label>
                     <select class="form-control"  name="feature_safety[power_modes]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Wind Screen</label>
                     <select class="form-control"  name="feature_safety[adjustable_wind_screen]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Mobile Connectivity</label>
                     <select class="form-control"  name="feature_safety[mobile_connectivity]" style="width: 100%;">
                    <option value="no">No</option>
                   <option value="yes">Yes</option>
                </select>
                </div>   

              </div>
               </div>
                 
                 <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Chasis & Dimension</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Chasis/Frame</label>
                     <input class="form-control"  name="chasis_suspension[chasis_frame]" style="width: 100%;" placeholder="e.g., diamond">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Suspension</label>
                    <input class="form-control"  name="chasis_suspension[front_suspension]" style="width: 100%;" placeholder="">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Suspension</label>
                    <input class="form-control"  name="chasis_suspension[rear_suspension]" style="width: 100%;" placeholder="">
                </div> 
                     </div>
                 </div>
                 
                 <div style="clear:both">
                    <div class="panel-body" style="font-size: 18px;">Dimension</div> 
                     
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Length</label>
                     <input class="form-control"  name="dimension[length]" style="width: 100%;" placeholder="Enter values in mm">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Height</label>
                    <input class="form-control"  name="dimension[height]" style="width: 100%;" placeholder="Enter values in mm">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Width</label>
                    <input class="form-control"  name="dimension[width]" style="width: 100%;" placeholder="Enter values in mm">
                </div> 
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ground Clearance</label>
                    <input class="form-control"  name="dimension[ground_clearance]" style="width: 100%;" placeholder="Enter values in mm">
                </div>
                        
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Saddle Height</label>
                    <input class="form-control"  name="dimension[saddle_height]" style="width: 100%;" placeholder="Enter values in mm">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Kerb Weight</label>
                    <input class="form-control"  name="dimension[kerb_weight]" style="width: 100%;" placeholder="Enter values in kg">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Base</label>
                    <input class="form-control"  name="dimension[wheel_base]" style="width: 100%;" placeholder="Enter values in mm">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Capacity</label>
                    <input class="form-control"  name="dimension[fuel_capacity]" style="width: 100%;" placeholder="Enter values in liters">
                </div>         
                     </div>
                 </div>
                 
                <div style="clear:both">
                    <div class="panel-body" style="font-size: 18px;">Electricals</div> 
                     
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Head Lamp</label>
                     <input class="form-control"  name="electricals[head_lamp]" style="width: 100%;" placeholder="">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tail Lamp</label>
                    <input class="form-control"  name="electricals[tail_lamp]" style="width: 100%;" placeholder="">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Turn Signal Lamp</label>
                    <input class="form-control"  name="electricals[turn_signal_lamp]" style="width: 100%;" placeholder="Enter values in mm">
                </div> 
                                          
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Pass Switch</label>
                    <select class="form-control"  name="electricals[pass_switch]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Projector HeadLight</label>
                    <select class="form-control"  name="electricals[projector_headlight]" style="width: 100%;">
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    
                    </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Battery Type</label>
                    <input class="form-control"  name="electricals[battery_type]" style="width: 100%;" placeholder="e.g, maintenance Free">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Battery Capacity</label>
                    <input class="form-control"  name="electricals[battery_capacity]" style="width: 100%;" placeholder="">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Low Fuel Indicator</label>
                    <select class="form-control"  name="electricals[low_fuel_indicator]" style="width: 100%;">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    
                    </select>
                </div>         
                        
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Pilot Lamps</label>
                    <select class="form-control"  name="electricals[pilot_lamps]" style="width: 100%;">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    
                    </select>
                </div>         
                         
                     </div>
                 </div> 
                 
                <div style="clear: both;">
               <div class="panel-body" style="font-size: 18px;">Bike Photos</div>
              <div class="form-group">
                 <div class="col-md-3">
               <div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span>Choose file</span>
	<input type="file" id="fileupload" name="bike_info[images][]" multiple/></span>
    <span class="fileinput-filename"></span><span class="fileinput-new">No file chosen</span>
</div>
</div>
 <div class="col-md-9">
<div id="dvPreview">
        </div>
              </div>

                  </div>
                  <div style="clear: both;"></div>
<div class="panel-body" style="font-size: 18px;">Bike Category</div>
                     <div class="form-group">
                 <div class="col-md-6">
                     
                   <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="latest" class="minimal" id="cat">&nbsp;Latest
    </label>  
                   <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="featured" class="minimal" id="cat" required>&nbsp;Featured

    </label>
    <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="upcoming" class="minimal" id="cat">&nbsp;Upcoming
    </label>
    <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="recommended" class="minimal" id="cat">&nbsp;Recommended
    </label>
                 </div>
                 <div class="col-md-3" id="upcoming-date" style="display: none">
                
                  <input type="text" class="form-control pull-right" name="bike_info[upcoming_date]"  id="datepicker" placeholder="Upcoming Date">
                 </div>
               </div>

                 <div style="clear: both;"></div>
                 <div class="panel-body" style="font-size: 18px;">Enter Details</div>
                <div class="form-group">
                 <textarea id="editor1" name="bike_info[details]" rows="10" cols="80">
                                            This is my textarea for car details.
                    </textarea>
                </div>
               </div>
              
              <!-- /.box-body -->
               <div style="clear:both;">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
            
          </div>
         
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
            </form>
      </div>
      <!-- /.row -->
      </div>
          </div>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <?php include('include/footer.php');?>

    <script type="text/javascript">
        $(document).ready(function(){
  
  $(document).on("change","#select_bike",function(){
    var bike_id = $(this).val();
	$.post("<?php echo base_url();?>index.php/Admin/fetch_bike_model",{bike_id:bike_id},function(o){
          // console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
    
    $(document).on("change","#model",function(){
    var model = $('#model option:selected').val();
   //alert(model);
     $.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_variant",{model_id:model},function(o){
          // console.log(o);
           $("#variant").html(o);
           //$("#model").hide();    
           });
      });
    });  
   
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>
      <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var bike = $('#select_bike option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/fetch_bike_variant",{bike_name:bike,model_id:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 