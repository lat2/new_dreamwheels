<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your car has been successfully added.
  </div>
        <?php  } ?>
            <form name="form" action="<?php echo base_url();?>index.php/Admin/add_new_car" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Enter Car Name</label>
                 
                
                   <?php if(isset($query)) { ?>
                   <?php $count = count($query);?>

                    <select class="form-control select2"  name="car_info[car]" style="width: 100%;" id="select_car">
                  <option value="">Select </option>
                
                 
                <?php foreach ($query as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
                </div>
                <div class="col-md-3" id="show_model">
                  <label for="exampleInputPassword1">Model</label>
                  <select class="form-control select2"  name="car_info[model]" style="width: 100%;" id="model">
                  <option value="">Select </option>
                  
                 
                </select>
                </div>
                 <div class="col-md-3" id="show_model">
                 <label for="exampleInputEmail1"> Variant</label>
                 <select class="form-control select2" name="car_info[variant]" style="width: 100%!important;" id="select_variant"></select>
                 </div>
                  <div class="col-md-3" id="car_type">
                 <label for="exampleInputEmail1"> Car Type</label>
                 <select class="form-control select2" name="car_info[car_type]" style="width: 100%!important;" id="car_type"> 
                  <option value="">Select </option>
                  
                  <option value="coupe">Coupe </option>
                  <option value="convertible">Convertible </option>
                  <option value="hatchback">Hatchback </option>
                  <option value="suv">Suv </option>
                  <option value="pickup">Pickup </option>
                   <option value="sedan">Sedan </option>
                    <option value="minicar">Minicar </option>
              </select>
                 </div>
                         
              </div>
                <div class="form-group">    
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" name="car_info[price]" class="form-control" id="price" placeholder="Enter On Road Price">
                </div>
				<div class="col-md-3">
                  <label for="exampleInputEmail1">Search Price</label>
                  <input type="text" name="price" class="form-control" id="price">
                </div>
                </div>     
                  
                  <div style="clear:both;">
                  
                   <div class="panel-body" style="font-size: 18px;">Engine & Transmission</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Type</label>
                    <input class="form-control"  name="engine_transmission[engine_type]" style="width: 100%;" placeholder="e.g.,Air Cooled, 4 Stroke">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Displacement</label>
				  <input class="form-control"  name="engine_transmission[displacement]" style="width: 100%;" placeholder="e.g.,Air Cooled, 4 Stroke">
                    
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Type</label>
					<select class="form-control"  name="engine_transmission[fuel_type]" style="width: 100%;">
                  <option value="cng">CNG</option>
                  <option value="diesel">Diesel</option>
                  <option value="petrol">Petrol</option>
                </select>
                </div>   
                                  
                <div class="col-md-3">
                    <label for="exampleInputPassword1">Power</label>
                    <input class="form-control"  name="engine_transmission[maximum_power]" style="width: 100%;" placeholder="e.g.,10 Ps @ 400 rpm">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Torque</label>
                    <input class="form-control"  name="engine_transmission[maximum_torque]" style="width: 100%;" placeholder="e.g.,10.3 nm @ 5500 rpm">
                </div>  
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">No of Cylinders</label>
                    <select class="form-control"  name="engine_transmission[no_of_cylinder]" style="width: 100%;">
                      <?php for ($i=1; $i <=12; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Transmission</label>
                    <input class="form-control"  name="engine_transmission[transmission]" style="width: 100%;" placeholder="e.g.,manual, automatic..">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Box</label>
                    <select class="form-control"  name="engine_transmission[gear_box]" style="width: 100%;">
                    <option value="" selected="">No of gears</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select>    
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Drive Type</label>
                    <input class="form-control"  name="engine_transmission[drive_type]" style="width: 100%;" placeholder="e.g., FWD, AWD">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Paddle Shift</label>
                    <select class="form-control"  name="engine_transmission[paddle_shift]" style="width: 100%;">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                    </select>    
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Kerb Weight</label>
                    <input class="form-control"  name="engine_transmission[kerb_weight]" style="width: 100%;" placeholder="kg">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Mileage (ARAI) kmpl</label>
                    <input class="form-control"  name="engine_transmission[mileage]" style="width: 100%;" placeholder="In kmpl">
                </div>
              </div>
                </div> 
                 <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Brakes, Steering, Suspension and Tyres</div>
  
               <div class="form-group">
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Suspension Front</label>
                    <input class="form-control"  name="tyres_brakes[suspension_front]" style="width: 100%;" placeholder="e.g., Front">
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Suspension Rear</label>
                    <input class="form-control"  name="tyres_brakes[suspension_rear]" style="width: 100%;" placeholder="e.g., Rear">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake</label>
                     <select class="form-control"  name="tyres_brakes[front_break]" style="width: 100%;">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake</label>
                     <select class="form-control"  name="tyres_brakes[rear_break]" style="width: 100%;">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Streering Type</label>
                    <input class="form-control"  name="tyres_brakes[steering_type]" style="width: 100%;" placeholder="e.g., Rear">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Minimum Turning Radius</label>
                    <input class="form-control"  name="tyres_brakes[turning_radius]" style="width: 100%;" placeholder="e.g., turning radius">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size]" style="width: 100%;" placeholder="e.g., size">
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size]" style="width: 100%;" placeholder="e.g., size">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Type</label>
                    <input class="form-control"  name="tyres_brakes[tyre_type]" style="width: 100%;" placeholder="e.g., tubeless">
                </div>
              </div>
               </div>     
               
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Interior Dimensions</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seating Capacity</label>
                     <select class="form-control"  name="dimensions[seating_capacity]" style="width: 100%;">
                    <option value="" selected="">Select seating capacity</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select> 
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Doors</label>
                    <select class="form-control"  name="dimensions[doors]" style="width: 100%;">
                    <option value="" selected="">Select doors</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Boot Space</label>
                    <input class="form-control"  name="dimensions[boot_space]" style="width: 100%;" placeholder="in liters">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Capacity</label>
                    <input class="form-control"  name="dimensions[fuel_capacity]" style="width: 100%;" placeholder="In liters">
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Exterior Dimensions</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Length Width Height</label>
                    <input class="form-control"  name="dimensions[lwh]" style="width: 100%;" placeholder="Length/Width/Height">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheelbase</label>
                    <input class="form-control"  name="dimensions[wheelbase]" style="width: 100%;" placeholder="in mm">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ground Clearance</label>
                    <input class="form-control"  name="dimensions[ground_clearance]" style="width: 100%;" placeholder="In mm">
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Interior Appearance/Features</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ventilated Seats</label>
                    <select class="form-control"  name="appearance[ventilated_seats]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Dual Tone Dashboard</label>
                    <select class="form-control"  name="appearance[dual_tone_dashboard]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Leather Seats</label>
                    <select class="form-control"  name="appearance[leather_seats]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Fabric Upholstery</label>
                    <select class="form-control"  name="appearance[fabric_upholstery]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Seat Headrest</label>
                    <select class="form-control"  name="appearance[rear_seat_headrest]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Additional Interior Features</label>
                    <textarea class="form-control"  name="appearance[additional_interior_features]" style="width: 100%;">
                    
                </textarea>
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Exterior Appearance/Features</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Trunk Opener</label>
                    <input class="form-control"  name="appearance[trunk_opener]" style="width: 100%;">
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Alloy Wheels</label>
                    <select class="form-control"  name="appearance[alloy_wheels]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Outside Rear View Mirror</label>
                    <select class="form-control"  name="appearance[outside_rear_view_mirror]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Turn indicators on ORVM</label>
                    <select class="form-control"  name="appearance[turn_indicators]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Xenon Headlights</label>
                    <select class="form-control"  name="appearance[xenon_lights]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Spoiler</label>
                    <select class="form-control"  name="appearance[rear_spoiler]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Roof Rails</label>
                    <select class="form-control"  name="appearance[roof_rails]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Trunk Light</label>
                    <select class="form-control"  name="appearance[trunk_light]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Antenna</label>
                    <select class="form-control"  name="appearance[antenna]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Additional Exterior Features</label>
                    <textarea class="form-control"  name="appearance[additional_exterior_features]" style="width: 100%;">
                    
                </textarea>
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Comfort and Convenience</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Shift Indicator</label>
                    <select class="form-control"  name="comfort[gear_shift_indicator]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Steering Mounted Tripmeter</label>
                    <select class="form-control"  name="comfort[steering_mounted_tripmeter]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Luggage Hook and Net</label>
                    <select class="form-control"  name="comfort[luggage_hook]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">In Car Entertainment (Audio)</label>
                    <select class="form-control"  name="comfort[in_car_entertainment]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Tailgate Ajar</label>
                    <select class="form-control"  name="comfort[tailgate_ajar]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Lane Change Indicator</label>
                    <select class="form-control"  name="comfort[lane_change_indicator]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">USB Charger</label>
                    <select class="form-control"  name="comfort[usb_charger]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Curtain</label>
                    <select class="form-control"  name="comfort[rear_curtain]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Central Control Armrest</label>
                    <select class="form-control"  name="comfort[central_control_armrest]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">AC</label>
                    <select class="form-control"  name="comfort[ac]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear AC Ducts</label>
                    <select class="form-control"  name="comfort[rear_ac_ducts]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Heater</label>
                    <select class="form-control"  name="comfort[heater]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Steering</label>
                    <select class="form-control"  name="comfort[power_steering]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Steering Mounted Audio Control</label>
                    <select class="form-control"  name="comfort[steering_audio_control]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Cruise Control</label>
                    <select class="form-control"  name="comfort[cruise_control]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Windows ( Front / Rear )</label>
                  <input class="form-control"  name="comfort[power_windows]" style="width: 100%;">
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Driver Seat</label>
                    <select class="form-control"  name="comfort[adjustable_driver_seat]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Heated Seats Rear</label>
                    <select class="form-control"  name="comfort[heated_seat_rear]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Heated Seat Front</label>
                    <select class="form-control"  name="comfort[heated_seat_front]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Front Passenger Seat</label>
                    <select class="form-control"  name="comfort[ajustable_front_passenger_seat]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Lumbar Support</label>
                    <select class="form-control"  name="comfort[lumbar_support]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Folding Rear Seat</label>
                  <input class="form-control"  name="comfort[folding_rear_seat]" style="width: 100%;">
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Seat Center Armrest</label>
                    <select class="form-control"  name="comfort[rearseat_armrest]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Tachometer</label>
                    <select class="form-control"  name="comfort[tachometer]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Door & Trunk Ajar Warning</label>
                    <select class="form-control"  name="comfort[door_trunk_ajar_warning]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Low Fuel Warning Lamp</label>
                    <select class="form-control"  name="comfort[low_fuel_warning_lamp]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Belt Warning</label>
                    <select class="form-control"  name="comfort[seat_belt_warning]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clock</label>
                    <select class="form-control"  name="comfort[clock]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Push Ignition</label>
                    <select class="form-control"  name="comfort[push_ignition]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">AM/FM Radio</label>
                    <select class="form-control"  name="comfort[radio]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">USB Auxiliary Input</label>
                    <select class="form-control"  name="comfort[aux]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">DVD Playback</label>
                    <select class="form-control"  name="comfort[dvd_playback]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Touch-screen Display</label>
                    <select class="form-control"  name="comfort[touch_screen_display]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">GPS Navigation System</label>
                  <input class="form-control"  name="comfort[gps_navigation_system]" style="width: 100%;">
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Speakers Rear</label>
                    <select class="form-control"  name="comfort[speakers_rear]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Speakers Front</label>
                    <select class="form-control"  name="comfort[speakers_front]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                      
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Auto Rain Sensing Wipers</label>
                    <select class="form-control"  name="comfort[auto_rain_sensing]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Follow me home headlamps</label>
                    <select class="form-control"  name="comfort[follow_me_home_headlamps]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Head Lights</label>
                    <select class="form-control"  name="comfort[adjustable_headlights]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Parking Sensors</label>
                    <select class="form-control"  name="comfort[parking_sensors]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Remote Operated Fuel Tank Lid</label>
                    <select class="form-control"  name="comfort[remote_fuel_lid]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Cupholders</label>
                    <select class="form-control"  name="comfort[front_cupholders]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">12V Power Outlets</label>
                    <select class="form-control"  name="comfort[power_outlets]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Cigarette Lighter</label>
                    <select class="form-control"  name="comfort[cigarette_lights]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Bottle Holder</label>
                    <select class="form-control"  name="comfort[bottle_holder]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Moon Roof</label>
                    <select class="form-control"  name="comfort[moon_roof]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Sun Roof</label>
                  <input class="form-control"  name="comfort[sun_roof]" style="width: 100%;">
                </div>
                                        
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Addtional Comfort Features</label>
                    <textarea class="form-control"  name="comfort[additional_comfort_features]" style="width: 100%;">
                    
                </textarea>
                </div>         
                     </div>
                 </div>   
                  
            <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Safety & Security</div>
  
               <div class="form-group">
                
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Airbags</label>
                     <input class="form-control"  name="safety_security[airbags]" style="width: 100%;">
                   
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Belts</label>
                    <select class="form-control"  name="safety_security[seat_belts]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Anti-lock Braking System (ABS)</label>
                    <select class="form-control"  name="safety_security[abs]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Electronic Brakeforce Distribution (EBD)</label>
                    <select class="form-control"  name="safety_security[ebd]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Brake Assist (BA)</label>
                    <select class="form-control"  name="safety_security[ba]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>   
                   
            
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Electronic Stability Program (ESP)</label>
                    <select class="form-control"  name="safety_security[esp]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Traction Control System (TCS)</label>
                    <select class="form-control"  name="safety_security[tcs]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Immobilizer</label>
                     <select class="form-control"  name="safety_security[engine_immobilizer]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div> 
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Child Safety Lock</label>
                     <select class="form-control"  name="safety_security[child_safety_lock]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fog Lamps</label>
                     <select class="form-control"  name="safety_security[fog_lamps]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Wash Wiper</label>
                        <select class="form-control"  name="safety_security[rear_wash_wiper]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Defogger</label>
                     <select class="form-control"  name="safety_security[rear_defogger]" style="width: 100%;">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   

              </div>
               </div>
             <div style="clear: both;">
               <div class="panel-body" style="font-size: 18px;">Car Photos</div>
              <div class="form-group">
                 <div class="col-md-3">
               <div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span>Choose file</span><input type="file" id="fileupload" name="car_info[images][]" multiple/></span>
    <span class="fileinput-filename"></span><span class="fileinput-new">No file chosen</span>
</div>
</div>
 <div class="col-md-9">
<div id="dvPreview">
        </div>
              </div>

                  </div>
                  <div style="clear: both;"></div>
<div class="panel-body" style="font-size: 18px;">Car Category</div>
                     <div class="form-group">
                 <div class="col-md-6">
                     <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="latest" class="minimal" id="cat">&nbsp;Latest
    </label> 
                     
                   <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="featured" class="minimal" id="cat" required>&nbsp;Featured

    </label>
    <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="upcoming" class="minimal" id="cat">&nbsp;Upcoming
    </label>
    <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="recommended" class="minimal" id="cat">&nbsp;Recommended
    </label>
                 </div>
                 <div class="col-md-3" id="upcoming-date" style="display: none">
                
                  <input type="text" class="form-control pull-right" name="car_info[upcoming_date]"  id="datepicker" placeholder="Upcoming Date">
                 </div>
               </div>

                 <div style="clear: both;"></div>
                 <div class="panel-body" style="font-size: 18px;">Enter Details</div>
                <div class="form-group">
                 <textarea id="editor1" name="car_info[details]" rows="10" cols="80">
                                            Enter Car details.
                    </textarea>
                </div>
               </div>
              
               /.box-body 
               <div style="clear:both;">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
           
               </div>
               </div>
          </form>
          </div> 
           </div>        
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('include/footer.php');?>

    <script type="text/javascript">
  $(document).on("change","#select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_car_model",{car_id:car_id},function(o){
           console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>
      <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{model_name:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 