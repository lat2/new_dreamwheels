<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your bike has been successfully added.
  </div>
        <?php  } ?>
            <form id="form_action" name="form" action="" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                <div class="form-group">
			       <div class="col-md-4"></div>
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Select Vehicle Type</label>
                  <select class="form-control select2" name="bike_info[bike_type]" style="width: 100%!important;" id="type" required>
                  <option value="">Select Type </option>				  
                  <option value="car">Car </option>
                  <option value="bike">Bike</option>
              </select>
                </div>
              
              </div>
			  
                <div style="clear: both;"></div>
              <div class="form-group">
			       <div class="col-md-4"></div>
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Select Make</label>
				  <select class="form-control select2" name="" style="width: 100%!important;" id="select_make" required>
                  <option value="">Select Make</option>
              </select>
                </div>
              
              </div>
			  <div style="clear: both;"></div>
              <div class="form-group">
			       <div class="col-md-4"></div>
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Model Name</label>
                  <input type="text" name="model_name" class="form-control" id="model_name" placeholder="Enter Name" required>
                </div>
              
              </div>
			  <input type="hidden" name="" class="form-control" id="vehicle_name">
			  
			  

               
              <!-- /.box-body -->
              <div style="clear: both;"></div>
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
            
          </div>
         
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
            </form>
      </div>
      <!-- /.row -->
      </div>
          </div>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <?php include('include/footer.php');?>

    <script type="text/javascript">
        $(document).ready(function(){
        $(document).on("change","#type",function(){
			var type = $(this).val();
			if(type=='car'){
				$("#select_make").attr("name","car_id");
				$("#vehicle_name").attr("name","car_name");
				$("#form_action").attr("action","<?php echo base_url();?>index.php/Admin/add_car_model");
				$.post("<?php echo base_url();?>index.php/Admin/select_car_name",'',function(o){
					$("#select_make").html(o);
				});
			}else if(type=='bike'){
				$("#select_make").attr("name","bike_id");
				$("#vehicle_name").attr("name","bike_name");
				$("#form_action").attr("action","<?php echo base_url();?>index.php/Admin/add_bike_model");
				$.post("<?php echo base_url();?>index.php/Admin/select_bike_name",'',function(o){
					$("#select_make").html(o);
					console.log(o);
				});
			}
		});
		
		$(document).on("change","#select_make",function(){
			var vehicle_name = $(this).find('option:selected').text();
			$("#vehicle_name").val(vehicle_name);
		});
    });  
   
  </script>
  
