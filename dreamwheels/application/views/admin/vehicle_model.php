<?php include('include/header.php');?>
  <aside class="main-sidebar">
<?php include('include/sidebar.php');?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Vehicle Model List
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Vehicle</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          
          <div class="box">
            <div class="box-header">
			<div class="col-md-9">
              <h3 class="box-title">List of Added Vehicle Models </h3> <button type="submit" class="btn btn-default" id="car_model">Car</button> <a href="<?php echo base_url();?>index.php/Admin/bike_model"><button type="submit" class="btn btn-default" id="bike_model">Bike</button></a>
			  </div>
			  <div class="col-md-1"></div>
			  <div class="col-md-2">
			  <a href="<?php echo base_url();?>index.php/Admin/add_vehicle_model"><button type="submit" class="btn btn-danger">Add Model</button></a>
			  </div>
            </div>
			
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Deleted Successfully..!!
              </div>
              <?php }else if(isset($_GET['msg2'])) { ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Updated Successfully..!!
              </div>
              <?php } ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th> 
                  <th>Make</th>
				  <th>Model</th>
                  <th>Type</th>
				  <th>Action</th>
                </tr>
                </thead>
                <tbody id="vehicle_data">
                  <?php 
				  $i=1;
				  if(isset($query)) {
				  foreach($query as $name){
                  ?>
                <tr>
                  <td><?php echo $i++;?></td>
				  <td><?php echo $name['car_name'];?></td>
				  <td><?php echo $name['model_name'];?></td>
				  <td><?php echo "Car";?></td>
				  <td> <a href="#" data-toggle="modal" data-target="#modal-default" vehicle_id="<?php echo $name['id'];?>" vehicle_type="<?php echo $name['type'];?>" id="del_button"><i class="fa fa-fw fa-remove"></i></a> </td>
				</tr>
                  <?php }} ?>				
                </tbody>
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? Want to delete this</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="" id="del_vehicle"><button type="button" class="btn btn-primary">Yes</button></a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- jQuery 3 -->
<?php include('include/footer.php');?>

  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
  $(document).on("click","#car_model",function(){
	  location.reload();
  });

  
    $(document).on("click","#del_button",function(){
	  var vehicle_id = $(this).attr("vehicle_id");
	  var vehicle_type = $(this).attr("vehicle_type");
	  console.log(vehicle_id);
	  $("#del_vehicle").attr("href","delete_vehicle_model?id="+vehicle_id+"&type="+vehicle_type);
  });

</script>