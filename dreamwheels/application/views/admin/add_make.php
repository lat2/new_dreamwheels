<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your bike has been successfully added.
  </div>
        <?php  } ?>
            <form id="form_action" name="form" action="" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                
                
              <div class="form-group">
			       <div class="col-md-4"></div>
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Make Name</label>
                  <input type="text" name="bike_name" class="form-control" id="make_name" placeholder="Enter Name" required>
                </div>
              
              </div>
			  <div style="clear: both;"></div>
			  <div class="form-group">
			       <div class="col-md-4"></div>
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Select Vehicle Type</label>
                  <select class="form-control select2" name="bike_info[bike_type]" style="width: 100%!important;" id="type" required>
                  <option value="">Select Type </option>				  
                  <option value="car">Car </option>
                  <option value="bike">Bike</option>
              </select>
                </div>
              
              </div>

               
              <!-- /.box-body -->
              <div style="clear: both;"></div>
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
            
          </div>
         
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
            </form>
      </div>
      <!-- /.row -->
      </div>
          </div>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <?php include('include/footer.php');?>

    <script type="text/javascript">
        $(document).ready(function(){
        $(document).on("change","#type",function(){
			var type = $(this).val();
			//alert(type);
			if(type=='car'){
				$("#make_name").attr("name","car_name");
				$("#form_action").attr("action","<?php echo base_url();?>index.php/Admin/add_car_make");
			}else if(type=='bike'){
				$("#make_name").attr("name","name");
				$("#form_action").attr("action","<?php echo base_url();?>index.php/Admin/add_bike_make");
			}
		});
    });  
   
  </script>
  
