 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
    
      </div>
    
      <ul class="sidebar-menu" data-widget="tree">
      
     <li>
	  <a href="<?php echo base_url();?>index.php/Admin/dashboard">
	   <i class="fa fa-dashboard"></i> 
	   <span>Dashboard</span>
	  </a>
	 </li>
        
    <li>
	  <a href="<?php echo base_url();?>index.php/Admin/vehicle_make">
	   <i class="fa fa-dashboard"></i> 
	   <span>Vehicle Make</span>
	  </a>
	</li>
	<li>
	  <a href="<?php echo base_url();?>index.php/Admin/vehicle_model">
	   <i class="fa fa-dashboard"></i> 
	   <span>Vehicle Model</span>
	  </a>
	</li>
	<li>
	  <a href="<?php echo base_url();?>index.php/Admin/vehicle_variant">
	   <i class="fa fa-dashboard"></i> 
	   <span>Vehicle Variant</span>
	  </a>
	</li>
            <li>
	   <a href="<?php echo base_url();?>index.php/Admin/add_car">
	    <i class="fa fa-car"></i> 
	    <span>Add Car</span>
	   </a>
	  </li>
	  
            <li>
	   <a href="<?php echo base_url();?>index.php/Admin/car_list">
	    <i class="fa fa-list-ol"></i> 
	    <span>Car Listings</span>
	   </a>
	  </li>
          <li>
	   <a href="<?php echo base_url();?>index.php/Admin/used_car_list">
	    <i class="fa fa-list-ol"></i> 
	    <span>Used Cars Listings</span>
	   </a>
	  </li>
	  
	   <li>
	   <a href="<?php echo base_url();?>index.php/Admin/add_bike">
	    <i class="fa fa-motorcycle"></i> 
	    <span>Add Bike</span>
	   </a>
	  </li>
	  
            <li>
	   <a href="<?php echo base_url();?>index.php/Admin/bike_list">
	    <i class="fa fa-list-ol"></i> 
	    <span>Bike Listings</span>
	   </a>
	  </li>
          
          <li>
	   <a href="<?php echo base_url();?>index.php/Admin/used_bikes_list">
	    <i class="fa fa-list-ol"></i> 
	    <span>Used Bikes Listings</span>
	   </a>
	  </li>
	  
            <li>
	   <a href="<?php echo base_url();?>index.php/Admin/users">
	    <i class="fa fa-users"></i>
	    <span>Users</span>
	   </a>
	  </li> 
         
	   <li>
	   <a href="<?php echo base_url();?>index.php/Admin/interested_buyer">
	    <i class="fa fa-users"></i>
	    <span>Interested Buyers</span>
	   </a>
	  </li> 
	  
        <li>
         <a href="<?php echo base_url();?>index.php/Admin/my_account">
	<i class="fa fa-cog"></i>
	<span>Account Details</span>
         </a>
        </li>
      
         <li>
        <a data-title="Really want to logout ?" id="confirm" href="<?php echo base_url();?>index.php/Admin/logout">
         <i class="fa fa-power-off"></i> 
         <span>Log Out</span>
        </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>