
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your car has been successfully added.
  </div>
        <?php  } ?>
            <form id="admin_form" name="form" action="<?php echo base_url();?>index.php/Admin/update_admin" method="post" enctype="multipart/form-data> 
              <div class="box-body">
                <div class="form-group">


                <div class="col-md-4">
                  <label for="exampleInputEmail1">User ID</label>
                    <input class="form-control"  name="email" style="width: 100%;" placeholder="User id" required>
                 
                </div>
                    
                <div class="col-md-4">
                    <label for="exampleInputEmail1">Password</label><span style="color:red" id="psw_msg"></span>
                    <input type="password" class="form-control" style="width: 100%;" placeholder="Password" id="pass1" required> 
                </div>
                    
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" name="password" style="width: 100%;" placeholder="Confirm Password" id="pass2" required>
                 
                </div>    

              </div>
                  <br><br>
                   
              <!-- /.box-body -->
            <div class="row">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
            </div> 
                     <!-- /.box -->
        </div>
          </form>    
         
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  
  <script>
      $(document).ready(function(){
          
         $(document).on("blur","#pass2",function(){
             var pass2 = $(this).val();
             var pass1 = $("#pass1").val();
             if(pass1!=pass2)
             {
                 $("#psw_msg").html(" * Password didn't match.");   
             }else{
                 $("#psw_msg").html("");
             }
         });
         $("#admin_form").on("submit",function(){
             var pass2 = $("#pass2").val();
             var pass1 = $("#pass1").val();
             if(pass1!=pass2)
             {
                 $("#psw_msg").html(" * Password didn't match.");   
                 event.preventDefault();
             }else{
                 $("#psw_msg").html("");
             }
         });
         
      });
  </script>    