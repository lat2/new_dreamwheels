<?php include('include/header.php');?>
  <aside class="main-sidebar">
<?php include('include/sidebar.php');?>
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bike List
         
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Bike</a></li>
        <li class="active"> Bike List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of Added Used Bikes</h3>
            </div>
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Deleted Successfully..!!
            </div>
              <?php }else if(isset($_GET['msg2'])) { ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Item Updated Successfully..!!
              </div>
              <?php } ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th> 
                  <th>Bike</th>
                  <th>Registration Year</th>
                  <th>Price</th>
                  <th>City</th>
                  <th>Seller</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php if(isset($query)) { 
                      $i=1; ?>
                  <?php foreach ($query as $bikes) {
                      $json_bikes = json_decode($bikes->vehicle_info);
                         if($bikes->status==1){
                             $status='Enabled';
                             $status_btn = "label label-success";
                         }else{
                             $status='Disabled';
                             $status_btn = "label label-danger";
                         }
                         $bike_id=$json_bikes->vehicle_info->bike_id;
			             $model_id=$json_bikes->vehicle_info->model_id;
						 foreach($query_model as $model_name){
						    if($model_id==$model_name->id){
							        $bike_name = $model_name->bike_name;
                                           //var_dump($model_name->model_name);
                                    $model = $model_name->model_name;
							}
							
						 }
                      ?>
                    <?php $user_info = json_decode($bikes->user_info);?>
                <tr>
                  <td><?php echo $i++;?></td>  
                  <td><?php echo $bike_name;?>&nbsp;<?php echo $model;?></td>
                  <td><?php echo ucwords($json_bikes->vehicle_info->reg_year);?></td>
                  <td><?php echo $bikes->price;?></td>
                  <td><span class=""><?php echo ucwords($json_bikes->vehicle_info->city);?></span></td>
                  <td><?php echo $user_info->name;?></td>
                  <td><span class="<?php echo $status_btn;?>"><?php echo $status;?></span></td>
                  <td><a href="#" id="enable" data-id="<?php echo $bikes->id;?>">Enable</a> | <a href="#" data-id="<?php echo $bikes->id;?>"  id="disable">Disable</a> | <a href="#" data-id="<?php echo $bikes->id;?>" data-toggle="modal" data-target="#modal-default"><i class="fa fa-fw fa-remove"></i></a></td>
                </tr>
                <?php } } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? Want to delete this</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="delete_used_bike?id=<?php echo $bikes->id;?>"><button type="button" class="btn btn-primary">Yes</button></a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- jQuery 3 -->
<?php include('include/footer.php');?>

  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
  
  $(document).on("click","#enable",function(){
      var status = 1;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_used_bike",{status:status,id:id},function(){
          location.reload();
      });
  });
  $(document).on("click","#disable",function(){
      var status = 0;
      var id = $(this).attr("data-id");
      $.post("<?php echo base_url();?>index.php/Admin/update_status_used_bike",{status:status,id:id},function(){
          location.reload();
      });
  });
</script>