<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account
        <small>My Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">My Account</li>
      </ol>
    </section>

    <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Account Updated Successfully..!!
            </div>
    <?php } ?>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         <?php if(isset($query)) { ?>
          <?php  //var_dump($_SESSION);?>
         <?php foreach ($query as $account) { ?>
           
         
         
          <!-- /.box -->

          <div class="box" style="margin-bottom: 20px;" id="edit_box">
            <div class="box-header">
              <h3 class="box-title">Account Details</h3>
              <span class="pull-right"><a href="#" id="edit_btn">Edit</a></span>
            </div>
            <div class="row" style="margin-top: 20px;">
              <div class="col-md-2"></div>
  <div class="col-md-4"><i class="fa fa-user fa-1x" aria-hidden="true"></i>&nbsp; <?php echo $account['email'];?></div>
  <div class="col-md-4 offset-md-4"><i class="fa fa-cal fa-1x" aria-hidden="true"></i>&nbsp; <?php echo date("d/m/Y", strtotime($account['dateofcreation'])); ?></div>
</div>


             </div>

             <?php }  } ?>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?>
<!-- page script -->

<script>
    $(document).ready(function(){
        $(document).on("click",'#edit_btn',function(){
            $.post("<?php echo base_url();?>index.php/Admin/edit_admin",'',function(o){
                
                $("#edit_box").html(o);
            });
            
        }); 
    });
</script> 
