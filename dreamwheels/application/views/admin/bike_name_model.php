<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your bike has been successfully added.
  </div>
        <?php  } ?>
            <form name="form" action="<?php echo base_url();?>index.php/Admin/add_new_bike" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Enter Bike Name</label>
                 
                
                   <?php if(isset($query)) { ?>
                   <?php $count = count($query);?>

                    <select class="form-control select2"  name="bike_info[bike]" style="width: 100%;" id="select_bike">
                  <option value="">Select </option>
                
                 
                <?php for ($i=0; $i < $count; $i++) { ?>
               
                  
                  <option value=<?php echo $query[$i]['bikes'];?>><?php echo trim($query[$i]['bikes'],'"');?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
                </div>
                <div class="col-md-3" id="show_model">
                  <label for="exampleInputPassword1">Model</label>
                  <select class="form-control select2"  name="bike_info[model]" style="width: 100%;" id="model">
                  <option value="">Select </option>
                  
                 
                </select>
                </div>
                 <div class="col-md-3" id="show_model">
                 <label for="exampleInputEmail1"> Variant</label>
                 <select class="form-control select2" name="bike_info[Variant]" style="width: 100%!important;" id="select_variant">
                  <option value="">Select </option>
                  </select>
                 </div>
                  <div class="col-md-3" id="car_type">
                 <label for="exampleInputEmail1"> Bike Type</label>
                 <select class="form-control select2" name="bike_info[bike_type]" style="width: 100%!important;" id="car_type"> 
                  <option value="">Select </option>
                  <option value="Commuter">Commuter</option>
                  <option value="Scooter">Scooter </option>
                  <option value="tourer">Tourer </option>
                  <option value="sports">Sports </option>
                  <option value="cruiser">Cruiser </option>
                  <option value="naked">Naked </option>
                  <option value="cafe racer">Cafe Racer </option>
              </select>
                 </div>
              </div>
                   <div class="form-group">
                   <div class="col-md-6">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" name="bike_info[price]" class="form-control" id="price" placeholder="Enter On Road Price">
                </div>
                
              </div>
               
        </div>
        <!--/.col (right) -->
            </form>
      </div>
      <!-- /.row -->
      </div>
          </div>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <?php include('include/footer.php');?>

    <script type="text/javascript">
  $(document).on("change","#select_bike",function(){
   var bike = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_bike_model",{bike_name:bike},function(o){
          // console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
    
//    $(document).on("change","#model",function(){
//   var bike = $("#select_bike").val();
//   var model = $(this).val();
//     $.post("<?php echo base_url();?>index.php/Admin/select_bike_variant",{bike_name:bike,model_name:model},function(o){
//          // console.log(o);
//           $("#variant").html(o);
//           //$("#model").hide();    
//           });
//      });
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>
      <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var bike = $('#select_bike option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_bike_variant",{bike_name:bike,model_name:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 