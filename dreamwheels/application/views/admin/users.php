<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
        <small>Users Listing</small>
      </h1>
<!--      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Users</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users</h3>
              
            </div>
              <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                User Deleted Successfully..!!
            </div>
              <?php } ?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Number</th>
                  <th>E-mail</th>
                  <th>Date</th>
                  <th>Action</th> 
                </tr>
                </thead>
                <tbody>
                    <?php $i=1;
                    foreach($query as $users){
                        ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $users->name; ?></td>
                  <td><?php echo $users->phone; ?></td>
                  <td><?php echo $users->email; ?></td>
                  <td><?php echo date("d/m/Y", strtotime($users->dateofcreation)); ?></td>
                  <td> <a href="#" data-toggle="modal" data-target="#modal-default" data-id="<?php echo $users->id;?>" id="del_btn"><i class="fa fa-fw fa-remove"></i></a></td>
                </tr>
                    <?php } ?>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!-- DELETE Modal -->
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure? Want to delete this</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="" id="del_btn2"><button type="button" class="btn btn-primary">Yes</button></a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
 <?php require('include/footer.php');?>
<!-- page script -->

<script>
    $(document).ready(function(){
        $(document).on("click",'#del_btn',function(){
            var user_id = $(this).attr("data-id");
            //alert(user_id);
            $("#del_btn2").prop("href","delete_user?id="+user_id);
            
        }); 
    });
</script>    
