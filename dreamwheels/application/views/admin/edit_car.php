<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your car has been successfully added.
  </div>
        <?php  } ?>
        <?php if(isset($car)) { ?>
        <?php foreach ($car as $cars) { ?>
        <?php $json_car = json_decode($cars['car_info']);
              $engine_transmission = json_decode($cars['engine_transmission']);
              $tyres_brakes = json_decode($cars['tyres_brakes']);
              $dimensions = json_decode($cars['dimensions']);
              $appearance = json_decode($cars['appearance']);
              $comfort = json_decode($cars['comfort']);
              $safety_security = json_decode($cars['safety_security']);?> 
            <form name="form" action="<?php echo base_url();?>index.php/Admin/update_car?id=<?php echo $cars['id'];?>" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
                <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Enter Car Name</label>
                 
                <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="car_info[car]" style="width: 100%;" id="select_car" data="<?php echo $json_car->car;?>">
                  <option value="">Select </option>
                
                 
                <?php foreach ($query as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
                    
                </div>
                <div class="col-md-3" id="show_model">
                  <label for="exampleInputPassword1">Model</label>
                  <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="car_info[model]" style="width: 100%;" id="model" data="<?php echo $json_car->model;?>">
                  <option value="">Select </option>
                
                <?php foreach ($query_name_model as $car_model) {
						if($car_model['car_id']==$json_car->car){
				?>
               
                  
                  <option value=<?php echo $car_model['id'];?>><?php echo $car_model['model_name'];?></option>
                 <?php }} ?>
                </select>
                <?php  } ?>
                </div>
                 <div class="col-md-3" id="">
                 <label for="exampleInputEmail1"> Variant</label>
                 <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="car_info[variant]" style="width: 100%;" id="select_variant" data="<?php echo $json_car->variant;?>">
                  <option value="">Select </option>
                
                 
                 <?php foreach ($query_variant as $variant_name) { 
						if($variant_name->model_id==$json_car->model){
				?>
               
                  
                  <option value=<?php echo $variant_name->id;?>><?php echo $variant_name->variant;?></option>
                 <?php }} ?>
                </select>
                <?php  } ?>
                 </div>
                  <div class="col-md-3" id="car_type">
                 <label for="exampleInputEmail1"> Car Type</label>
                 <select class="form-control select2" name="car_info[car_type]" style="width: 100%!important;" id="car_type" data="<?php echo $json_car->car_type;?>"> 
                  <option value="">Select </option>
                  <option value="coupe">Coupe </option>
                  <option value="convertible">Convertible </option>
                  <option value="hatchback">Hatchback </option>
                  <option value="suv">Suv </option>
                  <option value="pickup">Pickup </option>
                   <option value="sedan">Sedan </option>
                    <option value="minicar">Minicar </option>
              </select>
                 </div>
              </div>
                   <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" name="car_info[price]" class="form-control" id="price" value="<?php echo $json_car->price;?>">
                </div>
				<div class="col-md-3">
                  <label for="exampleInputEmail1">Search Price</label>
                  <input type="text" name="price" class="form-control" id="price" value="<?php echo $cars['price'];?>">
                </div>
                
              </div>
                 <div style="clear:both;">
                  
                   <div class="panel-body" style="font-size: 18px;">Engine & Transmission</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Type</label>
                    <input class="form-control"  name="engine_transmission[engine_type]" style="width: 100%;" placeholder="e.g.,Air Cooled, 4 Stroke" value="<?php echo $engine_transmission->engine_type;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Displacement</label>
                    <input class="form-control"  name="engine_transmission[displacement]" style="width: 100%;" placeholder="In cc" value="<?php echo $engine_transmission->displacement;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Type</label>
                    <select class="form-control"  name="engine_transmission[fuel_type]" style="width: 100%;" data="<?php echo $engine_transmission->fuel_type;?>">
                        <option value="cng">CNG</option>
                        <option value="diesel">Diesel</option>
                        <option value="petrol">Petrol</option>
                    </select>
                </div>   
                                  
                <div class="col-md-3">
                    <label for="exampleInputPassword1">Power</label>
                    <input class="form-control"  name="engine_transmission[maximum_power]" style="width: 100%;" placeholder="e.g.,10 Ps @ 400 rpm" value="<?php echo $engine_transmission->maximum_power;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Torque</label>
                    <input class="form-control"  name="engine_transmission[maximum_torque]" style="width: 100%;" placeholder="e.g.,10.3 nm @ 5500 rpm" value="<?php echo $engine_transmission->maximum_torque;?>">
                </div>  
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">No of Cylinders</label>
                    <select class="form-control"  name="engine_transmission[no_of_cylinder]" style="width: 100%;" data="<?php echo $engine_transmission->no_of_cylinder;?>">
                      <?php for ($i=1; $i <=12; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Transmission</label>
                    <input class="form-control"  name="engine_transmission[transmission]" style="width: 100%;" placeholder="e.g.,manual, automatic.." value="<?php echo $engine_transmission->transmission;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Box</label>
                    <select class="form-control"  name="engine_transmission[gear_box]" style="width: 100%;" data="<?php echo $engine_transmission->gear_box;?>">
                    <option value="" selected="">No of gears</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select>    
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Drive Type</label>
                    <input class="form-control"  name="engine_transmission[drive_type]" style="width: 100%;" placeholder="e.g., FWD, AWD" value="<?php echo $engine_transmission->drive_type;?>">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Paddle Shift</label>
                    <select class="form-control"  name="engine_transmission[paddle_shift]" style="width: 100%;" data="<?php echo $engine_transmission->paddle_shift;?>">
                    <option value="yes">Yes</option>
                    <option value="yes">No</option>
                    </select>    
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Kerb Weight</label>
                    <input class="form-control"  name="engine_transmission[kerb_weight]" style="width: 100%;" placeholder="kg" value="<?php echo $engine_transmission->kerb_weight;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Mileage (ARAI) kmpl</label>
                    <input class="form-control"  name="engine_transmission[mileage]" style="width: 100%;" placeholder="In kmpl" value="<?php echo $engine_transmission->mileage;?>">
                </div>
              </div>
                </div> 
                 <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Brakes, Steering, Suspension and Tyres</div>
  
               <div class="form-group">
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Suspension Front</label>
                    <input class="form-control"  name="tyres_brakes[suspension_front]" style="width: 100%;" placeholder="e.g., Front" value="<?php echo $tyres_brakes->suspension_front;?>">
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Suspension Rear</label>
                    <input class="form-control"  name="tyres_brakes[suspension_rear]" style="width: 100%;" placeholder="e.g., Rear" value="<?php echo $tyres_brakes->suspension_rear;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake</label>
                     <select class="form-control"  name="tyres_brakes[front_break]" style="width: 100%;" data="<?php echo $tyres_brakes->front_break;?>">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake</label>
                     <select class="form-control"  name="tyres_brakes[rear_break]" style="width: 100%;" data="<?php echo $tyres_brakes->rear_break;?>">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Streering Type</label>
                    <input class="form-control"  name="tyres_brakes[steering_type]" style="width: 100%;" placeholder="e.g., Rear" value="<?php echo $tyres_brakes->steering_type;?>">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Minimum Turning Radius</label>
                    <input class="form-control"  name="tyres_brakes[turning_radius]" style="width: 100%;" placeholder="e.g., turning radius" value="<?php echo $tyres_brakes->turning_radius;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size]" style="width: 100%;" placeholder="e.g., size" value="<?php echo $tyres_brakes->tyre_size;?>">
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size]" style="width: 100%;" placeholder="e.g., size" value="<?php echo $tyres_brakes->wheel_size;?>">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Type</label>
                    <input class="form-control"  name="tyres_brakes[tyre_type]" style="width: 100%;" placeholder="e.g., tubeless" value="<?php echo $tyres_brakes->tyre_type;?>">
                </div>
              </div>
               </div>     
               
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Interior Dimensions</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seating Capacity</label>
                     <select class="form-control"  name="dimensions[seating_capacity]" style="width: 100%;" data="<?php echo $dimensions->seating_capacity;?>">
                    <option value="" selected="">Select seating capacity</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select> 
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Doors</label>
                    <select class="form-control"  name="dimensions[doors]" style="width: 100%;" data="<?php echo $dimensions->doors;?>">
                    <option value="" selected="">Select doors</option>
                       <?php for ($i=1; $i < 10; $i++) {  ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>

                          <?php } ?>
                    </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Boot Space</label>
                    <input class="form-control"  name="dimensions[boot_space]" style="width: 100%;" placeholder="in liters" value="<?php echo $dimensions->boot_space;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Capacity</label>
                    <input class="form-control"  name="dimensions[fuel_capacity]" style="width: 100%;" placeholder="In liters" value="<?php echo $dimensions->fuel_capacity;?>">
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Exterior Dimensions</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Length Width Height</label>
                    <input class="form-control"  name="dimensions[lwh]" style="width: 100%;" placeholder="Length/Width/Height" value="<?php echo $dimensions->lwh;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheelbase</label>
                    <input class="form-control"  name="dimensions[wheelbase]" style="width: 100%;" placeholder="in mm" value="<?php echo $dimensions->wheelbase;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ground Clearance</label>
                    <input class="form-control"  name="dimensions[ground_clearance]" style="width: 100%;" placeholder="In mm" value="<?php echo $dimensions->ground_clearance;?>">
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Interior Appearance/Features</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ventilated Seats</label>
                    <select class="form-control"  name="appearance[ventilated_seats]" style="width: 100%;" data="<?php echo $appearance->ventilated_seats;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Dual Tone Dashboard</label>
                    <select class="form-control"  name="appearance[dual_tone_dashboard]" style="width: 100%;" data="<?php echo $appearance->dual_tone_dashboard;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Leather Seats</label>
                    <select class="form-control"  name="appearance[leather_seats]" style="width: 100%;" data="<?php echo $appearance->leather_seats;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Fabric Upholstery</label>
                    <select class="form-control"  name="appearance[fabric_upholstery]" style="width: 100%;" data="<?php echo $appearance->fabric_upholstery;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Seat Headrest</label>
                    <select class="form-control"  name="appearance[rear_seat_headrest]" style="width: 100%;" data="<?php echo $appearance->rear_seat_headrest;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Additional Interior Features</label>
                    <textarea class="form-control"  name="appearance[additional_interior_features]" style="width: 100%;"><?php echo $appearance->additional_interior_features;?>
                </textarea>
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Exterior Appearance/Features</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Trunk Opener</label>
                    <input class="form-control"  name="appearance[trunk_opener]" style="width: 100%;" value="<?php echo $appearance->trunk_opener;?>">
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Alloy Wheels</label>
                    <select class="form-control"  name="appearance[alloy_wheels]" style="width: 100%;" data="<?php echo $appearance->alloy_wheels;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Outside Rear View Mirror</label>
                    <select class="form-control"  name="appearance[outside_rear_view_mirror]" style="width: 100%;" data="<?php echo $appearance->outside_rear_view_mirror;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Turn indicators on ORVM</label>
                    <select class="form-control"  name="appearance[turn_indicators]" style="width: 100%;" data="<?php echo $appearance->turn_indicators;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Xenon Headlights</label>
                    <select class="form-control"  name="appearance[xenon_lights]" style="width: 100%;" data="<?php echo $appearance->xenon_lights;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Spoiler</label>
                    <select class="form-control"  name="appearance[rear_spoiler]" style="width: 100%;" data="<?php echo $appearance->rear_spoiler;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Roof Rails</label>
                    <select class="form-control"  name="appearance[roof_rails]" style="width: 100%;" data="<?php echo $appearance->roof_rails;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Trunk Light</label>
                    <select class="form-control"  name="appearance[trunk_light]" style="width: 100%;" data="<?php echo $appearance->trunk_light;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Antenna</label>
                    <select class="form-control"  name="appearance[antenna]" style="width: 100%;" data="<?php echo $appearance->antenna;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Additional Exterior Features</label>
                    <textarea class="form-control"  name="appearance[additional_exterior_features]" style="width: 100%;">
                    <?php echo $appearance->additional_exterior_features;?>
                </textarea>
                </div>         
                     </div>
                 </div>
                   
                <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Comfort and Convenience</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Shift Indicator</label>
                    <select class="form-control"  name="comfort[gear_shift_indicator]" style="width: 100%;" data="<?php echo $comfort->gear_shift_indicator;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Steering Mounted Tripmeter</label>
                    <select class="form-control"  name="comfort[steering_mounted_tripmeter]" style="width: 100%;" data="<?php echo $comfort->steering_mounted_tripmeter;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Luggage Hook and Net</label>
                    <select class="form-control"  name="comfort[luggage_hook]" style="width: 100%;" data="<?php echo $comfort->luggage_hook;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">In Car Entertainment (Audio)</label>
                    <select class="form-control"  name="comfort[in_car_entertainment]" style="width: 100%;" data="<?php echo $comfort->in_car_entertainment;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Tailgate Ajar</label>
                    <select class="form-control"  name="comfort[tailgate_ajar]" style="width: 100%;" data="<?php echo $comfort->tailgate_ajar;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Lane Change Indicator</label>
                    <select class="form-control"  name="comfort[lane_change_indicator]" style="width: 100%;" data="<?php echo $comfort->lane_change_indicator;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">USB Charger</label>
                    <select class="form-control"  name="comfort[usb_charger]" style="width: 100%;" data="<?php echo $comfort->usb_charger;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Curtain</label>
                    <select class="form-control"  name="comfort[rear_curtain]" style="width: 100%;" data="<?php echo $comfort->rear_curtain;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Central Control Armrest</label>
                    <select class="form-control"  name="comfort[central_control_armrest]" style="width: 100%;" data="<?php echo $comfort->central_control_armrest;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">AC</label>
                    <select class="form-control"  name="comfort[ac]" style="width: 100%;" data="<?php echo $comfort->ac;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear AC Ducts</label>
                    <select class="form-control"  name="comfort[rear_ac_ducts]" style="width: 100%;" data="<?php echo $comfort->rear_ac_ducts;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Heater</label>
                    <select class="form-control"  name="comfort[heater]" style="width: 100%;" data="<?php echo $comfort->heater;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Steering</label>
                    <select class="form-control"  name="comfort[power_steering]" style="width: 100%;" data="<?php echo $comfort->power_steering;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Steering Mounted Audio Control</label>
                    <select class="form-control"  name="comfort[steering_audio_control]" style="width: 100%;" data="<?php echo $comfort->steering_audio_control;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Cruise Control</label>
                    <select class="form-control"  name="comfort[cruise_control]" style="width: 100%;" data="<?php echo $comfort->cruise_control;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Windows ( Front / Rear )</label>
                  <input class="form-control"  name="comfort[power_windows]" style="width: 100%;" value="<?php echo $comfort->power_windows;?>">
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Driver Seat</label>
                    <select class="form-control"  name="comfort[adjustable_driver_seat]" style="width: 100%;" data="<?php echo $comfort->adjustable_driver_seat;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Heated Seats Rear</label>
                    <select class="form-control"  name="comfort[heated_seat_rear]" style="width: 100%;" data="<?php echo $comfort->heated_seat_rear;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Heated Seat Front</label>
                    <select class="form-control"  name="comfort[heated_seat_front]" style="width: 100%;" data="<?php echo $comfort->heated_seat_front;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Front Passenger Seat</label>
                    <select class="form-control"  name="comfort[ajustable_front_passenger_seat]" style="width: 100%;" data="<?php echo $comfort->ajustable_front_passenger_seat;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Lumbar Support</label>
                    <select class="form-control"  name="comfort[lumbar_support]" style="width: 100%;" data="<?php echo $comfort->lumbar_support;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Folding Rear Seat</label>
                  <input class="form-control"  name="comfort[folding_rear_seat]" style="width: 100%;" value="<?php echo $comfort->folding_rear_seat;?>">
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Seat Center Armrest</label>
                    <select class="form-control"  name="comfort[rearseat_armrest]" style="width: 100%;" data="<?php echo $comfort->rearseat_armrest;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Tachometer</label>
                    <select class="form-control"  name="comfort[tachometer]" style="width: 100%;" data="<?php echo $comfort->tachometer;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Door & Trunk Ajar Warning</label>
                    <select class="form-control"  name="comfort[door_trunk_ajar_warning]" style="width: 100%;" data="<?php echo $comfort->door_trunk_ajar_warning;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Low Fuel Warning Lamp</label>
                    <select class="form-control"  name="comfort[low_fuel_warning_lamp]" style="width: 100%;" data="<?php echo $comfort->low_fuel_warning_lamp;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Belt Warning</label>
                    <select class="form-control"  name="comfort[seat_belt_warning]" style="width: 100%;" data="<?php echo $comfort->seat_belt_warning;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clock</label>
                    <select class="form-control"  name="comfort[clock]" style="width: 100%;" data="<?php echo $comfort->clock;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Push Ignition</label>
                    <select class="form-control"  name="comfort[push_ignition]" style="width: 100%;" data="<?php echo $comfort->push_ignition;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">AM/FM Radio</label>
                    <select class="form-control"  name="comfort[radio]" style="width: 100%;" data="<?php echo $comfort->radio;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">USB Auxiliary Input</label>
                    <select class="form-control"  name="comfort[aux]" style="width: 100%;" data="<?php echo $comfort->aux;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">DVD Playback</label>
                    <select class="form-control"  name="comfort[dvd_playback]" style="width: 100%;" data="<?php echo $comfort->dvd_playback;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Touch-screen Display</label>
                    <select class="form-control"  name="comfort[touch_screen_display]" style="width: 100%;" data="<?php echo $comfort->touch_screen_display;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">GPS Navigation System</label>
                  <input class="form-control"  name="comfort[gps_navigation_system]" style="width: 100%;" value="<?php echo $comfort->gps_navigation_system;?>">
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Speakers Rear</label>
                    <select class="form-control"  name="comfort[speakers_rear]" style="width: 100%;" data="<?php echo $comfort->speakers_rear;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Speakers Front</label>
                    <select class="form-control"  name="comfort[speakers_front]" style="width: 100%;" data="<?php echo $comfort->speakers_front;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                      
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Auto Rain Sensing Wipers</label>
                    <select class="form-control"  name="comfort[auto_rain_sensing]" style="width: 100%;" data="<?php echo $comfort->auto_rain_sensing;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Follow me home headlamps</label>
                    <select class="form-control"  name="comfort[follow_me_home_headlamps]" style="width: 100%;" data="<?php echo $comfort->follow_me_home_headlamps;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Head Lights</label>
                    <select class="form-control"  name="comfort[adjustable_headlights]" style="width: 100%;" data="<?php echo $comfort->adjustable_headlights;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Parking Sensors</label>
                    <select class="form-control"  name="comfort[parking_sensors]" style="width: 100%;" data="<?php echo $comfort->parking_sensors;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Remote Operated Fuel Tank Lid</label>
                    <select class="form-control"  name="comfort[remote_fuel_lid]" style="width: 100%;" data="<?php echo $comfort->remote_fuel_lid;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
               <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Cupholders</label>
                    <select class="form-control"  name="comfort[front_cupholders]" style="width: 100%;" data="<?php echo $comfort->front_cupholders;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">12V Power Outlets</label>
                    <select class="form-control"  name="comfort[power_outlets]" style="width: 100%;" data="<?php echo $comfort->power_outlets;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Cigarette Lighter</label>
                    <select class="form-control"  name="comfort[cigarette_lights]" style="width: 100%;" data="<?php echo $comfort->cigarette_lights;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         <div class="col-md-3">
                  <label for="exampleInputPassword1">Bottle Holder</label>
                    <select class="form-control"  name="comfort[bottle_holder]" style="width: 100%;" data="<?php echo $comfort->bottle_holder;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                       <div class="col-md-3">
                  <label for="exampleInputPassword1">Moon Roof</label>
                    <select class="form-control"  name="comfort[moon_roof]" style="width: 100%;" data="<?php echo $comfort->moon_roof;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                                <div class="col-md-3">
                  <label for="exampleInputPassword1">Sun Roof</label>
                  <input class="form-control"  name="comfort[sun_roof]" style="width: 100%;" value="<?php echo $comfort->sun_roof;?>">
                </div>
                                        
                         
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Addtional Comfort Features</label>
                    <textarea class="form-control"  name="comfort[additional_comfort_features]" style="width: 100%;" >
                    <?php if(isset($comfort->additional_comfort_features)){echo $comfort->additional_comfort_features;}?>
                </textarea>
                </div>         
                     </div>
                 </div>   
                  
            <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Safety & Security</div>
  
               <div class="form-group">
                
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Airbags</label>
                     <input class="form-control"  name="safety_security[airbags]" style="width: 100%;" value="<?php echo $safety_security->airbags;?>">
                   
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Belts</label>
                    <select class="form-control"  name="safety_security[seat_belts]" style="width: 100%;" data="<?php echo $safety_security->seat_belts;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Anti-lock Braking System (ABS)</label>
                    <select class="form-control"  name="safety_security[abs]" style="width: 100%;" data="<?php echo $safety_security->abs;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Electronic Brakeforce Distribution (EBD)</label>
                    <select class="form-control"  name="safety_security[ebd]" style="width: 100%;" data="<?php echo $safety_security->ebd;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Brake Assist (BA)</label>
                    <select class="form-control"  name="safety_security[ba]" style="width: 100%;" data="<?php echo $safety_security->ba;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>   
                   
            
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Electronic Stability Program (ESP)</label>
                    <select class="form-control"  name="safety_security[esp]" style="width: 100%;" data="<?php echo $safety_security->esp;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Traction Control System (TCS)</label>
                    <select class="form-control"  name="safety_security[tcs]" style="width: 100%;" data="<?php echo $safety_security->tcs;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Immobilizer</label>
                     <select class="form-control"  name="safety_security[engine_immobilizer]" style="width: 100%;" data="<?php echo $safety_security->engine_immobilizer;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div> 
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Child Safety Lock</label>
                     <select class="form-control"  name="safety_security[child_safety_lock]" style="width: 100%;" data="<?php echo $safety_security->child_safety_lock;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fog Lamps</label>
                     <select class="form-control"  name="safety_security[fog_lamps]" style="width: 100%;" data="<?php echo $safety_security->fog_lamps;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Wash Wiper</label>
                        <select class="form-control"  name="safety_security[rear_wash_wiper]" style="width: 100%;" data="<?php echo $safety_security->rear_wash_wiper;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Defogger</label>
                     <select class="form-control"  name="safety_security[rear_defogger]" style="width: 100%;" data="<?php echo $safety_security->rear_defogger;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   

              </div>
               </div>
          <!-- /.box -->
          
          <div style="clear: both;">
               <div class="panel-body" style="font-size: 18px;">Car Photos</div>
              <div class="form-group">
                 <div class="col-md-3">
               <div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span>Choose file</span><input type="file" id="fileupload" name="car_info[images][]" multiple/></span>
    <span class="fileinput-filename"></span><span class="fileinput-new"></span>
</div>
</div>
 <div class="col-md-9">
<div id="dvPreview">
    <?php
               if(isset($json_car->images)){
               $len = count($json_car->images);    //var_dump($len);
            
               for($i=0;$i<$len;$i++){
        ?>
        <img src="<?php echo base_url();?>uploads/car/<?php echo $json_car->images[$i];?>" width="100px" height="100px">
              <input type="text" name ="car_info[images][0]" value="<?php  echo $json_car->images[0];?>" hidden/>
               <?php }}else{ ?>No file chosen <?php } ?>
        </div>
              </div>

                  </div>
                  <div style="clear: both;"></div>
<div class="panel-body" style="font-size: 18px;">Car Category</div>
                     <div class="form-group">
                 <div class="col-md-6">
                     
                     <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="latest" class="minimal" id="cat" <?php echo ($json_car->category=='latest')?'checked':'';?>>&nbsp;Latest
    </label> 
                   <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="featured" class="minimal" id="cat" required  <?php echo ($json_car->category=='featured')?'checked':'';?>>&nbsp;Featured 

    </label>
    <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="upcoming" class="minimal" id="cat" <?php echo ($json_car->category=='upcoming')?'checked':'';?>>&nbsp;Upcoming
    </label>
    <label class="radio-inline">
      <input type="radio" name="car_info[category]" value="recommended" class="minimal" id="cat" <?php echo ($json_car->category=='recommended')?'checked':'';?>>&nbsp;Recommended
    </label>
                 </div>
                 <div class="col-md-3" id="upcoming-date" style="display: none">
                
                  <input type="text" class="form-control pull-right" name="car_info[upcoming_date]"  id="datepicker" placeholder="Upcoming Date">
                 </div>
               </div>

                 <div style="clear: both;"></div>
                 <div class="panel-body" style="font-size: 18px;">Enter Details</div>
                <div class="form-group">
                 <textarea id="editor1" name="car_info[details]" rows="10" cols="80">
                                            <?php echo $json_car->details;?>
                    </textarea>
                </div>
               </div>
          
          <div style="clear:both;">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
           
               </div>
        </div>
            
            </form>
            <?php } } ?>
        <!--/.col (right) -->
      </div>     
          
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('include/footer.php');?>

    <script type="text/javascript">
  $(document).on("change","#select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_car_model",{car_id:car_id},function(o){
           console.log(o);
           $("#model").html(o);   
           });
      });
        $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{model_name:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                                console.log(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>

  <script type="text/javascript">
  	$(".select2").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
  <script type="text/javascript">
  	$("select").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
   <script type="text/javascript">
  	$(".select3").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>