<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Used car details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your car has been successfully added.
  </div>
        <?php  } ?>
		<?php if(isset($query)) { 
                      $i=1;?>
                  <?php foreach ($query as $cars) {
                         if($cars->status==1){
                             $status='Enabled';
                             $status_btn = "label label-success";
                         }else{
                             $status='Disabled';
                             $status_btn = "label label-danger";
                         }
                      ?>
                  <?php $json_car = json_decode($cars->vehicle_info);
				        $model_id = $json_car->vehicle_info->model;
                  $variant_id = $json_car->vehicle_info->varient;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
				  ?>
                    <?php $additional_info = json_decode($cars->additional_info);$user_info = json_decode($cars->user_info);?>
                <table class="table table-striped">
				    <thead>
					    <th>Car Details</th>
					</thead>
					<tbody>
                                            
                                              <tr>
					    <td>Photo</td>
						<td> <img src="<?php render_item_image("uploads/used_cars/".$additional_info->additional_info->img[0]);?>" alt="<?php echo $car_name; ?> available on dreamwheels" style="width:240px;" style="height:280px;"/></td>
					  </tr>
					  <tr>
					    <td>Car Name & Model</td>
						<td><?php echo $car_name,' ',$model_name; ?></td>
					  </tr>
                      <tr>
					    <td>Registration Year</td>
						<td><?php echo $json_car->vehicle_info->reg_year; ?></td>
					  </tr>
                      <tr>
					    <td>Kilometer Driven</td>
						<td><?php echo $json_car->vehicle_info->kilometer_driven." KM"; ?></td>
					  </tr>
                      <tr>
					    <td>Color</td>
						<td><?php echo $json_car->vehicle_info->color; ?></td>
					  </tr>
					  
                      <tr>
					    <td>Fuel</td>
						<td><?php echo $json_car->vehicle_info->fuel; ?></td>
					  </tr>
                      
                      <tr>
					    <td>City</td>
						<td><?php echo $json_car->vehicle_info->city; ?></td>
					  </tr>
                      <tr>
					    <td>Price</td>
						<td><?php echo inr($cars->price); ?></td>
					  </tr>
                      <tr>
					    <td>Users Comment</td>
						<td><?php echo "<b>".$additional_info->comment."</b>"; ?></td>
					  </tr>					  
					</tbody>
				</table>
				<table class="table table-striped">
				    <thead>
					    <th>User Details</th>
					</thead>
					<tbody>
					  <tr>
					    <td>Name</td>
						<td><?php echo $user_info->name ?></td>
					  </tr>
                      <!--<tr>
					    <td>Email</td>
						<td><?php echo $user_info->user_info->email; ?></td>
					  <tr>-->
                      <tr>
					    <td>Phone</td>
						<td><?php echo $user_info->phone; ?></td>
					  </tr>
                      <!--<tr>
					    <td>Address</td>
						<td><?php echo $user_info->user_info->address; ?></td>
					  <tr>
                      <tr>
					    <td>City</td>
						<td><?php echo $user_info->user_info->city; ?></td>
					  <tr>
					  <tr>
					    <td>State</td>
						<td><?php echo $user_info->user_info->state; ?></td>
					  <tr>-->
                      <tr>
					    <td>Date of Creation</td>
						<td><?php echo $cars->dateofcreation; ?></td>
					  </tr>					  
					</tbody>
				</table>
				<?php } } ?>
          </div> 
           </div>        
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('include/footer.php');?>

    <script type="text/javascript">
  $(document).on("change","#select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_car_model",{car_id:car_id},function(o){
           console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>
      <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{model_name:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 