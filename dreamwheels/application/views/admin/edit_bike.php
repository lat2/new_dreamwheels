<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your bike has been successfully added.
  </div>
        <?php  } ?>
        <?php if(isset($bike)) { ?>
        <?php foreach ($bike as $bikes) { 
		       $bike_id=$bikes['bike_id'];
			   $model_id=$bikes['model_id'];
			   $variant_id=$bikes['variant_id'];
		?>
        <?php $json_bike = json_decode($bikes['bike_info']);
              $json_price = json_decode($bikes['price_info']);
              $engine_transmission = json_decode($bikes['engine_transmission']);
              $tyres_brakes = json_decode($bikes['tyres_brakes']);
              $feature_safety = json_decode($bikes['feature_safety']);
              $chasis_suspension = json_decode($bikes['chasis_suspension']);
              $dimension = json_decode($bikes['dimension']);
              $electricals = json_decode($bikes['electricals']);
        ?> 
            <form name="form" action="<?php echo base_url();?>index.php/Admin/update_bike?id=<?php echo $bikes['id'];?>" method="post" enctype="multipart/form-data"> 
			<input type="hidden" name="id" value="<?php echo $bikes['id']; ?>">
              <div class="box-body">
                <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Enter Bike Name</label>
                 
                
                  

                    <select class="form-control select2"  name="bike_id" style="width: 100%;" id="select_bike" data="<?php echo $bike_id;?>">
                      <option value="">Select bike</option>
													<?php foreach ($query_name as $bikes_name) { ?>
														<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
												<?php	} ?>
                
                 </select>
                </div>
                <div class="col-md-3" id="show_model">
                  <label for="exampleInputPassword1">Model</label>
                  <select class="form-control select2"  name="model_id" style="width: 100%;" id="model" data="<?php echo $model_id;?>">
                  <option value="">Select </option>
                  <?php  foreach ($query_model as $bikes_model) { if($bike_id==$bikes_model->bike_id){  ?>
														<option value="<?php echo $bikes_model->id;?>"><?php echo $bikes_model->model_name;?></option>
												<?php	}} ?>
                  
                </select>
                </div>
                 <div class="col-md-3" id="show_model">
                 <label for="exampleInputEmail1"> Variant</label>
                 <select class="form-control select2" name="variant_id" style="width: 100%!important;" id="select_variant" data="<?php echo $variant_id;?>">
                  <option value="">Select </option>
                   <?php foreach ($query_variant as $bikes_variant) { if($model_id==$bikes_variant->model_id){ ?>
														<option value="<?php echo $bikes_variant->id;?>"><?php echo $bikes_variant->variant;?></option>
												<?php	}} ?>
                 </select>
                 </div>
                  <div class="col-md-3" id="bike_type">
                 <label for="exampleInputEmail1"> Bike Type</label>
                 <select class="form-control select2" name="bike_info[bike_type]" style="width: 100%!important;" id="bike_type" data="<?php echo $json_bike->bike_type;?>"> 
                  <option value="bikes">Bikes</option>
                  <option value="scooter">Scooter </option>
                  <option value="tourer">Tourer </option>
                  <option value="sport">Sports </option>
                  <option value="cruiser">Cruiser </option>
                  <option value="cafe racer">Cafe Racer </option>
              </select>
                 </div>
              </div>
            <div class="form-group">
                   <div class="col-md-3">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="text" name="bike_info[price]" class="form-control" id="price" value="<?php echo $json_bike->price;?>">
                </div>
				
				<div class="col-md-3">
                  <label for="exampleInputEmail1">Search Price</label>
                  <input type="text" name="price" class="form-control" id="price" value="<?php echo $bikes['price'];?>">
                </div>
               
                
            </div>
                  <div class="form-group">
                <div style="clear: both;">
                 <div class="panel-body" style="font-size: 18px;">Engine & Transmission</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Engine Type</label>
                  <input class="form-control"  name="engine_transmission[engine_type]" style="width: 100%;" placeholder="e.g.,Air Cooled, 4 Stroke" value="<?php echo $engine_transmission->engine_type;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Displacement</label>
                    <input class="form-control"  name="engine_transmission[displacement]" style="width: 100%;" placeholder="In cc" value="<?php echo $engine_transmission->displacement;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Mileage</label>
                    <input class="form-control"  name="engine_transmission[mileage]" style="width: 100%;" placeholder="kmpl" value="<?php echo $engine_transmission->mileage;?>">
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Power</label>
                    <input class="form-control"  name="engine_transmission[maximum_power]" style="width: 100%;" placeholder="e.g.,10 Ps @ 400 rpm" value="<?php echo $engine_transmission->maximum_power;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Torque</label>
                    <input class="form-control"  name="engine_transmission[maximum_torque]" style="width: 100%;" placeholder="e.g.,10.3 nm @ 5500 rpm" value="<?php echo $engine_transmission->maximum_torque;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Starting</label>
                    <input class="form-control"  name="engine_transmission[starting]" style="width: 100%;" placeholder="Kick or Self start" value="<?php echo $engine_transmission->starting;?>">
                </div>
                   
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Maximum Speed</label>
                    <input class="form-control"  name="engine_transmission[maximum_speed]" style="width: 100%;" placeholder="In kmph" value="<?php echo $engine_transmission->maximum_speed;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Cooling System</label>
                    <input class="form-control"  name="engine_transmission[cooling_system]" style="width: 100%;" placeholder="e.g.,air cooled" value="<?php echo $engine_transmission->cooling_system;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Gear Box</label>
                    <input class="form-control"  name="engine_transmission[gear_box]" style="width: 100%;" placeholder="e.g.,4 speed or CVT" value="<?php echo $engine_transmission->gear_box;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clutch</label>
                    <input class="form-control"  name="engine_transmission[clutch]" style="width: 100%;" placeholder="e.g.,wet, multi plate" value="<?php echo $engine_transmission->clutch;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">No of Cylinders</label>
                    <select class="form-control"  name="engine_transmission[no_of_cylinder]" style="width: 100%;"data="<?php echo $engine_transmission->no_of_cylinder;?>">
                      <?php for ($i=1; $i < 6; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Drive Type</label>
                    <input class="form-control"  name="engine_transmission[drive_type]" style="width: 100%;" placeholder="e.g., chain, belt or shaft" value="<?php echo $engine_transmission->drive_type;?>">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Valves Per Cylinder</label>
                     <select class="form-control"  name="engine_transmission[valves_per_cylinder]" style="width: 100%;" data="<?php echo $engine_transmission->valves_per_cylinder;?>">
                      <?php for ($i=1; $i < 6; $i++) {  ?>
                       
                   <option value="<?php echo $i;?>"><?php echo $i;?></option>
                   <?php } ?>

                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Bore</label>
                    <input class="form-control"  name="engine_transmission[bore]" style="width: 100%;" placeholder="e.g., 52.4 mm" value="<?php echo $engine_transmission->bore;?>">
                   
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Stroke</label>
                    <input class="form-control"  name="engine_transmission[stroke]" style="width: 100%;" placeholder="e.g., 57.8 mm" value="<?php echo $engine_transmission->stroke;?>">
                   
                </div>
                   
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Supply System</label>
                    <input class="form-control"  name="engine_transmission[supply_system]" style="width: 100%;" placeholder="e.g., Carbuerator, FI" value="<?php echo $engine_transmission->supply_system;?>">
                   
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Transmission Type</label>
                    <input class="form-control"  name="engine_transmission[transmission_type]" style="width: 100%;" placeholder="e.g., Manual" value="<?php echo $engine_transmission->transmission_type;?>">
                   
                </div>   
               
              </div>
                 
                 <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Tyres & Brakes</div>
  
               <div class="form-group">
                   

                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size Front</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size_front]" style="width: 100%;" placeholder="e.g., 80/100-18" value="<?php echo $tyres_brakes->tyre_size_front;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Size Rear</label>
                    <input class="form-control"  name="tyres_brakes[tyre_size_rear]" style="width: 100%;" placeholder="e.g., 80/100-18" value="<?php echo $tyres_brakes->tyre_size_rear;?>">
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tyre Type</label>
                    <input class="form-control"  name="tyres_brakes[tyre_type]" style="width: 100%;" placeholder="e.g., tubeless" value="<?php echo $tyres_brakes->tyre_type;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Radial tyres</label>
                    <select class="form-control"  name="tyres_brakes[radial_tyres]" style="width: 100%;" data="<?php echo $tyres_brakes->radial_tyres;?>">
                   <option value="yes">Yes</option>
                    <option value="No">No</option>
                </select>
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size Front</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size_front]" style="width: 100%;" placeholder="Enter the values in inches" value="<?php echo $tyres_brakes->wheel_size_front;?>">
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Size Rear</label>
                    <input class="form-control"  name="tyres_brakes[wheel_size_rear]" style="width: 100%;" placeholder="Enter the values in inches" value="<?php echo $tyres_brakes->wheel_size_rear;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake</label>
                     <select class="form-control"  name="tyres_brakes[front_break]" style="width: 100%;" data="<?php echo $tyres_brakes->front_break;?>">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake</label>
                     <select class="form-control"  name="tyres_brakes[rear_break]" style="width: 100%;" data="<?php echo $tyres_brakes->rear_break;?>">
                   <option value="disc">Disc</option>
                    <option value="drum">Drum</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Brake Diameter</label>
                    <input class="form-control"  name="tyres_brakes[front_brake_diameter]" style="width: 100%;" placeholder="Enter the values in mm" value="<?php echo $tyres_brakes->front_brake_diameter;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Brake Diameter</label>
                    <input class="form-control"  name="tyres_brakes[rear_brake_diameter]" style="width: 100%;" placeholder="Enter the values in mm" value="<?php echo $tyres_brakes->rear_brake_diameter;?>">
                </div>   
                     
               
              </div>
               </div>     
                 
            <div style="clear:both;"> 
                 
                  <div class="panel-body" style="font-size: 18px;">Features & Safety</div>
  
               <div class="form-group">
                   

               <div class="col-md-3">
                  <label for="exampleInputPassword1">Speedometer</label>
                     <select class="form-control"  name="feature_safety[speedometer]" style="width: 100%;" data="<?php echo $feature_safety->speedometer;?>">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div>
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Odometer</label>
                     <select class="form-control"  name="feature_safety[odometer]" style="width: 100%;" data="<?php echo $feature_safety->odometer;?>">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tachometer</label>
                     <select class="form-control"  name="feature_safety[tachometer]" style="width: 100%;" data="<?php echo $feature_safety->tachometer;?>">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div> 
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tripmeter</label>
                     <select class="form-control"  name="feature_safety[tripmeter]" style="width: 100%;" data="<?php echo $feature_safety->tripmeter;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">ABS</label>
                    <select class="form-control"  name="feature_safety[abs]" style="width: 100%;" data="<?php echo $feature_safety->abs;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">CBS</label>
                    <select class="form-control"  name="feature_safety[cbs]" style="width: 100%;" data="<?php echo $feature_safety->cbs;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">EBS</label>
                    <select class="form-control"  name="feature_safety[ebs]" style="width: 100%;" data="<?php echo $feature_safety->ebs;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Hook</label>
                    <select class="form-control"  name="feature_safety[seat_hook]" style="width: 100%;" data="<?php echo $feature_safety->seat_hook;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Seat Type</label>
                    <input class="form-control"  name="feature_safety[seat_type]" style="width: 100%;" placeholder="e.g., single or split" value="<?php echo $feature_safety->seat_type;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Console</label>
                    <select class="form-control"  name="feature_safety[console]" style="width: 100%;" data="<?php echo $feature_safety->console;?>">
                   <option value="analogue">Analogue</option>
                    <option value="digital">Digital</option>
                    <option value="analogue digital">Analogue/Digital</option>
                </select>
                </div>
                   
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Clock</label>
                    <select class="form-control"  name="feature_safety[clock]" style="width: 100%;" data="<?php echo $feature_safety->clock;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                   <div class="col-md-3">
                  <label for="exampleInputPassword1">UnderSeat Storage</label>
                    <select class="form-control"  name="feature_safety[underseat_storage]" style="width: 100%;" data="<?php echo $feature_safety->underseat_storage;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Traction Control</label>
                     <select class="form-control"  name="feature_safety[traction_control]" style="width: 100%;" data="<?php echo $feature_safety->traction_control;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div> 
                 
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Cruise Control</label>
                     <select class="form-control"  name="feature_safety[cruise_control]" style="width: 100%;" data="<?php echo $feature_safety->cruise_control;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Navigation</label>
                     <select class="form-control"  name="feature_safety[navigation]" style="width: 100%;" data="<?php echo $feature_safety->navigation;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Quick Shifter</label>
                     <select class="form-control"  name="feature_safety[quick_shifter]" style="width: 100%;" data="<?php echo $feature_safety->quick_shifter;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Launch Control</label>
                     <select class="form-control"  name="feature_safety[launch_control]" style="width: 100%;" data="<?php echo $feature_safety->launch_control;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   
                 
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Power Modes</label>
                     <select class="form-control"  name="feature_safety[power_modes]" style="width: 100%;" data="<?php echo $feature_safety->power_modes;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Adjustable Wind Screen</label>
                     <select class="form-control"  name="feature_safety[adjustable_wind_screen]" style="width: 100%;" data="<?php echo $feature_safety->adjustable_wind_screen;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>
                   
                 <div class="col-md-3">
                  <label for="exampleInputPassword1">Mobile Connectivity</label>
                     <select class="form-control"  name="feature_safety[mobile_connectivity]" style="width: 100%;" data="<?php echo $feature_safety->mobile_connectivity;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                </div>   

              </div>
               </div>
                 
                 <div style="clear:both">
                     
                    <div class="panel-body" style="font-size: 18px;">Chasis & Dimension</div> 
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Chasis/Frame</label>
                     <input class="form-control"  name="chasis_suspension[chasis_frame]" style="width: 100%;" placeholder="e.g., diamond" value="<?php echo $chasis_suspension->chasis_frame;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Front Suspension</label>
                    <input class="form-control"  name="chasis_suspension[front_suspension]" style="width: 100%;" placeholder="" value="<?php echo $chasis_suspension->front_suspension;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Rear Suspension</label>
                    <input class="form-control"  name="chasis_suspension[rear_suspension]" style="width: 100%;" placeholder="" value="<?php echo $chasis_suspension->rear_suspension;?>">
                </div> 
                     </div>
                 </div>
                 
                 <div style="clear:both">
                    <div class="panel-body" style="font-size: 18px;">Dimension</div> 
                     
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Length</label>
                     <input class="form-control"  name="dimension[length]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->length;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Height</label>
                    <input class="form-control"  name="dimension[height]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->height;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Width</label>
                    <input class="form-control"  name="dimension[width]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->width;?>">
                </div> 
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Ground Clearance</label>
                    <input class="form-control"  name="dimension[ground_clearance]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->ground_clearance;?>">
                </div>
                        
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Saddle Height</label>
                    <input class="form-control"  name="dimension[saddle_height]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->saddle_height;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Kerb Weight</label>
                    <input class="form-control"  name="dimension[kerb_weight]" style="width: 100%;" placeholder="Enter values in kg" value="<?php echo $dimension->kerb_weight;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Wheel Base</label>
                    <input class="form-control"  name="dimension[wheel_base]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $dimension->wheel_base;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Fuel Capacity</label>
                    <input class="form-control"  name="dimension[fuel_capacity]" style="width: 100%;" placeholder="Enter values in liters" value="<?php echo $dimension->fuel_capacity;?>">
                </div>         
                     </div>
                 </div>
                 
                <div style="clear:both">
                    <div class="panel-body" style="font-size: 18px;">Electricals</div> 
                     
                     <div class="form-group">
                            
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Head Lamp</label>
                     <input class="form-control"  name="electricals[head_lamp]" style="width: 100%;" placeholder="" value="<?php echo $electricals->head_lamp;?>">
                </div>   
                
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Tail Lamp</label>
                    <input class="form-control"  name="electricals[tail_lamp]" style="width: 100%;" placeholder="" value="<?php echo $electricals->tail_lamp;?>">
                </div>
                   
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Turn Signal Lamp</label>
                    <input class="form-control"  name="electricals[turn_signal_lamp]" style="width: 100%;" placeholder="Enter values in mm" value="<?php echo $electricals->turn_signal_lamp;?>">
                </div> 
                                          
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Pass Switch</label>
                    <select class="form-control"  name="electricals[pass_switch]" style="width: 100%;" data="<?php echo $electricals->pass_switch;?>">
                   <option value="yes">Yes</option>
                    <option value="no">No</option>
                    
                </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Projector HeadLight</label>
                    <select class="form-control"  name="electricals[projector_headlight]" style="width: 100%;" data="<?php echo $electricals->projector_headlight;?>">
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    
                    </select>
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Battery Type</label>
                    <input class="form-control"  name="electricals[battery_type]" style="width: 100%;" placeholder="e.g, maintenance Free" value="<?php echo $electricals->battery_type;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Battery Capacity</label>
                    <input class="form-control"  name="electricals[battery_capacity]" style="width: 100%;" placeholder="" value="<?php echo $electricals->battery_capacity;?>">
                </div>
                         
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Low Fuel Indicator</label>
                    <select class="form-control"  name="electricals[low_fuel_indicator]" style="width: 100%;" data="<?php echo $electricals->low_fuel_indicator;?>">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    
                    </select>
                </div>         
                        
                <div class="col-md-3">
                  <label for="exampleInputPassword1">Pilot Lamps</label>
                    <select class="form-control"  name="electricals[pilot_lamps]" style="width: 100%;" data="<?php echo $electricals->pilot_lamps;?>">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    
                    </select>
                </div>         
                         
                     </div>
                 </div> 
                 
                <div style="clear: both;">
               <div class="panel-body" style="font-size: 18px;">Bike Photos</div>
              <div class="form-group">
                 <div class="col-md-3">
               <div class="fileinput fileinput-new" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span>Choose file</span>
        <input type="file" id="fileupload" name="bike_info[images][]" multiple/>
            
    </span>
    <span class="fileinput-filename"></span><span class="fileinput-new">
        
    </span>
</div>
</div>
 <div class="col-md-9">
 <div id="dvPreview">
 </div>
<div id="dvPreview1">
    <?php
               if(isset($json_bike->images)){
               $len = count($json_bike->images);    //var_dump($len);
            
               for($i=0;$i<$len;$i++){

        ?>
        <input type="text" name="image[<?php echo $i;?>]" value= "<?php echo $json_bike->images[$i];?>" hidden/>
        <img src="<?php echo base_url();?>uploads/bikes/<?php echo $json_bike->images[$i];?>" width="100px" height="100px">
        
               <?php }}else{ ?>No file chosen <?php } ?>
        </div>
              </div>

                  </div>
                  <div style="clear: both;"></div>
<div class="panel-body" style="font-size: 18px;">Bike Category</div>
                     <div class="form-group">
                 <div class="col-md-6">
                     
                   <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="latest" class="minimal" id="cat" <?php echo ($json_bike->category=='latest')?'checked':'';?>>&nbsp;Latest
    </label>  
                   <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="featured" class="minimal" id="cat" required <?php echo ($json_bike->category=='featured')?'checked':'';?>>&nbsp;Featured

    </label>
    <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="upcoming" class="minimal" id="cat" <?php echo ($json_bike->category=='upcoming')?'checked':'';?>>&nbsp;Upcoming
    </label>
    <label class="radio-inline">
      <input type="radio" name="bike_info[category]" value="recommended" class="minimal" id="cat" <?php echo ($json_bike->category=='recommended')?'checked':'';?>>&nbsp;Recommended
    </label>
                 </div>
                 <div class="col-md-3" id="upcoming-date" style="display: none">
                
                  <input type="text" class="form-control pull-right" name="bike_info[upcoming_date]"  id="datepicker" placeholder="Upcoming Date">
                 </div>
               </div>

                 <div style="clear: both;"></div>
                 <div class="panel-body" style="font-size: 18px;">Enter Details</div>
                <div class="form-group">
                 <textarea id="editor1" name="bike_info[details]" rows="10" cols="80">
                                            <?php echo $json_bike->details;?>
                    </textarea>
                </div>
               </div>
              
              <!-- /.box-body -->
               <div style="clear:both;">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Submit</button></center>
              </div>
            
          </div>
         
          </div>
              </div>
                  </div>
            </form>
            <?php } } ?>
          
         
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('include/footer.php');?>

    <script type="text/javascript">
	$(document).ready(function(){
  $(document).on("change","#select_bike",function(){
   var bike = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/fetch_bike_model",{bike_id:bike},function(o){
          // console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
	});  
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                                console.log(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>
      <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var bike = $('#select_bike option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/fetch_bike_variant",{bike_name:bike,model_id:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 

  <script type="text/javascript">
  	$(".select2").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
  <script type="text/javascript">
  	$("select").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
   <script type="text/javascript">
  	$(".select3").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>