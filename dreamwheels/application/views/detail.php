<?php include('header.php');?>

<?php if(isset($query)) { 
      $car_name = "";
                          $model_name = "";
                          $variant_name = "";
    foreach ($query as $car) { ?>
        <?php $car_info = json_decode($car->car_info);
                  $model_id = $car_info->model;
                  $variant_id = $car_info->variant;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
                  foreach ($query_variant as $variant) {
                  
                      if($variant->id==$variant_id){
                          $variant_name = $variant->variant;
                       }                      
                  }
                  $engine_transmission = json_decode($car->engine_transmission);
                  $tyres_brakes = json_decode($car->tyres_brakes);
                  $dimensions = json_decode($car->dimensions);
                  $appearance = json_decode($car->appearance);
                  $comfort = json_decode($car->comfort);
                  $safety_security = json_decode($car->safety_security);
                  ?>
        
         <?php // $additional_info = json_decode($car->additional_info);?>
    
 <?php }
 } ?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s"><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h1>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/new_car" class="b-breadCumbs__page">New Cars</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active"><?php echo $car_name,' ',$model_name,' ',$variant_name;?></a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row wow zoomInUp" data-wow-delay="0.5s">
					<div class="col-xs-3">
						<div class="b-infoBar__premium">Premium Listing</div>
					</div>
					
				</div>
			</div>
		</div><!--b-infoBar-->
       
		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h1>
								 
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="b-detail__head-price">
								<div class="b-detail__head-price-num"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $car_info->price;?></div>
								<p>Ex Showroom Price.</p>
							</div>
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s">
									<div class="row m-smallPadding">
										<div class="col-xs-10">
											<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
												 <?php  foreach($car_info->images as $img1) { ?>
												<li class="s-relative">                                        

													<img class="img-responsive center-block" src="<?php render_item_image("uploads/car/".$img1);?>" alt="<?php echo $car_name; ?> available on dreamwheels" />
												</li>
												
												<?php } ?>
												
											</ul>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical">
											<div class="b-detail__main-info-images-small" id="bx-pager">
												<?php  $i=0; foreach($car_info->images as $img) { ?>
												<a href="#" data-slide-index="<?php echo $i;?>" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="<?php render_item_image("uploads/car/".$img);?>" alt="<?php echo $car_name; ?> available on dreamwheels" />
												</a>
													<?php $i++; } ?>
												
											</div>
										</div>
									</div>
								</div>

								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#info1'>About this Car</a>
										
									</div>
									<div id="info1">
										<p><?php echo $car_info->details;?></p>
									</div>
									
									
									
								</div>
							
							</div>
							<h2 class="border-heading-h2 margin-bottom-20" id="specs-overview-div"><?php echo $car_name,' ',$model_name;?> Variants</h2>
							<div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" >
					<tbody>
				<?php	foreach($query_model_variants as $qmv){ 
	foreach ($query_variant as $variant) {
                  
                      if($variant->id==$qmv->variant){
                          $variant_name2 = $variant->variant;
                       }                      
                  } ?>
					<tr>
					    <td><div class="engineleft enginetext"><a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $qmv->id;?>"><?php echo $variant_name2;?></a></div><?php echo " ( ".ucfirst($qmv->fuel_type)." )";?></td>
						<td><div class="engineright"><i class="fa fa-inr"></i> <?php echo $qmv->price." (Ex showroom price)";?> </div></td>
					</tr>
			<?php } ?>		
					</tbody>
				</table>.
            </div>
          </div>
							
                                                    <h2 class="border-heading-h2 margin-bottom-20" id="specs-overview-div"><?php echo $car_name,' ',$model_name;?> Specifications</h2>
                                                    <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><h4 class="panel-title">
            <b>Overview</b>
        </h4></a>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?> </div></td></tr><tr><td><div class="engineleft enginetext">Engine Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Transmission</div></td><td><div class="engineright"><?php echo $engine_transmission->transmission;?></div></td></tr><tr><td><div class="engineleft enginetext">Air Bags</div></td><td><div class="engineright"><?php echo ucfirst($safety_security->airbags);?></div></td></tr><tr><td><div class="engineleft enginetext">Power Windows</div></td><td><div class="engineright"><?php echo $comfort->power_windows;?></div></td></tr><tr><td><div class="engineleft enginetext">ABS</div></td><td><div class="engineright"> <?php echo ucfirst($safety_security->abs);?></div></td></tr><tr><td><div class="engineleft enginetext">Fog Lamps</div></td><td><div class="engineright"><?php echo ucfirst($safety_security->fog_lamps);?></div></td></tr><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?> </div></td></tr></tbody></table>.
            </div>
          </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><h4 class="panel-title">
            <b>Engine And Transmission</b>
        </h4></a>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
          <div class="panel-body"> 
              <div class="table-responsive">
            <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?> kmpl</div></td></tr><tr><td><div class="engineleft enginetext">Maximum Power</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_power;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Torque</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_torque;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?></div></td></tr><tr><td><div class="engineleft enginetext">No. of Cylinders</div></td><td><div class="engineright"><?php echo $engine_transmission->no_of_cylinder;?></div></td></tr><tr><td><div class="engineleft enginetext">Drive Type</div></td><td><div class="engineright"><?php echo $engine_transmission->drive_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Transmission Type</div></td><td><div class="engineright"><?php echo $engine_transmission->transmission;?></div></td></tr></tbody></table>
              </div> 
          </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
            <b>Tyres And Brakes</b>
        </h4></a>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Tyre Size</div></td><td><div class="engineright"><p><?php echo $tyres_brakes->tyre_size;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Tyre Type</div></td><td><div class="engineright"><?php echo $tyres_brakes->tyre_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheel Size</div></td><td><div class="engineright"><p><?php echo $tyres_brakes->wheel_size;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->front_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->rear_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Front Suspension</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->suspension_front);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Suspension</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->suspension_rear);?></div></td></tr><tr><td><div class="engineleft enginetext">Steering Type</div></td><td><div class="engineright"><?php echo $tyres_brakes->steering_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Turning radius</div></td><td><div class="engineright"><?php echo $tyres_brakes->turning_radius;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><h4 class="panel-title">
            <b>Interior/Exterior Dimensions</b>
        </h4></a>
      </div>
      <div id="collapse4" class="panel-collapse collapse"> 
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Seating Capacity</div></td><td><div class="engineright"><?php echo $dimensions->seating_capacity;?></div></td></tr><tr><td><div class="engineleft enginetext">Doors</div></td><td><div class="engineright"><?php echo $dimensions->doors;?></div></td></tr><tr><td><div class="engineleft enginetext">Fuel Capacity</div></td><td><div class="engineright"><?php echo $dimensions->fuel_capacity;?> liters</div></td></tr><tr><td><div class="engineleft enginetext">Length Width Height</div></td><td><div class="engineright"><?php echo $dimensions->lwh;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheelbase</div></td><td><div class="engineright"><?php echo $dimensions->wheelbase;?></div></td></tr><tr><td><div class="engineleft enginetext">Ground Clearance</div></td><td><div class="engineright"><?php echo $dimensions->ground_clearance;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>  
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><h4 class="panel-title">
            <b>Interior / Exterior Appearance</b>
        </h4></a>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><?php if(isset($appearance->additional_interior_features)){?><tr><td><div class="engineleft enginetext">Additional Interior Features</div></td><td><div class="engineright"><?php echo $appearance->additional_interior_features;?></div></td></tr><?php } ?><tr><td><div class="engineleft enginetext">Ventilated Seats</div></td><td><div class="engineright"><?php echo $appearance->ventilated_seats;?></div></td></tr><tr><td><div class="engineleft enginetext">Dual Tone Dashboard</div></td><td><div class="engineright"><?php echo $appearance->dual_tone_dashboard;?></div></td></tr><tr><td><div class="engineleft enginetext">Fabric Upholstery</div></td><td><div class="engineright"><?php echo $appearance->fabric_upholstery;?></div></td></tr><tr><td><div class="engineleft enginetext">Leather Seats</div></td><td><div class="engineright"><?php echo $appearance->leather_seats;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Seat Headrest</div></td><td><div class="engineright"><?php echo $appearance->rear_seat_headrest;?></div></td></tr><tr><td><div class="engineleft enginetext">Trunk Opener</div></td><td><div class="engineright"><?php echo $appearance->trunk_opener;?></div></td></tr><tr><td><div class="engineleft enginetext">Alloy Wheels</div></td><td><div class="engineright"><?php echo $appearance->alloy_wheels;?></div></td></tr><?php if(isset($appearance->additional_exterior_features)){?><tr><td><div class="engineleft enginetext">Additional Exterior Features</div></td><td><div class="engineright"><?php echo $appearance->additional_exterior_features;?></div></td></tr><?php } ?><tr><td><div class="engineleft enginetext">Outside Rear View Mirror</div></td><td><div class="engineright"><?php echo $appearance->outside_rear_view_mirror;?></div></td></tr><tr><td><div class="engineleft enginetext">Turn indicators on ORVM</div></td><td><div class="engineright"><?php echo $appearance->turn_indicators;?></div></td></tr><tr><td><div class="engineleft enginetext">Xenon Headlights</div></td><td><div class="engineright"><?php echo $appearance->xenon_lights;?></div></td></tr><tr><td><div class="engineleft enginetext">Trunk Light</div></td><td><div class="engineright"><?php echo $appearance->trunk_light;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Spoiler</div></td><td><div class="engineright"><?php echo $appearance->rear_spoiler;?></div></td></tr><tr><td><div class="engineleft enginetext">Antenna</div></td><td><div class="engineright"><?php echo $appearance->antenna;?></div></td></tr><tr><td><div class="engineleft enginetext">Roof Rails</div></td><td><div class="engineright"><?php echo $appearance->roof_rails;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><h4 class="panel-title">
            <b>Comfort and Convenience</b>
        </h4></a>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Rear Curtain</div></td><td><div class="engineright"><?php echo $comfort->rear_curtain;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Steering Mounted Tripmeter</div></td><td><div class="engineright"><?php echo $comfort->steering_mounted_tripmeter;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Gear Shift Indicator</div></td><td><div class="engineright"><?php echo $comfort->gear_shift_indicator;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Luggage Hook and Net</div></td><td><div class="engineright"><?php echo $comfort->luggage_hook;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Tailgate Ajar</div></td><td><div class="engineright"><?php echo $comfort->tailgate_ajar;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Addtional Comfort Features</div></td><td><div class="engineright"><?php echo $comfort->additional_comfort_features;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">USB Charger</div></td><td><div class="engineright"><?php echo $comfort->usb_charger;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">AC</div></td><td><div class="engineright"><?php echo $comfort->ac;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Rear AC Ducts</div></td><td><div class="engineright"><?php echo $comfort->rear_ac_ducts;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Heater</div></td><td><div class="engineright"><?php echo $comfort->heater ;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Power Steering</div></td><td><div class="engineright"><?php echo $comfort->power_steering;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Steering Mounted Audio Control</div></td><td><div class="engineright"><?php echo $comfort->steering_audio_control;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Cruise Control</div></td><td><div class="engineright"><?php echo $comfort->cruise_control;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Adjustable Driver Seat</div></td><td><div class="engineright"><?php echo $comfort->adjustable_driver_seat;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Heated Seats Rear</div></td><td><div class="engineright"><?php echo $comfort->heated_seat_rear;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Heated Seat Front</div></td><td><div class="engineright"><?php echo $comfort->heated_seat_front;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Adjustable Front Passenger Seat</div></td><td><div class="engineright"><?php echo $comfort->ajustable_front_passenger_seat;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Lumbar Support</div></td><td><div class="engineright"><?php echo $comfort->lumbar_support;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Folding Rear Seat</div></td><td><div class="engineright"><?php echo $comfort->folding_rear_seat;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Rear Seat Center Armrest</div></td><td><div class="engineright"><?php echo $comfort->rearseat_armrest;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Tachometer</div></td><td><div class="engineright"><?php echo $comfort->tachometer;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Door & Trunk Ajar Warning</div></td><td><div class="engineright"><?php echo $comfort->door_trunk_ajar_warning;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Low Fuel Warning Lamp</div></td><td><div class="engineright"><?php echo $comfort->low_fuel_warning_lamp;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Seat Belt Warning</div></td><td><div class="engineright"><?php echo $comfort->seat_belt_warning;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Clock</div></td><td><div class="engineright"><?php echo $comfort->clock;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Push Ignition</div></td><td><div class="engineright"><?php echo $comfort->push_ignition;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">AM/FM Radio</div></td><td><div class="engineright"><?php echo $comfort->radio;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">USB Auxiliary Input</div></td><td><div class="engineright"><?php echo $comfort->aux;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">DVD Playback</div></td><td><div class="engineright"><?php echo $comfort->dvd_playback;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Touch-screen Display</div></td><td><div class="engineright"><?php echo $comfort->touch_screen_display;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">GPS Navigation System</div></td><td><div class="engineright"><?php echo $comfort->gps_navigation_system;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Speakers Rear</div></td><td><div class="engineright"><?php echo $comfort->speakers_rear;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Speakers Front</div></td><td><div class="engineright"><?php echo $comfort->speakers_front;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Auto Rain Sensing Wipers</div></td><td><div class="engineright"><?php echo $comfort->auto_rain_sensing;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Follow me home headlamps</div></td><td><div class="engineright"><?php echo $comfort->follow_me_home_headlamps;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Adjustable Head Lights</div></td><td><div class="engineright"><?php echo $comfort->adjustable_headlights;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Parking Sensors</div></td><td><div class="engineright"><?php echo $comfort->parking_sensors;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Remote Operated Fuel Tank Lid</div></td><td><div class="engineright"><?php echo $comfort->remote_fuel_lid;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Front Cupholders</div></td><td><div class="engineright"><?php echo $comfort->front_cupholders;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Cigarette Lighter</div></td><td><div class="engineright"><?php echo $comfort->cigarette_lights;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">12V Power Outlets</div></td><td><div class="engineright"><?php echo $comfort->power_outlets;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Bottle Holder</div></td><td><div class="engineright"><?php echo $comfort->bottle_holder;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Sun Roof</div></td><td><div class="engineright"><?php echo $comfort->sun_roof;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Moon Roof</div></td><td><div class="engineright"><?php echo $comfort->moon_roof;?></div></td></tr>
                   </tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><h4 class="panel-title">
            <b>Safety and Security</b>
        </h4></a>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Airbags</div></td><td><div class="engineright"><?php echo $safety_security->airbags;?></div></td></tr><tr><td><div class="engineleft enginetext">Seat Belts</div></td><td><div class="engineright"><?php echo $safety_security->seat_belts;?></div></td></tr><tr><td><div class="engineleft enginetext">Anti-lock Braking System (ABS)</div></td><td><div class="engineright"><?php echo $safety_security->abs;?></div></td></tr><tr><td><div class="engineleft enginetext">Electronic Brakeforce Distribution (EBD)</div></td><td><div class="engineright"><?php echo $safety_security->ebd;?></div></td></tr><tr><td><div class="engineleft enginetext">Brake Assist (BA)</div></td><td><div class="engineright"><?php echo $safety_security->ba;?></div></td></tr><tr><td><div class="engineleft enginetext">Electronic Stability Program (ESP)</div></td><td><div class="engineright"><?php echo $safety_security->esp;?></div></td></tr><tr><td><div class="engineleft enginetext">Traction Control System (TCS)</div></td><td><div class="engineright"><?php echo $safety_security->tcs;?></div></td></tr><tr><td><div class="engineleft enginetext">Engine Immobilizer</div></td><td><div class="engineright"><?php echo $safety_security->engine_immobilizer;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Child Safety Lock</div></td><td><div class="engineright"><?php echo $safety_security->child_safety_lock;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Fog Lamps </div></td><td><div class="engineright"><?php echo $safety_security->fog_lamps;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext">Rear Wash Wiper </div></td><td><div class="engineright"><?php echo $safety_security->rear_wash_wiper;?></div></td></tr>
                   <tr><td><div class="engineleft enginetext"> Rear Defogger </div></td><td><div class="engineright"><?php echo $safety_security->rear_defogger;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>                                                    
  </div>
						</div>
						<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Description</h2>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Engine Type</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->engine_type;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Fuel Type</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo ucfirst($engine_transmission->fuel_type);?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Mileage</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->mileage;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Engine Displacement</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->displacement;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Transmission</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->transmission;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Gear</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->gear_box;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Power</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_power;?></p>
										</div>
									</div>
									
									 
								</div>
								
								
								
								</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-detail-->


		<?php include('footer.php');?>