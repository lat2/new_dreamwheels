<?php include('header.php');
	  if(!isset($_SESSION['email'])){
			redirect('Welcome/car_sell_login');
			exit(0);
		} 	
?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.3s">Submit Your Vehicle</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s">
					<h3>Add Your Vehicle In Our Listings</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.3s">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="submit3.html" class="b-breadCumbs__page m-active">Submit a Vehicle</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
						<div class="b-infoBar__progress wow zoomInUp" data-wow-delay="0.3s">
							<div class="b-infoBar__progress-line clearfix">
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
									<div class="b-infoBar__progress-line-step-circle m-last">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<div class="b-submit">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
						<aside class="b-submit__aside">
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 1</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-car"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Add YOUR Vehicle</h4>
										<p>Select your vehicle &amp; add info</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 2</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-list-ul"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>select details</h4>
										<p>Choose vehicle specifications</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-photo"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>SUBMIT &amp; PUBLISH</h4>
										<p>Add images / details of vehicle</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<!--<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 4</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-user"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Contact details</h4>
										<p>Choose vehicle specifications</p>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>SUBMIT &amp; PUBLISH</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>-->
							<!--<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 5</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-globe"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>SUBMIT &amp; PUBLISH</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>-->
						</aside>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
						<div class="b-submit__main">
							<form action="<?php echo base_url();?>index.php/Welcome/submit_car_data" method="post" enctype="multipart/form-data">
								<div class="s-form">
									<div class="b-submit__main-file">
										<header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.3s">
											<h2>Upload Your Vehicle Photos</h2>
										</header>
										<p class=" wow zoomInUp" data-wow-delay="0.3s">You can upload upto 3 photos of your vehicle here.</p>
										<label class="b-submit__main-file-label btn m-btn wow zoomInUp" data-wow-delay="0.3s">
											<input type="file" id="fileupload" class="" name="additional_info[img][]" multiple/>
											<span>CHOOSE A  PHOTO</span>
											<span class="fa fa-angle-right"></span>
										</label>
										<label>Max. file size: 10 MB. Allowed images: jpg, gif, png.</label>
										<div id="dvPreview">
        </div>
									</div>
									
									<div class="b-submit__main-file wow zoomInUp" data-wow-delay="0.3s">
										<header class="s-headerSubmit s-lineDownLeft">
											<h2>Write Some Additional Comments About Your Vehicle</h2>
										</header>
										<p></p>
										<textarea name="comment" placeholder="write additional comments" class="comments"></textarea>
									</div>
								</div>
								<div class="row">
										<div class="col-md-5 col-xs-12">
											<div class="b-submit__main-element">
												<?php 
                       
                        if (isset($_SESSION['phone'])) { ?> 
												<input type="hidden" name="user_info[name]" value="<?php echo $_SESSION['name'];?>" />
												<input type="hidden" name="user_info[phone]" value="<?php echo $_SESSION['phone'];?>" />
												<?php } ?>
										</div>
									</div>
								</div>
									<div class="row">
                                        <div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
											<input type="checkbox" name="check1" id="check1" required="">
<!--											<label class="s-submitCheckLabel" for="check1" style="border-radius: 0%!important;"><span class="fa fa-check"></span></label>--> 
											<label class="s-submitCheck" for="check1">All the informations are submitted by me are true as per my knowledge. </label>
										</div>
									</div>
								
								<div class="s-submit">
								<button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s">PUBLISH MY LISTING NOW<span class="fa fa-angle-right"></span></button>
								
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-submit-->

			<?php include('footer.php');?>

			<script>
    $("#fileupload").on("change", function() {
         if($("#fileupload")[0].files.length > 3) {
                   alert("You can select only 3 images");
                   event.preventdefault();
         }
    });
</script>