<?php require('header.php');?>
<section class="b-slider"> 
			<div id="carousel" class="slide carousel carousel-fade">
				<div class="carousel-inner">
					<div class="item next left">
						<img src="<?php echo base_url(); ?>media/main-slider/bike3.jpg" alt="sliderImg">
						
					</div>
				<div class="item active left">
						<img src="<?php echo base_url(); ?>media/main-slider/bike3.jpg" alt="sliderImg">
						
					</div>
					<div class="item">
						<img src="<?php echo base_url(); ?>media/main-slider/bike3.jpg" alt="sliderImg">
						
					</div>
                                    						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h1 class="form-title">Choose vehicle</h1>
								 
								<div class="form-group" id="bike-car-icon">
                                <div class="col-sm-2 col-sm-offset-3" style="width: 112px;"> 
      <a href="<?php echo base_url();?>">	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="47.6 310.9 500 220" enable-background="new 47.6 310.9 500 220" xml:space="preserve">
											<g style="fill: grey;">
												<path d="M258.3,343.6H277v41.2h-25.2l7.8-40.8L258.3,343.6L258.3,343.6z M295.4,343.6h39.4c18.3,1.8,33,8.7,47.2,19.7
													c-1.4,0.5-2.7,1.4-4.1,2.3v0.5c-3.7,3.7-5.5,13.3-5.5,18.8h-76.9V343.6L295.4,343.6z"></path>
												<path d="M107.6,476.4c-20.6,0-46.2-7.8-47.6-8.2l-2.7-1.4v-39.8l2.3-1.4c1.8-1.8,6.9-7.3,6.9-11v-7.8c0.5-5.5,0.5-11.9,0.9-17.4
													c0-3.7,6-11,11.9-13.3l1.8-0.5h146.5l7.3-38.9c1.4-6.9,6.9-11.4,14.2-11.4h86.5c27.5,2.3,43.5,15.6,61.4,29.8
													c8.2,6.4,16.5,15.1,26.1,21.1c4.1,0.5,10.5,0.5,17.9,0.9c43.5,1.8,83.3,4.6,87.5,24.3l4.1,34.8c2.7,1.8,6.4,5.5,6.4,10.5v15.1
													c0,4.1-0.5,6.4-4.1,9.2c-6.9,4.1-14.7,6-30.7,6h-6.4v-9.2h6.9c13.7,0,20.1-0.9,25.2-4.1c0,0,0,0.5,0-1.4V447c0-1.4-2.3-2.7-3.2-3.2
													l-2.7-0.9l-4.6-39.4c-3.2-13.3-54-15.1-78.3-16c-8.2-0.5-15.1-0.5-19.7-0.9l-1.8-0.5c-10.5-6.4-19.7-16-27.9-22.9
													c-16.9-13.7-31.6-25.6-55.4-27.5h-87.5c-2.3,0-4.1,0.9-4.6,3.2l-8.7,47.2H81.5c-2.7,1.4-5,4.1-5.5,5c0,4.6-0.5,11.4-0.5,16.9v7.3
													c0,6.9-6.4,13.3-9.2,16v28.4c7.3,2.7,26.6,8.7,41.7,8.2h8.7v9.2h-8.2C108.1,476.4,108.1,476.4,107.6,476.4L107.6,476.4z
													M423.6,476.4H185.5v-9.2h238.1V476.4L423.6,476.4z"></path>
												<polygon points="529.3,444.3 523.4,407.7 515.1,407.7 515.1,444.3 	"></polygon>
												<polygon points="327.4,398.5 304.5,398.5 304.5,393.9 327.4,393.9 	"></polygon>
												<path d="M70.5,412.2h5c9.6,0,22.4-5.5,22.9-14.2v-8.7H72.3C72.3,397.6,71,406.3,70.5,412.2L70.5,412.2z"></path>
												<path d="M381,368.7L381,368.7c-2.7,2.7-4.1,12.4-4.1,16c0,1.4,0.5,2.3,1.4,3.2c0.9,0.9,1.8,1.4,3.2,1.4H392c11.4,0-1.8-20.1-6-21.5
													C384.2,366.9,382.4,367.4,381,368.7L381,368.7z"></path>
												<polygon points="222.1,443.4 222.1,453.5 394.3,453.5 	"></polygon>
												<path d="M405.2,471.8h-4.6v-12.4c0-33.4,25.6-60.4,59.1-60.4s60,27,60,60.4v12.4h-4.6v-13.3c0-29.8-25.6-53.1-55.4-53.1
													c-29.8,0-54.5,24.3-54.5,54V471.8L405.2,471.8z"></path>
												<path d="M460.7,457.1c-6,0-10.5,4.6-10.5,10.5s4.6,10.5,10.5,10.5c6,0,10.5-4.6,10.5-10.5S466.6,457.1,460.7,457.1L460.7,457.1z"></path>
												<path d="M210.2,471.8h-6.4v-12.4c0-29.8-23.8-54-53.6-54s-51.7,24.3-51.7,54v12.4h-9.2V459c0-33.4,27-60,60.4-60
													s62.7,25.6,62.7,59.1L210.2,471.8L210.2,471.8z"></path>
												<path d="M460.7,413.2L460.7,413.2c12.8,0,24.3,5,32.5,13.7h-0.9c8.2,8.2,13.7,19.7,13.7,32.5v14.2v3.2h-4.6H500
													c-2.3,7.8-6.9,15.1-12.8,20.1c-6.9,6-16,11.4-26.1,11.4s-19.2-3.7-26.1-9.6V497c-6-5-10.5-12.4-12.8-20.1h-2.3h-5v-4.6v-12.4
													c0-12.8,5-24.3,13.7-32.5h0.5C436.8,418.2,448.3,413.2,460.7,413.2L460.7,413.2L460.7,413.2z M460.7,440.6c-14.7,0-27,11.9-27,27
													c0,14.7,11.9,27,27,27c14.7,0,27-11.9,27-27C487.7,452.5,475.8,440.6,460.7,440.6L460.7,440.6L460.7,440.6z M152,440.6
													c-14.7,0-27,11.9-27,27c0,14.7,11.9,27,27,27c14.7,0,27-11.9,27-27C179,452.5,166.7,440.6,152,440.6L152,440.6L152,440.6z
													M152,413.2L152,413.2c12.8,0,24.3,5,32.5,13.7h0.9c8.2,8.2,13.7,19.7,13.7,32.5v14.2v3.2h-4.6h-3.7c-2.3,7.8-6.9,15.1-12.8,20.1
													c-6.9,6-16,11.4-26.1,11.4c-10.1,0-19.2-5.5-26.1-11.4c-6-5-10.5-12.4-12.8-20.1h-2.3h-3.2v-4.6v-12.4c0-12.8,5-24.3,13.7-32.5
													C129.6,418.2,139.2,413.2,152,413.2L152,413.2z"></path>
												<path d="M152,457.1c-6,0-10.5,4.6-10.5,10.5s4.6,10.5,10.5,10.5c6,0,10.5-4.6,10.5-10.5S158,457.1,152,457.1L152,457.1z"></path>
											</g>
										</svg></a></div>
                                <div class="col-sm-2" style="margin-top: -12px;">

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 400.261 400.261" style="enable-background:new 0 0 400.261 400.261;" xml:space="preserve" width="75px" height="55px">
<path d="M343.653,167.446c0.103-0.514,0.161-1.039,0.161-1.569v-41.77c0-30.715-30.281-44.692-46.293-47.792  c-2.344-0.456-4.77,0.163-6.611,1.683c-1.842,1.52-2.909,3.783-2.909,6.171v36.235c-4.711-2.728-10.173-4.298-16-4.298h-55.542  c-17.645,0-32,14.332-32,31.948c0,0.066,0.005,0.132,0.005,0.198h-40.447c-1.705,0-7.512-0.353-10.902-4.884l-22.57-32.095  c-1.498-2.13-3.939-3.398-6.544-3.398H30.469c-7.322,0-13.027,2.98-15.652,8.176c-2.615,5.178-1.644,11.512,2.663,17.388  l27.459,38.624C18.352,185.106,0,212.447,0,244.001c0,44.162,35.929,80.091,80.091,80.091c32.877,0,61.177-19.928,73.493-48.337  h79.254c4.106,0,8.508-0.905,12.78-2.479c11.875,30.348,41.274,50.816,74.552,50.816c44.162,0,80.091-35.929,80.091-80.091  C400.261,208.504,376.669,177.591,343.653,167.446z M216.458,132.107H272c8.822,0,16,7.154,16,15.948  c0,8.794-7.178,15.948-16,15.948h-55.542c-8.822,0-16-7.154-16-15.948C200.458,139.262,207.636,132.107,216.458,132.107z   M80.091,308.092c-35.34,0-64.091-28.751-64.091-64.091c0-35.334,28.751-64.081,64.091-64.081c14.694,0,28.239,4.985,39.06,13.337  l-11.589,11.683c-7.991-5.645-17.502-8.7-27.476-8.7c-26.34,0-47.769,21.427-47.769,47.764c0,26.34,21.429,47.769,47.769,47.769  c26.334,0,47.758-21.429,47.758-47.769c0-4.418-3.582-8-8-8s-8,3.582-8,8c0,17.517-14.247,31.769-31.758,31.769  c-17.517,0-31.769-14.251-31.769-31.769c0-17.515,14.251-31.764,31.769-31.764c5.698,0,11.17,1.499,15.969,4.301l-21.654,21.832  c-3.112,3.137-3.091,8.202,0.046,11.313c1.56,1.548,3.597,2.32,5.634,2.32c2.058,0,4.116-0.79,5.68-2.366l44.746-45.112  c8.542,10.889,13.655,24.591,13.655,39.474C144.162,279.341,115.42,308.092,80.091,308.092z M158.598,259.755  c1.02-5.096,1.564-10.362,1.564-15.754c0-44.157-35.92-80.081-80.071-80.081c-6.754,0-13.312,0.849-19.582,2.431l-30.024-42.231  c-0.027-0.038-0.054-0.075-0.081-0.112c-0.032-0.044-0.063-0.087-0.094-0.129c0.052,0,0.105-0.001,0.16-0.001h69.376l20.239,28.78  c0.035,0.049,0.069,0.097,0.105,0.146c7.834,10.625,20.218,11.451,23.827,11.451h44.876c5.573,9.417,15.84,15.75,27.566,15.75H272  c17.645,0,32-14.332,32-31.948c0-1.002-0.052-1.992-0.143-2.97c0.091-0.482,0.143-0.978,0.143-1.486V95.032  c9.853,4.008,23.813,12.595,23.813,29.076v15.948h-7.446c-4.418,0-8,3.582-8,8s3.582,8,8,8h7.446v6.827l-3.792,4.355  c-0.015,0.017-0.03,0.034-0.044,0.051l-35.985,41.336c-2.749,2.505-5.203,5.327-7.315,8.402L252.3,249.624  c-0.142,0.163-0.277,0.332-0.405,0.505c-3.714,5.038-12.797,9.625-19.056,9.625H158.598z M292.828,227.685l21.659,21.94  c1.565,1.585,3.629,2.38,5.694,2.38c2.03,0,4.061-0.768,5.62-2.307c3.145-3.104,3.177-8.169,0.073-11.313l-21.657-21.939  c4.747-2.774,10.262-4.373,16.146-4.373c17.712,0,32.122,14.407,32.122,32.116c0,17.712-14.41,32.122-32.122,32.122  c-17.706,0-32.111-14.41-32.111-32.122C288.25,238.159,289.924,232.512,292.828,227.685z M320.17,308.092  c-27.483,0-51.662-17.44-60.528-43.01c1.862-1.61,3.534-3.343,4.92-5.176l8.124-9.332c3.133,23.527,23.311,41.739,47.675,41.739  c26.535,0,48.122-21.587,48.122-48.122c0-26.531-21.587-48.116-48.122-48.116c-0.079,0-0.156,0.006-0.235,0.006l12.927-14.849  c29.47,6.02,51.208,32.282,51.208,62.77C384.261,279.341,355.51,308.092,320.17,308.092z" fill="#f41818"></path>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg></div>
								</div>
								<div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#bikes" class="btn btn-default active" data-toggle="tab" id="new">
          <input type="radio" />New Bikes
        </a>
        <a href="#bikes" class="btn btn-default" data-toggle="tab" id="used">
          <input type="radio" />Used Bikes
        </a>
        <input type="hidden" id="vehicle_condition" value="new">
      </div>
       <div class="tab-content">
        <div class="tab-pane active" id="bikes">
			<div class="form-group col-xs-offset-2" style="padding-top: 10px;"> 
	<label class="radio-inline">
	 <input type="radio" name="optradio" value="by_budget" checked="checked" class="radion-opt search_flow">By Budget
	</label>
	<label class="radio-inline">
	 <input type="radio" name="optradio" value="by_brand" class="radion-opt search_flow">By Brand
	</label></div>
	
	<!-- By Budget -->
       <div class="mobile-option" id="by_budget">
       <div class="form-group">
       <select name="newBikeBrandSelect" id="newBikeBrandSelect" class="budget form-control">
	      <option value="default">-Select Budget-</option>
		  <option value="low_to_high">Low to High Budget</option>
          <option value="high_to_low">High to Low Budget</option>
		  <option value="50000">0-50000 Thousand</option>
		  <option value="100000">50000 Thousand-1 Lakh</option>
          <option value="120000">1 Lakh Above</option>
	    </select>
        </div>
           
          <div class="form-group">
	 <select name="newBikeBrandSelect" id="newBikeBrandSelect" class="vehicle_type form-control">
	  <option value="default">-Select Vehicle Types-</option>
          <option value="all_type">All Bikes</option>
          <option value="sport">Sport</option>
	  <option value="bikes">Bikes</option>
	  <option value="scooter">Scooter</option>
	  <option value="cruiser">Cruiser</option>
	 </select>
	 
	</div>
           
		</div>
       <!-- End By Budget --> 

     <!-- By Brand -->	   
<div class="mobile-option" style="display: none;" id="by_brand">
<div class="form-group budget bike_brand">
 <div class="col-sm-12 col-xs-12">
<?php  if(isset($query_bike_name)) { ?>
<select class="brand select_bike form-control" name="newBikeBrandSelect" id="newBikeBrandSelect">
<option value="default">-Select Brand-</option>
<option value="default">All Brands</option>
<?php foreach ($query_bike_name as $bikes_name) { ?>
<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
													<?php } } ?>
</select>
</div>
</div>
</div>
<!-- End By Brand -->	

</div>
</div>
	 
    <div class="form-group">
      <div class="col-sm-12 col-xs-12">
          <button class="btn btn-danger btn-lg center-block" id="search_button" style="margin-top: 15px;"><i class="fa fa-search"></i> Search</button>
      </div>
	</div>
</div>
      
</div>
                                    
                                              <div class="carousel-caption b-slider__info2" style="box-shadow:none;width: 24% !important">
	      <form class="b-blog__aside-search wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>" action="<?php echo base_url();?>index.php/Welcome/vehicle_search" method="GET" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
								<div>
                                                                    <input type="text" name="search" value="" placeholder="Search Cars or Brands eg. Verna, or Hyundai" style="border-radius:0px !important">
									<button type="submit"><span class="fa fa-search"></span></button>
									
									<input type="hidden" name="type" value="">
								</div>        
							</form>
	  </div>
				</div>
				<a class="carousel-control right" href="#carousel" data-slide="next">
					<span class="fa fa-angle-right m-control-right"></span>
				</a>
				<a class="carousel-control left" href="#carousel" data-slide="prev">
					<span class="fa fa-angle-left m-control-left"></span>
				</a>
			</div>
		</section>

<section class="b-search">
<div class="container">
                            
		  <form action="<?php echo base_url(); ?>index.php/Welcome/vehicle_search" method="GET" class="b-search__main">
					<div class="b-search__main-title wow zoomInUp" data-wow-delay="0.3s">
						<h2>Unsure which vehicle you are looking for? Find it here.</h2>
					</div>
					  <div class="b-search__main-type wow zoomInUp" data-wow-delay="0.3s">
						<div class="col-xs-12 col-md-2 s-noLeftPadding">
							<h4>I am looking for</h4>
						</div>
						<div class="col-xs-12 col-md-10">
							<div class="row">
								<div class="col-xs-2">
<a id="anchor_tag" href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=bike&vehicle_budget=default&vehicle_type=sport&vehicle_condition=new&vehicle_brand=default&vehicle_model=default" style="text-decoration: none;">
									<!--<input id="type1" type="radio" name="type" />-->
									<label for="type1" class="b-search__main-type-svg">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="130px" height="55px" viewBox="0 0 467.168 467.168" style="enable-background:new 0 0 467.168 467.168;" xml:space="preserve" class=""><g><g>
	<g>
		<path d="M76.849,210.531C34.406,210.531,0,244.937,0,287.388c0,42.438,34.406,76.847,76.849,76.847    c30.989,0,57.635-18.387,69.789-44.819l18.258,14.078c0,0,134.168,0.958,141.538-3.206c0,0-16.65-45.469,4.484-64.688    c2.225-2.024,5.021-4.332,8.096-6.777c-3.543,8.829-5.534,18.45-5.534,28.558c0,42.446,34.403,76.846,76.846,76.846    c42.443,0,76.843-34.415,76.843-76.846c0-42.451-34.408-76.849-76.843-76.849c-0.697,0-1.362,0.088-2.056,0.102    c5.551-3.603,9.093-5.865,9.093-5.865l-5.763-5.127c0,0,16.651-3.837,12.816-12.167c-3.848-8.33-44.19-58.28-44.19-58.28    s7.146-15.373-7.634-26.261l-7.098,15.371c0,0-18.093-12.489-25.295-10.084c-7.205,2.398-18.005,3.603-21.379,8.884l-3.358,3.124    c0,0-0.95,5.528,4.561,13.693c0,0,55.482,17.05,58.119,29.537c0,0,3.848,7.933-12.728,9.844l-3.354,4.328l-8.896,0.479    l-16.082-36.748c0,0-15.381,4.082-23.299,10.323l1.201,6.24c0,0-64.599-43.943-125.362,21.137c0,0-44.909,12.966-76.37-26.897    c0,0-0.479-12.968-76.367-10.565l5.286,5.524c0,0-5.286,0.479-7.444,3.841c-2.158,3.358,1.2,6.961,18.494,6.961    c0,0,39.153,44.668,69.17,42.032l42.743,20.656l18.975,32.42c0,0,0.034,2.785,0.23,7.045c-4.404,0.938-9.341,1.979-14.579,3.09    C139.605,232.602,110.832,210.531,76.849,210.531z M390.325,234.081c29.395,0,53.299,23.912,53.299,53.299    c0,29.39-23.912,53.294-53.299,53.294c-29.394,0-53.294-23.912-53.294-53.294C337.031,257.993,360.932,234.081,390.325,234.081z     M76.849,340.683c-29.387,0-53.299-23.913-53.299-53.295c0-29.395,23.912-53.299,53.299-53.299    c22.592,0,41.896,14.154,49.636,34.039c-28.26,6.011-56.31,11.99-56.31,11.99l3.619,19.933l55.339-2.444    C124.365,322.116,102.745,340.683,76.849,340.683z M169.152,295.835c1.571,5.334,3.619,9.574,6.312,11.394l-24.696,0.966    c1.058-3.783,1.857-7.666,2.338-11.662L169.152,295.835z"data-original="#000000" class="active-path" data-old_color="##86868fill:##86868fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000" fill="##86868"/>
	</g>
</g></g> </svg>
									</label>
									<h5><label for="type1" class="vehicle_type">Sport</label></h5>
									</a>
								</div>
								<div class="col-xs-2">
<a id="anchor_tag" href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=bike&vehicle_budget=default&vehicle_type=scooter&vehicle_condition=new&vehicle_brand=default&vehicle_model=default" style="text-decoration: none;">
									<!--<input id="type2" type="radio" name="type" />-->
									<label for="type2" class="b-search__main-type-svg">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 458.745 458.745" style="enable-background:new 0 0 458.745 458.745;" xml:space="preserve" width="130px" height="55px" class=""><g><g>
	<path d="M386.172,303.19c-16.701,0-30.288,13.587-30.288,30.289c0,16.701,13.587,30.288,30.288,30.288s30.288-13.587,30.288-30.288   C416.46,316.778,402.873,303.19,386.172,303.19z M386.172,348.767c-8.43,0-15.288-6.858-15.288-15.288   c0-8.43,6.858-15.289,15.288-15.289s15.288,6.858,15.288,15.289C401.46,341.908,394.602,348.767,386.172,348.767z" data-original="#000000" class="active-path" data-old_color="##86868fill:##86868fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000" fill="##86868"/>
	<path d="M457.003,274.505c-17.591-21.089-43.408-33.184-70.831-33.184c-4.595,0-9.11,0.343-13.526,0.995   c-3.044-19.741-21.612-45.869-39.606-67.244v-63.534c0-2.341-1.094-4.548-2.956-5.967c-1.864-1.42-4.286-1.885-6.538-1.263   l-12.224,3.372c-6.525,1.8-12.228,5.354-16.649,10.063l-6.421-29.535c-0.75-3.448-3.801-5.907-7.329-5.907h-13.093V71.497   c0-4.142-3.357-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v37.753c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5V97.301h7.048l9.238,42.495   c-3.953,4.174-6.626,9.346-7.751,15.042l-21.583,23.726c-2.787,3.064-2.563,7.808,0.501,10.595   c1.438,1.308,3.244,1.952,5.045,1.952c2.038,0,4.07-0.826,5.55-2.453l13.032-14.325c0.946,1.908,2.094,3.734,3.448,5.445   c8.495,10.734,20.248,27.153,27.999,44.422c8.361,18.627,10.337,34.108,5.873,46.013c-6.517,17.376-25.587,30.745-51.013,35.761   c-27.141,5.357-54.024-0.354-66.888-14.207c-5.667-6.103-8.205-12.161-7.759-18.52c0.793-11.308,10.844-24.881,29.872-40.342   c0.023-0.019,1.558-1.738,1.578-1.768c5.875-8.458,6.613-19.352,1.89-28.529c-4.779-9.287-14.237-15.056-24.682-15.056H91.014   c-20.699,0-37.722,15.993-39.388,36.27c-4.312,4.385-14.373,15.119-24.521,29.552C9.119,278.953,0,302.825,0,324.326   c0,4.142,3.357,7.5,7.5,7.5h16.016h9.65c-0.015,0.551-0.035,1.101-0.035,1.652c0,33.784,27.485,61.269,61.269,61.269   c28.427,0,52.391-19.462,59.276-45.761h173.22c6.886,26.298,30.849,45.761,59.276,45.761c33.784,0,61.27-27.485,61.27-61.269   c0-12.38-3.703-23.906-10.042-33.552l18.518-14.751c1.579-1.258,2.584-3.099,2.789-5.106   C458.91,278.06,458.296,276.054,457.003,274.505z M155.715,202.551h43.513c4.873,0,9.114,2.587,11.344,6.92   c1.672,3.248,1.888,6.845,0.699,10.113h-55.556V202.551z M91.014,202.551h49.701v17.033H67.652   C70.829,209.714,80.101,202.551,91.014,202.551z M94.399,379.748c-25.513,0-46.269-20.756-46.269-46.269   c0-0.552,0.016-1.103,0.035-1.652h15.991c-0.029,0.548-0.046,1.099-0.046,1.652c0,16.702,13.587,30.289,30.288,30.289   s30.289-13.587,30.289-30.289c0-0.553-0.017-1.104-0.046-1.652h15.992c0.019,0.55,0.035,1.1,0.035,1.652   C140.669,358.991,119.912,379.748,94.399,379.748z M79.111,333.479c0-0.556,0.033-1.107,0.091-1.652h30.395   c0.059,0.545,0.091,1.096,0.091,1.652c0,8.43-6.858,15.289-15.289,15.289C85.97,348.768,79.111,341.909,79.111,333.479z    M143.619,316.826H33.426c2.48-7.17,7.068-17.701,14.978-27.849c18.106-23.232,44.589-33.798,78.692-31.398   c15.116,1.063,24.613,5.046,28.229,11.839C160.05,278.299,156.111,294.238,143.619,316.826z M294.026,333.987h-138.37   c0.001-0.17,0.013-0.338,0.013-0.508c0-2.277-0.138-4.546-0.386-6.786c11.612-20.377,23.176-45.731,13.282-64.321   c-6.204-11.659-19.803-18.306-40.417-19.755c-49.421-3.476-77.299,18.568-91.987,37.672c-10.867,14.132-16.18,28.598-18.491,36.539   h-2.223c4.271-36.595,37.897-73.257,46.645-82.242h134.131c-13.002,13.279-19.779,25.68-20.616,37.613   c-0.742,10.577,3.204,20.595,11.729,29.776c16.521,17.792,48.231,25.138,80.783,18.716c10.553-2.082,20.231-5.461,28.726-9.909   c-1.847,7.264-2.832,14.868-2.832,22.698C294.014,333.648,294.025,333.817,294.026,333.987z M315.31,122.14l2.729-0.753v36.686   c-7.712-8.365-14.258-14.95-17.953-18.589C301.25,131.298,307.175,124.384,315.31,122.14z M294.118,170.468   c-2.605-3.291-3.753-7.403-3.231-11.578c0.249-2,0.865-3.892,1.805-5.604c21.769,21.912,63.111,68.162,65.292,92.45   c-8.806,2.836-17.037,6.962-24.471,12.157C333.936,238.347,325.374,209.961,294.118,170.468z M386.172,379.748   c-25.513,0-46.27-20.756-46.27-46.269s20.757-46.269,46.27-46.269s46.27,20.756,46.27,46.269S411.685,379.748,386.172,379.748z    M427.696,288.478c-10.926-10.09-25.515-16.269-41.524-16.269c-33.784,0-61.27,27.485-61.27,61.269c0,0.17,0.011,0.338,0.013,0.508   h-15.89c-0.001-0.17-0.012-0.338-0.012-0.508c0-42.545,34.613-77.158,77.158-77.158c20.385,0,39.713,7.985,54.104,22.137   L427.696,288.478z" data-original="#000000" class="active-path" data-old_color="##86868fill:##86868fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000" fill="##86868"/>
</g></g> </svg>
									</label>
									<h5><label for="type2" class="vehicle_type">Scooters</label></h5>
									</a>
								</div>
								<div class="col-xs-2">
<a id="anchor_tag" href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=bike&vehicle_budget=default&vehicle_type=bikes&vehicle_condition=new&vehicle_brand=default&vehicle_model=default" style="text-decoration: none;">
									<!--<input id="type3" type="radio" name="type" />-->
									<label for="type3" class="b-search__main-type-svg">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="130px" height="55px" class=""><g><g>
	<g>
		<path d="M422,256c-13.167,0-25.683,2.843-36.966,7.946l-13.776-20.542C390.777,232.294,413.294,226,437,226    c8.284,0,15-6.716,15-15v-30c0-57.897-47.103-105-105-105h-30c-8.284,0-15,6.716-15,15s6.716,15,15,15h15v30h-46    c-5.015,0-9.699,2.507-12.481,6.68L257.972,166H133.028l-7.532-11.298C117.688,142.992,104.625,136,90.55,136H15    c-8.284,0-15,6.716-15,15c0,38.538,29.218,70.381,66.666,74.54l20.344,30.515C38.763,257.639,0,297.375,0,346    c0,49.626,40.374,90,90,90c38.422,0,72.331-24.601,84.861-60c9.005,0,80.148,0,89.005,0c19.951,0,37.272-14.199,41.184-33.762    l2.125-10.626c5.57-27.849,19.914-52.22,39.828-70.514l13.139,19.593C342.821,297.103,332,320.311,332,346    c0,49.626,40.374,90,90,90c49.626,0,90-40.374,90-90C512,296.374,471.626,256,422,256z M237.972,196l-20,30h-44.944l-20-30    H237.972z M90,406c-33.084,0-60-26.916-60-60s26.916-60,60-60c2.706,0,5.381,0.192,8.024,0.543C76.512,289.906,60,308.56,60,331    c0,24.813,20.187,44.9,45,44.9h36.954C131.451,394.002,111.821,406,90,406z M105,346c-8.271,0-15-6.729-15-15s6.729-15,15-15    c8.277,0,72.52,0,81.729,0l15.001,30C200.198,346,107.753,346,105,346z M277.757,325.728l-2.125,10.626    c-1.118,5.59-6.067,9.646-11.767,9.646c-12.191,0-1.964,0-21.177,0c-4.575,0-8.688-2.542-10.733-6.633l-22.539-45.075    C206.875,289.21,201.682,286,196,286h-31h-15.55c-4.021,0-7.754-1.998-9.984-5.344c-6.634-9.951-45.675-68.512-51.985-77.977    C84.699,198.507,80.015,196,75,196c-19.556,0-36.239-12.539-42.43-30h57.98c4.021,0,7.754,1.998,9.985,5.344l12.011,18.016    c0.01,0.016,0.021,0.031,0.031,0.047l39.943,59.914c2.781,4.172,7.465,6.679,12.48,6.679h61c5.015,0,9.699-2.507,12.481-6.679    l39.942-59.914c0.011-0.016,0.021-0.032,0.032-0.048l15.572-23.459h52.92c0.018,0,0.035,0,0.052,0c8.284,0,15-6.616,15-14.9    v-43.493c34.191,6.969,60,37.275,60,73.493v15.69C351.231,203.221,291.808,255.472,277.757,325.728z M422,406    c-33.084,0-60-26.916-60-60c0-15.29,5.758-29.255,15.207-39.862l32.335,48.217c4.615,6.882,13.935,8.717,20.813,4.104    c6.88-4.614,8.718-13.932,4.104-20.813l-32.351-48.241C408.336,287.208,415.028,286,422,286c33.084,0,60,26.916,60,60    S455.084,406,422,406z"  data-original="#000000" class="active-path" data-old_color="##86868fill:##86868fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000" fill="##86868"/>
	</g>
</g></g> </svg>
									</label>
									<h5><label for="type3" class="vehicle_type">Bikes</label></h5>
									</a>
								</div>
								<div class="col-xs-2">
<a id="anchor_tag" href="<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow=by_budget&vehicle=bike&vehicle_budget=default&vehicle_type=cruiser&vehicle_condition=new&vehicle_brand=default&vehicle_model=default" style="text-decoration: none;">
									<!--<input id="type4" type="radio" name="type" />-->
									<label for="type4" class="b-search__main-type-svg">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="130px" height="55px" viewBox="0 0 486.486 486.486" style="enable-background:new 0 0 486.486 486.486;" xml:space="preserve" class=""><g><g>
	<g>
		<path d="M385.16,224.833c-8.327,2.793-15.945,6.915-22.73,12.041L322.9,191.917l-17.656-20.081l-10.396-11.826    c-1.303,0.622-2.376,1.281-3.442,1.94c-8.416-10.878-17.633-20.606-30.353-26.99c-17.235-8.642-37.82-3.184-55.579-10.187    c-11.327-4.466-16.188,13.649-5.017,18.054c11.143,4.396,22.403,4.899,34.268,5.466c21.724,1.042,33.722,13.695,45.688,30.176    c0.758,1.054,1.655,1.847,2.597,2.473c-1.042,2.827-6.087,4.036-6.087,4.036c-19.224-47.063-60.012-1.491-97.88,38.569    c-11.219,1.302-21.752,4.474-29.25,12.2c-9.185,9.466-23.363,24.342-32.398,6.474c-3.737-7.377-11.558-8.83-18.899-9.804    c-6.765-0.901-9.678,4.623-8.822,9.925c-32.495-20.24-73.621,10.994-73.621,10.994l8.165,4.067C9.567,267.954,0,285.034,0,304.369    c0,32.06,26.203,58.048,58.532,58.048c18.959,0,35.764-8.977,46.458-22.838c27.493,5.045,91.552,2.717,119.696,2.717    c33.979,0,37.441-20.603,37.441-20.603s33.987-107.367,35.566-117.044c1.57-9.672,8.179,2.184,8.179,2.184l10.388-5.623    c0,0,17.765,20.261,37.91,42.861c-5.995,6.025-11.101,12.986-14.964,20.817c-9.321,18.871-10.716,40.23-3.919,60.147    c1.113,3.266,4.163,5.313,7.43,5.313c0.837,0,1.694-0.136,2.528-0.421c4.104-1.395,6.304-5.867,4.893-9.975    c-5.441-15.921-4.323-33.021,3.135-48.121c2.969-6.003,6.824-11.385,11.356-16.062c1.227,1.358,2.437,2.705,3.647,4.063    c-10.564,10.792-17.096,25.491-17.096,41.722c0,33.093,27.046,59.919,60.411,59.919c33.37,0,60.42-26.834,60.42-59.919    c0-33.09-27.054-59.921-60.42-59.921c-13.116,0-25.223,4.19-35.121,11.231l-3.671-4.182c5.214-3.803,11.041-6.847,17.356-8.967    c33.394-11.197,69.749,6.586,81.05,39.674c1.396,4.1,5.855,6.295,9.967,4.893c4.092-1.398,6.292-5.867,4.89-9.967    C471.98,233.075,426.708,210.876,385.16,224.833z M50.932,270.727c2.449-0.537,4.979-0.874,7.586-0.874    c10.141,0,19.208,4.356,25.605,11.169c5.769,6.159,9.372,14.314,9.372,23.339c0,19.02-15.689,34.496-34.977,34.496    c-19.29,0-34.982-15.469-34.982-34.496C23.536,287.907,35.287,274.161,50.932,270.727z M116.091,314.58    c0.595-3.317,0.956-6.732,0.956-10.219c0-10.579-2.896-20.462-7.875-28.993l2.216-0.501    C132.543,305.62,121.799,312.869,116.091,314.58z M411.592,265.176c20.33,0,36.868,16.322,36.868,36.371    c0,20.054-16.538,36.368-36.868,36.368c-20.329,0-36.86-16.314-36.86-36.368c0-9.293,3.651-17.688,9.49-24.128    c14.138,15.48,25.278,27.202,26.758,27.406c4.399,0.621,5.979-5.923,5.979-5.923l-24.746-28.145    C397.855,267.276,404.463,265.176,411.592,265.176z" data-original="#000000" class="active-path" data-old_color="##86868fill:##86868fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000fill:#000000" fill="##86868" fill="#86868"/>
	</g>
</g></g> </svg>
									</label>
									<h5><label for="type2" class="vehicle_type">Cruiser</label></h5>
									</a>
								</div>
							</div>
						</div>
					</div>
						
<div class="b-search__main-form wow zoomInUp" data-wow-delay="0.3s">
 <div class="row">
 <input type="hidden" name="search_flow" value='by_brand'> 
 <input type='hidden' name='vehicle' value='bike'/>
 <input type='hidden' name='vehicle_type' value='default'/>
 <input type='hidden' name='vehicle_condition' value='new'/>

 <div class="col-xs-12 col-md-12">

 <div class="m-firstSelects">
<div class="col-xs-4">
									
<select name="vehicle_brand" id="select_bike">
<option value="default">Select a bike</option>
<?php  if(isset($query_bike_name)) { ?>
<?php foreach ($query_bike_name as $bikes_name) { ?>
<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
													<?php } } ?>
</select>
<span class="fa fa-caret-down"></span>
										<p>e.g. Hero, Honda, Yamaha...</p>
</div>
<div class="col-xs-3">
 <select name="vehicle_model" id="select_model">
<option value="default">Choose bike model</option>
</select>
<span class="fa fa-caret-down"></span>
<p>e.g. Hero(Splendor), Honda(Activa)...</p>
</div>

<div class="col-xs-3">
<select name="vehicle_budget">
<option value="default">Price range</option>
<option value="low_to_high">Low to High Budget</option>
<option value="high_to_low">High to Low Budget</option>
<option value="50000">50000 Thousand</option>
<option value="50000-100000">50000 Thousand-1 Lakh</option>
<option value="1 lac & above">Above 1 Lakh</option>
</select>
<span class="fa fa-caret-down"></span>
<p>e.g. 80000,1 lakh...</p>
</div>

<div class="col-md-2 col-xs-12 text-left s-noPadding">
<div class="b-search__main-form-submit">
<button type="submit" class="btn m-btn">Search <span class="fa fa-search"></span></button>
</div>
							</div>
						</div>
					</div>
					</div>
    </div>
				</form>
			</div>
</section>
		
<!--b-search-->
<?php
//   
//   echo "<pre>";
//     var_dump($latest);
//     echo "</pre>";
?>
<!--b-featured-->
<?php if(count($featured)){ ?>
<section class="b-featured">
    <div class="container">
    <h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Featured Bikes</h2>
    <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php 

    foreach ($featured as $vehicle_data) { 

        $bike_info = json_decode($vehicle_data->bike_info);
        $engine_info=json_decode($vehicle_data->engine_transmission);
        
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid;?>">
   <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $vehicle_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   
   <span class="m-premium">Featured</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $vehicle_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $engine_info->mileage; ?></span>
        <span class="more-info">Max Speed:<?php echo $engine_info->maximum_speed; ?></span>
        <span class="more-info">Starting:<?php echo (isset($engine_info->starting))?$engine_info->starting:''; ?></span>
       </div>
      </div>
     </div>
                                           <?php } 
                                           
                                           ?>

				</div>
			</div>
		</section>
<?php }  ?>
<!--Old vehicle-->
<?php if(count($used)){ ?>
<section class="b-featured">
    <div class="container">
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Used Bikes</h2>
				<div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php 
    
    foreach ($used as $vehicle_data) { 
        
        $bike_info = json_decode($vehicle_data->vehicle_info);
        $additional_info = json_decode($vehicle_data->additional_info);
  
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $vehicle_data->bkid;?>">
   <img src="<?php render_item_image("uploads/used_bikes/".$additional_info->additional_info->img[0],'bike');?>" alt="<?php echo $vehicle_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   
   <span class="m-premium">Ready for sale</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/bike_detail_used?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $vehicle_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Year:<?php echo $bike_info->vehicle_info->reg_year; ?></span>
        <span class="more-info">Color:<?php echo $bike_info->vehicle_info->color; ?></span>
        <span class="more-info">Fuel Type:<?php echo (isset($bike_info->vehicle_info->fuel))?$bike_info->vehicle_info->fuel:''; ?></span>
       </div>
      </div>
     </div>
                                           <?php } 
                                           
                                           ?>

				</div>
			</div>
		</section>
<?php } ?>
<!--latest vehicles-->
<?php if(count($latest)){ ?>
<section class="b-featured">
    <div class="container">
    <h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Latest Bikes</h2>
    <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php 

    foreach ($latest as $vehicle_data) { 

        $bike_info = json_decode($vehicle_data->bike_info);
        $engine_info=json_decode($vehicle_data->engine_transmission);
        
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid;?>">
   <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $vehicle_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   
   <span class="m-leasing">Latest</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $vehicle_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $engine_info->mileage; ?></span>
        <span class="more-info">Max Speed:<?php echo $engine_info->maximum_speed; ?></span>
        <span class="more-info">Starting:<?php echo (isset($engine_info->starting))?$engine_info->starting:''; ?></span>
       </div>
      </div>
     </div>
                                           <?php } 
                                           
                                           ?>

				</div>
			</div>
		</section>
<?php } ?>

<!--Recommended vehicle-->
<?php if(count($recommended)){ ?>
<section class="b-featured">
    <div class="container">
    <h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Recommended Bikes</h2>
    <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php 

    foreach ($recommended as $vehicle_data) { 

        $bike_info = json_decode($vehicle_data->bike_info);
        $engine_info=json_decode($vehicle_data->engine_transmission);
        
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid;?>">
   <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $vehicle_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   
   <span class="m-premium">Premium</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $vehicle_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $engine_info->mileage; ?></span>
        <span class="more-info">Max Speed:<?php echo $engine_info->maximum_speed; ?></span>
        <span class="more-info">Starting:<?php echo (isset($engine_info->starting))?$engine_info->starting:''; ?></span>
       </div>
      </div>
     </div>
                                           <?php } 
                                           
                                           ?>

				</div>
			</div>
		</section>
<?php } ?>

<!--Upcoming vehicle-->
<?php if(count($upcoming)){ ?>
<section class="b-featured">
    <div class="container">
    <h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Upcoming Bikes</h2>
    <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
    <?php 

    foreach ($upcoming as $vehicle_data) { 

        $bike_info = json_decode($vehicle_data->bike_info);
        $engine_info=json_decode($vehicle_data->engine_transmission);
        
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
?>


     <div>
      <div class="b-featured__item wow" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="150">
		<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid;?>">
   <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="<?php echo $vehicle_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" width="186" height="113" />
   
   <span class="m-leasing">Upcoming</span>
    </a>
          
       </a>
       <div class="b-featured__item-price">
        <?php inr($vehicle_data->price); ?>
       </div>
       <div class="clearfix"></div>
       <h5><a href="<?php echo base_url(); ?>index.php/Welcome/bike_details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $vehicle_name,' ',$model_name; ?></a></h5>

       <div class="b-featured__item-links">

        <span class="more-info">Mileage:<?php echo $engine_info->mileage; ?></span>
        <span class="more-info">Max Speed:<?php echo $engine_info->maximum_speed; ?></span>
        <span class="more-info">Starting:<?php echo (isset($engine_info->starting))?$engine_info->starting:''; ?></span>
       </div>
      </div>
     </div>
                                           <?php } 
                                           
                                           ?>

				</div>
			</div>
		</section>
<?php } ?>

<section class="b-asks">
 <div class="container">
  <div class="row">
   <div class="col-md-6 col-sm-10 col-sm-offset-1 col-md-offset-0 col-xs-12"><a href="<?php echo base_url(); ?>index.php/Welcome/new_bike" style="text-decoration: none;">
     <div class="b-asks__first wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
      <div class="b-asks__first-circle">
       <span class="fa fa-motorcycle"></span>
      </div>
      <div class="b-asks__first-info">
       <h2>Are you looking for a bike?</h2>
       <p>Search our inventory with thousands of bikes & More 
        bikes are adding on daily basis</p>
      </div>
      <div class="b-asks__first-arrow">
       <span class="fa fa-angle-right"></span>
      </div>
     </div>
    </a>
   </div>


   <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-12 col-md-offset-0">
    <a href="<?php echo base_url(); ?>index.php/Welcome/post_bike" style="text-decoration: none;">
     <div class="b-asks__first m-second wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
      <div class="b-asks__first-circle">
       <span class="fa fa-inr"></span>
      </div>
      <div class="b-asks__first-info">
       <h2>Do you want to sell a bike?</h2>
       <p>Just fill in few details and sell your car without paying any listing fee.</p>  
      </div>
      <div class="b-asks__first-arrow">
       <span class="fa fa-angle-right"></span>
      </div>
     </div>
    </a>
   </div>
   <div class="col-xs-12">
    <p class="b-asks__call wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Need Help ? Call Us : <span>+91-9044 988 899</span></p>
   </div>
  </div>
 </div>
</section><!--b-asks--> 

<section class="b-auto">
 <div class="container">
  <h5 class="s-titleBg wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Giving our Customers best deals</h5><br />
  <h2 class="s-title wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">Best Offer from Dreamwheels</h2>
  <div class="row">

   <div class="col-md-12">
    <div class="b-auto__main">
     <div class="col-md-12">
      <a href="#"  class="b-auto__main-toggle s-lineDownCenter m-active j-tab wow zoomInLeft" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100" data-to="#first">Upcoming Cars</a>
      <a href="#" class="b-auto__main-toggle j-tab wow zoomInRight" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100" data-to="#second">Recommended Cars</a>
     </div>
   <div class="clearfix"></div>
     <div class="row m-margin" id="first">
         <!--   Start of Recomended cars -->
      <?php if (count($upcoming)) { 
          
          foreach ($upcoming as $vehicle_data) {
           
                   $bike_info = json_decode($vehicle_data->vehicle_info);
        $additional_info = json_decode($vehicle_data->additional_info);
  
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
            ?>
         
        <div class="col-md-4 col-sm-6 col-xs-12">
         <div class="b-auto__main-item wow slideInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
          
               
               
      
             <img class="img-responsive" src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="mers"alt="<?php echo $bike_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:270px; height:150px;"/>

          <div class="b-world__item-val">

          </div>
          <h2><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $bike_name,' ',$model_name; ?> </a></h2>
          <div class="b-auto__main-item-info">
           <span class="m-price">
     <?php inr($vehicle_data->price); ?>
           </span>

          </div>
          <div class="b-featured__item-links m-auto">

  <?php $date = $bike_info->upcoming_date; ?>

           Launch Date : <?php $date = date_create($date);
  echo date_format($date, 'd M Y');
  ?>


          </div>
         </div>
        </div>
      <?php } }else{  echo "Coming Soon!"; } ?>

     </div>

     <div class="row m-margin" id="second">
 <?php if (count($recommended)) { 
          
          foreach ($recommended as $vehicle_data) {
           
                   $bike_info = json_decode($vehicle_data->vehicle_info);
        $additional_info = json_decode($vehicle_data->additional_info);
  
                          $vehicle_name = $vehicle_data->bike_name;
                          $model_name = $vehicle_data->model_name;
                          $variant_name = "";
            ?>
         
        <div class="col-md-4 col-sm-6 col-xs-12">
         <div class="b-auto__main-item wow slideInUp" data-wow-delay="<?php echo data_wow_delay ;?>" data-wow-offset="100">
          
               
               
      
             <img class="img-responsive" src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt="mers"alt="<?php echo $bike_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:270px; height:150px;"/>

          <div class="b-world__item-val">

          </div>
          <h2><a href="<?php echo base_url(); ?>index.php/Welcome/details?id=<?php echo $vehicle_data->bkid; ?>"><?php echo $bike_name,' ',$model_name; ?> </a></h2>
          <div class="b-auto__main-item-info">
           <span class="m-price">
     <?php inr($vehicle_data->price); ?>
           </span>

          </div>
          <div class="b-featured__item-links m-auto">

  <?php $date = $bike_info->upcoming_date; ?>

           Launch Date : <?php $date = date_create($date);
  echo date_format($date, 'd M Y');
  ?>


          </div>
         </div>
        </div>
 <?php } }else{
     
     echo "Coming Soon!";
 } ?>

     </div>
    </div>
   </div>
  </div>
</section><!--b-auto-->





<?php include('footer.php');?>
<script type="text/javascript">
$(document).ready(function(){    
$(document).on('change','.select_bike',function(){
var bike_id = $(this).val();
$.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_model",{bike_id:bike_id},function(o){
						//console.log(o);
$(".model").html(o);
});
});

$(document).on('change','#select_bike',function(){
var bike_id = $(this).val();
$.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_model",{bike_id:bike_id},function(o){
$("#select_model").html(o);
});
});


$(document).on('click', '#used', function () {
  $("#vehicle_condition").val('used');
  //alert($("#bikes_type").val());
 });
 $(document).on('click', '#new', function () {
  $("#vehicle_condition").val('new');
 });
 

var vehicle,vehicle_type,vehicle_budget,vehicle_brand,vehicle_model,vehicle_condition,search_flow;
 search_flow=$(".search_flow").val();
  $(document).on('click','.search_flow',function () {
       $("#by_budget,#by_brand").hide();
        search_flow=$(this).val();
        //console.log(search_flow);
        $("#"+search_flow).show();

 });
 
 $(document).on("click","#search_button",function(){
     vehicle_condition = $("#vehicle_condition").val();
     vehicle_budget=$(".budget").find("option:selected").val();
     vehicle_type=$(".vehicle_type").find("option:selected").val();
     vehicle_brand=$(".brand").find("option:selected").val();
     vehicle_model="default";
      
     var page = "<?php echo base_url(); ?>index.php/Welcome/vehicle_search?search_flow="+search_flow+"&vehicle=bike&vehicle_budget=" +vehicle_budget + "&vehicle_type=" +vehicle_type+"&vehicle_condition="+vehicle_condition+"&vehicle_brand="+vehicle_brand+"&vehicle_model="+vehicle_model;
window.location.href=page;
 });
 
});                              
</script>