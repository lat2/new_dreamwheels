<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Interested Buyers
        <small>Buyers Listing</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Interested Buyers</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Interested Buyers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Name</th>
                  <th>Number</th>
                  <th>E-mail</th>
                  <th>Date</th> 
                </tr>
                </thead>
                <tbody>
                    <?php $i=1;
                    foreach($query as $buyer){
                        ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $buyer['name']; ?></td>
                  <td><?php echo $buyer['phone']; ?></td>
                  <td><?php echo $buyer['email']; ?></td>
                  <td><?php echo date("d/m/Y", strtotime($buyer['dateofcreation'])); ?></td>
                </tr>
                    <?php } ?>
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?>
<!-- page script -->


