 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
    
      </div>
    
      <ul class="sidebar-menu" data-widget="tree">
      
           <li>
	  <a href="<?php echo base_url();?>index.php/User/index">
	   <i class="fa fa-dashboard"></i> 
	   <span>Dashboard</span>
	  </a>
	 </li>
        

            <li>
	   <a href="<?php echo base_url();?>index.php/User/car_listing">
	    <i class="fa fa-car"></i> 
	    <span>Cars Listing</span>
	   </a>
	  </li>
	  
            <li>
	   <a href="<?php echo base_url();?>index.php/User/bike_listing">
	    <i class="fa fa-motorcycle"></i> 
	    <span>Bikes</span>
	   </a>
	  </li>
	  
            <li>
	   <a href="<?php echo base_url();?>index.php/User/interesred_buyer?user_p=<?php echo $this->session->userdata('phone');?>">
	    <i class="fa fa-users"></i>
	    <span>Interested Buyers</span>
	   </a>
	  </li> 
         
        <li>
         <a href="<?php echo base_url();?>index.php/User/my_account">
	<i class="fa fa-cog"></i>
	<span>Account Details</span>
         </a>
        </li>
      
         <li>
        <a data-title="Really want to logout ?" id="confirm" href="<?php echo base_url();?>index.php/User/logout">
         <i class="fa fa-power-off"></i> 
         <span>Log Out</span>
        </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>