<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php include('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!--<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
               
              <h3><?php if(isset($query_car)){echo count($query_car);}else{ echo "0";}?></h3>
 
              <p>List of Cars</p>
            </div>
            <div class="icon">
              <i class="fa fa-car"></i>
            </div>
            <a href="<?php echo base_url();?>index.php/User/car_listing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
       <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php if(isset($query)){echo count($query);}else{ echo "0";}?></h3>
 
              <p>List of Bikes</p>
            </div>
            <div class="icon">
              <i class="fa fa-motorcycle"></i>
            </div>
            <a href="<?php echo base_url();?>index.php/User/bike_listing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php if(isset($query_buyer)){echo count($query_buyer);}else{ echo "0";}?><sup style="font-size: 20px"></sup></h3>

              <p>Interested Buyers</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo base_url();?>index.php/User/interesred_buyer?user_p=<?php echo $this->session->userdata('phone');?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>-->
		
		<div class="col-lg-3 col-xs-6">
     <a href="<?php echo base_url();?>index.php/Welcome/post_car">
          <div class="small-box bg-red">
            <div class="inner">
               
              <h3>Sell Cars</h3>
 
              <!--<p>Sell Cars</p>-->
            </div>
            <div class="icon">
              <i class="fa fa-car"></i>
            </div>
           <div class="small-box-footer">Click here to sell your car. <i class="fa fa-arrow-circle-right"></i></div>
          </div>
     </a>
        </div>
       
       <div class="col-lg-3 col-xs-6">
         <a href="<?php echo base_url();?>index.php/Welcome/post_bike">
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php echo "Sell Bikes";?></h3>
 
              <!--<p>Sell Bikes</p>-->
            </div>
            <div class="icon">
              <i class="fa fa-motorcycle"></i>
            </div>
          <div class="small-box-footer">Click here to sell your bike <i class="fa fa-arrow-circle-right"></i></div>
          </div>
         </a>
        </div>
       
      </div>
      <!-- /.row -->
      <!-- Main row -->
     
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?> 
