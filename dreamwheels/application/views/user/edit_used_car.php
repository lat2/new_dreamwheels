<?php include('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include('include/sidebar.php');?>
    <!-- /.sidebar -->
  </aside>
<?php $cars_color = array('White','Silver','Black','Gray','Blue','Red','Brown','Green','Gold','Orange','Violet','Yellow','Others');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Fill the details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(isset($_GET['msg'])) { ?>
            <div class="alert alert-success">
    <strong>Success!</strong> Your car has been successfully added.
  </div>
        <?php  } ?>
        <?php  if(isset($query)) {  ?>
        <?php foreach ($query as $cars) { ?>
        <?php 
		        $json_car = json_decode($cars['vehicle_info']);
			    $json_additional = json_decode($cars['additional_info']); 	
		?>
		
            <form name="form" action="<?php echo base_url();?>index.php/User/update_used_car?id=<?php echo $cars['id'];?>" method="post" enctype="multipart/form-data"> 
              <div class="box-body">
			  <input type="hidden" name="edit_id" value="<?php echo $cars['id'];?>">
                <div class="form-group">
					<div class="col-md-6">
											<label>Enter Registration Year <span>*</span></label>
											<div class='s-relative'>
												<select class="form-control select2" style="width: 100%;" name="vehicle_info[reg_year]" data="<?php echo $json_car->vehicle_info->reg_year;?>" required >
													<option value="">Select Year </option>
													<?php for($i=1980; $i<2018; $i++) { ?>
                                                 <option value="<?php echo $i;?>"><?php echo $i;?></option>
												   <?php } ?>
												</select>
											</div>
										</div>
                   <div class="col-md-6">
                  <label for="exampleInputEmail1">Enter Car Name *</label>
                 
                <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="vehicle_info[car]" style="width: 100%;" id="select_car" data="<?php echo $json_car->vehicle_info->car;?>">
                  <option value="">Select </option>
                
                 
                <?php foreach ($query_name as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name['id'];?>><?php echo $car_name['car_name'];?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
                    
                </div>
                <div class="col-md-6" id="show_model">
                  <label for="exampleInputPassword1">Model *</label>
                  <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="vehicle_info[model]" style="width: 100%;" id="model" data="<?php echo $json_car->vehicle_info->model;?>">
                  <option value="">Select </option>
                
                <?php foreach ($query_name_model as $car_model) {
						if($car_model['car_id']==$json_car->vehicle_info->car){
				?>
               
                  
                  <option value=<?php echo $car_model['id'];?>><?php echo $car_model['model_name'];?></option>
                 <?php }} ?>
                </select>
                <?php  } ?>
                </div>
                 <div class="col-md-6" id="">
                 <label for="exampleInputEmail1"> Variant *</label>
                 <?php if(isset($query)) { ?>

                    <select class="form-control select2"  name="vehicle_info[varient]" style="width: 100%;" id="select_variant" data="<?php echo $json_car->vehicle_info->varient;?>">
                  <option value="">Select </option>
                
                 
                 <?php foreach ($query_variant as $variant_name) { 
						if($variant_name->model_id==$json_car->vehicle_info->model){
				?>
               
                  
                  <option value=<?php echo $variant_name->id;?>><?php echo $variant_name->variant;?></option>
                 <?php }} ?>
                </select>
                <?php  } ?>
                 </div>
				 <div class="col-md-6" id="">
					<label>Kilometers Driven <span id="warning_box">*</span></label>
					<input placeholder="Kilometers Driven" type="text" class="form-control" name="vehicle_info[kilometer_driven]" maxlength="6" id="kms" value="<?php echo $json_car->vehicle_info->kilometer_driven;?>" required/>
												 
				</div>
                  <div class="col-md-6" id="car_type">
                 <label for="exampleInputEmail1"> Car Type *</label>
                 <select class="form-control select2" name="vehicle_info[car_type]" style="width: 100%!important;" id="car_type" data="<?php echo $json_car->vehicle_info->car_type;?>"> 
                  <option value="">Select </option>
                  <option value="coupe">Coupe </option>
                  <option value="convertible">Convertible </option>
                  <option value="hatchback">Hatchback </option>
                  <option value="suv">Suv </option>
                  <option value="pickup">Pickup </option>
                   <option value="sedan">Sedan </option>
                    <option value="minicar">Minicar </option>
              </select>
                 </div>
              </div>
                   <div class="form-group">
				    <div class="col-md-6" id="car_type">
                 <label for="exampleInputEmail1"> City *</label>
                 <select class="form-control select2" name="vehicle_info[city]" style="width: 100%!important;" id="car_type" data="<?php echo $json_car->vehicle_info->city;?>"> 
                  <option value="">Select </option>
                  <?php  foreach($query_cities as $cities){  ?>
                        <option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
                                                                
                                                                <?php } ?>
              </select>
                 </div>
				 <div class="col-md-6" id="car_type">
                 <label for="exampleInputEmail1"> Select color *</label>
                 <select class="form-control select2" name="vehicle_info[color]" style="width: 100%!important;" id="car_type" data="<?php echo $json_car->vehicle_info->color;?>"> 
                 <option value="">Select Color</option>
													<?php foreach($cars_color as $color) { ?>
													<option value="<?php echo $color;?>"><?php echo $color;?></option>
													<?php } ?>
              </select>
                 </div>
				 <div class="col-md-6" id="car_type">
                 <label for="exampleInputEmail1"> Fuel Type *</label>
                 <select class="form-control select2" name="vehicle_info[fuel]" style="width: 100%!important;" id="car_type" data="<?php echo $json_car->vehicle_info->fuel;?>"> 
                 <option value="" selected disabled="disabled">Select Fuel Type</option>
													<option value="Diesel">Diesel</option>
													<option value="Petrol">Petrol</option>
													<option value="cng">CNG</option>
              </select>
                 </div>
                   <div class="col-md-6">
                  <label for="exampleInputEmail1">Price <span id="warning_box2">*</span></label>
                  <input type="text" name="price" class="form-control" id="price" value="<?php echo $cars['price'];?>">
                </div>
                
              </div>
                
          <!-- /.box -->
          
          <div style="clear: both;">
               <div class="panel-body" style="font-size: 18px;">Car Photos *</div>
              <div class="form-group">
                 <div class="col-md-3">
               <div class="fileinput fileinput-new" data-provides="fileinput">
			   <p>You can upload upto 3 photos of your vehicle here.</p>
    <span class="btn btn-default btn-file"><span>Choose file</span><input type="file" id="fileupload" name="additional_info[img][]" multiple/></span>
    <span class="fileinput-filename"></span><span class="fileinput-new"></span>
</div>
</div>
 <div class="col-md-9">
<div id="dvPreview">
    <?php
               if(isset($json_additional->additional_info->img)){
               $len = count($json_additional->additional_info->img);    //var_dump($len);
            
               for($i=0;$i<$len;$i++){
        ?>
        <img src="<?php echo base_url();?>uploads/used_cars/<?php echo $json_additional->additional_info->img[$i];?>" width="100px" height="100px">
        
               <?php }}else{ ?>No file chosen <?php } ?>
        </div>
              </div>

                  </div>
                  <div style="clear: both;"></div>


                 <div style="clear: both;"></div>
                 <div class="panel-body" style="font-size: 18px;">Enter Details</div>
                <div class="form-group">
                 <textarea id="editor1" name="comment" rows="10" cols="80">
                                            <?php echo $cars['comment'];?>
                    </textarea>
                </div>
               </div>
          
          <div style="clear:both;">
              <div class="box-footer">
              <center>  <button type="submit" class="btn btn-primary">Update</button></center>
              </div>
           
               </div>
        </div>
            
            </form>
            <?php } } ?>
        <!--/.col (right) -->
      </div>     
          
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('include/footer.php');?>

    <script type="text/javascript">
  $(document).on("change","#select_car",function(){
   var car_id = $(this).val();
     $.post("<?php echo base_url();?>index.php/Admin/select_car_model",{car_id:car_id},function(o){
           console.log(o);
           $("#model").html(o);   
           });
      });
        $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{model_name:model},function(o){
           console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script>
   <script language="javascript" type="text/javascript">
        $(function () {
            $("#fileupload").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#dvPreview");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "height:100px;width: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                                console.log(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
		 $(document).on("keyup","#kms",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box").html("*");
				   }
			   });
			   $(document).on("keyup","#price",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box2").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box2").html("*");
				   }
			   });
			   $("#fileupload").on("change", function() {
         if($("#fileupload")[0].files.length > 3) {
                   alert("You can select only 3 images");
                   event.preventdefault();
         }
    });
    </script>
    <script type="text/javascript">
       
  $('input').on('ifChecked', function(){
 var cat = $( "input:checked" ).val();
if(cat=='upcoming'){
  $('#upcoming-date').css("display","block");
}
else{
  $('#upcoming-date').css("display","none");
}
});
    </script>

  <script type="text/javascript">
  	$(".select2").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
  <script type="text/javascript">
  	$("select").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>
   <script type="text/javascript">
  	$(".select3").each(function(){
    
    var selected_value=$(this).attr('data');
    
    $(this).find("option").each(function(){
    
    //alert(selected_value);
    if($(this).val()==selected_value)
    {
      $(this).attr("selected",true);
    }
    });
    });
  </script>