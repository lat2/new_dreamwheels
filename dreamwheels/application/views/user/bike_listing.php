<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
            <!-- /.box --> <?php //var_dump($_SESSION);?>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of bikes for sell</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Bike</th>
                  <th>Views</th>
                  <th>Interested Buyer</th>
<!--                  <th>Actions</th> -->
                </tr>
                </thead>
                <tbody>
                  <?php if(isset($query)) { ?>
                  <?php $i=1;$j=0; foreach ($query as $bike_data) { ?>
                       
                  <?php $vehicle_info = json_decode($bike_data['vehicle_info']); 
                    
                    $bike_id=$vehicle_info->vehicle_info->bike_id;
		    $model_id=$vehicle_info->vehicle_info->model_id;
		    $vehicle_id = $bike_data['id'];						
			foreach($query_model as $model_name){
			    if($model_id==$model_name->id){
				$bike_name = $model_name->bike_name;
                                //var_dump($model_name->model_name);
                                $model = $model_name->model_name;
				}
                        }    
                        foreach($query_buyer as $buyer_name){
			    if($vehicle_id==$buyer_name['vehicle_id']){
				//var_dump($model_name->model_name);
                                ++$j;
				}
                                
							
			}   ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php  echo $bike_name,' ',$model;?>
                  </td>
                  <td>95</td>
                  <td> <?php echo $j;?></td>
<!--                  <td><i class="fa fa-pencil-square-o" aria-hidden="true"></i></td>-->
                </tr>
               <?php $i++; } } ?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
            <center> <a href="<?php echo base_url();?>index.php/Welcome/post_bike"> <button type="button" class="btn btn-primary btn-sm btn-flat" style="margin-bottom: 15px;">Upload Bikes</button></a></center>
          </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?>
<!-- page script -->


