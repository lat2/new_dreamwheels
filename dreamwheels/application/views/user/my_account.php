<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account
        <small>My Account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">My Account</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         <?php if(isset($query)) { ?>
          <?php  //var_dump($query);?>
         <?php foreach ($query as $account) { ?>
           
         
         
          <!-- /.box -->

          <div class="box" style="margin-bottom: 20px;">
            <div class="box-header">
              <h3 class="box-title">Account Details</h3>
              <span class="pull-right"><a href="#">Edit</a></span>
            </div>
            <div class="row" style="margin-top: 20px;">
              <div class="col-md-2"></div>
  <div class="col-md-4"><i class="fa fa-user fa-1x" aria-hidden="true"></i>&nbsp; <?php echo $account->name;?></div>
  <div class="col-md-4 offset-md-4"><i class="fa fa-envelope fa-1x" aria-hidden="true"></i>&nbsp; <?php echo $account->email;?></div>
</div>

<div class="row" style="margin-top: 20px;">
              <div class="col-md-2"></div>
  <div class="col-md-4"><i class="fa fa-phone fa-1x" aria-hidden="true"></i>&nbsp; <?php echo $account->phone;?></div>
  <div class="col-md-4 offset-md-4"><i class="fa fa-map-marker fa-1x" aria-hidden="true"></i>&nbsp;226003, Ahmedabad</div>
</div> 
            <!-- /.box-header -->
             </div>

             <?php }  } ?>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?>
<!-- page script -->


