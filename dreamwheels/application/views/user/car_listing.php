<?php require('include/header.php');?>
  <!-- Left side column. contains the logo and sidebar -->
 <?php require('include/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
         
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of cars for sell</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S/N</th>
                  <th>Car</th>
                  <!--<th>Views</th>-->
				  <th>Price</th>
				  <th>Posted Date</th>
                  <!--<th>Interested Buyer</th>-->
                  <th>Actions</th> 
                </tr>
                </thead>
                <tbody>
                  <?php $car_name=""; $model_name=""; if(isset($query)) { ?>
                  <?php $i=1; foreach ($query as $car_data) { ?>
                       
                  <?php $vehicle_info = json_decode($car_data['vehicle_info']); 
				        $model_id = $vehicle_info->vehicle_info->model;
						//var_dump($vehicle_info);
                  $variant_id = $vehicle_info->vehicle_info->varient;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
				  $date = strtotime($car_data['dateofcreation']);
				  ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php  echo $car_name.' '.$model_name;?>
                  </td>
                  <td><i class="fa fa-inr"></i> <?php echo $car_data['price'];?></td>
                  <td> <?php echo date("d-m-Y",$date);?></td>
                  <td><a href="<?php echo base_url();?>index.php/User/edit_used_car?id=<?php echo $car_data['id'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                </tr>
               <?php $i++; } } ?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
            <center> <a href="<?php echo base_url();?>index.php/Welcome/post_car/"> <button type="button" class="btn btn-primary btn-sm btn-flat" style="margin-bottom: 15px;">Upload Cars</button></a></center>
          </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php require('include/footer.php');?>
<!-- page script -->


