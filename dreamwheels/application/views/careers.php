<?php include('header.php');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.7s">Careers</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s">
					<h3>The Largest Auto Dealer Online</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/careers" class="b-breadCumbs__page m-active">Careers</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-best">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="b-best__info">
							<header class="s-lineDownLeft b-best__info-head">
								<h2 class="wow zoomInUp" data-wow-delay="0.5s">Careers</h2>
							</header>
							
                         

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Are you a car or bike enthusiast with a flair in writing? Currently we are looking for freelance automotive writers who can contribute for our blog. If interested contact us at info@dreamwheels.in</p>

                            
						</div>
					</div>
<!--					<div class="col-sm-4 col-xs-12">
                                            <p><img src="<?php echo base_url();?>images/finance.jpg" width="350"></p> 
						
                                            <p class="wow zoomInUp" data-wow-delay="0.5s"><b>You can buy the car you want:</b> The first and foremost benefit is that you can buy the car that you want. Of course, you cannot go overboard as the lending limit depends on your credit score and yearly income, but you can buy anything that falls in your range without having to worry about the entire cost of the vehicle at the time. These days, it is quite easy to secure vehicle finance so you can just walk in to a bank and drive out with your brand new car.</p>
					</div>-->
				</div>
			</div>
		</section><!--b-best-->

		
		<?php include('footer.php');?>