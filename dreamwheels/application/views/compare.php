<?php include('header.php');?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInLeft;">Add cars to compare</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInRight;">
					<h3>Compare Favourite Cars</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active">Compare Vehicles</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-compare s-shadow">
			<div class="container">
				<div class="b-compare__images">
					<div class="row">
						<form name="form1" id="compare_cars">
						<div class="col-md-3 col-sm-4 col-xs-12 col-md-offset-2">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
								<h3 class="car_heading1">Car1</h3>
								 
                               
								<img class="img-responsive center-block car1" src="<?php echo base_url();?>images/car_icon.png" style="width: 263px;height: 175px;">
								<div class="b-compare__images-item-price m-right">
								<select name="car" class="form-control" id="select_car" required>
                                                            	<option value="" selected="selected" disabled="">Select Car</option>
																 <?php if(isset($query_car)) { ?>
<?php foreach ($query_car as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                <?php  } ?>                  
                                </select>

                                      
                             
              <select name="model" class="form-control" id="model" required>
              	<option value="">Model</option>
              </select>
           
             <select name="variant" id="select_variant" class="form-control" required> 
             <option value="">Select Variant</option>
             </select>
              <div class="b-compare__images-item-price-vs">vs</div>$90,600</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-4 col-xs-12 ">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
								<h3 class="car_heading2">Car2</h3>
								<img class="img-responsive center-block car2" src="<?php echo base_url();?>images/car_icon.png" alt="car1" style="width: 263px;height: 175px;">
								<div class="b-compare__images-item-price m-right m-left">
									<select name="car2" class="form-control option-2" id="select_car2" required>
                                <option value="" selected="" disabled="">Select Cars</option>
                                <?php if(isset($query_car)) { ?>
<?php foreach ($query_car as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                <?php  } ?>  
                  							</select>
							 

                             <select name="model2" class="form-control option-2" id="model2" required>
                                <option value="">Models</option>
							</select>
                            
                              <select name="variant2" class="form-control option-2" id="select_variant2" required>
                                <option value="">Select Variant</option>
							</select>
							<div class="b-compare__images-item-price-vs">vs</div>$52,650</div>

							</div>
							
						</div>
						<div class="col-md-3 col-sm-4 col-xs-12">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
								<h3 class="car_heading3">Car3</h3>
								<img class="img-responsive center-block car3" src="<?php echo base_url();?>images/car_icon.png" alt="car3" style="width: 263px;height: 175px;">

								<div class="b-compare__images-item-price m-left">  
									<select name="car3" class="form-control option-3" id="select_car3">
                                <option value="" selected="" disabled="">Select Car</option>
                                <?php if(isset($query_car)) { ?>
<?php foreach ($query_car as $car_name) { ?>
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                <?php  } ?>  
                  							</select>
                            
                             <select name="model3" class="form-control option-3" id="model3">
                                <option value="">Select Models</option>
							</select>
							 
							 <select name="variant3" class="form-control option-3" id="select_variant3">
                                <option value="">Select Variant</option>
							</select>
						</div>

							</div>
						</div>
					</form>
					</div>
				</div>

				<br><br><br> 
				<div class="loader center-block" style="display:none;"></div>
<div style="margin-left: 170px;"><input type="submit" class="btn btn-lg center-block compare_car" value="Compare"></div>

<br><br><br>
                <div id="car_comparison">
				
			</div>
			
			</div>
		</section><!--b-compare-->
		
		 <script type="text/javascript">

  $(document).on("change","#select_car",function(){
   var car = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
          // console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
  </script>

   <script type="text/javascript">
  $(document).on("change","#select_car2",function(){
   var car = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
          // console.log(o);
           $("#model2").html(o);
           //$("#model").hide();    
           });
      });
  </script>

   <script type="text/javascript">
  $(document).on("change","#select_car3",function(){
   var car = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
          // console.log(o);
           $("#model3").html(o);
           //$("#model").hide();    
           });
      });
  </script>

  <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var car = $('#select_car option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{car_name:car,model_name:model},function(o){
          // console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 

  <script type="text/javascript">
  $(document).on("change","#model2",function(){
   var model = $('#model2 option:selected').val();
   var car = $('#select_car2 option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{car_name:car,model_name:model},function(o){
           console.log(o);
           $("#select_variant2").html(o);
           //$("#model").hide();    
           });
      });
  </script> 

  <script type="text/javascript">
  $(document).on("change","#model3",function(){
   var model = $('#model3 option:selected').val();
   var car = $('#select_car3 option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{car_name:car,model_name:model},function(o){
           console.log(o);
           $("#select_variant3").html(o);
           //$("#model").hide();    
           });
      });
  </script> 

  <script type="text/javascript">
  	$('.option-2').attr('disabled','true');
  		$('.option-3').attr('disabled','true');
  		$(document).on("change","#select_variant",function(){
  		  var variant = $('#select_variant option:selected').val();
  		  var car = $('#select_car option:selected').val();
  		  var model = $('#model option:selected').val();
		  var car_name = $('#select_car option:selected').text();
  		  var model_name = $('#model option:selected').text();
  		  var variant_name = $('#select_variant option:selected').text();
  		   $.post("<?php echo base_url();?>index.php/Welcome/first_compare",{car_name:car,model_name:model,variant_name:variant},function(o){
  		  var image1 =	"<?php echo base_url();?>uploads/car/"+o;
		  console.log(o);
  		  var full_car_name = car_name+" "+model_name+" "+variant_name;
          $('.car_heading1').text(full_car_name);
  		  //console.log(car_name);
          $('.car1').attr("src", image1);
  		   
  		   	 });
  		 if(variant !=''){
  		 	//alert('gfggf');
  		 	 $(".option-2").prop("disabled", false);
  		 }
  		  });
  		$(document).on("change","#select_variant2",function(){
  			 $(".option-3").prop("disabled", false);
  		  var variant = $('#select_variant2 option:selected').val();
  		  var car = $('#select_car2 option:selected').val();
  		  var model = $('#model2 option:selected').val();
		  var car_name = $('#select_car2 option:selected').text();
  		  var model_name = $('#model2 option:selected').text();
  		  var variant_name = $('#select_variant2 option:selected').text();
		  
  		   $.post("<?php echo base_url();?>index.php/Welcome/first_compare",{car_name:car,model_name:model,variant_name:variant},function(o){
  		  var image1 =	"<?php echo base_url();?>uploads/car/"+o;
  		  var full_car_name = car_name+" "+model_name+" "+variant_name;
          $('.car_heading2').text(full_car_name);
  		 // console.log(car_name);
          $('.car2').attr("src", image1);
  		   
  		   	 });
  		 });	
  		$(document).on("change","#select_variant3",function(){
  			 $(".option-3").prop("disabled", false);
  			  var variant = $('#select_variant3 option:selected').val();
  		  var car = $('#select_car3 option:selected').val();
  		  var model = $('#model3 option:selected').val();
		  var car_name = $('#select_car2 option:selected').text();
  		  var model_name = $('#model2 option:selected').text();
  		  var variant_name = $('#select_variant2 option:selected').text();
  		   $.post("<?php echo base_url();?>index.php/Welcome/first_compare",{car_name:car,model_name:model,variant_name:variant},function(o){
  		  var image1 =	"<?php echo base_url();?>uploads/car/"+o;
  		  var full_car_name = car_name+" "+model_name+" "+variant_name;
          $('.car_heading3').text(full_car_name);
  		  console.log(car_name);
          $('.car3').attr("src", image1);
  		   
  		   	 });
  		 });	
  </script>

  <script type="text/javascript">
  	$(document).on("click",".compare_car",function(){
        var car3_val = $('#select_car3 option:selected').val();
        var model3_val = $('#model3 option:selected').val();
        var variant3_val = $('#select_variant3 option:selected').val();
                
		var car1_val = $('#select_car option:selected').val();
		var model1_val = $('#model option:selected').val();
		var variant1_val = $('#select_variant option:selected').val();
		
		var car2_val = $('#select_car2 option:selected').val();
		var model2_val = $('#model2 option:selected').val();
		var variant2_val = $('#select_variant2 option:selected').val();
		
		if(car1_val==''){
			alert('Select Car');
			$( "#select_car" ).focus();
			return false;
			
		}
		if(model1_val==''){
			alert('Select Model');
			$( "#model" ).focus();
			return false;
			
		}
		if(variant1_val==''){
			alert('Select Variant');
			$( "#select_variant" ).focus();
			return false;
			
		}
		
		if(car2_val==''){
			alert('Select Car');
			$( "#select_car2" ).focus();
			return false;
			
		}
		if(model2_val==''){
			alert('Select Model');
			$( "#model2" ).focus();
			return false;
			
		}
		if(variant2_val==''){
			alert('Select Variant');
			$( "#select_variant2" ).focus();
			return false;
			
		}
		if((car1_val && model1_val && variant1_val) == (car2_val && model2_val && variant2_val))
		{
			alert('Same car Selected.PLease select a different car');
			return false;
		}
                 if(((car1_val && model1_val && variant1_val && car2_val && model2_val && variant2_val)!="") && ((car3_val && model3_val && variant3_val)==""))
		{
			$.post("<?php echo base_url();?>index.php/Welcome/cars2_data",{car1_val:car1_val,model1_val:model1_val,variant1_val:variant1_val,car2_val:car2_val,model2_val:model2_val,variant2_val:variant2_val},function(o){
                            $('#car_comparison').html(o);
                            });
		}else if(((car1_val && model1_val && variant1_val && car2_val && model2_val && variant2_val)!="") && ((car3_val && model3_val && variant3_val)!="")){
     //alert();
                    $.post("<?php echo base_url();?>index.php/Welcome/cars2_data2",{car1_val:car1_val,model1_val:model1_val,variant1_val:variant1_val,car2_val:car2_val,model2_val:model2_val,variant2_val:variant2_val,car3_val:car3_val,model3_val:model3_val,variant3_val:variant3_val},function(o){
                            $('#car_comparison').html(o);
                            });
                    }
  		//var data = $('#compare_cars').serialize();
               // console.log(data);
        //var myObj1, x,myObj3,myObj2;
//        $('.loader').css("display","block");
//		$('body').css("opacity","0.2");
//        if(car3_val !=''){
//             alert("3");
//             $("#car_comparison").css("display","block");
//$('.loader').css("display","none");
//		$('body').css("opacity","1"); 
// $.post("<?php echo base_url();?>index.php/Welcome/cars3_compare",{car_data:data},function(o){
//alert("3 in")
//console.log(o);
//
//
//myObj1 = JSON.parse(o[0].car_info);
//myObj2 = JSON.parse(o[1].car_info);
// 
//myObj3 = JSON.parse(o[2].car_info);
////x = myObj1.fuel;
//
//document.getElementById("fuel1").innerHTML = myObj1.fuel;
//document.getElementById("fuel2").innerHTML = myObj2.fuel;
//document.getElementById("fuel3").innerHTML = myObj3.fuel;
//document.getElementById("milage1").innerHTML = myObj1.mileage;
//document.getElementById("milage2").innerHTML = myObj2.mileage;
//document.getElementById("milage3").innerHTML = myObj3.mileage;
//document.getElementById("tank1").innerHTML = myObj1.tank;
//document.getElementById("tank2").innerHTML = myObj2.tank;
//document.getElementById("tank3").innerHTML = myObj3.tank;
//document.getElementById("seating1").innerHTML = myObj1.seat;
//document.getElementById("seating2").innerHTML = myObj2.seat;
//document.getElementById("seating3").innerHTML = myObj3.seat;
//document.getElementById("engine1").innerHTML = myObj1.engine;
//document.getElementById("engine2").innerHTML = myObj2.engine;
//document.getElementById("engine3").innerHTML = myObj3.engine;
//document.getElementById("cylinder1").innerHTML = myObj1.cylinder;
//document.getElementById("cylinder2").innerHTML = myObj2.cylinder;
//document.getElementById("cylinder3").innerHTML = myObj3.cylinder;
//document.getElementById("boot1").innerHTML = myObj1.boot;
//document.getElementById("boot2").innerHTML = myObj2.boot;
//document.getElementById("boot3").innerHTML = myObj3.boot;
//document.getElementById("gear1").innerHTML = myObj1.gear;
//document.getElementById("gear2").innerHTML = myObj2.gear;
//document.getElementById("gear3").innerHTML = myObj3.gear;
//document.getElementById("bhp1").innerHTML = myObj1.bhp;
//document.getElementById("bhp2").innerHTML = myObj2.bhp;
//document.getElementById("bhp3").innerHTML = myObj3.bhp;
//document.getElementById("seat1").innerHTML = myObj1.seat;
//document.getElementById("seat2").innerHTML = myObj2.seat;
//document.getElementById("seat3").innerHTML = myObj3.seat;
//
////if(myObj1.break=='yes' || myObj2.break=='yes' || myObj3.break=='yes' ||  myObj1.ac=='yes' || myObj2.ac=='yes' || myObj3.ac=='yes' || myObj1.bags=='yes' || myObj2.bags=='yes' || myObj3.bags=='yes' || myObj1.sensor=='yes' || myObj2.sensor=='yes' || myObj3.sensor=='yes' || myObj1.lock=='yes' || myObj2.lock=='yes' || myObj3.lock=='yes') {
//
//if(myObj1.break == 'yes'){
//
//var check =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj2.break == 'yes'){
//
//var check1 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check1 =  "<span class='fa fa-times'></span>";
//}
//if(myObj3.break=='yes'){
//
//var check2=  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check2 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.ac=='yes'){
//
//var check3 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check3 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.ac=='yes'){
//
//var check4 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check4 =  "<span class='fa fa-times'></span>";
//}
//if(myObj3.ac=='yes'){
//
//var check5 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check5 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.bags=='yes'){
//
//var check6 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check6 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.bags=='yes'){
//
//var check7 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check7 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj3.bags=='yes'){
//
//var check8 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check8 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.sensor=='yes'){
//
//var check9 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check9 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.sensor=='yes'){
//
//var check10 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check10 =  "<span class='fa fa-times'></span>";
//}
//if(myObj3.sensor=='yes'){
//
//var check11 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check11 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.lock=='yes'){
//
//var check12 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check12 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.lock=='yes'){
//
//var check13 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check13 =  "<span class='fa fa-times'></span>";
//}
//if(myObj3.lock=='yes'){
//
//var check14 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check14 =  "<span class='fa fa-times'></span>";
//}
//$("#misc1 ul li:first-child").html(check+"Break"); 
//$("#misc1 ul li:nth-child(2)").html(check3+"Air Conditioning"); 
//$("#misc1 ul li:nth-child(3)").html(check6+"Air Bags"); 
//$("#misc1 ul li:nth-child(4)").html(check9+"Sensor"); 
//$("#misc1 ul li:nth-child(5)").html(check12+"Anti-Theft"); 
//
//$("#misc2 ul li:first-child").html(check1+"Break"); 
//$("#misc2 ul li:nth-child(2)").html(check4+"Air Conditioning"); 
//$("#misc2 ul li:nth-child(3)").html(check7+"Air Bags"); 
//$("#misc2 ul li:nth-child(4)").html(check10+"Sensor"); 
//$("#misc2 ul li:nth-child(5)").html(check13+"Anti-Theft"); 
//
//$("#misc3 ul li:first-child").html(check2+"Break"); 
//$("#misc3 ul li:nth-child(2)").html(check5+"Air Conditioning"); 
//$("#misc3 ul li:nth-child(3)").html(check8+"Air Bags"); 
//$("#misc3 ul li:nth-child(4)").html(check11+"Sensor"); 
//$("#misc3 ul li:nth-child(5)").html(check14+"Anti-Theft"); 
//   },'json');   
// }
// else{
// alert("2");
// $("#car_comparison").css("display","block");
//$('.loader').css("display","none");
//		$('body').css("opacity","1"); 
//	 $.post("<?php echo base_url();?>index.php/Welcome/cars2_compare",{car_data1:car1_val,model1_val:model1_val,variant1_val:variant1_val,car_data2:car2_val,model2_val:model2_val,variant2_val:variant2_val},function(o){
//         
// alert();
//console.log(o);
////
////myObj1 = JSON.parse(o[0].car_info);
////myObj2 = JSON.parse(o[1].car_info);
//////x = myObj1.fuel;
////
////document.getElementById("fuel1").innerHTML = myObj1.fuel;
////document.getElementById("fuel2").innerHTML = myObj2.fuel;
////
////document.getElementById("milage1").innerHTML = myObj1.mileage;
////document.getElementById("milage2").innerHTML = myObj2.mileage;
////
////document.getElementById("tank1").innerHTML = myObj1.tank;
////document.getElementById("tank2").innerHTML = myObj2.tank;
////
////document.getElementById("seating1").innerHTML = myObj1.seat;
////document.getElementById("seating2").innerHTML = myObj2.seat;
////
////document.getElementById("engine1").innerHTML = myObj1.engine;
////document.getElementById("engine2").innerHTML = myObj2.engine;
////
////document.getElementById("cylinder1").innerHTML = myObj1.cylinder;
////document.getElementById("cylinder2").innerHTML = myObj2.cylinder;
////
////document.getElementById("boot1").innerHTML = myObj1.boot;
////document.getElementById("boot2").innerHTML = myObj2.boot;
////
////document.getElementById("gear1").innerHTML = myObj1.gear;
////document.getElementById("gear2").innerHTML = myObj2.gear;
////
////document.getElementById("bhp1").innerHTML = myObj1.bhp;
////document.getElementById("bhp2").innerHTML = myObj2.bhp;
////
////document.getElementById("seat1").innerHTML = myObj1.seat;
////document.getElementById("seat2").innerHTML = myObj2.seat;
//
////if(myObj1.break=='yes' || myObj2.break=='yes' || myObj3.break=='yes' ||  myObj1.ac=='yes' || myObj2.ac=='yes' || myObj3.ac=='yes' || myObj1.bags=='yes' || myObj2.bags=='yes' || myObj3.bags=='yes' || myObj1.sensor=='yes' || myObj2.sensor=='yes' || myObj3.sensor=='yes' || myObj1.lock=='yes' || myObj2.lock=='yes' || myObj3.lock=='yes') {
//
////if(myObj1.break == 'yes'){
////
////var check =  "<span class='fa fa-check'></span>";
//}

//else{
//	var check =  "<span class='fa fa-times'></span>";
//}

//if(myObj2.break == 'yes'){
//
//var check1 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check1 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.ac=='yes'){
//
//var check3 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check3 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.ac=='yes'){
//
//var check4 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check4 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.bags=='yes'){
//
//var check6 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check6 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.bags=='yes'){
//
//var check7 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check7 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.sensor=='yes'){
//
//var check9 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check9 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.sensor=='yes'){
//
//var check10 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check10 =  "<span class='fa fa-times'></span>";
//}
//
//if(myObj1.lock=='yes'){
//
//var check12 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check12 =  "<span class='fa fa-times'></span>";
//}
//if(myObj2.lock=='yes'){
//
//var check13 =  "<span class='fa fa-check'></span>";
//}
//
//else{
//	var check13 =  "<span class='fa fa-times'></span>";
//////}
//////
//////$("#misc1 ul li:first-child").html(check+"Break"); 
//////$("#misc1 ul li:nth-child(2)").html(check3+"Air Conditioning"); 
//////$("#misc1 ul li:nth-child(3)").html(check6+"Air Bags"); 
//////$("#misc1 ul li:nth-child(4)").html(check9+"Sensor"); 
//////$("#misc1 ul li:nth-child(5)").html(check12+"Anti-Theft"); 
//////
//////$("#misc2 ul li:first-child").html(check1+"Break"); 
//////$("#misc2 ul li:nth-child(2)").html(check4+"Air Conditioning"); 
//////$("#misc2 ul li:nth-child(3)").html(check7+"Air Bags"); 
//////$("#misc2 ul li:nth-child(4)").html(check10+"Sensor"); 
//////$("#misc2 ul li:nth-child(5)").html(check13+"Anti-Theft"); 
////   });  
////   $('#feature3').hide();
//// }
		 
                 });
  </script>
  
<?php include('footer.php');?>