<?php include('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Used Cars Listing</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query);?> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
                            <a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-sm-9 col-xs-12">
						<div class="b-items__cars">
						<table id="targetTable">
						<thead>
								<tr>
									<td>Cars</td>
								</tr>
							</thead>
              <tbody  id="ajax-data">
							<?php  if(count($query)) {
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $car_list) {
                         
                          $car_info = json_decode($car_list->vehicle_info);
                          $car_name = $car_list->car_name;
                          $model_name =$car_list->model_name;
                          $variant_name =$car_list->variant;
                              
                  $additional_info = json_decode($car_list->additional_info);
                 ?>
						<tr>
							<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>">
                              <img src="<?php render_item_image("uploads/used_cars/".$additional_info->additional_info->img[0]);?>" alt="<?php echo $car_name; ?> available on dreamwheels.in" style="width:240px;" style="height:280px;"/>
                  </a>
								
								</div>
									<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name;?></h2>
										<span><?php inr($car_list->price); ?> </span>
									</header>
									<p>
                    <?php  if(isset($additional_info->additional_info->comments)){echo $additional_info->additional_info->comments; }?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $car_info->vehicle_info->kilometer_driven;?> KM
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $car_info->vehicle_info->reg_year;?></a>
											<a href="#">Used</a>
											<a href="#">Color:-<?php echo $car_info->vehicle_info->color;?> </a>
											<a href="#"><?php echo $car_info->vehicle_info->fuel;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/used_car_details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
							<?php  } ?>
              </tbody>
						</table>	
                             
              <?php }else{ ?>
                          <h3><center>No result found</center></h3>
                         <?php } ?>
						</div>
                                            <div class="ajax-load text-center " style="display:none">
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>

					</div>
					<div class="col-lg-3 col-sm-4 col-xs-12">
						<aside class="b-items__aside">
							<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">Refine your search</h2>
							<div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s">
								<form action="<?php echo base_url();?>index.php/Welcome/search_cars" method="post"> 
									<div class="b-items__aside-main-body">
										<div class="b-items__aside-main-body-item">
											<label>By Car	</label>
										
											<div>
												<select name="car" class="m-select" id="select_car">
													<option value="" selected="">Select car</option>
													<?php  if(isset($query_car)) { ?>
													<?php foreach ($query_car as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
													<?php } } ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>By Model</label>
											<div>
												<select name="model" class="m-select" id="select_model">
													<option value="">Select model</option>

												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>By Price</label>
											<div>
												<select name="price" class="m-select" id="select_price">
													<option value="0-200000" selected>0-2 Lakh</option>
													<option value="200000-500000">2-5 Lakh</option>
													<option value="500000-800000">5-8 Lakh</option>
													<option value="800000-1000000">8-10 Lakh</option>
                                                    <option value="1000000-100000000">10+ Lakh</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
									</div>
									<footer class="b-items__aside-main-footer">
<!--										<button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br />-->
										 
									</footer>
								</form>
							</div>
						
						</aside>
					</div>
				</div>
			</div>
		</section><!--b-items-->


		<?php include('footer.php');?>

<script>
		$(document).ready(function(){
		$("#targetTable").DataTable({
     "bPaginate": false,
     "bInfo": false
});
    
    var page = 1;
var loadmore=true;
var rows=$("#ajax-data tr").length;
var waiting_queue=false;
$(window).scroll(function() {

    if($(window).scrollTop() >= $("#targetTable").height()) {
           if(loadmore && !waiting_queue)
           {
page++;
waiting_queue=true;
console.log("Page No",page);
loadMoreData(page); 

           }  
    }
});

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#ajax-data").append(data);
              if($("#ajax-data tr").length>rows){rows=$("#ajax-data tr").length;waiting_queue=false;}
              else{ loadmore=false; }
              $("#total_results").html($("#ajax-data tr").length);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
    
    
				    $(document).on('change','#select_car',function(){
					    var car = $(this).val();
                                            $('#select_price').val("");
				$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_car",{car:car},function(o){
						//console.log(o);
						    $("#ajax-data").html(o);
						});
				$.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
           
           $("#select_model").html(o);
           });		

			            });   
                                    $(document).on('change','#select_model',function(){
					    var car_model = $(this).val();
				$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_model",{car_model:car_model},function(o){
						//console.log(o);
						    $("#ajax-data").html(o);
						});

			            });
                                    $(document).on('change','#select_price',function(){
					    var price = $(this).val();
                        var car_model = $('#select_model').val();
						//alert(car_model);
						if(car_model==""){
				$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_price",{price:price},function(o){
						    $("#ajax-data").html(o);
						});
						}else{
							
							$.post("<?php echo base_url();?>index.php/Welcome/fetch_vehicle_by_price_model",{price:price,car_model:car_model},function(o){
						    $("#ajax-data").html(o);
						});
						}

			            }); 
});
</script>