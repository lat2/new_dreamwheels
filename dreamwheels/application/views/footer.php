<div class="b-features">
 <div class="container">
  <div class="row">
   <div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
    <ul class="b-features__items">
     <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
     <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
     <li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
    </ul>
   </div>
  </div>
 </div>
</div><!--b-features-->

<div class="b-info">
 <div class="container">
  <div class="row">
   <div class="col-md-3 col-xs-6">
    <aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s">
<!--     <article class="b-info__aside-article">
      <h3><a href="https://twitter.com/dreamwheelsin?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @dreamwheelsin</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></h3>
	  <div class="b-info__aside-article-item" style="height:220px;overflow:scroll">
      <a class="twitter-timeline" href="https://twitter.com/dreamwheelsin?ref_src=twsrc%5Etfw">Tweets by dreamwheelsin</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	  
	 <div>
     </article>-->
     <article class="b-info__aside-article">
      <h3>About us</h3>
      <p>Are you looking out for a good deal to sell your car or bike? It’s simple here.

       All you need to do is register your vehicle with us and we will help you reach out to millions of car and bike buyers across the country.</p>
     </article>
     <a href="<?php echo base_url(); ?>index.php/Welcome/about" class="btn m-btn">Read More<span class="fa fa-angle-right"></span></a>
    </aside>
   </div>
   <div id="latest_vehicles">
    
   </div>    
   <div class="col-md-3 col-xs-6">
    <address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s">
     <p>contact us</p>
     <div class="b-info__contacts-item">
      <span class="fa fa-map-marker"></span>
      <em>A 142 SOUTH CITY,LUCKNOW - 17</em>
     </div>
     <div class="b-info__contacts-item">
      <span class="fa fa-phone"></span>
      <em>Phone:  +91-9044988899</em> 
     </div>
     <div class="b-info__contacts-item">
      <span class="fa fa-whatsapp"></span>
      <em>Dream Wheels : 9044988899</em>
     </div>
     <div class="b-info__contacts-item">
      <span class="fa fa-envelope"></span>
      <em>Email:  info@dreamwheels.com</em>
     </div>
    </address>
    <address class="b-info__map">
     <a href="<?php echo base_url(); ?>index.php/Welcome/contacts">Open Location Map</a>
    </address>
   </div>
  </div>
 </div>
</div><!--b-info-->


<footer class="b-footer" style="padding:0px !important">
 <a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
 <div class="container">
  <div class="row">
   <div class="col-xs-3">
    <div class="b-footer__company wow fadeInLeft" data-wow-delay="0.3s">
        
         <nav class="b-footer__content-nav">
      <ul>
 <li><a href="<?php echo base_url(); ?>">&copy; <?php echo "2014-".date("Y"); ?> Dreamwheels.in</a></li>
  </ul>
     </nav>
    </div>
   </div>
   
   <div class="col-xs-6">
    <div class="b-footer__content wow fadeInRight" data-wow-delay="0.3s">
 <nav class="b-footer__content-nav">
      <ul>
	      <li><a href="<?php echo base_url(); ?>index.php/Welcome/terms">Terms and Conditions</a></li>
	      <li><a href="<?php echo base_url(); ?>index.php/Welcome/policy">Privacy Policy</a></li>
	      <li><a href="<?php echo base_url(); ?>index.php/Welcome/faq">Frequently Asked Questions (FAQ's)</a></li>
	      <li><a href="https://dreamwheelsindia.wordpress.com/" target="_blank"> Blogs </a></li>
	   
      </ul>
     </nav>
    
    </div>
   </div>
   
   <div class="col-xs-3">
    <div class="b-footer__content wow fadeInRight" data-wow-delay="0.3s">
     <div class="b-footer__content-social">
      <a href="https://www.facebook.com/dreamwheels.in" target="_blank"><span class="fa fa-facebook"></span></a>
      <a href="https://twitter.com/dreamwheelsin" target="_blank"><span class="fa fa-twitter"></span></a>
      <a href="https://plus.google.com/112561929079833542068/about" target="_blank"><span class="fa fa-google-plus"> </span></a>
      <a href="https://www.pinterest.com/dreamwheels/" target="_blank"><span class="fa fa-pinterest"></span></a>
<!--      <a href="#"><span class="fa fa-youtube" target="_blank"></span></a>-->
      <a href="tel:+91-9044988899" target="_blank"><span class="fa fa-skype"></span></a>
      
     </div>
    
    </div>
   </div>
  </div>
 </div>
</footer><!--b-footer-->
<script type="text/javascript">
 $(document).on("change", "#model", function () {
  var model = $('#model option:selected').val();
  var car = $('#select_car option:selected').val();

  $.post("<?php echo base_url(); ?>index.php/Admin/select_variant", {car_name: car, model_name: model}, function (o) {
   //console.log(o);
   $("#select_variant").html(o);
   //$("#model").hide();    
  });
 });
</script> 
<script type="text/javascript">
// $(document).on("change", "#select_car", function () {
//  var car_name = $(this).val();
////alert('hii'+car_name);
//  $.post("<?php echo base_url(); ?>index.php/Admin/select_model", {car_name: car_name}, function (o) {
////console.log(o);
//   $("#select_model").html(o);
//  });
// });
</script>
<script src="<?php echo base_url(); ?>assets/bxslider/jquery.bxslider.js"></script>
<!--Theme-->
<script src="<?php echo base_url(); ?>assets/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.smooth-scroll.js"></script>
<script src="<?php echo base_url(); ?>js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.placeholder.min.js"></script>
<script src="<?php echo base_url(); ?>js/theme.js"></script>

<div class="popup" data-popup="popup-1">
 <div class="popup-inner">
  <h4><center>Login/Register</center></h4> 
  <form id="loginPopup" name="form1" action="<?php echo base_url(); ?>index.php/User/login_user" method="post">
   <div class="form-group" id="msg">

   </div>
   <div class="form-group">
	<div class="col-md-5"> 
  <label for="pwd">Mobile no. OR e-mail</label>
  <input type="text" name="phone" class="form-control" id="mobile" required>
  </div>
  </div>
  <div class="form-group">
     <div class="col-md-5"> 
     <label for="pwd">Password</label>
      <input type="password" name="password" class="form-control" id="password" required>
     </div>
   </div>
    <div class="form-group">
    <div class="col-md-2"> 
     <label for="pwd"></label>
    <input type="submit" class="btn btn-primary" value="Submit" id="submit"/>
   </div>
    </div>
      
   <div class="form-group"><div class="col-md-5"></div></div>
   <div class="form-group"><div class="col-md-5">
       <span style="color:#f76d2b">Forget your password ?</span> <a href="<?php echo base_url(); ?>index.php/Welcome/Recoverpass">Click Here</a>
       </div></div>
   <div class="form-group"><div class="col-md-2"></div></div>
	 

  
  
   <div class="form-group">
   <div class="col-md-12" style="margin-top: 20px;"> 
   <hr>
   <center><span style="color:#f76d2b">Still don't have Dreamwheels account ?</span> <a href="<?php echo base_url(); ?>index.php/Welcome/Register">Register Now</a>
  <img src="<?php echo base_url(); ?>images/logo.png" style="margin-top:5px;">
   </center>
   </div>
   </div>
  </form>
<!-- <div class="row"><p class="subtitle fancy"><span>OR LOGIN WITH</span></p></div>
<div class="row">
<div class="form-group">
        <label for="pwd"></label>
       <center><span class="fb fa fa-facebook"></span>&nbsp;<span class="tw fa fa-twitter"></span>&nbsp;<span class="gp fa fa-google-plus"></span></center>
</div> </div>-->

  <a class="popup-close" data-popup-close="popup-1" href="#">x</a>

 </div>
</div>
<div class="popup" data-popup="popup-2">
 <div class="popup-inner">
  <h4><center>Select City</center></h4>
  <form id="locationPopup" name="form1" action="<?php echo base_url(); ?>index.php/User/login_user" method="post">
   <div class="form-group">
    <label for="pwd"></label>
    <select class="form-control" name="city" id="select_city_footer">
     <option value="" selected disabled="disabled">Select City</option>  
    </select>
   </div>
  </form>
<!-- <div class="row"><p class="subtitle fancy"><span>OR LOGIN WITH</span></p></div>
<div class="row">
<div class="form-group">
        <label for="pwd"></label>
       <center><span class="fb fa fa-facebook"></span>&nbsp;<span class="tw fa fa-twitter"></span>&nbsp;<span class="gp fa fa-google-plus"></span></center>
</div> </div>-->
<!--  <div class="row"><center><a href="<?php echo base_url(); ?>index.php/Welcome/Register">Register</a> For New User</center></div>-->
  <a class="popup-close" data-popup-close="popup-2" href="#">x</a>

 </div>
</div>
</body>
</html>

 <script>
  $(document).ready(function(){
   
   
   
 $.post("<?php echo base_url(); ?>index.php/Welcome/cities", '', function (o) {
  $("#select_city_footer").html(o);
 });

 $.post("<?php echo base_url(); ?>index.php/Welcome/latest_vehicles", '', function (o) {
  $("#latest_vehicles").html(o);
  //console.log(o);
 });

 $(document).on("change", "#select_city_footer", function (o) {
  var city_name = $(this).text();
  var city_id = $(this).val();
  //alert(city_id);
  $.post("<?php echo base_url(); ?>index.php/Welcome/session_city", {city_id: city_id}, function () {
   //console.log(o);

   //location.reload();
  });
  $(".popup-close").trigger("click");
 });


 $("#loginPopup").submit(function () {
  var phone = $("#mobile").val();
  var email = $("#email").val();
  var password = $("#password").val();

  if (phone == "") {
    
    if(email == "" || password == "")
    {
     $("#msg").html("<p style='color:red;'>* Enter Mobile Number Or Email & Password To login</p>");
      event.preventDefault();
    }
   
  
  }
 });
   
   $(document).on("click","#confirm,.confirm",function(){
    var title=$(this).attr("data-title");
    if(title==undefined || title=='undefined')
    {
     title='Are you sure ? ';
    }
    
   
    if(!confirm(title))
    {
     return false;
    }
    
   })
   
  });
  </script>