<?php include('header.php');
	  if(!isset($_SESSION['email'])){
			redirect('Welcome/bike_sell_login');
			exit(0);
		} 	
?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.3s">Submit Your Vehicle</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s">
					<h3>Add Your Vehicle In Our Listings</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.3s">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="submit4.html" class="b-breadCumbs__page m-active">Submit a Vehicle</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
						<div class="b-infoBar__progress wow zoomInUp" data-wow-delay="0.3s">
							<div class="b-infoBar__progress-line clearfix">
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step m-active">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
									<div class="b-infoBar__progress-line-step-circle m-last">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<div class="b-submit">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
						<aside class="b-submit__aside">
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 1</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-motorcycle"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Add Your Vehicle</h4>
										<p>Select your vehicle &amp; add info</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 2</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-picture-o"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Photos &amp; Videos</h4>
										<p>Add images / videos of vehicle</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
<!--							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.3s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-phone-square"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Contact details</h4>
										<p>Choose vehicle specifications</p>
                                                                                <div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>-->
							<div class="b-submit__aside-step wow m-active zoomInUp" data-wow-delay="0.3s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-globe"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Submit &amp; Publish</h4>
										<p>Confirm and submit your post.</p>
                                                                                <div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
						</aside>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
						<div class="b-submit__main">
							<form action="<?php echo base_url();?>index.php/Welcome/submit_bike_data" method="post" class='s-submit'>
								<div class="b-submit__main-contacts wow zoomInUp" data-wow-delay="0.3s">
									 
								</div>
								
								<div class="b-submit__main-plan wow zoomInUp" data-wow-delay="0.3s">
								<input type="hidden" name="user_type" value="bike">
									<header class="s-headerSubmit s-lineDownLeft">
										<h2>Submit And Publish</h2>
									</header>
		 							<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
											<input type="checkbox" name="check1" id="check1" required="">
											<label class="s-submitCheckLabel" for="check1" style="border-radius: 0%!important;"><span class="fa fa-check"></span></label>
											<label class="s-submitCheck" for="check1">All the informations are submitted by me are true as per my knowledge. </label>
									</div>

								</div>
								
								
								<div class="b-submit__main-contacts wow zoomInUp" data-wow-delay="0.3s">
									
									<div class="row">
										<div class="col-md-6 col-xs-12">
											<div class="b-submit__main-element">
												<?php if (isset($_SESSION['phone'])) { ?> 
												<input type="hidden" name="user_info[name]" value="<?php echo $_SESSION['name'];?>" />
												<input type="hidden" name="user_info[authority_id]" value="<?php echo @$_SESSION['authority_id'];?>" />
												<input type="hidden" name="user_info[phone]" value="<?php echo $_SESSION['phone'];?>" />
												<input type="hidden" name="user_info[email]" value="<?php echo $_SESSION['email'];?>" />
												<?php } ?>
										    </div>
										</div>
									</div>
								</div>
								 
								<button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.3s">Publish Your Listing Now<span class="fa fa-angle-right"></span></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-submit-->

		<?php include('footer.php');?>
		
			<script>
			    $(document).ready(function(){
				
			   $(document).on("keyup","#phone",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box2").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box2").html("*");
				   }
			    });
			   });
			   </script>