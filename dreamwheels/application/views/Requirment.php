<?php include('header.php');?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Post Requirment</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Get In Touch With Us Now</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="contacts.html" class="b-breadCumbs__page m-active">Post Requirment</a>
			</div>
		</div><!--b-breadCumbs-->

		  
		<section class="b-contacts s-shadow">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<div class="b-contacts__form">
							<header class="b-contacts__form-header s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
								<h2 class="s-titleDet">Requirment Form</h2> 
							</header>
							 
							<div id="success"></div>
							<form id="contactForm" class="s-form wow zoomInUp"  method="post" data-wow-delay="0.5s" id="submit-form">
								 
								<input type="text" placeholder="YOUR NAME"  name="user_info[user_name]" id="user-name" required/>
								<input type="text" placeholder="EMAIL ADDRESS %" name="user_info[user_email]" id="user-email" required/>
								<input type="text" placeholder="PHONE NO." name="user_info[user_phone]" id="user-phone" required/>
								<input type="text" placeholder="YOUR CAR" name="user_info[car_name]" id="user_car" required/>
								<textarea id="user-message" name="requirment" placeholder="YOUR REQUIRMENT" required></textarea>
								<input type="submit" class="btn m-btn" name="submit" value="submit" style="background: #f66c2b;float: left;padding-right: 21px;font-size: 12px;color: #fff;" />
							</form>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="b-contacts__address">
							<div class="b-contacts__address-hours">
								<h2 class="s-titleDet wow zoomInUp" data-wow-delay="0.5s">opening hours</h2>
								<div class="b-contacts__address-hours-main wow zoomInUp" data-wow-delay="0.5s">
									<div class="row">
										<div class="col-md-6 col-xs-12">
											<h5>Sales Department</h5>
											<p>Mon-Sat : 8:00am - 5:00pm <br/>Sunday is closed</p>
										</div>
										<div class="col-md-6 col-xs-12">
											<h5>Service Department</h5>
											<p>Mon-Sat : 8:00am - 5:00pm <br/>Sunday is closed</p>
										</div>
									</div>
								</div>
							</div>
							 
								 
								</address>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-contacts-->

		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<div class="b-info">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s">
							<article class="b-info__aside-article">
								<h3>OPENING HOURS</h3>
								<div class="b-info__aside-article-item">
									<h6>Sales Department</h6>
									<p>Mon-Sat : 8:00am - 5:00pm<br />
										Sunday is closed</p>
								</div>
								<div class="b-info__aside-article-item">
									<h6>Service Department</h6>
									<p>Mon-Sat : 8:00am - 5:00pm<br />
										Sunday is closed</p>
								</div>
							</article>
							<article class="b-info__aside-article">
								<h3>About us</h3>
								<p>Vestibulum varius od lio eget conseq
									uat blandit, lorem auglue comm lodo 
									nisl non ultricies lectus nibh mas lsa 
									Duis scelerisque aliquet. Ante donec
									libero pede porttitor dacu msan esct
									venenatis quis.</p>
							</article>
							<a href="about.html" class="btn m-btn">Read More<span class="fa fa-angle-right"></span></a>
						</aside>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__latest">
							<h3>LATEST AUTOS</h3>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-audi"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="detail.html">MERCEDES-AMG GT S</a></h6>
									<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
								</div>
							</div>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-audiSpyder"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="#">AUDI R8 SPYDER V-8</a></h6>
									<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
								</div>
							</div>
							<div class="b-info__latest-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__latest-article-photo m-aston"></div>
								<div class="b-info__latest-article-info">
									<h6><a href="#">ASTON MARTIN VANTAGE</a></h6>
									<p><span class="fa fa-tachometer"></span> 35,000 KM</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-info__twitter">
							<h3>from twitter</h3>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
									<span>20 minutes ago</span>
								</div>
							</div>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
									<span>20 minutes ago</span>
								</div>
							</div>
							<div class="b-info__twitter-article wow zoomInUp" data-wow-delay="0.3s">
								<div class="b-info__twitter-article-icon"><span class="fa fa-twitter"></span></div>
								<div class="b-info__twitter-article-content">
									<p>Duis scelerisque aliquet ante donec libero pede porttitor dacu</p>
									<span>20 minutes ago</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s">
							<p>contact us</p>
							<div class="b-info__contacts-item">
								<span class="fa fa-map-marker"></span>
								<em>202 W 7th St, Suite 233 Los Angeles,
									California 90014 USA</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-phone"></span>
								<em>Phone:  1-800- 624-5462</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-fax"></span>
								<em>FAX:  1-800- 624-5462</em>
							</div>
							<div class="b-info__contacts-item">
								<span class="fa fa-envelope"></span>
								<em>Email:  info@domain.com</em>
							</div>
						</address>
						<address class="b-info__map">
							<a href="contacts.html">Open Location Map</a>
						</address>
					</div>
				</div>
			</div>
		</div><!--b-info-->


		<!--Main-->   
		<?php include('footer.php');?>
				 <script type="text/javascript">
		 	 //alert( "Handler for .submit() called." );
		$( "#contactForm" ).submit(function(event) {

			var user_name = $('#user-name').val();

			var email = $('#user-phone').val();

            var car = $('#user_car').val();

			var user_info = [user_name,email,car];
             
            var requirment = $('#user-message').val(); 
		 
              event.preventDefault();
            
             $.post("<?php echo base_url();?>index.php/Welcome/post_user_requirment", {'user': user_info,'msg': requirment}, function(o){
             console.log(o);
             $("#success").fadeIn("slow").html(o);
          });

       });
		 </script>
		 