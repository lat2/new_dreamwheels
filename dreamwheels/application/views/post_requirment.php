<?php include('header.php');?>
<?php $cars =  $query_car; ?>
<?php $cars_color = array('White','Silver','Black','Gray','Blue','Red','Brown','Green','Gold','Orange','Violet','Yellow','Others');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s">Submit Your Vehicle</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Add Your Vehicle In Our Listings</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->
 
		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/post_car" class="b-breadCumbs__page m-active">Submit a Vehicle</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
						<div class="b-infoBar__progress wow zoomInUp" data-wow-delay="0.5s">
							<div class="b-infoBar__progress-line clearfix">
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner m-active"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
								<div class="b-infoBar__progress-line-step">
									<div class="b-infoBar__progress-line-step-circle">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
									<div class="b-infoBar__progress-line-step-circle m-last">
										<div class="b-infoBar__progress-line-step-circle-inner"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<div class="b-submit">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
						<aside class="b-submit__aside">
							<div class="b-submit__aside-step m-active wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 1</h3>
								<div class="b-submit__aside-step-inner m-active clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-car"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Add YOUR Vehicle</h4>
										<p>Select your vehicle &amp; add info</p>
										<div class="b-submit__aside-step-inner-info-triangle"></div>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 2</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-list-ul"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>select details</h4>
										<p>Choose vehicle specifications</p>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 3</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-photo"></span>
									</div>
									<div class="b-submit__aside-step-inner-info">
										<h4>Photos &amp; videos</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>
							<div class="b-submit__aside-step wow zoomInUp" data-wow-delay="0.5s">
								<h3>Step 4</h3>
								<div class="b-submit__aside-step-inner clearfix">
									<div class="b-submit__aside-step-inner-icon">
										<span class="fa fa-user"></span>
									</div>
									<!--<div class="b-submit__aside-step-inner-info">
										<h4>Contact details</h4>
										<p>Choose vehicle specifications</p>
									</div>-->
									<div class="b-submit__aside-step-inner-info">
										<h4>SUBMIT &amp; PUBLISH</h4>
										<p>Add images / videos of vehicle</p>
									</div>
								</div>
							</div>
						</aside>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
						<div class="b-submit__main">
							<header class="s-headerSubmit s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
								<h2 class="">Add Your Vehicle Details</h2>
							</header>
							<form class="s-submit clearfix" action="<?php echo base_url();?>index.php/Welcome/post_requirement_step2" method="POST">
									<input type="hidden"  name="vehicle_type" value="car"/>
								<div class="row">
									<div class="col-md-6 col-xs-12">
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Enter Registration Year <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[reg_year]" required>
													<option value="">Select Year </option>
													<?php 
                          $year=date("Y");
                          for($i=$year; $i>=1980; $i--) {
                           
                           ?>
                                                 <option value="<?php echo $i;?>"><?php echo $i;?></option>
												   <?php } ?>
												</select>
												 <span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Model<span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[model]" required id="car_model">
											
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Kilometers Driven <span id="warning_box">*</span></label>
											<div class='s-relative'>
												<input placeholder="Kilometers Driven" type="text"  name="vehicle_info[kilometer_driven]" maxlength="6" id="kms" required/>
												 
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select City <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select single_select" name="vehicle_info[city]" required>
                                                                                                    <option value="" selected disabled="disabled">Select City</option>
													<?php  foreach($query_cities as $cities){  ?>
                        <option value="<?php echo $cities->city_name;?>"><?php echo $cities->city_name;?></option>
                                                                
                                                                <?php } ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Fuel Type <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[fuel]" required>
													<option value="" selected disabled="disabled">Select Fuel Type</option>
													<option value="Diesel">Diesel</option>
													<option value="Petrol">Petrol</option>
													<option value="cng">CNG</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-xs-12">
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Car <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[car]" required id="select_car">
													<option>Select Car</option>
													<?php foreach ($query_car as $cars_list) { ?>
														<option value="<?php echo $cars_list->id;?>"><?php echo $cars_list->car_name;?></option>
												<?php	} ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Variant <span>*</span></label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[varient]" required id="variant">
											
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Car Type <span>*</span></label>
											<div class='s-relative'>
											<select class="m-select" name="vehicle_info[car_type]" required>
                          
													<option value="minicar">Mini Car </option>
													<option value="suv">SUV </option>
                          <option value="xuv">XUV </option>
                          <option value="kuv">KUV </option>
													<option value="coupe">Coupe </option>
													<option value="convertible">Convertible </option>
													<option value="sedan">Sedan </option>
                          <option value="others">Others </option>
												</select>
											
<!--											<input placeholder="Enter Car Type" type="text"  name="vehicle_info[car_type]" id="car_type" required/>-->
										</div>
									</div>
										
										<div class="b-submit__main-element wow zoomInUp" data-wow-delay="0.5s">
											<label>Select Color </label>
											<div class='s-relative'>
												<select class="m-select" name="vehicle_info[color]">
													<option value="">Select Color</option>
													<?php foreach($cars_color as $color) { ?>
													<option value="<?php echo $color;?>"><?php echo $color;?></option>
													<?php } ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
									</div>
								</div>
								<button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.5s">Save &amp; PROCEED to next step<span class="fa fa-angle-right"></span></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-submit-->

	

			<?php include('footer.php'); ?>
			
			<script>
			$(document).ready(function(){
			   //$(".single_select").select2();
			   $(document).on("keyup","#kms",function(){
			       if(isNaN($(this).val())){
				   $("#warning_box").html("* Please enter numeric value.");
				   $(this).val("");
				   }else{
				   $("#warning_box").html("*");
				   }
			   });
			   $(document).on("change","#select_car",function(){
				   var car = $(this).val();
           $("#car_model").html("<option>loading...</option>"); 
				   $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
					  $("#car_model").html(o); 
				   });
			   });
			   });
			</script>
			<script>
			   $(document).on("change","#car_model",function(){
				   var model_name = $(this).val();
           $("#variant").html("<option>loading...</option>"); 
           $("#car_type").html("<option>loading...</option>"); 
				   $.post("<?php echo base_url();?>index.php/Welcome/select_variant",{model_name:model_name},function(o){
					  $("#variant").html(o); 
				   });
				   $.post("<?php echo base_url();?>index.php/Welcome/select_car_type",{model_name:model_name},function(o){
				   //console.log(o);
					  $("#car_type").val(o); 
				   });
			   });
			</script>