<?php include('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Used Bike Listings</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query);?> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/used_bike_listing" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-sm-8 col-xs-12">
						<div class="b-items__cars" id="bike_filter">
                                                    <div class="bike_details_after_filter">
													<table id="targetTable">
													<thead>
													    <tr>
														    <td>Bikes</td>
														  </tr>
													</thead>
                          <tbody  id="ajax-data">
                           
							<?php  if(count($query)) {
                          $bike_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $bike_list) {
                         
                          $bike_info = json_decode($bike_list->vehicle_info);
                          $bike_name = $bike_list->bike_name;
                          $model_name =$bike_list->model_name;
                          $variant_name =$bike_list->variant;
                              
                  $additional_info = json_decode($bike_list->additional_info);
                 ?>
							  
							  <tr>
							    <td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>"><img src="<?php render_item_image("uploads/used_bikes/".$additional_info->additional_info->img[0],'bike');?>" alt="<?php echo $bike_name; ?> available on dreamwheels.in" height="230px" width="270px"/></a>
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name,' ',$model_name;?></h2>
										<span><?php echo inr($bike_list->price);?></span>
									</header>
									<p>
										<?php echo $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span> <?php echo $bike_info->vehicle_info->kilometer_driven;?> KMs
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
											<a href="#">Registered <?php echo $bike_info->vehicle_info->reg_year;?></a>
											
											<a href="#">Color:-<?php echo $bike_info->vehicle_info->color;?> </a>
                                            <a href="#">City:-<?php echo $bike_info->vehicle_info->city;?></a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_detail_used?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
							  </tr>
						<?php  } ?>
              </tbody>
						</table>	
                             
              <?php }else{ ?>
                          <h3><center>No result found</center></h3>
                         <?php } ?>
						</div>
                                                    <div class="ajax-load text-center " style="display: none;">
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>
            </div>
          	</div>
					<div class="col-lg-3 col-sm-4 col-xs-12">
						<aside class="b-items__aside">
							<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">REFINE YOUR SEARCH</h2>
							<div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s">
								<form action="<?php echo base_url();?>index.php/Welcome/search_cars" method="post"> 
									<div class="b-items__aside-main-body">
										<div class="b-items__aside-main-body-item">
											<label>By Bike</label>
										
											<div>
												<select name="bike" class="m-select" id="select_bike">
													<option value="" selected="" disabled>Select Bike</option>
													<?php  if(isset($query_bike_name)) { ?>
													<?php foreach ($query_bike_name as $bikes_name) { ?>
														<option value="<?php echo $bikes_name->id;?>"><?php echo $bikes_name->name;?></option>
													<?php } } ?>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>By Model</label>
											<div>
												<select name="model" class="m-select" id="select_model">
													<option value="">Select Model</option>

												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>By Price</label>
											<div id="price_tab">
												<select name="price" class="m-select" id="select_price">
                                                                                                    <option value="">Select Price</option>								<option value="50000">0-50000 Thousand</option>
                                                                                                    <option value="50000-100000">50000 Thousand - 1 lacs</option>
													<option value="1 lac & above">1 lacs & Above</option>
                                                   </select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
										<div class="b-items__aside-main-body-item">
											<label>KMs RIDDEN</label>
											<div>
												<select name="price" class="m-select" id="select_kms">
                                                                                                    <option value="">Select Kms</option>		
                                                                                                    <option value="15000">0-15000 Kms</option>
													<option value="30000">0-30000 Kms</option>
													<option value="50000">0-50000 Kms</option>
													<option value="80000">0-80000 Kms</option>													<option value="100000">0-100000 Kms And Above</option>												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
									</div>
									<footer class="b-items__aside-main-footer">
<!--										<button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br />-->
										 
									</footer>
								</form>
							</div>
						
						</aside>
					</div>
				</div>
			</div>
		</section><!--b-items-->


		<?php include('footer.php');?>

<script>
		$(document).ready(function(){
			$("#targetTable").DataTable({
     "bPaginate": false,
     "bInfo": false
});
    
      var page = 1;
var loadmore=true;
var rows=$("#ajax-data tr").length;
var waiting_queue=false;
$(window).scroll(function() {

    if($(window).scrollTop() >= $("#targetTable").height()) {
           if(loadmore && !waiting_queue)
           {
page++;
waiting_queue=true;
console.log("Page No",page);
loadMoreData(page); 

           }  
    }
});

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#ajax-data").append(data);
              if($("#ajax-data tr").length>rows){rows=$("#ajax-data tr").length;waiting_queue=false;}
              else{ loadmore=false; }
              $("#total_results").html($("#ajax-data tr").length);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
    
    
				    $(document).on('change','#select_bike',function(){
					    var bike_id = $(this).val();
                                            $('#select_price').val("");
                                            $('#select_price2').val("");
					    $('#select_kms').val("");
				$.post("<?php echo base_url();?>index.php/Welcome/fetch_bike_model",{bike_id:bike_id},function(o){
						//console.log(o);
						    $("#select_model").html(o);
						});
                                                
					
                                        $.post("<?php echo base_url();?>index.php/Welcome/bike_filter",{bike_id:bike_id},function(o){
                                           $(".bike_details_after_filter").html(o); 
                                        });
			            });
                                    
                                    $(document).on('change','#select_model',function(){
					    var model_id = $(this).val();
						
                                            $('#select_price').val("");
                                            $('#select_price2').val("");
					                        $('#select_kms').val("");
                                        $.post("<?php echo base_url();?>index.php/Welcome/bike_model_filter",{model_id:model_id},function(o){
                                           $(".bike_details_after_filter").html(o); 
                                        });
			            });
                                    
                                    $(document).on('change','#select_price',function(){
					    var price = $(this).val();
				            var model_id = $("#select_model").val();
                            var bike_id = $("#select_bike").val();
							$('#select_kms').val("");
                                            //console.log(model_id,' ',bike_id);
					if(price=='custom'){
                                           // alert(price);
                                            $("#price_tab").html("<input type='text' name='price' class='m-input' id='select_price2'>");
                                        }else{
                                        $.post("<?php echo base_url();?>index.php/Welcome/bike_price_filter",{price:price,bike_id:bike_id,model_id:model_id},function(o){
                                           $(".bike_details_after_filter").html(o); 
                                        });
                                    }
			            });
                                    
                                    $(document).on('change','#select_kms',function(){
                                            var kms = $(this).val(); 
					    var price = $("#select_price").val();
				            var model_id = $("#select_model").val();
                                            var bike_id = $("#select_bike").val();
                                            console.log(model_id,' ',bike_id);
					
                                        $.post("<?php echo base_url();?>index.php/Welcome/bike_kms_filter",{kms:kms,price:price,bike_id:bike_id,model_id:model_id},function(o){
                                           $(".bike_details_after_filter").html(o); 
                                        });
                                    
			            });
				});
			</script>