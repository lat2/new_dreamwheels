<?php include('header.php');?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Upcoming Cars</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query);?> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/upcoming_car" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
					<div class="col-lg-11 col-sm-10 col-xs-12" id="targetTable">
						<div class="b-items__cars"  id="ajax-data">
            
            	<?php  if(count($query)) {
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                 
                          foreach($query as $car_list) {
                         
                          $car_info = json_decode($car_list->car_info);
                          $car_name = $car_list->car_name;
                          $model_name =$car_list->model_name;
                          $variant_name =$car_list->variant;
                              
                 
                  $engine_transmission = json_decode($car_list->engine_transmission);
                 ?>
							
							<div class="b-items__cars-one wow zoomInUp car_rows" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
								<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>">	
                 <img src="<?php render_item_image("uploads/car/".$car_info->images[0]);?>" alt='dodge' style="width:240px;" style="height:280px;"/>
              </a>
								
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $car_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> KMPL
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
                                                                                    <a >Displacement: <?php echo $engine_transmission->displacement;?></a>
                                                                                    <a >Power: <?php echo $engine_transmission->maximum_power;?></a>
                                                                                    <a >Torque: <?php echo $engine_transmission->maximum_torque;?> </a>
											<a >Fuel: <?php echo $engine_transmission->fuel_type;?></a>
											<a >Gear: <?php echo $engine_transmission->gear_box;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  }  ?>
							
						</div>
					    <?php }else{ ?>
                          <h3><center>No result found</center></h3>
                         <?php } ?>
                          <div class="ajax-load text-center "  style="display: none;">
    <img src="<?php echo DEFAULT_LOADER; ?>">Loading...
            </div>
					</div>

				</div>
			</div>
		</section><!--b-items-->


		<?php include('footer.php');?>


<script>
		$(document).ready(function(){

    var page = 1;
var loadmore=true;
var rows=$(".car_rows").length;
var waiting_queue=false;
$(window).scroll(function() {

    if($(window).scrollTop() >= $("#targetTable").height()) {
           if(loadmore && !waiting_queue)
           {
page++;
waiting_queue=true;
console.log("Page No",page);
loadMoreData(page); 

           }  
    }
});

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#ajax-data").append(data);
              if($(".car_rows").length>rows){rows=$(".car_rows").length;waiting_queue=false;}
              else{ loadmore=false; }
              $("#total_results").html($(".car_rows").length);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
    
    
				
});
</script>