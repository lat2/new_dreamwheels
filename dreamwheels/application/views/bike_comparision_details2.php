<?php if(isset($query1)){
          foreach ($query1 as $bike1) {
              $model_id=$bike1->model_id;
		foreach($query_model as $model_name1){
		    if($model_id==$model_name1->id){
			$bike_name1 = $model_name1->bike_name;
                        //var_dump($model_name->model_name);
                        $model1 = $model_name1->model_name;
		    }
							
		}
              
              ?>
        <?php $bike_info1 = json_decode($bike1->bike_info);
              $engine_transmission1 = json_decode($bike1->engine_transmission);
              $tyres_brakes1 = json_decode($bike1->tyres_brakes);
              $feature_safety1 = json_decode($bike1->feature_safety);
              $chasis_suspension1 = json_decode($bike1->chasis_suspension);
              $dimension1 = json_decode($bike1->dimension);
              $electricals1 = json_decode($bike1->electricals);
 } }
        ?>
 <?php
if(isset($query2)){
 foreach ($query2 as $bike2) {
     $model_id=$bike2->model_id;
		foreach($query_model as $model_name2){
		    if($model_id==$model_name2->id){
			$bike_name2 = $model_name2->bike_name;
                        //var_dump($model_name->model_name);
                        $model2 = $model_name2->model_name;
		    }
							
		} 
     ?>
        <?php $bike_info2 = json_decode($bike2->bike_info);
              $engine_transmission2 = json_decode($bike2->engine_transmission);
              $tyres_brakes2 = json_decode($bike2->tyres_brakes);
              $feature_safety2 = json_decode($bike2->feature_safety);
              $chasis_suspension2 = json_decode($bike2->chasis_suspension);
              $dimension2 = json_decode($bike2->dimension);
              $electricals2 = json_decode($bike2->electricals);
 }  }      
      ?>

 <?php
if(isset($query3)){
 foreach ($query3 as $bike3) {
     $model_id=$bike3->model_id;
		foreach($query_model as $model_name3){
		    if($model_id==$model_name3->id){
			$bike_name3 = $model_name3->bike_name;
                        //var_dump($model_name->model_name);
                        $model3 = $model_name3->model_name;
		    }
							
		} 
     ?>
        <?php $bike_info3 = json_decode($bike3->bike_info);
              $engine_transmission3 = json_decode($bike3->engine_transmission);
              $tyres_brakes3 = json_decode($bike3->tyres_brakes);
              $feature_safety3 = json_decode($bike3->feature_safety);
              $chasis_suspension3 = json_decode($bike3->chasis_suspension);
              $dimension3 = json_decode($bike3->dimension);
              $electricals3 = json_decode($bike3->electricals);
 }  }      
      ?>

<style>
    .sticky {
    display:none; }
    .fixed {
    position: fixed;
    top:0; left:0;
    width: 100%; 
    z-index: 2;
    display:block;}
</style>
<div class="sticky">
<div class="b-infoBar" id="infobar1"> 
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-xs-3 wow zoomInUp" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
						
					</div>
					<div class="col-sm-3 col-xs-3">
						
                                                    <center>
                                                        <h5 class=""><?php echo $bike_name1.' '.$model1;?></h5>
                                                        <h5><span> <i class="fa fa-inr"></i></span> <?php echo $bike_info1->price;?></h5><br>
                                                    </center>		
						
					</div>
                                    <div class="col-sm-3 col-xs-3">
						
						    <center>
                                    <h5 class=""><?php echo $bike_name2.' '.$model2;?></h5>
                                    <h5><span> <i class="fa fa-inr"></i></span> <?php echo $bike_info2->price;?></h5><br>
                                                    </center>	
						
			            </div>
                                    <div class="col-sm-3 col-xs-">
						
						    <center>
                                    <h5 class=""><?php echo $bike_name3.' '.$model3;?></h5>
                                    <h5><span> <i class="fa fa-inr"></i></span> <?php echo $bike_info2->price;?></h5><br>
                                                    </center>	
						
			            </div>
				</div>
			</div>
		</div>
</div>
<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">Details</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Engine Type
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->engine_type;?><
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->engine_type;?><
								</div>
							</div>
<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->engine_type;?><
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Engine Displacement(cc)
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->displacement;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->displacement;?>
								</div>
							</div>
<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->displacement;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Mileage
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->mileage;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->mileage;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->mileage;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Power (PS@rpm)
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->maximum_power;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->maximum_power;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->maximum_power;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Torque (Nm@rpm)
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->maximum_torque;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->maximum_torque;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->maximum_torque;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Starting
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->starting;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->starting;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->starting;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Maximum Speed
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->maximum_speed;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->maximum_speed;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->maximum_speed;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Cooling System
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->cooling_system;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->cooling_system;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->cooling_system;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Bore
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->bore;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->bore;?>
								</div>
							</div>
						<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->bore;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Stroke
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->stroke;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->stroke;?>
								</div>
							</div>
  						        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->stroke;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									No Of Cylinders
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->no_of_cylinder;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->no_of_cylinder;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->no_of_cylinder;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Drive Type
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->drive_type;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->drive_type;?>
								</div>
							</div>
		  				        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->drive_type;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Valves (per cylinder)
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->valves_per_cylinder;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->valves_per_cylinder;?>
								</div>
							</div>
					<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->valves_per_cylinder;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Fuel System
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->supply_system;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->supply_system;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->supply_system;?>
								</div>
							</div>
						</div>
                                            
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									No Of Gears
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission1->gear_box;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission2->gear_box;?>
								</div>
							</div>
				<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $engine_transmission3->gear_box;?>
								</div>
							</div>
						</div>
					</div>
				</div>

<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">TYRES AND BRAKES</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									TYRE SIZE FRONT
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->tyre_size_front;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->tyre_size_front;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->tyre_size_front;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									TYRE SIZE REAR
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->tyre_size_rear;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->tyre_size_rear;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->tyre_size_rear;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									WHEEL SIZE FRONT
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->wheel_size_front;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->wheel_size_front;?>
								</div>
							</div>
			                                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->wheel_size_front;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									WHEEL SIZE REAR
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->wheel_size_rear;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->wheel_size_rear;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->wheel_size_rear;?>
								</div>
							</div>
						</div>
                            
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									FRONT BRAKES
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes1->front_break);?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes2->front_break);?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes3->front_break);?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									REAR BRAKES
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes2->front_break);?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes2->front_break);?>
								</div>
							</div>
			                                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($tyres_brakes3->front_break);?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Front Brake Diameter
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->front_brake_diameter;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->front_brake_diameter;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->front_brake_diameter;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									REAR Brake Diameter
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes1->rear_brake_diameter;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes2->rear_brake_diameter;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $tyres_brakes3->rear_brake_diameter;?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">FEATURES AND SAFETY</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Speedometer
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety1->speedometer);?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety2->speedometer);?>
								</div>
							</div>
					                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety3->speedometer);?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Odometer
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety1->odometer);?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety2->odometer);?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo ucfirst($feature_safety3->odometer);?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Tripmeter
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->tripmeter;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->tripmeter;?>
								</div>
							</div>
			                                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->tripmeter;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									ABS
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->abs;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->abs;?>
								</div>
							</div>
					                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->abs;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									CBS
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->cbs;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->cbs;?>
								</div>
							</div>
				                        <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->cbs;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									EBS
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->ebs;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->ebs;?>
								</div>
							</div>
					                <div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->ebs;?>
								</div>
							</div>
						</div>
<!--                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Carry Hook
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									8-speed shiftable automatic
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									7-speed shiftable automatic
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Variable-speed automatic
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Fuel Economy Indicator
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									8-speed shiftable automatic
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									7-speed shiftable automatic
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Variable-speed automatic
								</div>
							</div>
						</div>-->
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Seat Type
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->seat_type;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->seat_type;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->seat_type;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Console
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->console;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->console;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->console;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Clock
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->clock;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->clock;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->clock;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Traction Control
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->traction_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->traction_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->traction_control;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Cruise Control
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->cruise_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->cruise_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->cruise_control;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Navigation
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->navigation;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->navigation;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->navigation;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Quick Shifter
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->quick_shifter;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->quick_shifter;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->quick_shifter;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Launch Control
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->launch_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->launch_control;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->launch_control;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Power Modes
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->power_modes;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->power_modes;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->power_modes;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Adjustable Windscreen
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->adjustable_wind_screen;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->adjustable_wind_screen;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->adjustable_wind_screen;?>
								</div>
							</div>
						</div>
                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Mobile Connectivity	
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety1->mobile_connectivity;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety2->mobile_connectivity;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $feature_safety3->mobile_connectivity;?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">CHASSIS AND SUSPENSION</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Chasis/Frame
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension1->chasis_frame;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension2->chasis_frame;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension3->chasis_frame;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Front Suspension
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension1->front_suspension;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension2->front_suspension;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension3->front_suspension;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Rear Suspension
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension1->rear_suspension;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension2->rear_suspension;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $chasis_suspension3->rear_suspension;?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">Dimensions</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Length
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->length;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->length;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->length;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Height
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->height;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->height;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->height;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Width
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->width;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->width;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->width;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Ground Clearance
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->ground_clearance;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->ground_clearance;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->ground_clearance;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Saddle Height
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->saddle_height;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->saddle_height;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->saddle_height;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Kerb Weight
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->kerb_weight;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->kerb_weight;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->kerb_weight;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Wheelbase
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->wheel_base;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->wheel_base;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->wheel_base;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Fuel Capacity
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension1->fuel_capacity;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension2->fuel_capacity;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $dimension3->fuel_capacity;?>
								</div>
							</div>
						</div>
					</div>
				</div>
<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow m-active">
						<h3 class="s-titleDet">Electricals</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside m-active">
<!--						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Ignition
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Optional
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
						</div>-->
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Head Lamp
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals1->head_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->head_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->head_lamp;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Tail Lamp
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals1->tail_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->tail_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals3->tail_lamp;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Turn Signal Lamp
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals1->turn_signal_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->turn_signal_lamp;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals3->turn_signal_lamp;?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Pass Switch
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals1->pass_switch;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->pass_switch;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals3->pass_switch;?>
								</div>
							</div>
						</div>
<!--						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Projector Headlight
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Optional
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									N / A
								</div>
							</div>
						</div>-->
<!--						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Battery Type
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
						</div>-->
<!--						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Pilot Lamps
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Optional
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Standard
								</div>
							</div>
						</div>-->
<!--                                            <div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									LED Tail Light
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals1->led_tail_light;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals2->led_tail_light;?>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									<?php echo $electricals3->led_tail_light;?>
								</div>
							</div>
						</div>-->
					</div>
				</div>
<!--				<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow">
						<h3 class="s-titleDet">Electricals</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Misc. Options
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value s-lineDownCenter">
									<ul>
										<li><span class="fa fa-check"></span>Security System</li>
										<li><span class="fa fa-check"></span>Air Conditioning</li>
										<li><span class="fa fa-check"></span>Alloy Wheels</li>
										<li><span class="fa fa-check"></span>Anti-Lock Brakes (ABS)</li>
										<li><span class="fa fa-check"></span>Anti-Theft</li>
										<li><span class="fa fa-check"></span>Anti-Starter</li>
									</ul>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value s-lineDownCenter">
									<ul>
										<li><span class="fa fa-check"></span>Security System</li>
										<li><span class="fa fa-check"></span>Air Conditioning</li>
										<li><span class="fa fa-check"></span>Alloy Wheels</li>
										<li><span class="fa fa-check"></span>Anti-Lock Brakes (ABS)</li>
										<li><span class="fa fa-check"></span>Anti-Theft</li>
										<li><span class="fa fa-check"></span>Anti-Starter</li>
										<li><span class="fa fa-check"></span>CD Player</li>
									</ul>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value s-lineDownCenter">
									<ul>
										<li><span class="fa fa-check"></span>Security System</li>
										<li><span class="fa fa-check"></span>Air Conditioning</li>
										<li><span class="fa fa-check"></span>Alloy Wheels</li>
										<li><span class="fa fa-check"></span>CD Player</li>
										<li><span class="fa fa-check"></span>Driver Side Airbag</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>-->
<!--				<div class="b-compare__links wow zoomInUp" data-wow-delay="0.3s">
					<div class="row">
						<div class="col-sm-3 col-xs-12 col-sm-offset-3">
							<a href="detail.html" class="btn m-btn">VIEW LISTINGS<span class="fa fa-angle-right"></span></a>
						</div>
						<div  class="col-sm-3 col-xs-12">
							<a href="detail.html" class="btn m-btn">VIEW LISTINGS<span class="fa fa-angle-right"></span></a>        
						</div>
						<div class="col-sm-3 col-xs-12">
							<a href="detail.html" class="btn m-btn">VIEW LISTINGS<span class="fa fa-angle-right"></span></a>      
						</div>
					</div>
				</div>-->
<script>
      $(window).scroll(function(){
  var sticky = $('.sticky'),
      scroll = $(window).scrollTop();

  if (scroll >= 750){ sticky.addClass('fixed');}
  else{ sticky.removeClass('fixed');}
  
        
    
});
</script>