	<?php  if(isset($query)) {
                          $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                        foreach($query as $car_list) { 
                          $car_info = json_decode($car_list->car_info);
                          $car_name = $car_list->car_name;
                          $model_name =$car_list->model_name;
                          $variant_name =$car_list->variant;
                              
                  $engine_transmission = json_decode($car_list->engine_transmission);
                 ?>
						<tr>
							<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>">
                              <img src="<?php render_item_image("uploads/car/".$car_info->images[0]);?>" alt="<?php echo $car_name." ".$model_name." ".$variant_name; ?> available on dreamwheels.in" style="width:240px;" style="height:280px;"/>
                  </a>
								
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $car_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> KMPL
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
                                                                                    <a >Displacement: <?php echo $engine_transmission->displacement;?></a>
                                                                                    <a >Power: <?php echo $engine_transmission->maximum_power;?></a>
                                                                                    <a >Torque: <?php echo $engine_transmission->maximum_torque;?> </a>
											<a >Fuel: <?php echo $engine_transmission->fuel_type;?></a>
											<a >Gear: <?php echo $engine_transmission->gear_box;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
						<?php  } ?>
            
                             
              <?php }else{ ?>
                          <h3><center>No more results</center></h3>
                         <?php } ?>