<?php  if(isset($query)) { 
                          $bike_name = "";
                          $model_name = "";
                          $variant_name = "";
                foreach($query as $bike_list) {
                 
                 $bike_info = json_decode($bike_list->bike_info);
                          $bike_name = $bike_list->bike_name;
                          $model_name =$bike_list->model_name;
                          $variant_name =$bike_list->variant;
                              
                  $engine_transmission = json_decode($bike_list->engine_transmission);
                  $dimension = json_decode($bike_list->dimension);
?>
                             
<tr>
								<td>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="<?php echo data_wow_delay ;?>">
								<div class="b-items__cars-one-img">
									<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>">
  <img src="<?php render_item_image("uploads/bikes/".$bike_info->images[0],'bike');?>" alt='dodge' style="width:240px;" style="height:280px;"/>
                  </a>
									
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $bike_name;?> <?php echo $model_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $bike_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> 
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
										
											<a >Displacement: <?php echo $engine_transmission->displacement;?> </a>
                                                                                        <a >Power: <?php echo $engine_transmission->maximum_power;?> </a>
											<a >Tank:<?php echo $dimension->fuel_capacity;?> </a>
											<a >Kerb Weight:<?php echo $dimension->kerb_weight;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $bike_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							</td>
						</tr>	
            <?php  } ?>
            
                             
              <?php }else{ ?>
                          <h3><center>No more results</center></h3>
                         <?php } ?>
            