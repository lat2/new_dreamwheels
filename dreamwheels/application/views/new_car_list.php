<?php include('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.5s">Car Listings</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned <?php echo count($query);?> results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-items s-shadow">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-sm-8 col-xs-12" id="search_cars3">
						<div class="b-items__cars">
							<?php  if(isset($query)) {
                                                            $car_name = "";
                          $model_name = "";
                          $variant_name = "";
                                                            
                                                            ?>
							 <?php $car_option = array();?>
							<?php  foreach($query as $car_list) { 
                                                 $car_info = json_decode($car_list->car_info);
                                                             $model_id = $car_info->model;
                  $variant_id = $car_info->variant;
                  foreach ($query_name_model as $name) {
                      if($name['id']==$model_id){
                          $car_name = $name['car_name'];
                          $model_name = $name['model_name'];
                       }                      
                  }
                  foreach ($query_variant as $variant) {
                  
                      if($variant->id==$variant_id){
                          $variant_name = $variant->variant;
                       }                      
                  }
                              
                  $engine_transmission = json_decode($car_list->engine_transmission);
                 ?>
                              <?php  // $cars = $car_info->vehicle_info->car;?>
                             
                              
                              <?php // array_push($car_option, $cars);?>
							<div class="b-items__cars-one wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cars-one-img">
									<img src="<?php echo base_url();?>/uploads/car/<?php  echo $car_info->images[0];?>" alt='dodge' style="width:240px;" style="height:280px;"/>
								
								</div>
								<div class="b-items__cars-one-info">
									<header class="b-items__cars-one-info-header s-lineDownLeft">
										<h2><?php echo $car_name,' ',$model_name,' ',$variant_name;?></h2>
										<span><i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $car_info->price;?></span>
									</header>
									<p>
										<?php // echo  $additional_info->additional_info->comments;?>
									</p>
									<div class="b-items__cars-one-info-km">
										<span class="fa fa-tachometer"></span>mileage  <?php echo $engine_transmission->mileage;?> KMPL
									</div>
									<div class="b-items__cars-one-info-details">
										<div class="b-featured__item-links">
                                                                                    <a >Displacement: <?php echo $engine_transmission->displacement;?></a>
                                                                                    <a >Power: <?php echo $engine_transmission->maximum_power;?></a>
                                                                                    <a >Torque: <?php echo $engine_transmission->maximum_torque;?> </a>
											<a >Fuel: <?php echo $engine_transmission->fuel_type;?></a>
											<a >Gear: <?php echo $engine_transmission->gear_box;?> </a>
										</div>
										<a href="<?php echo base_url();?>index.php/Welcome/details?id=<?php echo $car_list->id;?>" class="btn m-btn">VIEW DETAILS<span class="fa fa-angle-right"></span></a>
									</div>
								</div>
							</div>
							<?php  } } ?>
							
						</div>
						<?php if(empty($query)) { ?>
                          <h3><center>No result found</center></h3>
                         <?php } ?>
							
						<div class="b-items__pagination wow zoomInUp" data-wow-delay="0.5s">
							<div class="b-items__pagination-main">
								<a data-toggle="modal" data-target="#myModal" href="#" class="m-left"><span class="fa fa-angle-left"></span></a>
								<span class="m-active"><a href="#">1</a></span>
								<span><a href="#">2</a></span>
								<span><a href="#">3</a></span>
								<span><a href="#">4</a></span>
								<a href="#" class="m-right"><span class="fa fa-angle-right"></span></a>    
							</div>
						</div>

					</div>
					<div class="col-lg-3 col-sm-4 col-xs-12">
						<aside class="b-items__aside">
							<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">REFINE YOUR SEARCH</h2>
							<div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s">
								<form action="<?php echo base_url();?>index.php/Welcome/search_cars" method="post"> 
									<div class="b-items__aside-main-body">
										<div class="b-items__aside-main-body-item">
											<label>Select a car	</label>
										
											<div>
												<?php if(isset($query_car)) { ?>

                    <select name="car" class="m-select" id="select_car">
                  <option value="">-Select car-</option>
                
                 
                <?php foreach ($query_car as $car_name) { ?>
               
                  
                  <option value=<?php echo $car_name->id;?>><?php echo $car_name->car_name;?></option>
                 <?php } ?>
                </select>
                <?php  } ?>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>Select a model</label>
											<div>
												<select name="model" class="m-select" id="select_model">
													<option value="">Select Model</option>

												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>SELECT Price</label>
											<div>
												<select name="price" class="m-select" id="select_price3">
												<option value="">Select Price</option>
													<option value="1-lakh-5-lakh">1 Lakh - 5 Lakh</option>
	  <option value="5-lakh-10-lakh">5 Lakh - 10 Lakh</option>
	  <option value="10-lakh-20-lakh">10 Lakh - 20 Lakh</option>
	  <option value="20-lakh-50-lakh">20 Lakh - 50 Lakh</option>
	  <option value="50-lakh-1-crore">50 Lakh - 1 Crore</option>
	  <option value="above-1-crore">Above 1 Crore</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										
									</div>
									<footer class="b-items__aside-main-footer">
<!--										<button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br />-->
										 
									</footer>
								</form>
							</div>
						
						</aside>
					</div>
				</div>
			</div>
		</section><!--b-items-->

		<?php include('footer.php');?>

<script>
		$(document).ready(function(){
				    $(document).on('change','#select_car',function(){
					    var car = $(this).val();
                        $('#select_price').val("");
				$.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
						//console.log(o);
						    $("#select_model").html(o);
						});
				$.post("<?php echo base_url();?>index.php/Welcome/search_cars3",{car:car},function(o){
						//console.log(o);
						    $("#search_cars3").html(o);
						});		

			            });   
                    $(document).on('change','#select_model',function(){
					    var car_model = $(this).val();
				$.post("<?php echo base_url();?>index.php/Welcome/search_cars4",{car_model:car_model},function(o){
						//console.log(o);
						    $("#search_cars3").html(o);
						});

			            });
                    $(document).on('change','#select_price3',function(){
					    var price = $(this).val();
                        var car_model = $('#select_model').val();
				$.post("<?php echo base_url();?>index.php/Welcome/search_cars_by_price",{price:price,car_model:car_model},function(o){
						console.log(o);
						    $("#search_cars3").html(o);
						});

			            }); 
});
</script>