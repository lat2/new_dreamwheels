<?php include('header.php');?>
   <section class="b-pageHeader">
            <div class="container">
                <h1 class="wow zoomInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInLeft;">EMI Calculator</h1>
                <div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInRight;">
                    <h3>EMI Calculator</h3>
                </div>
            </div>
        </section><!--b-pageHeader-->

        <div class="b-breadCumbs s-shadow">
            <div class="container wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
                <a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active">EMI Calculator</a>
            </div>
        </div><!--b-breadCumbs-->

         

        <section class="b-compare s-shadow">
            <div class="container">
                <div class="b-compare__images">
                    <div class="row">
                    <p>It is very easy to calculate the EMI for your car loan. You will get EMI as soon as you enter the required loan amount and the interest rate. Installment in EMI calculator is calculated on reducing balance. As per the rules of financing institutions, processing fee or possible charges may be applicable which are not shown in the EMI we calculate.</[>
                        <br><br>
                        <div class="col-md-3">
                        <select class="form-control" id="select_car">
                        <option value="" selected="" disabled>Select Car</option>
                        <?php if(isset($car_list)) { ?>
                        <?php foreach($car_list as $cars){ ?>
                        <option value=<?php echo $cars->id;?>><?php echo trim($cars->cars,'"');?></option>
                        <?php } } ?>
                        </select>
                        </div>
                        <div class="col-md-3">
                        <select class="form-control" id="model">
                        <option value="">Select Model</option>
                       
                        </select>
                        </div>
                        <div class="col-md-3">
                        <select class="form-control" id="select_variant">
                        <option>Select Variant</option>
                        </select>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="price_val" name="price" value="">
               
                 <div class="col-md-9">
                 <div class="panel panel-default">
<div class="panel-heading" id="car_details"></div>
    <div class="panel-body" style="display:none;"><b>1 ) Down Payment you will make</b><p class="pull-right" id="down"><b>0</b></p></div>
     <div class="panel-body" style="display:none;"><div id="slider3"> <div id="custom-handle" class="ui-slider-handle"></div></div></div>
     <div class="panel-body"><b>1 ) Down Payment you will make</b><p class="pull-right"><b><output></output></b></p></div>
     <div class="panel-body"> 
<!--         <input type="range" value="0" min="0" max="1000000" data-rangeslider id="down_payment_range">-->
         <input type="text" value="" id="down_payment_range" placeholder="Enter downpayment amount" class="form-control col-xs-12 col-sm-12 col-lg-12">
     </div>
    <div class="panel-body"><b>2 ) Bank Interest Rate(%)</b><p class="pull-right" id="rate"><b>0</b></p></div>
     <div class="panel-body" ><div id="slider1"> &nbsp; <div id="custom-handle" class="ui-slider-handle"></div></div></div>
    <div class="panel-body"><b>3 ) Loan Period(Months)</b><p class="pull-right" id="period"><b>0</b></p></div>
    <div class="panel-body"><div id="slider4"> <div id="custom-handle" class="ui-slider-handle"></div></div></div>
    <div class="panel-body" style="text-align:center;"><b>EMI per month:<p id="emi_total"> <i class="fa fa-inr" aria-hidden="true"></i> 0</p> </b></div>
    <div class="panel-body" style="text-align:center;">Total Loan Amount: <b><span id="total_loan"> <i class="fa fa-inr" aria-hidden="true"></i> 0</span> </b> / Payable Amount <b><span id="total_emi"><i class="fa fa-inr" aria-hidden="true"></i> 0</span></b></div>
  </div>
  </div>
                
        </section><!--b-compare-->

        
<script type="text/javascript">
             $(document).on("change","#select_variant",function(){

                 var model = $('#model option:selected').val();
                 var car = $('#select_car option:selected').val();
                 var variant =  $('#select_variant option:selected').val();
				 var car_name = $('#select_car option:selected').text();
				 var model_name = $('#model option:selected').text();
				 var variant_name = $('#select_variant option:selected').text();
                  $.post("<?php echo base_url();?>index.php/Welcome/select_price",{car_id:car,model_id:model,variant_name:variant},function(o){
                    //console.log(o);
					var parse = jQuery.parseJSON(o);
					//console.log(parse[0].price2);
                 $("#price_val").val(parse[0].price);
                 $("#car_details").html("<b>"+car_name+" "+model_name+" "+variant_name+ " Ex Showroom Price - <i class='fa fa-inr' aria-hidden='true'></i> "+parse[0].price2+"</b>");
				 
                 
//                 $("#down_payment_range").attr("max",o);
                  });

              });
</script>

        
   <script type="text/javascript">

  $(document).on("change","#select_car",function(){
   var car = $(this).val();
     $.post("<?php echo base_url();?>index.php/Welcome/select_car_model",{car_id:car},function(o){
           //console.log(o);
           $("#model").html(o);
           //$("#model").hide();    
           });
      });
  </script>

  <script type="text/javascript">
  $(document).on("change","#model",function(){
   var model = $('#model option:selected').val();
   var car = $('#select_car option:selected').val();
   
     $.post("<?php echo base_url();?>index.php/Admin/select_variant",{car_name:car,model_name:model},function(o){
          // console.log(o);
           $("#select_variant").html(o);
           //$("#model").hide();    
           });
      });
  </script> 


 <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider3" ).slider({
        min: 10000,
      max: 1000000,
        step: 1000,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#down').text(ui.value);
      }
    });
  } );
  </script>
  <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider1" ).slider({
        min: 8,
      max: 26,
        step: 0.1,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#rate').text(ui.value);
        emi_calculate();
      }
    });
  } );
  </script>
  <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider2" ).slider({
        min: 10000,
      max: 1000000,
        step: 1000,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#down2').text(ui.value);
      }
    });
  } );
  </script>
   <script>
  $( function() {
    var handle = $( "#custom-handle" );
    $( "#slider4" ).slider({
        min: 12,
      max: 108,
        step: 1,
      create: function() {
        handle.text( $( this ).slider( "value" ) );
      },
      slide: function( event, ui ) {
        handle.text( ui.value );
        $('#period').text(ui.value);
        emi_calculate();
      }
    });
  } );
  </script>


    <script>
    $(function() {

        var $document = $(document);
        var selector = '[data-rangeslider]';
        var $element = $(selector);

        // For ie8 support
        var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

        // Example functionality to demonstrate a value feedback
        function valueOutput(element) {
            var value = element.value;
            var output = element.parentNode.getElementsByTagName('output')[0] || element.parentNode.parentNode.getElementsByTagName('output')[0];
            output[textContent] = value;
        }

        $document.on('input', 'input[type="range"], ' + selector, function(e) {
            valueOutput(e.target);
        });

      

        // Basic rangeslider initialization
        $element.rangeslider({

            // Deactivate the feature detection
            polyfill: false,

            // Callback function
            onInit: function() {
                valueOutput(this.$element[0]);
            },

            // Callback function
            onSlide: function(position, value) {
                console.log('onSlide');
                console.log('position: ' + position, 'value: ' + value);
            },

            // Callback function
            onSlideEnd: function(position, value) {
                console.log('onSlideEnd');
                console.log('position: ' + position, 'value: ' + value);
            }
        });

    });
    </script>
      <script>
	  $('#down_payment_range').on('keyup',function(){
	      emi_calculate();
	  });
  function emi_calculate(){
  var down_payment=0,rate=0,period=0,price=0,princ=0,term=0,intr=0;
  //down_payment = down_payment+Number($('#down5').text());
  down_payment = down_payment+Number($('#down_payment_range').val());
  rate = Number($('#rate').text())+Number(rate);
  period = Number($('#period').text())+period;
  price = Number($('#price_val').val())+price;
  princ = Number(price)-Number(down_payment);
   term  = Number(period);
   intr   = Number(rate / 1200);
   //console.log(rate,term,price);
 if(rate >0 && term >0 && price>0){
   //console.log("down pay: "+down_payment+" rate "+rate+" period "+period+" price "+price+" princ "+princ+"  term "+ term+" intr "+intr);
 emi = princ*intr/(1-(Math.pow(1/(1+intr),term)));
 emi_total = Number(Math.floor(emi));
 //console.log("emi: "+emi+" emi_total "+emi_total);
 $('#emi_total').html('<i class="fa fa-inr" aria-hidden="true"></i> '+emi_total);
 $('#total_loan').html(princ);
 var total_emi = Number(emi_total) * Number(period);
  $('#total_emi').html(total_emi);
  }
}
  </script>
         <?php include('footer.php');?>