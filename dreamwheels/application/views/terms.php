<?php include('header.php');?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.7s">Terms and conditions</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.7s">
					<h3>The Largest Auto Dealer Online</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container">
				<a href="<?php echo base_url();?>" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/terms" class="b-breadCumbs__page m-active">Terms and conditions</a>
			</div>
		</div><!--b-breadCumbs-->

		<section class="b-best">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-xs-12">
						<div class="b-best__info">
							<header class="s-lineDownLeft b-best__info-head">
								<h2 class="wow zoomInUp" data-wow-delay="0.5s">Terms and conditions</h2>
							</header>
							
                         

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Experience the safety and flexibility with our services by using our website. As a visitor of our site, you must agree with each terms and conditions mentioned here. We recommend you to review the site eventually, because if any changes may be done in agreement or in any terms, we publish them online on the website. This is a binding document between you and our company. All the information provided by us doesn't have any warranty or guaranty, certification on the content regarding any of our services.</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> The site and content are governed by the agreement and mentioned terms. Before you finalize any deal, it's advisable to refer and acknowledge by accepting all terms because, as per our right we timely modified terms and content.</p>
                            
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Latest car offers and discounts , latest news and new launches.</p>
                            
                            <h4>Objective:</h4>
                            
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> By using the website, you must agree with all restrictions and must not rely on all information mentioned in this site about all materials for any purpose except our site specified under the agreement. Otherwise you will be responsible for any consequences for your particular requirements. Besides this you also agree that your personal details provided to us will be shared to trusted parties or used for analysis purpose legally as per privacy policy. We will provide services such as: online used car listing, specifications, price quotes. This website does not claim that data is wholly accurate because it may vary as per situations and conditions.</p>
                            
                            <h4>Definitions for your reference:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> By "Content" or "Contents" we mean any information, data, text, software, music, image, graphics, logo, material, news, notice, article, contract, e-mail, message, e-cards which may be downloaded on or through the site.</p>

<p class="wow zoomInUp" data-wow-delay="0.5s">"Feature" or "features" mentioned on the website means any URL, linking network with our site, value added service or other advanced attribute implemented on the website.</p>
                            
                            <h4>Warranty declaration:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> For any loss, error, interruption, deletion, defect, delay in any transmission, virus or communication failure or distraction, unauthorized access, damage raised in information on the site, we, our staff or associates shall not be in any way responsible and no guaranty or warranty will be given for these reasons. Because, information accuracy, reliability, completeness, suitability may depend on the particular situation.</p>
                            
                            <h4>Your role:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> We have all legal rights to move, alter, delete, accept – refuse your request, on the available content. From where the content originated, that person has sole responsibility about it whether it is publicly or privately posted. So, you may understand that you and we are not entirely responsible for each post or content available on the site.</p>
                            
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> You must not use the site to directly or indirectly harm the site, interrupt the normal flow that affects negatively, do any illegal activity with original content available on the website, disturb any other user, disrupt a server, and intentionally-unintentionally break any laws.</p>
                            
                            <h4>Linking to other resources:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Some hyperlinks may be included in the site from other parties; this inclusion doesn't contain any certification of such sites or endorsement with their operators. We never provide any warranty or any decision of that content's correctness or authenticity. You are solely responsible for any consequences raised from the use of that link's service.</p>
                            
                            <h4>Intellectual property rights for site:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> All the text, designs, audios, graphics and other content included on the site are having copyrights by us unless it's mentioned. Contents are for only personal use. The alteration of content means a violation of copyrights of Dreamwheels or its associates and it may not be reproduced, copied, used, uploaded-posted anywhere or distributed without taking our prior permission.</p>
                            
                            <h4>Licensing:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> We provide you to access and use the information of site personally not for commercial use, and we never allow any modification rights. You are restricted from exploiting the website for any commercial purpose using any media. Without our permission, you can't use framing technique to bound logo, trademark, images, page layout, text, or any proprietary information of ours. Any unauthorized use may abort all licenses and permission under this agreement.</p>
                            
                            <h4>Selling and buying regard:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> We forcibly advice you not to fill false purchase request. It's a crime and affiliated under violation of law to enter false identity or hide true information. Intentionally –unintentionally entering erroneous purchase request face prosecution. As a seller, you must list all your true information on the site. Disobeying this may create problems for you.</p>

                            <p class="wow zoomInUp" data-wow-delay="0.5s"> As a registered seller, you may list your car related all details such as: specification, graphics and images. You must not mention additional contact details or other data for your personal business purpose. Besides this, all listing data must exact match with the actual condition of the car otherwise you should agree to resend all amounts to regarding buyer.</p>
                            
                            <h4>Site modification:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> We have all rights reserved to alter, delete any information and content, cancel any section or service directly without any prior notice. You must agree with this liability. You must use all information and content on site at your own risk, so it's advisable to inquire about all content before using them. Refer site eventually, because we may time to time modify terms and conditions.</p>
                            
                            <h4>Disclaimer:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> DreamWheels provides no warranty that: all your requirements will be fulfilled. This site is 100% secure and contains error and virus free content. All results and quality you get from each deal will be accurate and satisfy your expectations.</p>
                            
                            <h4>All about login:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> We maintain login details for differentiating our members with visitors. Instead of this, we reserved all rights to discontinue your membership or terminate access to content if we get to know of violation of any terms, condition, copyright, or other proprietary rights.</p>
                            <h4>Liability restrictions:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Our site is just a medium for your transaction regarding used bikes & cars. You may buy-sell them, but we aren't providing any guaranty - warranty for their quality, condition, life, worth, from third party. It's your responsibility to verify all details and investigate everything from different resources. All these liabilities will apply if any damages raised due to misuse of information.</p>
                            
                            <h4>Safeguard:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> Security against loss. You must agree to repay us or to our affiliates' harmless from any claims, attorney's fees, your agreement violation etc.</p>
                            
                            <h4>Law arbitration:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> All the legal aspects are proceedings to the court due to any term's violation issues. All jurisdictions will considerable under liable and regarding acts.</p>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> All the dealings and transaction activities and our other terms formations have been complied with all applicable laws and legal acts.</p>
                            
                            <h4> Relationship with acceptor:</h4>
                            <p class="wow zoomInUp" data-wow-delay="0.5s"> The agreement and each terms of use of our site, doesn't comprise with any partnership, employment, relationship between you and us.</p>
                            
						</div>
					</div>
<!--					<div class="col-sm-4 col-xs-12">
                                            <p><img src="<?php echo base_url();?>images/finance.jpg" width="350"></p> 
						
                                            <p class="wow zoomInUp" data-wow-delay="0.5s"><b>You can buy the car you want:</b> The first and foremost benefit is that you can buy the car that you want. Of course, you cannot go overboard as the lending limit depends on your credit score and yearly income, but you can buy anything that falls in your range without having to worry about the entire cost of the vehicle at the time. These days, it is quite easy to secure vehicle finance so you can just walk in to a bank and drive out with your brand new car.</p>
					</div>-->
				</div>
			</div>
		</section><!--b-best-->

		
		<?php include('footer.php');?>