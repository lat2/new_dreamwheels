<?php include('header.php');?>
  <?php if(isset($query)) { ?>
       <?php foreach ($query as $bike) { 
	         $model_id=$bike->model_id;
		    foreach($query_model as $model_name){
			if($model_id==$model_name->id){
			    $bike_name = $model_name->bike_name;
                            //var_dump($model_name->model_name);
                            $model = $model_name->model_name;
                            
			}
		    }
	   ?>
        <?php $bike_info = json_decode($bike->bike_info);
              $engine_transmission = json_decode($bike->engine_transmission);
              $tyres_brakes = json_decode($bike->tyres_brakes);
              $feature_safety = json_decode($bike->feature_safety);
              $chasis_suspension = json_decode($bike->chasis_suspension);
              $dimension = json_decode($bike->dimension);
              $electricals = json_decode($bike->electricals);
        
        ?>
        <?php // $user_info = json_decode($bike->user_info);?>
         <?php // $additional_info = json_decode($bike->additional_info);?>
    
 <?php } } ?>
		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s"><?php echo $bike_name;?> <?php echo $model;?></h1>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
                            <a href="<?php echo base_url();?>index.php/Welcome/bike" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="<?php echo base_url();?>index.php/Welcome/new_bike" class="b-breadCumbs__page">New Bikes</a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page"><?php echo $bike_name;?></a><span class="fa fa-angle-right"></span><a href="#" class="b-breadCumbs__page m-active"><?php echo $model;?></a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row wow zoomInUp" data-wow-delay="0.5s">
					<div class="col-xs-3">
						<div class="b-infoBar__premium">Premium Listing</div>
					</div>

				</div>
			</div>
		</div><!--b-infoBar-->
     
		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?php echo $bike_name;?> <?php echo $model;?> <?php // echo $bike_info->Variant;?></h1>
								 
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="b-detail__head-price">
								<div class="b-detail__head-price-num"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $bike_info->price;?></div>
								<p>Ex Showroom Price</p>
							</div>
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s">
									<div class="row m-smallPadding">
										<div class="col-xs-10">
											<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
<?php  foreach($bike_info->images as $img1) { ?>
												<li class="s-relative">                                        
													<img class="img-responsive center-block" src="<?php render_item_image("uploads/bikes/".$img1,'bike');?>" alt="<?php echo $bike_name." ".$model; ?> available on dreamwheels.in" />
												</li>
												
												<?php } ?>
												
											</ul>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical">
											<div class="b-detail__main-info-images-small" id="bx-pager">
												<?php  $i=0; foreach($bike_info->images as $img) { ?>
												<a href="#" data-slide-index="<?php echo $i;?>" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="<?php render_item_image("uploads/bikes/".$img,'bike');?>" alt="<?php echo $bike_name." ".$model; ?> available on dreamwheels.in" />
												</a>
													<?php $i++; } ?>
												
											
												
											</div>
										</div>
									</div>
								</div>

								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#info1'>About <?php echo $bike_name,' ',$model;?></a>
										
									</div>
									<div id="info1">
										<p><?php  echo $bike_info->details;?></p>
									</div>
                                                                  								</div>
							
							</div>
							<h3 class="border-heading-h2 margin-bottom-20" id="specs-overview-div"><?php echo $bike_name,' ',$model;?> Variants </h3>
							<div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" >
					<tbody>
   				<?php $variant_name2="";
				foreach($query_model_variants as $qmv){ 
	foreach ($query_variants as $variant) {
                  
                      if($variant->id==$qmv->variant){
                          $variant_name2 = $variant->variant;
                       }                      
                  } ?>
					<tr>
					    <td><div class="engineleft enginetext"><a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $qmv->id;?>"><?php echo $variant_name2;?></a></div></td>
						<td><div class="engineright"><i class="fa fa-inr"></i> <?php echo $qmv->price." (Ex showroom price)";?> </div></td>
					</tr>
			<?php } ?>		
					</tbody>
				</table>.
            </div>
          </div>
                                                    <h3 class="border-heading-h2 margin-bottom-20" id="specs-overview-div"><?php echo $bike_name,' ',$model;?> Specifications</h3>
                                                    <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><h4 class="panel-title">
            <b>Overview</b>
        </h4></a>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->front_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->rear_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Wheel Type</div></td><td><div class="engineright">Alloy</div></td></tr><tr><td><div class="engineleft enginetext">Starting</div></td><td><div class="engineright"><?php echo $engine_transmission->starting;?></div></td></tr><tr><td><div class="engineleft enginetext">Fuel Capacity</div></td><td><div class="engineright"><?php echo $dimension->fuel_capacity;?></div></td></tr><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Speed</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_speed;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?></div></td></tr></tbody></table>.
            </div>
          </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><h4 class="panel-title">
            <b>Engine And Transmission</b>
        </h4></a>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
          <div class="panel-body"> 
              <div class="table-responsive">
            <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Engine Type</div></td><td><div class="engineright"><?php echo $engine_transmission->engine_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Displacement</div></td><td><div class="engineright"><?php echo $engine_transmission->displacement;?></div></td></tr><tr><td><div class="engineleft enginetext">Mileage</div></td><td><div class="engineright"><?php echo $engine_transmission->mileage;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Power</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_power;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Torque</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_torque;?></div></td></tr><tr><td><div class="engineleft enginetext">Starting</div></td><td><div class="engineright"><?php echo $engine_transmission->starting;?></div></td></tr><tr><td><div class="engineleft enginetext">Maximum Speed</div></td><td><div class="engineright"><?php echo $engine_transmission->maximum_speed;?></div></td></tr><tr><td><div class="engineleft enginetext">Cooling System</div></td><td><div class="engineright"><?php echo $engine_transmission->cooling_system;?></div></td></tr><tr><td><div class="engineleft enginetext">Gear Box</div></td><td><div class="engineright"><?php echo $engine_transmission->gear_box;?></div></td></tr><tr><td><div class="engineleft enginetext">Clutch</div></td><td><div class="engineright"><?php echo $engine_transmission->clutch;?></div></td></tr><tr><td><div class="engineleft enginetext">No. of Cylinders</div></td><td><div class="engineright"><?php echo $engine_transmission->no_of_cylinder;?></div></td></tr><tr><td><div class="engineleft enginetext">Drive Type</div></td><td><div class="engineright"><?php echo $engine_transmission->drive_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Valves (per cylinder)</div></td><td><div class="engineright"><?php echo $engine_transmission->valves_per_cylinder;?></div></td></tr><tr><td><div class="engineleft enginetext">Bore</div></td><td><div class="engineright"><?php echo $engine_transmission->bore;?></div></td></tr><tr><td><div class="engineleft enginetext">Stroke</div></td><td><div class="engineright"><?php echo $engine_transmission->stroke;?></div></td></tr><tr><td><div class="engineleft enginetext">Supply System</div></td><td><div class="engineright"><?php echo $engine_transmission->supply_system;?></div></td></tr><tr><td><div class="engineleft enginetext">Transmission Type</div></td><td><div class="engineright"><?php echo $engine_transmission->transmission_type;?></div></td></tr></tbody></table>
              </div> 
          </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
            <b>Tyres And Brakes</b>
        </h4></a>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Tyre Size</div></td><td><div class="engineright"><p>	<strong>Front :-</strong><?php echo $tyres_brakes->tyre_size_front;?>,</p><p>	<strong>Rear :-</strong><?php echo $tyres_brakes->tyre_size_rear;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Tyre Type</div></td><td><div class="engineright"><?php echo $tyres_brakes->tyre_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheel Size</div></td><td><div class="engineright"><p>	<strong>Front :-</strong><?php echo $tyres_brakes->wheel_size_front;?>,</p><p>	<strong>Rear :-</strong><?php echo $tyres_brakes->wheel_size_rear;?></p></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->front_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake</div></td><td><div class="engineright"><?php echo ucfirst($tyres_brakes->rear_break);?></div></td></tr><tr><td><div class="engineleft enginetext">Front Brake Diameter</div></td><td><div class="engineright"><?php echo $tyres_brakes->front_brake_diameter;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Brake Diameter</div></td><td><div class="engineright"><?php echo $tyres_brakes->rear_brake_diameter;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><h4 class="panel-title">
            <b>Features And Safety</b>
        </h4></a>
      </div>
      <div id="collapse4" class="panel-collapse collapse"> 
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Speedometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->speedometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Tachometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->tachometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Odometer</div></td><td><div class="engineright"><?php echo ucfirst($feature_safety->odometer);?></div></td></tr><tr><td><div class="engineleft enginetext">Tripmeter</div></td><td><div class="engineright"><?php echo $feature_safety->tripmeter;?></div></td></tr><tr><td><div class="engineleft enginetext">ABS</div></td><td><div class="engineright"><?php echo $feature_safety->abs;?></div></td></tr><tr><td><div class="engineleft enginetext">CBS</div></td><td><div class="engineright"><?php echo $feature_safety->cbs;?></div></td></tr><tr><td><div class="engineleft enginetext">EBS</div></td><td><div class="engineright"><?php echo $feature_safety->ebs;?></div></td></tr><tr><td><div class="engineleft enginetext">Seat Type</div></td><td><div class="engineright"><?php echo $feature_safety->seat_type;?></div></td></tr><tr><td><div class="engineleft enginetext">Console</div></td><td><div class="engineright"><?php echo $feature_safety->console;?></div></td></tr><tr><td><div class="engineleft enginetext">Clock</div></td><td><div class="engineright"><?php echo $feature_safety->clock;?></div></td></tr><tr><td><div class="engineleft enginetext">Traction Control</div></td><td><div class="engineright"><?php echo $feature_safety->traction_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Cruise Control</div></td><td><div class="engineright"><?php echo $feature_safety->cruise_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Navigation</div></td><td><div class="engineright"><?php echo $feature_safety->navigation;?></div></td></tr><tr><td><div class="engineleft enginetext">Quick Shifter</div></td><td><div class="engineright"><?php echo $feature_safety->quick_shifter;?></div></td></tr><tr><td><div class="engineleft enginetext">Launch Control</div></td><td><div class="engineright"><?php echo $feature_safety->launch_control;?></div></td></tr><tr><td><div class="engineleft enginetext">Power Modes</div></td><td><div class="engineright"><?php echo $feature_safety->power_modes;?></div></td></tr><tr><td><div class="engineleft enginetext">Adjustable Windscreen</div></td><td><div class="engineright"><?php echo $feature_safety->adjustable_wind_screen;?></div></td></tr><tr><td><div class="engineleft enginetext">Mobile Connectivity</div></td><td><div class="engineright"><?php echo $feature_safety->mobile_connectivity;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>  
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><h4 class="panel-title">
            <b>Chasis And Suspension</b>
        </h4></a>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Chassis/Frame</div></td><td><div class="engineright"><?php echo $chasis_suspension->chasis_frame;?></div></td></tr><tr><td><div class="engineleft enginetext">Front Suspension</div></td><td><div class="engineright"><?php echo $chasis_suspension->front_suspension;?></div></td></tr><tr><td><div class="engineleft enginetext">Rear Suspension</div></td><td><div class="engineright"><?php echo $chasis_suspension->rear_suspension;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><h4 class="panel-title">
            <b>Dimensions</b>
        </h4></a>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Length</div></td><td><div class="engineright"><?php echo $dimension->length;?></div></td></tr><tr><td><div class="engineleft enginetext">Height</div></td><td><div class="engineright"><?php echo $dimension->height;?></div></td></tr><tr><td><div class="engineleft enginetext">Width</div></td><td><div class="engineright"><?php echo $dimension->width;?></div></td></tr><tr><td><div class="engineleft enginetext">Ground Clearance</div></td><td><div class="engineright"><?php echo $dimension->ground_clearance;?></div></td></tr><tr><td><div class="engineleft enginetext">Saddle Height</div></td><td><div class="engineright"><?php echo $dimension->saddle_height;?></div></td></tr><tr><td><div class="engineleft enginetext">Kerb Weight</div></td><td><div class="engineright"><?php echo $dimension->kerb_weight;?></div></td></tr><tr><td><div class="engineleft enginetext">Wheelbase</div></td><td><div class="engineright"><?php echo $dimension->wheel_base;?></div></td></tr><tr><td><div class="engineleft enginetext">Fuel Capacity</div></td><td><div class="engineright"><?php echo $dimension->fuel_capacity;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>
                                                        
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><h4 class="panel-title">
            <b>Electricals</b>
        </h4></a>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
          <div class="panel-body">
              <div class="table-responsive">
               <table class="table table-striped table-bordered" ><tbody><tr><td><div class="engineleft enginetext">Head Lamp</div></td><td><div class="engineright"><?php echo $electricals->head_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Tail Lamp</div></td><td><div class="engineright"><?php echo $electricals->tail_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Turn Signal Lamp</div></td><td><div class="engineright"><?php echo $electricals->turn_signal_lamp;?></div></td></tr><tr><td><div class="engineleft enginetext">Pass Switch</div></td><td><div class="engineright"><?php echo $electricals->pass_switch;?></div></td></tr><tr><td><div class="engineleft enginetext">LOW FUEL INDICATOR</div></td><td><div class="engineright"><?php echo $electricals->low_fuel_indicator;?></div></td></tr></tbody></table>
              </div>
          </div>
      </div>
    </div>                                                    
  </div>
                                                                                                      
						</div>
                                            
						<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Description</h2>
                                                                        <div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Engine Type</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->engine_type;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Mileage</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->mileage;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Displacement</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->displacement;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Power </h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_power;?> kmpl</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Torque</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_torque;?></p>
										</div>
									</div>
                                                                        
                                                                        <div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Maximum Speed</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->maximum_speed;?></p>
										</div>
									</div>
									
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Gear</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $engine_transmission->gear_box;?></p>
										</div>
									</div>
									
									 
								</div>
								
								</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
                      
			</div>
		</section><!--b-detail-->
<section class="b-related m-home">
			<div class="container">
				<h5 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s">Find out more</h5><br/>
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">You can also check</h2>
                                <div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
				<?php if(isset($query_bike_type)) { ?>
					<?php //var_dump($query_bike_type);?>
					<?php foreach($query_bike_type as $features) { ?>
					
					<?php $bike_info2 = json_decode($features->bike_info);
                                              $engine_transmission2 = json_decode($features->engine_transmission);
                                              $dimension2 = json_decode($features->dimension);
                                              
                                              $model_id=$features->model_id;
		    foreach($query_model as $model_name){
			if($model_id==$model_name->id){
			    $bike_name = $model_name->bike_name;
                            //var_dump($model_name->model_name);
                            $model = $model_name->model_name;
			}
		    }
                                        ?>	
                                    <div>
                                            
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $features->id;?>">
                                                            <img src="<?php echo base_url();?>uploads/bikes/<?php echo $bike_info2->images[0];?>?>" alt="bike"  width="186" height="113"/>
								
							</a>
							<div class="b-featured__item-price">
								<i class="fa fa-inr" aria-hidden="true"></i> <?php echo $bike_info2->price;?>
							</div>
							<div class="clearfix"></div>
							<h2><a href="<?php echo base_url();?>index.php/Welcome/bike_details?id=<?php echo $features->id;?>"><?php echo $bike_name,' ',$model;?></a></h2>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span><?php echo $engine_transmission2->mileage;?> </div>
							<div class="b-featured__item-links">
								<a href="#"><?php echo $engine_transmission2->displacement;?></a>
								<a href="#"><?php echo $engine_transmission2->maximum_power;?></a>
								<a href="#"><?php echo $engine_transmission2->maximum_torque;?></a>
							</div>
						</div>
					</div>
                                    <?php } } ?>
<!--					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/audi.jpg" alt="audi" />
							</a>
							<div class="b-featured__item-price">
								$95,900
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">AUDI R8 SPYDER V-8</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>0.00 KM</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2015</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/aston.jpg" alt="aston" />
								<span class="m-leasing">LEASING AVAILABLE</span>
							</a>
							<div class="b-featured__item-price">
								$101,025
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">ASTON MARTIN VANTAGE</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>35,000 KM</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/jaguar.jpg" alt="jaguar" />
							</a>
							<div class="b-featured__item-price">
								$130,825
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">JAGUAR F-TYPE R</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>0.00</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2015</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/mers.jpg" alt="mers" />
								<span class="m-premium">Premium</span>
							</a>
							<div class="b-featured__item-price">
								$184,900
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">MERCEDES-AMG GT / GT S</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>35,000 KM</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/audi.jpg" alt="audi" />
							</a>
							<div class="b-featured__item-price">
								$95,900
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">AUDI R8 SPYDER V-8</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>0.00 KM</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2015</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/aston.jpg" alt="aston" />
								<span class="m-leasing">LEASING AVAILABLE</span>
							</a>
							<div class="b-featured__item-price">
								$101,025
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">ASTON MARTIN VANTAGE</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>35,000 KM</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div>
						<div class="b-featured__item wow rotateIn" data-wow-delay="0.3s" data-wow-offset="150">
							<a href="detail.html">
								<img src="media/186x113/jaguar.jpg" alt="jaguar" />
							</a>
							<div class="b-featured__item-price">
								$130,825
							</div>
							<div class="clearfix"></div>
							<h5><a href="detail.html">JAGUAR F-TYPE R</a></h5>
							<div class="b-featured__item-count"><span class="fa fa-tachometer"></span>0.00</div>
							<div class="b-featured__item-links">
								<a href="#">Used</a>
								<a href="#">2015</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>-->
				</div>
				
			</div>
		</section>

		<?php include('footer.php');?>