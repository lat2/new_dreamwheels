<?php class Welcome_model extends CI_Model {
	 public function __construct()
        {
                parent::__construct();
                
        }
        
   public function used_bikes_list()
    {
//        $sql = "SELECT t1.*,t2.bike_name,t2.model_name from bike_sell INNER JOIN from bike_models ON(t2.id=t1.vehicle_info->'$.model_id')";
//        $query = $this->db->query($sql)->result();
//        return $query;
        $query = $this->db->get('bike_sell');
             return $query->result();
    }
    
    public function bike_names_models()
    {
        $query = $this->db->get('bike_models');
        return $query->result();
    }   
    
    public function used_bikes_list_by_bike_id($bike_id)
    {
       $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }

    public function used_bikes_list_by_model_id($model_id)
    {
       $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.model_id'='$model_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }       
}