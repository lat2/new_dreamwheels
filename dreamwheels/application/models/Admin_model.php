<?php class Admin_model extends CI_Model {
	 public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }

public function login($email,$pass)
        {
                $this->db->where('email', $email);
                $this->db->where('password', $pass);
                $query = $this->db->get('admin');
                return $query->num_rows();
                //return $query->result();
        }
 public function fetch_cities()
        {
            $query = $this->db->get('cities');
            return $query->result();
        }
        
  public function fetch_car_name()
        {
            $query = $this->db->get('car_names');
            return $query->result();
        }
		
   public function fetch_car_model()
	    {
			$sql = "Select * from car_models" ;
			$query = $this->db->query($sql);
			return $query->result_array();
        }
 
 public function fetch_car_variant()
        {
            $query = $this->db->get('car_variants');
            return $query->result();
        }
 public function fetch_car_variant_by_car()
        {
            $sql = "SELECT t1.*,t2.car_name FROM `car_variants` t1 JOIN car_names t2 ON(t2.id=t1.car_id)";
            $query = $this->db->query($sql);
            return $query->result();
        }		
 public function fetch_users()
        {
            $query = $this->db->get('user');
            return $query->result();
        } 

  public function fetch_buyers()
        {
            $query = $this->db->get('buyer');
            return $query->result();
        }
        
 public function select_admin_by_phone($email)
        {
            $sql = "Select * from admin where email='$email'" ;
            $query = $this->db->query($sql);
            return $query->result_array(); 
        }       
 public function car_insert($data){
    $query =  $this->db->insert('cars2', $data);
 }
 
  public function car_make_insert($data){
    $query =  $this->db->insert('car_names', $data);
 }
 
   public function bike_make_insert($data){
    $query =  $this->db->insert('bike_names', $data);
 }
 
   public function car_model_insert($data){
    $query =  $this->db->insert('car_models', $data);
 }
 
   public function bike_model_insert($data){
    $query =  $this->db->insert('bike_models', $data);
 }
 
    public function car_variant_insert($data){
    $query =  $this->db->insert('car_variants', $data);
 }
 
   public function bike_variant_insert($data){
    $query =  $this->db->insert('bike_variants', $data);
 }
 
 public function car_update($data){
     $id = $_GET['id'];
     $this->db->where('id',$id);
     $this->db->update('cars2',$data);
	 $this->output->enable_profiler(TRUE);
 }
 public function fetch_model($car){
    $sql = "Select model_name,id from car_models where car_id=$car" ;
    $query = $this->db->query($sql);
    return $query->result_array();

 }   
  public function fetch_car(){
    $sql = "Select DISTINCT car_info->'$.car' As cars from cars";
    $query = $this->db->query($sql);
    return $query->result_array();
  } 
  public function fetch_car_variant_by_model($model){
    $sql = "Select * from car_variants Where model_id=$model";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
  public function fetch_used_cars_list()
  {
    $sql = "Select * from car_sell order by id desc";
    $query = $this->db->query($sql);
      return $query->result();
  }
public function fetch_used_cars_by_id($id)
  {
      $sql = "Select * from car_sell where id=$id" ;
      $query = $this->db->query($sql);
      return $query->result();
  }
  
  public function fetch_used_bikes_list()
  {
     $sql = "Select * from bike_sell order by id desc";
    $query = $this->db->query($sql);
      return $query->result();
  } 
  public function update_status_used_bike($status,$id)
  {
      $this->db->set('status',$status);
      $this->db->where('id',$id);
      $this->db->update('bike_sell');
  }
  
  public function update_status_bike($status,$id)
  {
      $this->db->set('status',$status);
      $this->db->where('id',$id);
      $this->db->update('bikes');
  }
 
  public function update_status_used_car($status,$id)
  {
      $this->db->set('status',$status);
      $this->db->where('id',$id);
      $this->db->update('car_sell');
  }
  
  public function update_status_car($status,$id)
  {
      $this->db->set('status',$status);
      $this->db->where('id',$id);
      $this->db->update('cars2');
  }
  
  public function car_listing(){
    $sql = "SELECT * from cars2";
    $query = $this->db->query($sql);
    return $query->result_array();
  }    

  public function car_delete($id){
   $this->db->where('id', $id);
   $this->db->delete('cars2');
  }
  
  
  public function used_car_delete($id)
  {
	  $this->db->where('id',$id);
	  $this->db->delete('car_sell');
  } 
  
    public function car_delete_name($id)
	{
   $this->db->where('id', $id);
   $this->db->delete('car_names');
  }
  
   public function bike_delete_name($id){
   $this->db->where('id', $id);
   $this->db->delete('bike_names');
  }
  
      public function car_delete_model($id){
   $this->db->where('id', $id);
   $this->db->delete('car_models');
  }
        public function car_delete_variant($id){
   $this->db->where('id', $id);
   $this->db->delete('car_variants');
  }
      public function bike_delete_variant($id){
   $this->db->where('id', $id);
   $this->db->delete('bike_variants');
  }



  public function edit_car_by_Id($id){
   $this->db->where('id', $id);
  $query = $this->db->get('cars2');
   return $query->result_array();
  }
  
  ///////////////////////////////
  //////////////////////////////
  /////////BIKES///////////////
  /////////////////////////////
  ////////////////////////////
  
  
  public function fetch_bike(){
    $sql = "Select DISTINCT bike_info->'$.bike' As bikes from bikes";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
      public function fetch_bike_names()
        {
            $query = $this->db->get('bike_names');
            return $query->result();
        }
        
    public function fetch_bike_models($bike_id)
    {
        $sql = "SELECT * from bike_models where bike_id='$bike_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
	public function fetch_bike_models3()
	    {
			$sql = "Select * from bike_models" ;
			$query = $this->db->query($sql);
			return $query->result_array();
        }
	public function fetch_bike_models2()
    {
        $query = $this->db->get('bike_models');
            return $query->result();
    }
    
    public function fetch_bike_variants($model_id)
    {
        $sql = "SELECT * from bike_variants where model_id='$model_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
	
	public function fetch_bike_variants2()
    {
        $query = $this->db->get('bike_variants');
            return $query->result();
    }
	
	public function fetch_img_details($id)
	{
	    $sql = "SELECT bike_info FROM bikes WHERE id='$id'";
		$query = $this->db->query($sql)->result();
		return $query;
	}
  
  public function bike_insert($data)
    {
        $query =  $this->db->insert('bikes', $data);
    }
 
 public function bike_update($data)
    {
        $id = $_GET['id'];
        $this->db->where('id',$id);
      $this->db->update('bikes',$data);
    }
 public function fetch_bike_model($bike){
    $sql = "Select DISTINCT bike_info->'$.model' As models from bikes where bike_info->'$.bike'='$bike'" ;
    $query = $this->db->query($sql);
    return $query->result_array();

 }

  public function fetch_bike_variant($bike,$model){
   $sql = "Select DISTINCT bike_info->'$.Variant' As variant from bikes Where bike_info->'$.bike'='$bike' AND bike_info->'$.model'='$model'";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
   public function fetch_bike_variant_by_bike()
        {
            $sql = "SELECT t1.*,t2.name FROM `bike_variants` t1 JOIN bike_names t2 ON(t2.id=t1.bike_id)";
            $query = $this->db->query($sql);
            return $query->result();
        }
  
  public function bike_listing(){
    $sql = "SELECT * from bikes";
    $query = $this->db->query($sql);
    return $query->result_array();
  } 
  
   public function bike_delete($id){
   $this->db->where('id', $id);
   $this->db->delete('bikes');
  }
  
  
  
   public function bike_delete_model($id){
   $this->db->where('id', $id);
   $this->db->delete('bike_models');
  }
  public function delete_used_ike($id){
   $this->db->where('id', $id);
   $this->db->delete('bike_sell');
  }
  
  public function user_delete($id){
   $this->db->where('id', $id);
   $this->db->delete('user');
  }
  
   public function buyer_delete($id){
   $this->db->where('id', $id);
   $this->db->delete('buyer');
  }
  
  public function edit_bike_by_Id($id){
      $this->db->where('id', $id);
      $query = $this->db->get('bikes');
      return $query->result_array();
  }
  
  public function update_admin($data)
  {
     return $this->db->update('admin',$data);
      
  }
  
  
    }