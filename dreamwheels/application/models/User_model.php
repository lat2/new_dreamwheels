<?php class User_model extends CI_Model {
	 public function __construct()
        {
                parent::__construct();
                // Your own constructor code
        }

public function insert_car($data)
        {
                $query =  $this->db->insert('car_sell', $data);
                //return $query->result();
        }

	public function insert_bike($data)
        {
                $query =  $this->db->insert('bike_sell', $data);
                //return $query->result();
        }	
		
		public function fetch_car_name()
	    {
			$sql = "Select * from car_names" ;
			$query = $this->db->query($sql);
			return $query->result_array();
        }
		
	   public function fetch_car_model()
	    {
			$sql = "Select * from car_models" ;
			$query = $this->db->query($sql);
			return $query->result_array();
        }
 
 public function fetch_car_variant()
        {
            $query = $this->db->get('car_variants');
            return $query->result();
        }	
 public function insert_user($data)
        {
                $query =  $this->db->insert('user', $data);
                //return $query->result();
        }
 public function insert_user2($data)
        {
			$phone = $data['phone'];
			$email = $data['email'];
		     echo  $sql = "SELECT * FROM user WHERE phone='$phone' AND email='$email'";
				$query2 = $this->db->query($sql)->result();
				//var_dump($query2);die();
				if(empty($query2)){
                $query =  $this->db->insert('user', $data);
				}
                //return $query->result();
        }	

 public function fetch_cities()
        {
            $query = $this->db->get('cities');
            return $query->result();
        }
        		
		
 public function insert_user3($data)
        {
			$phone = $data['phone'];
		     echo  $sql = "SELECT * FROM user WHERE phone='$phone'";
				$query2 = $this->db->query($sql)->result();
				//var_dump($query2);die();
				if(empty($query2)){
                $query =  $this->db->insert('user', $data);
				}
                //return $query->result();
        }			

 public function insert_buyer($data1)
        {
                $query =  $this->db->insert('buyer', $data1);
                //return $query->result();
        }        
        
   public function select_user($email,$pass)   /******** Get User By email and password ***/
        {
               
                $this->db->where('email', $email);
                $this->db->where('password', $pass);
                $query = $this->db->get('user');
                return $query->result();
        }
        
    public function phone_validate($phone)
        {
            $sql = "SELECT phone FROM user WHERE phone='$phone'";
            $query = $this->db->query($sql)->result();
            return $query;
        } 
        
       public function get_password($mobile,$email)
               {
           if($mobile)
           {
                     $sql = "SELECT password FROM user WHERE phone='$mobile'"; 
           }
           else if($email)
           {
                        echo $sql = "SELECT password FROM user WHERE email='$email'";   
           }

            $query = $this->db->query($sql)->result();
            return $query;
               }
    
    public function email_validate($email)
        {
            $sql = "SELECT email FROM user WHERE email='$email'";
            $query = $this->db->query($sql)->result();
            return $query;
        }    
    public function select_user_by_phone($phone,$pass)   /******* Get User By Phone ****/
        {
                $this->db->where('phone', $phone);
				$this->db->where('password', $pass);
                $query = $this->db->get('user');
                return $query->result();
        }
     public function get_car_list($phone)   /******* Get User By Phone ****/
        {
               $sql = "SELECT * FROM `car_sell` where user_info->'$.phone'='$phone'";
               $query = $this->db->query($sql);
               return $query->result_array();
        }
		
	public function get_car_detail($id)   /******* Get Detail By Id ****/
        {
               $sql = "SELECT * FROM `car_sell` where id='$id'";
               $query = $this->db->query($sql);
               return $query->result_array();
        }
		
     public function get_bike_list($email,$phone)   /******* Get User By Phone ****/
        {
               $sql = "SELECT * FROM `bike_sell` where user_info->'$.email'='$email' AND user_info->'$.phone'='$phone'";
               $query = $this->db->query($sql);
               return $query->result_array();
        }
     public function fetch_bike_name()
        {
            $query = $this->db->get('bike_models');
            return $query->result();
        }
    public function get_buyer($user_info)
    {
        $sql = "SELECT * FROM buyer WHERE user_info='$user_info'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
	
	public function update_used_car($data){
     $id = $_POST['edit_id'];
     $this->db->where('id',$id);
     $this->db->update('car_sell',$data);
 }
                            
    }