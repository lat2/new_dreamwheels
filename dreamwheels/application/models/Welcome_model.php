<?php class Welcome_model extends CI_Model {
 
 private $perPage = 5;
	 public function __construct()
        {
                parent::__construct();
                
        }

 public function select_user_by_phone($phone,$pass)   /******* Get User By Phone ****/
        {
                $this->db->where('phone', $phone);
				$this->db->where('password', $pass);
                $query = $this->db->get('user');
                return $query->result();
        }

   public function select_user($email,$pass)   /******** Get User By email and password ***/
        {
               
                $this->db->where('email', $email);
                $this->db->where('password', $pass);
                $query = $this->db->get('user');
                return $query->result();
        }
        		

public function fetch_cities()
        {
            $query = $this->db->get('cities');
            return $query->result();
        }
        
 public function fetch_car_name()
        {
            $query = $this->db->get('car_names');
            return $query->result();
        }
   public function fetch_car_model(){
    $sql = "Select * from car_models" ;
    $query = $this->db->query($sql);
    return $query->result_array();

 }
 
 public function fetch_car_variant()
        {
            $query = $this->db->get('car_variants');
            return $query->result();
        }
  public function fetch_car_variant_by_model($model){
    $sql = "Select * from car_variants Where model_id=$model";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function fetch_car_type_by_model($model){
    $sql = "Select DISTINCT car_info->>'$.car_type' from cars2 Where car_info->'$.model'='$model'";
    $query = $this->db->query($sql);
    return $query->result_array();
  }        

public function fetch_city_by_id($city_id)
       {
           $sql = "SELECT city_name FROM cities WHERE city_id=$city_id";
           $query = $this->db->query($sql)->result();
           return $query;
       }        
        
public function car_list()
        {
             $sql = "SELECT * from car_sell where status=1";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
public function fetch_vehicle_by_car($car)
        {
             $sql = "SELECT * from car_sell where vehicle_info->'$.vehicle_info.car'='$car' and status=1";
             $query = $this->db->query($sql)->result();
             return $query;
        }

public function fetch_vehicle_by_model($car_model)
        {
             $sql = "SELECT * from car_sell where vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
public function fetch_used_vehicle_by_price($price)
    {
        if($price=="0-200000"){
           $sql = "SELECT * from car_sell where price<=200000 and status=1";
       } elseif ($price=="200000-500000") {
           $sql = "SELECT * from car_sell where (price>=200000 and price<=500000) and status=1";
       } elseif ($price=="500000-800000") {
           $sql = "SELECT * from car_sell where price>=500000 and price<=800000 and status=1";
       } elseif ($price=="800000-1000000") {
           $sql = "SELECT * from car_sell where price>=800000 and price<=1000000 and status=1";
       } elseif ($price=="1000000-100000000") {
           $sql = "SELECT * from car_sell where price>=1000000 and status=1";
       }
        $query = $this->db->query($sql)->result();
        return $query;
    }

public function fetch_used_vehicle_by_price_model($price,$car_model)
    {
       if($price=="0-200000"){
           $sql = "SELECT * from car_sell where price<=200000 and vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
       } elseif ($price=="200000-500000") {
           $sql = "SELECT * from car_sell where (price>=200000 and price<=500000) and vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
       } elseif ($price=="500000-800000") {
           $sql = "SELECT * from car_sell where price>=500000 and price<=800000 and vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
       } elseif ($price=="800000-1000000") {
           $sql = "SELECT * from car_sell where price>=800000 and price<=1000000 and vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
       } elseif ($price=="1000000-100000000") {
           $sql = "SELECT * from car_sell where price>=1000000 and vehicle_info->'$.vehicle_info.model'='$car_model' and status=1";
       }
	   else{
		    $sql = "SELECT * from car_sell where price<=200000 and vehicle_info->'$.vehicle_info.model'=$car_model and status=1";
	   }
    
	//echo $sql; die;
        $query = $this->db->query($sql)->result();
        return $query;
    }	
    
    public function fetch_new_vehicle_by_car($car)
        {
             $sql = "SELECT * from cars2 where car_info->'$.car'='$car' and status=1 GROUP BY car_info->'$.model'";
             $query = $this->db->query($sql)->result();
             return $query;
        }

public function fetch_new_vehicle_by_model($car_model)
        {
             $sql = "SELECT * from cars2 where car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
public function fetch_new_vehicle_by_price($price,$car_model)
    {
    //var_dump($price,$car_model);
    if($car_model!="")
        {
       if($price=="0-200000"){
           $sql = "SELECT * from cars2 where price<=200000 and car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="200000-500000") {
           $sql = "SELECT * from cars2 where (price>=200000 and price<=500000) and car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="500000-800000") {
           $sql = "SELECT * from cars2 where price>=500000 and price<=800000 car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="800000-1000000") {
           $sql = "SELECT * from cars2 where price>=800000 and price<=1000000 car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="1000000-100000000") {
           $sql = "SELECT * from cars2 where price>=1000000 car_info->'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
       }
    }elseif($car_model==""){
        if($price=="0-200000"){
           $sql = "SELECT * from cars2 where price<=200000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="200000-500000") {
           
           $sql = "SELECT * from cars2 where (price>=200000 and price<=500000) and status=1 GROUP BY car_info->'$.model'";
          // var_dump($price);
       } elseif ($price=="500000-800000") {
           $sql = "SELECT * from cars2 where price>=500000 and price<=800000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="800000-1000000") {
           $sql = "SELECT * from cars2 where price>=800000 and price<=1000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="1000000-100000000") {
           $sql = "SELECT * from cars2 where price>=1000000 and status=1 GROUP BY car_info->'$.model'";
       }
    }
    //var_dump($sql);
    //die;
        $query = $this->db->query($sql)->result();
        return $query;
    }
        
        public function fetch_latest_cars()
        {    
             $sql = "SELECT * from cars2 where car_info->>'$.category'='latest' and status=1 GROUP BY car_info->'$.model'";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
        public function fetch_latest_cars2()
        {    
             $sql = "SELECT * from cars2 where car_info->'$.category'='latest' and status=1 GROUP BY car_info->'$.model' ORDER BY id DESC limit 3";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
        public function fetch_latest_bikes()
        {    
             $sql = "SELECT * from bikes where bike_info->>'$.category'='latest'";
             $query = $this->db->query($sql)->result();
             return $query;
        }
        
        public function fetch_latest_bikes2()
        {    
             $sql = "SELECT * from bikes where bike_info->>'$.category'='latest' GROUP BY model_id ORDER BY id DESC limit 3"; 
             $query = $this->db->query($sql)->result();
             return $query;
        }

public function cars_list_by_price($price,$car_type)
    {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where (price>=100000 and price<=500000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where (price>=500000 and price<=1000000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where price>=1000000 and price<=2000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where price>=2000000 and price<=5000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where price>=5000000 and price<=10000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where price>10000000 and car_info->>'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       }
       
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
public function used_cars_list_by_price($price,$car_type)
    {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from car_sell where price<=500000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from car_sell where price>=500000 and price<=1000000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from car_sell where price>=1000000 and price<=2000000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from car_sell where price>=2000000 and price<=5000000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from car_sell where price>=5000000 and price<=10000000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from car_sell where price>10000000 and vehicle_info->'$.vehicle_info.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       }
        $query = $this->db->query($sql)->result();
        return $query;
    }

public function cars_list_by_model($car_name,$model_name)
{
    $sql = "SELECT * FROM cars2 WHERE car_info->>'$.car'='$car_name' AND car_info->>'$.model'='$model_name' and status=1 GROUP BY car_info->'$.model'";
    $query =  $this->db->query($sql)->result();
    return $query;
}
 public function fetch_model($car){
    $sql = "Select model_name,id from car_models where car_id=$car" ;
    $query = $this->db->query($sql);
    return $query->result_array();

 }
public function used_cars_list_by_model2($car_name,$model_name)
{
    $sql = "SELECT * FROM car_sell WHERE vehicle_info->'$.vehicle_info.car'='$car_name' AND vehicle_info->'$.vehicle_info.model'='$model_name' and status=1";
    $query =  $this->db->query($sql)->result();
    return $query;
}
    

public function vehicle_search()
        {
    
    //Car OR Bike > New OR Old > (Budget AND VehicleType) OR (Budget OR VehicleType)
   // print_r($_GET);die;
    
    $search_flow=$this->input->get("search_flow");
     
    $vehicle=$this->input->get("vehicle");
    
    $vehicle_condition=$this->input->get("vehicle_condition");
    
    $budget=$this->input->get("vehicle_budget");
    
    $vehicle_type=$this->input->get("vehicle_type");
    
    $brand=$this->input->get("vehicle_brand");
    
    $model=$this->input->get("vehicle_model");
    
    $vehicle_fuel_type=$this->input->get("vehicle_fuel_type");
    
    $filter="";
    $order_by="";
  
//New
if($vehicle_condition=='new')
       {
   if($vehicle=='car')
   {
       //Budget
       if($search_flow=='by_budget')
       {
     if($budget=="1-lakh-5-lakh"){
            $filter = "AND (t1.price>=100000 AND t1.price<=500000)";
       } elseif ($budget=="5-lakh-10-lakh") {
             $filter = "AND (t1.price>=500000 AND t1.price<=1000000)";
       } elseif ($budget=="10-lakh-20-lakh") {
             $filter = "AND (t1.price>=1000000 AND t1.price<=2000000)";
       } elseif ($budget=="20-lakh-50-lakh") {
             $filter = "AND (t1.price>=2000000 AND t1.price<=5000000)";
       } elseif ($budget=="50-lakh-1-crore") {
             $filter = "AND (t1.price>=5000000 AND t1.price<=10000000)";
       } elseif ($budget=="above-1-crore") {
             $filter = "AND (t1.price>10000000)";
       }
       elseif($budget=="high_to_low") {
           $order_by=" ORDER BY t1.price ASC";
         }
           elseif($budget=="low_to_high") {
             $order_by=" ORDER BY t1.price DESC";
         }
       else{
        $filter = " AND (t1.price>0)"; 
       }
       
    
       //Vehicle Type
   if($vehicle_type=="default" || $vehicle_type=="all_type"){
            $filter .= "";
       } else{
          $filter.= " AND t1.car_info->>'$.car_type'='$vehicle_type'";
       } 
       }
       else if($search_flow=='by_brand')
       {
           
           $filter=(isset($brand) && $brand!='default')?" AND t1.car_info->>'$.car'='$brand'":"";
           $filter.=(isset($model) && $model!='default')?" AND t1.car_info->>'$.model'='$model'":"";
           
       }
       
       //Vehicle Fuel Type
       
         if($vehicle_fuel_type=="default" || $vehicle_fuel_type=="all_type"){
            $filter .= "";
       } else{
           if(isset($vehicle_fuel_type))
           {
                $filter.= " AND t1.engine_transmission->>'$.fuel_type'='$vehicle_fuel_type'";
           }
         
       } 
  
   
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars

       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.status=1 $filter GROUP BY t1.car_info->>'$.model' $order_by LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.status=1 $filter GROUP BY t1.car_info->>'$.model' $order_by LIMIT ".$this->perPage;
       
     }
       
       
   }
   else
   {
   
       //New Bikes

           if($search_flow=='by_budget')
       {
     if($budget=="50000"){
            $filter = "AND (t1.price<=50000)";
       } elseif ($budget=="100000") {
             $filter = "AND (t1.price>=50000 AND t1.price<=100000)";
       } elseif ($budget=="100001") {
             $filter = "AND (t1.price>=100000)";
       }
       elseif($budget=="high_to_low") {
           $order_by=" ORDER BY t1.price ASC";
         }
           elseif($budget=="low_to_high") {
             $order_by=" ORDER BY t1.price DESC";
         }
       else{
        $filter = " AND (t1.price>0)"; 
       }
       
    
       //Vehicle Type
   if($vehicle_type=="default" || $vehicle_type=="all_type"){
            $filter .= "";
       } else{
          $filter.= " AND t1.bike_info->>'$.bike_type'='$vehicle_type'";
       } 
       }
       else if($search_flow=='by_brand')
       {
           
           $filter=(isset($brand) && $brand!='default')?" AND t1.bike_id='$brand'":"";
         
           
       }
       
           if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //bikes
       $sql="SELECT t1.*,t2.name as bike_name,t4.model_name,t3.variant FROM bikes t1 INNER JOIN bike_models t4 ON(t4.id=t1.model_id) INNER JOIN bike_names t2 ON(t1.bike_id=t2.id) INNER JOIN bike_variants t3 ON(t3.model_id=t4.id) WHERE t1.status=1 $filter GROUP BY t1.model_id $order_by LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.name as bike_name,t3.variant,t4.model_name FROM bikes t1 INNER JOIN bike_models t4 ON(t4.id=t1.model_id) INNER JOIN bike_names t2 ON(t1.bike_id=t2.id) INNER JOIN bike_variants t3 ON(t3.model_id=t4.id) WHERE t1.status=1 $filter GROUP BY t1.bike_id $order_by LIMIT ".$this->perPage;
       
     }
     
   // echo $sql;
   }
   }
   else{
      
       //Old Vehicles
       
         if($vehicle=='car')
   {
       //Budget
       if($search_flow=='by_budget')
       {
     if($budget=="1-lakh-5-lakh"){
            $filter = "AND (t1.price>=100000 AND t1.price<=500000)";
       } elseif ($budget=="5-lakh-10-lakh") {
             $filter = "AND (t1.price>=500000 AND t1.price<=1000000)";
       } elseif ($budget=="10-lakh-20-lakh") {
             $filter = "AND (t1.price>=1000000 AND t1.price<=2000000)";
       } elseif ($budget=="20-lakh-50-lakh") {
             $filter = "AND (t1.price>=2000000 AND t1.price<=5000000)";
       } elseif ($budget=="50-lakh-1-crore") {
             $filter = "AND (t1.price>=5000000 AND t1.price<=10000000)";
       } elseif ($budget=="above-1-crore") {
             $filter = "AND (t1.price>10000000)";
       }
       else{
        $filter = "AND (t1.price>0)"; 
       }
       
    
       //Vehicle Type
     if($vehicle_type=="default" || $vehicle_type=="all_type"){
            $filter .= "";
       } else{
          $filter .= " AND t1.vehicle_info->>'$.vehicle_info.car_type'='$vehicle_type'";
       } 
     } else if($search_flow=='by_brand')
       {
           
           $filter=(isset($brand) && $brand!='default')?" AND t1.vehicle_info->>'$.vehicle_info.car'='$brand'":"";
           $filter.=(isset($model) && $model!='default')?" AND t1.vehicle_info->>'$.vehicle_info.model'='$model'":"";
           
       }
       
            //Vehicle Fuel Type
       
         if($vehicle_fuel_type=="default" || $vehicle_fuel_type=="all_type"){
            $filter .= "";
       } else{
           if(isset($vehicle_fuel_type))
           {
                $filter.= " AND t1.vehicle_info->>'$.vehicle_info.fuel'='$vehicle_fuel_type'";
           }
         
       } 
  
   
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Used Cars
 $sql="SELECT t1.*,t1.vehicle_info as car_info,t2.car_name,t2.model_name,t3.variant FROM `car_sell` t1 INNER JOIN car_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model') INNER JOIN car_variants t3 ON(t3.id=t1.vehicle_info->>'$.vehicle_info.varient') WHERE t1.status=1 $filter GROUP BY t1.vehicle_info->>'$.vehicle_info.model' LIMIT $start,".$this->perPage;
     }else{
         
          $sql="SELECT t1.*,t1.vehicle_info as car_info,t2.car_name,t2.model_name,t3.variant FROM `car_sell` t1 INNER JOIN car_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model') INNER JOIN car_variants t3 ON(t3.id=t1.vehicle_info->>'$.vehicle_info.varient') WHERE t1.status=1 $filter GROUP BY t1.vehicle_info->>'$.vehicle_info.model' LIMIT ".$this->perPage;
     
       
     }
       
       
   }
   else
   {
       
       
           if($search_flow=='by_budget')
       {
     if($budget=="50000"){
            $filter = "AND (t1.price<=50000)";
       } elseif ($budget=="100000") {
             $filter = "AND (t1.price>=50000 AND t1.price<=100000)";
       } elseif ($budget=="100001") {
             $filter = "AND (t1.price>=100000)";
       }
       elseif($budget=="high_to_low") {
           $order_by=" ORDER BY t1.price ASC";
         }
           elseif($budget=="low_to_high") {
             $order_by=" ORDER BY t1.price DESC";
         }
       else{
        $filter = " AND (t1.price>0)"; 
       }
       
    
       //Vehicle Type
   if($vehicle_type=="default" || $vehicle_type=="all_type"){
            $filter .= "";
       } else{
          $filter.= " AND t1.vehicle_info->>'$.vehicle_type'='$vehicle_type'";
       } 
       }
       else if($search_flow=='by_brand')
       {
           $filter=(isset($brand) && $brand!='default')?" AND t1.vehicle_info->>'$.vehicle_info.bike_id'='$brand'":"";
       }
       
       
       
       
           if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.status=1 $filter GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' $order_by LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.status=1 $filter GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' $order_by LIMIT ".$this->perPage;
       
     }
   }
       
   }
   //echo $sql;
 $query = $this->db->query($sql);
        return $query->result();

        }

public function fetch_new_cars()
        {
             $this->db->where('status',1);
			 $this->db->group_by("car_info->'$.model'");
             $query = $this->db->get('cars2');
             return $query->result(); 
        }
        
public function fetch_new_cars_ajax($price=FALSE,$car_type=FALSE)
        {
   $filter="";
    if($price && $car_type)
    {
             if($price=="1-lakh-5-lakh"){
            $filter = "AND (t1.price>=100000 AND t1.price<=500000 AND t1.car_info->>'$.car_type'='$car_type')";
       } elseif ($price=="5-lakh-10-lakh") {
             $filter = "AND (t1.price>=500000 AND t1.price<=1000000 AND t1.car_info->>'$.car_type'='$car_type')";
       } elseif ($price=="10-lakh-20-lakh") {
             $filter = "AND (t1.price>=1000000 AND t1.price<=2000000 AND t1.car_info->>'$.car_type'='$car_type')";
       } elseif ($price=="20-lakh-50-lakh") {
             $filter = "AND (t1.price>=2000000 AND t1.price<=5000000 AND t1.car_info->>'$.car_type'='$car_type')";
       } elseif ($price=="50-lakh-1-crore") {
             $filter = "AND (t1.price>=5000000 AND t1.price<=10000000 AND t1.car_info->>'$.car_type'='$car_type')";
       } elseif ($price=="above-1-crore") {
             $filter = "AND (t1.price>10000000 AND t1.car_info->>'$.car_type'='$car_type')";
    }
    
    }
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars

       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.status=1 $filter GROUP BY t1.car_info->>'$.model' LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.status=1 $filter GROUP BY t1.car_info->>'$.model' LIMIT ".$this->perPage;
       
     }
    
 $query = $this->db->query($sql);
        return $query->result();

        }
        
     public function fetch_upcomming_car_listing_ajax()
        {
        //$sql = "SELECT * from cars2 WHERE car_info->'$.category'='upcoming' and status=1 GROUP BY car_info->'$.model'";
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.car_info->'$.category'='upcoming' AND t1.status=1 GROUP BY t1.car_info->>'$.model' LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM cars2 t1 INNER JOIN car_models t2 ON(t2.id=t1.car_info->>'$.model') INNER JOIN car_variants t3 ON(t3.id=t1.car_info->>'$.variant') WHERE t1.car_info->'$.category'='upcoming' AND t1.status=1 GROUP BY t1.car_info->>'$.model' LIMIT ".$this->perPage;
       
     }
     
 $query = $this->db->query($sql);
        return $query->result();

        }   
        public function fetch_car_listing_ajax()
        {
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM car_sell t1 INNER JOIN car_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model') INNER JOIN car_variants t3 ON(t3.id=t1.vehicle_info->>'$.vehicle_info.varient') WHERE t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model' LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.car_name,t2.model_name,t3.variant FROM car_sell t1 INNER JOIN car_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model') INNER JOIN car_variants t3 ON(t3.id=t1.vehicle_info->>'$.vehicle_info.varient') WHERE t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model' LIMIT ".$this->perPage;
       
     }
     //echo $sql; die;
 $query = $this->db->query($sql);
        return $query->result();

        }
        
        
    public function fetch_car_by_id($id)
        {    
           $this->db->where('id', $id);
		   $this->db->group_by("car_info->'$.model'");
           $this->db->where('status',1);
             $query = $this->db->get('cars2');
             return $query->result();
        }

	public function fetch_model_variants($id)
        {    
           $sql = "SELECT t1.car_info->'$.model',t2.id,t2.car_info->>'$.variant' as variant,t2.car_info->>'$.price' as price,t2.engine_transmission->>'$.fuel_type' as fuel_type FROM `cars2` t1 INNER JOIN cars2 t2 ON (t1.car_info->>'$.model'=t2.car_info->>'$.model') where t1.id=$id and t1.status=1";
		   $query = $this->db->query($sql);
             return $query->result();
        }  		
        
    public function fetch_used_car_by_id($id)
        {    
           $this->db->where('id', $id);
           $this->db->where('status',1);
             $query = $this->db->get('car_sell');
             return $query->result();
        }    
  public function Insert_post_data($data){ /***** Insert Requirment Data *****/

  $query = $this->db->insert('post_requirment',$data);
 return $query;
  } 

  public function fetch_all_model($car){
  $sql = "SELECT vehicle_info->'$.vehicle_info.model' as model FROM `car_sell` WHERE vehicle_info->'$.vehicle_info.car'='$car' and status=1";
  $query =$this->db->query($sql)->result();
  return $query;
  } 

 public function fetch_all_cars($car_name,$model,$fuel_type,$price){
	 //echo $car_name,$model,$fuel_type,$price;
  if($car_name!='' && $model!='' && $fuel_type=='' && $price==''){
      $sql = "SELECT * FROM `cars2` WHERE car_info->>'$.car'=$car_name and car_info->>'$.model'=$model  and status=1 GROUP BY car_info->'$.model'";
  }elseif($car_name!='' && $model=='' && $fuel_type=='' && $price==''){
      $sql = "SELECT * FROM `cars2` WHERE car_info->>'$.car'=$car_name and status=1 GROUP BY car_info->'$.model'";
  }elseif($car_name!='' && $model!='' && $fuel_type!='' && $price==''){
      $sql = "SELECT * FROM `cars2` WHERE car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and engine_transmission->'$.fuel_type'='$fuel_type' and status=1 GROUP BY car_info->'$.model'";
  }elseif($car_name=='' && $model=='' && $fuel_type!='' && $price==''){
      $sql = "SELECT * FROM `cars2` WHERE engine_transmission->'$.fuel_type'='$fuel_type' and status=1 GROUP BY car_info->'$.model'";
  }elseif($car_name=='' && $model=='' && $fuel_type!='' && $price==''){
      $sql = "SELECT * FROM `cars2` WHERE engine_transmission->>'$.fuel_type'='$fuel_type' and status=1 GROUP BY car_info->'$.model'";
  }elseif($car_name=='' && $model=='' && $fuel_type=='' && $price!=''){
      if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where (price>=100000 and price<=500000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where (price>=500000 and price<=1000000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where price>=1000000 and price<=2000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where price>=2000000 and price<=5000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where price>=5000000 and price<=10000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where price>10000000 and status=1 GROUP BY car_info->'$.model'";
       } 
  }elseif ($car_name!='' && $model!='' && $fuel_type!='' && $price!='') {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and (price>=100000 and price<=500000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and (price>=500000 and price<=1000000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and price>=1000000 and price<=2000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and price>=2000000 and price<=5000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and price>=5000000 and price<=10000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and car_info->>'$.fuel_type'=$fuel_type and price>10000000 and car_info->>'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       }     
        }elseif ($car_name!='' && $model!='' && $fuel_type=='' && $price!='') {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and (price>=100000 and price<=500000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and (price>=500000 and price<=1000000) and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and price>=1000000 and price<=2000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and price>=2000000 and price<=5000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and price>=5000000 and price<=10000000 and car_info->'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.car'=$car_name and car_info->>'$.model'=$model and price>10000000 and car_info->>'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
       }     
        }

$query = $this->db->query($sql)->result();
  return $query;       
}

public function fetch_all_cars_by_price($model,$price){
if ($model!='' && $price!='') {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and (price>=100000 and price<=500000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and (price>=500000 and price<=1000000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and price>=1000000 and price<=2000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and price>=2000000 and price<=5000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and price>=5000000 and price<=10000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where car_info->>'$.model'=$model and price>10000000and status=1 GROUP BY car_info->'$.model'";
       }     
        }elseif ($model=='' && $price!='') {
       if($price=="1-lakh-5-lakh"){
           $sql = "SELECT * from cars2 where (price>=100000 and price<=500000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="5-lakh-10-lakh") {
           $sql = "SELECT * from cars2 where (price>=500000 and price<=1000000) and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from cars2 where price>=1000000 and price<=2000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from cars2 where price>=2000000 and price<=5000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from cars2 where price>=5000000 and price<=10000000 and status=1 GROUP BY car_info->'$.model'";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from cars2 where price>10000000and status=1 GROUP BY car_info->'$.model'";
       }     
        }
$query = $this->db->query($sql)->result();
  return $query;       
}

public function fetch_all_cars2($car_type)
{
    $sql = "SELECT * from cars2 where car_info->>'$.car_type'='$car_type' and status=1 GROUP BY car_info->'$.model'";
    $query = $this->db->query($sql)->result();
    return $query;       
}

public function search_vehicles($info)
{
    //$sql = "SELECT * from cars2 where car_info->>'$.car_type'='$car_type' and status=1";
	$sql = "SELECT *  FROM `dreamwheels`.`car_models` JOIN `dreamwheels`.`cars2` ON(`dreamwheels`.`cars2`.car_info->>'$.model'=`dreamwheels`.`car_models`.id) WHERE (CONVERT(`car_models`.`id` USING utf8) LIKE '%$info %' OR CONVERT(`car_models`.`car_id` USING utf8) LIKE '%$info%' OR CONVERT(`car_models`.`car_name` USING utf8) LIKE '%$info%' OR CONVERT(`car_models`.`model_name` USING utf8) LIKE '%$info%') and status=1";
    $query = $this->db->query($sql)->result();
    return $query;       
}

public function search_vehicles_bike($info)
{
    //$sql = "SELECT * from cars2 where car_info->>'$.car_type'='$car_type' and status=1";
	$sql = "SELECT *  FROM `dreamwheels`.`bike_models` JOIN `dreamwheels`.`bikes` ON(`dreamwheels`.`bikes`.model_id=`dreamwheels`.`bike_models`.id) WHERE (CONVERT(`bike_models`.`id` USING utf8) LIKE '%$info%' OR CONVERT(`bike_models`.`bike_id` USING utf8) LIKE '%$info%' OR CONVERT(`bike_models`.`bike_name` USING utf8) LIKE '%$info%' OR CONVERT(`bike_models`.`model_name` USING utf8) LIKE '%$info%')";
    $query = $this->db->query($sql)->result();
    return $query;       
}

public function fetch_all_cars3($car)
{
    $sql = "SELECT * from cars2 where car_info->>'$.car'='$car' and status=1 GROUP BY car_info->'$.model'";
    $query = $this->db->query($sql)->result();
    return $query;       
}

public function fetch_all_cars4($car_model)
{
    $sql = "SELECT * from cars2 where car_info->>'$.model'='$car_model' and status=1 GROUP BY car_info->'$.model'";
    $query = $this->db->query($sql)->result();
    return $query;       
}
public function fetch_list_cars(){
 $sql = "SELECT DISTINCT car_name As cars,id from car_names where status=1";
 $query =$this->db->query($sql)->result();
 return $query;
}

public function cars_all(){
 $sql = "SELECT distinct vehicle_info->'$.vehicle_info.car' As car_name FROM `car_sell` where status=1";
 $query =$this->db->query($sql)->result();
 return $query;
}

public function cars_name(){
 $sql = "SELECT distinct car_info->'$.car' As car_name FROM `cars` where status=1";
 $query =$this->db->query($sql)->result();
 return $query;
}

public function featured_car(){   /********** Get All Featured Cars *********/
  $sql = "SELECT * from cars2 WHERE car_info->'$.category'='featured' and status=1 GROUP BY car_info->'$.model'";
  $query = $this->db->query($sql)->result();
  return $query;
}



public function used_car(){   /********** Get All Used Cars *********/
  $sql = "SELECT * from car_sell WHERE status=1 ORDER BY RAND() LIMIT 4";
  $query = $this->db->query($sql)->result();
  return $query;
}

public function upcoming_car(){    /*************** Upcoming Car ***********/
  $sql = "SELECT * from cars2 WHERE car_info->'$.category'='upcoming' and status=1 GROUP BY car_info->'$.model'";
  $query = $this->db->query($sql)->result();
  return $query;
}

public function recommended_car(){  /****************** Recommended Car *************/
  $sql = "SELECT * from cars2 WHERE car_info->'$.category'='recommended' and status=1 GROUP BY car_info->'$.model'";
  $query = $this->db->query($sql)->result();
  return $query;
}

public function compare_car1($car1,$model1,$variant1){  /****************** Recommended Car *************/
  $sql = "SELECT car_info->'$.images[0]' As car_image,car_info->'$.car' As car_name  from cars2 WHERE car_info->'$.car'='$car1' and car_info->'$.model'='$model1' And car_info->'$.variant' ='$variant1' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_cars1($carNames1,$modelNames1,$variantNames1){ 
  $sql = "SELECT * from cars2 WHERE car_info->'$.car'='$carNames1' and car_info->'$.model'='$modelNames1' And car_info->'$.variant' ='$variantNames1' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_cars2($carNames2,$modelNames2,$variantNames2){ 
  $sql = "SELECT * from cars2 WHERE car_info->'$.car'='$carNames2' and car_info->'$.model'='$modelNames2' And car_info->'$.variant' ='$variantNames2' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_cars3($carNames3,$modelNames3,$variantNames3){ 
  $sql = "SELECT * from cars2 WHERE car_info->'$.car'='$carNames3' and car_info->'$.model'='$modelNames3' And car_info->'$.variant' ='$variantNames3' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

public function get_carId($car_name,$car_model,$car_variant){
$sql = "SELECT id from cars WHERE car_info->'$.car'='$car_name' and car_info->'$.model'='$car_model' And car_info->'$.variant' ='$car_variant' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
} 

public function compare($arr_car){
  $sql = "SELECT * from cars where id IN($arr_car) and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

public function get_price($car,$model,$variant){
  $sql = "SELECT car_info->>'$.price[0]' As price2,price from cars2 WHERE car_info->'$.car'='$car' and car_info->'$.model'='$model' And car_info->'$.variant' ='$variant' and status=1";
  //echo $sql;
   $query = $this->db->query($sql)->result();
  return $query;
}

public function get_price_calc($car,$model,$variant){
  $sql = "SELECT price from cars2 WHERE car_info->'$.car'='$car' and car_info->'$.model'='$model' And car_info->'$.variant' ='$variant' and status=1";
  //echo $sql;
   $query = $this->db->query($sql)->result();
  return $query;
}

public function used_bikes_list_by_price_city($price,$city)
    {
       if($city!=""){
                if($price=='50000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.city'='$city' AND  price<=$price and status=1"; 
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.city'='$city' AND price<='100000' AND price>='50000' and status=1"; 
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.city'='$city' AND price>='100000' and status=1";
                }
       }elseif($city==""){
                if($price=='50000'){
                    $sql = "SELECT * from bike_sell where price<=$price and status=1"; 
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bike_sell where price<='100000' AND price>='50000' and status=1"; 
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bike_sell where price>='100000' and status=1";
                }
       }   
           
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function used_cars_list_by_price_city($price,$city)
    {
       if($price=='50000'){
           $sql = "SELECT * from car_sell where price<='100000' and price<='200000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       }elseif($price=='50000-100000'){
           $sql = "SELECT * from car_sell where price<='500000' and price<='1000000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       } elseif ($price=="10-lakh-20-lakh") {
           $sql = "SELECT * from car_sell where price<='1000000' and price<='2000000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       } elseif ($price=="20-lakh-50-lakh") {
           $sql = "SELECT * from car_sell where price<='2000000' and price<='5000000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       } elseif ($price=="50-lakh-1-crore") {
           $sql = "SELECT * from car_sell where price<='5000000' and price<='10000000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       } elseif ($price=="above-1-crore") {
           $sql = "SELECT * from car_sell where price>'10000000' and vehicle_info->'$.car_type'='$car_type' and status=1";
       }
        $query = $this->db->query($sql)->result();
        return $query;
    }
	
         ///////////////////////////////////
        ///////////////////////////////////
       ///////////////BIKES///////////////
      ///////////////////////////////////
     ///////////////////////////////////
    
    
    public function fetch_new_bikes_ajax()
        {
          if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //bikes
       $sql="SELECT t1.*,t2.name as bike_name,t4.model_name,t3.variant FROM bikes t1 INNER JOIN bike_models t4 ON(t4.id=t1.model_id) INNER JOIN bike_names t2 ON(t1.bike_id=t2.id) INNER JOIN bike_variants t3 ON(t3.model_id=t4.id) WHERE t1.status=1 GROUP BY t1.model_id LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.name as bike_name,t3.variant,t4.model_name FROM bikes t1 INNER JOIN bike_models t4 ON(t4.id=t1.model_id) INNER JOIN bike_names t2 ON(t1.bike_id=t2.id) INNER JOIN bike_variants t3 ON(t3.model_id=t4.id) WHERE t1.status=1 GROUP BY t1.bike_id LIMIT ".$this->perPage;
       
     }
      $query = $this->db->query($sql);
        return $query->result();
        }
        
    public function fetch_new_bikes()
        {
          //$sql = "SELECT DISTINCT model_id,bike_id,variant_id,bike_info,engine_transmission,tyres_brakes,feature_safety,chasis_suspension,dimension,electricals,price_info,status,id FROM `bikes` WHERE status='1'";
          //$query = $this->db->query($sql);
            $this->db->where('status',1);
			$this->db->group_by('model_id');
            $query = $this->db->get('bikes');
            return $query->result();
        }
    
    public function fetch_new_launched_bikes()
        {
            $sql = "SELECT * FROM bikes WHERE bike_info->'$.category'='latest' and status=1 GROUP BY model_id";
            $query = $this->db->query($sql)->result();
            return $query;
        }
        
	public function fetch_bike_model_variants($id)
        {    
           $sql = "SELECT t2.id,t2.variant_id as variant,t2.bike_info->>'$.price' as price FROM `bikes` t1 INNER JOIN bikes t2 ON (t1.model_id=t2.model_id) where t1.id=$id and t1.status=1";
		   $query = $this->db->query($sql);
             return $query->result();
        }  	
        public function fetch_featured_bikes()
        {
            $sql = "SELECT * FROM bikes WHERE bike_info->'$.category'='featured' and status=1 GROUP BY model_id";
            $query = $this->db->query($sql)->result();
            return $query;
        } 
        
        public function fetch_bike_names()
        {
            $query = $this->db->get('bike_names');
            return $query->result();
        }
        
               public function fetch_bike_listing_ajax()
        {
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' LIMIT ".$this->perPage;
       
     }
    //echo $sql; die;
 $query = $this->db->query($sql);
        return $query->result();

        }
        
 public function new_launch_bikes_ajax_data()
        {
  
     if(!empty($this->input->get("page"))){
       $start = ceil($this->input->get("page") * $this->perPage);
       //Cars
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.bike_info->'$.category'='latest' AND t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' LIMIT $start,".$this->perPage;
     }else{
       $sql="SELECT t1.*,t2.bike_name,t2.model_name,t3.variant FROM bike_sell t1 INNER JOIN bike_models t2 ON(t2.id=t1.vehicle_info->>'$.vehicle_info.model_id') INNER JOIN bike_variants t3 ON(t3.bike_id=t1.vehicle_info->>'$.vehicle_info.bike_id') WHERE t1.bike_info->'$.category'='latest' AND t1.status=1 GROUP BY t1.vehicle_info->>'$.vehicle_info.model_id' LIMIT ".$this->perPage;
       
     }
    //echo $sql; die;
 $query = $this->db->query($sql);
        return $query->result();

        }
        
       
		public function fetch_bike_models2()
    {
        $query = $this->db->get('bike_models');
            return $query->result();
    }
        public function fetch_bike_variants2()
		{
		    $query = $this->db->get('bike_variants');
			return $query->result();
		}
    
     public function bikes_list_by_model_id($model_id)
    {
       $sql = "SELECT * from bikes where model_id='$model_id' and status=1 GROUP BY model_id";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function bikes_list_by_price($price)
    {
        $sql = "SELECT * from bikes where price<=$price and status=1 GROUP BY model_id";
		//var_dump($sql);die;
        $query = $this->db->query($sql)->result();
        return $query;
    }
    public function bikes_list_by_price2($price)
    {
        $sql = "SELECT * from bikes where price>100000 and status=1 GROUP BY model_id";
        $query = $this->db->query($sql)->result();
        return $query;
    }
	
	public function fetch_bikes_list($bike_id,$model_id,$price)
    {
		if($bike_id!="" && $model_id=="" && $price==""){
        $sql = "SELECT * from bikes where bike_id='$bike_id' and status=1 GROUP BY model_id";
		}elseif($bike_id!="" && $model_id!="" && $price==""){
        $sql = "SELECT * from bikes where bike_id='$bike_id' and model_id='$model_id' and status=1 GROUP BY model_id";
		}elseif($bike_id!="" && $model_id=="" && $price!=""){
        
		        if($price=='50000'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and price<50000 and status=1 GROUP BY model_id";
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and price>50000 and price<100000 and status=1 GROUP BY model_id";
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and price>100000 and status=1 GROUP BY model_id";
                }
		}elseif($bike_id!="" && $model_id!="" && $price!=""){
        
		        if($price=='50000'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and model_id='$model_id' and price<50000 and status=1 GROUP BY model_id";
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and model_id='$model_id' and price>50000 and price<100000 and status=1 GROUP BY model_id";
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bikes where bike_id='$bike_id' and model_id='$model_id' and price>100000 and status=1 GROUP BY model_id";
                }
		}elseif($bike_id=="" && $model_id=="" && $price!=""){
        
		        if($price=='50000'){
                    $sql = "SELECT * from bikes where price<50000 and status=1 GROUP BY model_id";
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bikes where price>50000 and price<100000 and status=1 GROUP BY model_id";
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bikes where price>100000 and status=1 GROUP BY model_id";
                }
		}
		//echo $sql;
        $query = $this->db->query($sql)->result();
        return $query;
    }
	
	public function bikes_list_by_type($bike_type)
    {
        $sql = "SELECT * from bikes where bike_info->'$.bike_type'='$bike_type' and status=1 GROUP BY model_id";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    public function used_bikes_list_by_price2($price)
    { 
        if($price!='Above'){
       $sql = "SELECT * from bike_sell where price<=$price and status=1";
        }else{
       $sql = "SELECT * from bike_sell where price>$price and status=1";     
        }
        $query = $this->db->query($sql)->result();
        return $query;
    }
                
    public function fetch_bike_by_id($id)
        {    
           $this->db->where('id', $id);
             $query = $this->db->get('bikes');
             return $query->result();
        }
        
    public function fetch_used_bike_by_id($id)
        {    
           $this->db->where('id', $id);
           $this->db->where('status', 1);
             $query = $this->db->get('bike_sell');
             return $query->result();
        }    
        
    public function fetch_bike_models($bike_id)
    {
        $sql = "SELECT * from bike_models where bike_id='$bike_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function fetch_bike_names2($model_id)
    {
        $sql = "SELECT * from bike_models where id='$model_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function fetch_bike_variants($model_id)
    {
        $sql = "SELECT * from bike_variants where model_id='$model_id'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function get_bike_price($bike_id,$model_id,$variant_id){
        $sql = "SELECT price from bikes WHERE bike_id='$bike_id' and model_id='$model_id' And variant_id ='$variant_id'  and status=1";
        //echo $sql;
         $query = $this->db->query($sql)->result();
        return $query;
    }
    
        public function used_bikes_list()
    {
//        $sql = "SELECT t1.*,t2.bike_name,t2.model_name from bike_sell INNER JOIN from bike_models ON(t2.id=t1.vehicle_info->'$.model_id')";
//        $query = $this->db->query($sql)->result();
//        return $query;
            $this->db->where('status', 1);
        $query = $this->db->get('bike_sell');
             return $query->result();
    }
    
    public function insert_buyer_details($buyer_name,$buyer_email,$buyer_phone){ 
  $sql = "INSERT INTO buyer_info WHERE id='$bike3' and model_id='$model3' And variant_id ='$variant3'";
  $query = $this->db->query($sql)->result();
  return $query;
}
    
    public function bike_names_models()
    {
        $query = $this->db->get('bike_models');
        return $query->result();
    }   
    
    public function used_bikes_list_by_bike_id($bike_id)
    {
       $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' and status=1";
        $query = $this->db->query($sql)->result();
        return $query;
    }

    public function used_bikes_list_by_model_id($model_id)
    {
       $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.model_id'='$model_id' and status=1";
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function used_bikes_list_by_price($price,$bike_id,$model_id)
    {
       if($bike_id!="" && $model_id!="")
           {
                if($price=='50000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.model_id'='$model_id' AND price<=$price and status=1"; 
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.model_id'='$model_id' AND price<=100000 AND price>=50000 and status=1"; 
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.model_id'='$model_id' AND price>=100000 and status=1";

                }

           }else if($bike_id!="" && $model_id==""){
                if($price=='50000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND  price<=$price and status=1"; 
                }elseif($price=='50000-100000'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND price<=100000 AND price>=50000 and status=1"; 
                }elseif($price=='1 lac & above'){
                    $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND price>=100000 and status=1";

                }
           
           }
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
     public function used_bikes_list_by_kms($kms,$price,$bike_id,$model_id)
    {
       if($bike_id!="" && $model_id!="" && $price!="")
           {
           $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.model_id'='$model_id' AND vehicle_info->'$.vehicle_info.kilometer_driven'<=$kms AND price<=$price and status=1";    
           }else if($bike_id!="" && $model_id=="" && $price!="")
               {               
           $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND  vehicle_info->'$.vehicle_info.kilometer_driven'<='$kms' AND   price<=$price and status=1";
           }else if($bike_id!="" && $model_id!="" && $price=="")
               {               
           $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.model_id'='$model_id' AND vehicle_info->'$.vehicle_info.kilometer_driven'<=$kms and status=1";
           }else if($bike_id!="" && $model_id=="" && $price=="")
               {               
           $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.bike_id'='$bike_id' AND vehicle_info->'$.vehicle_info.kilometer_driven'<=$kms and status=1";
           }else{ 
       $sql = "SELECT * from bike_sell where vehicle_info->'$.vehicle_info.kilometer_driven'<='$kms' and status=1";
           }
        $query = $this->db->query($sql)->result();
        return $query;
    }
    
    public function compare_bike1($bike1,$model1,$variant1){ 
  $sql = "SELECT bike_info->'$.images[0]' As bike_image from bikes WHERE bike_id='$bike1' and model_id='$model1' And variant_id ='$variant1'  and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_bikes1($bike1,$model1,$variant1){ 
  $sql = "SELECT * from bikes WHERE bike_id='$bike1' and model_id='$model1' And variant_id ='$variant1' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_bikes2($bike2,$model2,$variant2){ 
  $sql = "SELECT * from bikes WHERE bike_id='$bike2' and model_id='$model2' And variant_id ='$variant2' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function compare_all_bikes3($bike3,$model3,$variant3){ 
  $sql = "SELECT * from bikes WHERE bike_id='$bike3' and model_id='$model3' And variant_id ='$variant3' and status=1";
  $query = $this->db->query($sql)->result();
  return $query;
}

 public function fetch_bike_by_type_displacement($bike_type,$engine_displacement,$id)
 {
     
	$eng_disp = trim(str_replace("cc","",strtolower($engine_displacement))," ");
        $eng_disp=intval(str_replace(',', '',$eng_disp));
        //echo $eng_disp;die;
     $engine_displacement1 = ($eng_disp+50);
     $engine_displacement2 = ($eng_disp-50);
	 
     if($bike_type=='scooter')
         {
         $sql = "SELECT * from bikes WHERE bike_info->>'$.bike_type'='$bike_type' and engine_transmission->>'$.displacement'<=$engine_displacement1 and engine_transmission->>'$.displacement'>=$engine_displacement2 and id!='$id' and status=1 GROUP BY model_id";
     }else{
         $sql = "SELECT * from bikes WHERE engine_transmission->>'$.displacement'<=$engine_displacement1 and engine_transmission->>'$.displacement'>=$engine_displacement2 and id!='$id' and status=1 GROUP BY model_id";
     }
     
  $query = $this->db->query($sql)->result();
  return $query;
 }
 
 /********** Get All Featured Bikes *********/
 public function get_bike_list_via_category($category=FALSE){   
     if(!$category)
     {
        $category="featured";
     }
  $sql = "SELECT t1.*,t1.id as bkid,t2.* from bikes t1 INNER JOIN `bike_models` t2 ON(t2.bike_id=t1.bike_id) WHERE t1.bike_info->>'$.category'='$category'  AND t1.status=1 ORDER BY RAND() LIMIT 8";
  
        $query = $this->db->query($sql);
        return $query->result();
}

/********** Get All Used Bikes *********/
public function get_bike_list_via_condition_old(){   
   $sql = "SELECT t1.*,t1.id as bkid,t2.* from `bike_sell` t1 INNER JOIN `bike_models` t2 ON(t1.vehicle_info->>'$.vehicle_info.bike_id'=t2.bike_id) WHERE t1.status=1 ORDER BY RAND() LIMIT 8";
  
  $query = $this->db->query($sql)->result();
  return $query;
}



 /********** Get All Featured Cars *********/
 public function get_car_list_via_category($category=FALSE){   
     if(!$category)
     {
        $category="featured";
     }
  $sql = "SELECT t1.*,t1.id as ckid,t2.* from cars2 t1 INNER JOIN `car_models` t2 ON(t2.car_id=t1.car_info->>'$.car') WHERE t1.car_info->>'$.category'='$category'  AND t1.status=1 ORDER BY RAND() LIMIT 8";
  
        $query = $this->db->query($sql);
        return $query->result();
}

/********** Get All Used Cars *********/
public function get_car_list_via_condition_old(){   
   $sql = "SELECT t1.*,t1.id as ckid,t2.* from `car_sell` t1 INNER JOIN `car_models` t2 ON(t1.vehicle_info->>'$.vehicle_info.car'=t2.car_id) WHERE t1.status=1 ORDER BY RAND() LIMIT 8";
  
  $query = $this->db->query($sql)->result();
  return $query;
}
}
