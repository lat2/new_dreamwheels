<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
public function __construct() 
{   
parent::__construct();
$this->load->database();
$this->load->model('Admin_model');
$this->load->library('session');
$this->load->helper('global_functions');
}
public function index()   /********** Home Page or Index Page For Admin *******/
{
	$this->load->view('admin/login');
}

public function dashboard(){  /***************** Dashboard Panel ********/

        $data['query_users'] = $this->Admin_model->fetch_users();
        $data['query_buyers'] = $this->Admin_model->fetch_buyers();
        $data['query'] = $this->Admin_model->car_listing();
        $data['query_bike'] = $this->Admin_model->bike_listing();    
        $this->load->view('admin/index',$data);
}

 
public function login(){ /************* Login Panel for Admin *****/
	$email = $this->input->post('email');
	$pass = $this->input->post('password');

	$query = $this->Admin_model->login($email,$pass);
    
    if($query==1){
    $this->session->set_userdata('email',$email);
    redirect('admin/dashboard');	
    }else{
      $msg = "Error";
    header('Location: ' . $_SERVER['HTTP_REFERER']);
 
    }

}

public function my_account(){
$email = $this->session->userdata('email');
$data['query'] = $this->Admin_model->select_admin_by_phone($email);	
$this->load->view('admin/account_details',$data);
}
public function upload_car(){
	$this->load->view('admin/upload');
}

public function add_car(){       /*********** Add car Page ****/      
   $data['query'] = $this->Admin_model->fetch_car_name();  
   
	$this->load->view('admin/add_car',$data);
}
public function vehicle_make(){       /*********** Vehicle Make Page ****/       
   
    $data['query'] = $this->Admin_model->fetch_car_name();
	$this->load->view('admin/vehicle_make',$data);
}
public function add_vehicle_make(){       /*********** Vehicle Make Page ****/       
   
	$this->load->view('admin/add_make');
}
public function bike_make(){       /*********** Vehicle Make Page ****/       
   
	$data['query_name'] = $this->Admin_model->fetch_bike_names();
	$this->load->view('admin/bike_make',$data);
}

public function bike_model(){       /*********** Vehicle Model Page ****/       
   
	$data['query_model'] = $this->Admin_model->fetch_bike_models3();
	$this->load->view('admin/bike_model',$data);
}

public function add_car_make()
{
    $car_name = $_POST['car_name'];

$data = array('car_name' => $car_name);
$data['query']	= $this->Admin_model->car_make_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/vehicle_make'.'?msg='.$msg);	
}

public function add_bike_make()
{
    $name = $_POST['name'];

$data = array('name' => $name);
$data['query']	= $this->Admin_model->bike_make_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/vehicle_make'.'?msg='.$msg);	
}

public function vehicle_model(){       /*********** Vehicle Model Page ****/       
   $data['query'] = $this->Admin_model->fetch_car_model();
   $this->load->view('admin/vehicle_model',$data);
}

public function add_vehicle_model(){
	$this->load->view('admin/add_model');
}

public function select_car_name(){  /************** Select Model By Car Name **************/ 
	
    $data = $this->Admin_model->fetch_car_name();
	
   //var_dump($data[0]['models']);
    echo "<option value='' selected='selected' >Select Car Name</option>";
   foreach ($data as $car_name) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$car_name->id.">".$car_name->car_name."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";	  
    
}
 //  echo "</select>";
}

public function add_car_model()
{
    $car_name = $_POST['car_name'];
	$car_id = $_POST['car_id'];
	$model_name = $_POST['model_name'];

$data = array('car_name' => $car_name,'car_id' => $car_id,'model_name' => $model_name);
$data['query']	= $this->Admin_model->car_model_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/vehicle_model'.'?msg='.$msg);	
}

public function select_Bike_name(){  /************** Select Bike Name **************/ 
	
    $data = $this->Admin_model->fetch_bike_names();
	
   //var_dump($data[0]['models']);
    echo "<option value='' selected='selected' >Select Bike Name</option>";
   foreach ($data as $bike_name) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$bike_name->id.">".$bike_name->name."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";	  
    
}
 //  echo "</select>";
}

public function add_bike_model()
{
    $bike_name = $_POST['bike_name'];
	$bike_id = $_POST['bike_id'];
	$model_name = $_POST['model_name'];

$data = array('bike_name' => $bike_name,'bike_id' => $bike_id,'model_name' => $model_name);
$data['query']	= $this->Admin_model->bike_model_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/vehicle_model'.'?msg='.$msg);	
}

public function vehicle_variant(){       /*********** Vehicle Variant Page ****/
   $data['query'] = $this->Admin_model->fetch_car_variant_by_car();       
   $this->load->view('admin/vehicle_variant',$data);
}

public function bike_variant(){       /*********** Vehicle Variant Page ****/
   $data['query'] = $this->Admin_model->fetch_bike_variant_by_bike();       
   $this->load->view('admin/bike_variant',$data);
}

public function add_vehicle_variant(){       /*********** Vehicle Variant Page ****/       
   $this->load->view('admin/add_variant');
}

public function add_car_variant()
{
    $variant = $_POST['variant'];
	$car_id = $_POST['car_id'];
	$model_id = $_POST['model_id'];
	$model_name = $_POST['model_name'];

$data = array('variant' => $variant,'car_id' => $car_id,'model_id' => $model_id,'model_name' => $model_name);
$data['query']	= $this->Admin_model->car_variant_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/add_vehicle_variant'.'?msg='.$msg);	
}
	
public function add_bike_variant()
{
    $variant = $_POST['variant'];
	$bike_id = $_POST['bike_id'];
	$model_id = $_POST['model_id'];
	$model_name = $_POST['model_name'];

$data = array('variant' => $variant,'bike_id' => $bike_id,'model_id' => $model_id,'model_name' => $model_name);
$data['query']	= $this->Admin_model->bike_variant_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/add_vehicle_variant'.'?msg='.$msg);	
}

public function car_list(){
  $data['query_name_model'] = $this->Admin_model->fetch_car_model();
  $data['query_variant'] = $this->Admin_model->fetch_car_variant();  
  $data['query'] = $this->Admin_model->car_listing();
  $this->load->view('admin/car_list',$data); 
}
public function add_new_car(){  /**************** Add New Car from Dashboard *********/

$count = count($_FILES["car_info"]["name"]["images"]);
//var_dump($_FILES);

for($i=0; $i<$count; $i++){
  // $_FILES['car_info']['images']['name'][$i];
   $car_name[$i] = $_FILES["car_info"]["name"]["images"][$i];
   $car_type[$i] = $_FILES['car_info']['type']['images'][$i];
   $car_temp[$i] = $_FILES['car_info']['tmp_name']['images'][$i];
   $target_dir = 'uploads/car/';
   $target_file[$i] = $target_dir . basename($car_name[$i]);
   move_uploaded_file($car_temp[$i], $target_file[$i]);
   $_POST['car_info']['images'][$i] =  $_FILES["car_info"]["name"]["images"][$i];
  }

//var_dump($car_name);
$car_info = json_encode($_POST['car_info']);
$engine_transmission = json_encode($_POST['engine_transmission']);
$tyres_brakes = json_encode($_POST['tyres_brakes']);
$dimensions = json_encode($_POST['dimensions']);
$appearance = json_encode($_POST['appearance']);
$comfort = json_encode($_POST['comfort']);
$safety_security = json_encode($_POST['safety_security']);

$data =  array('car_info' => $car_info,'engine_transmission' => $engine_transmission,'tyres_brakes' => $tyres_brakes,'dimensions' => $dimensions,'appearance' => $appearance,'comfort' => $comfort,'safety_security' =>$safety_security,'price' => $_POST['price']);
$data['query']	= $this->Admin_model->car_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/car_list'.'?msg='.$msg);
}

public function update_car(){  /**************** Update Car from Dashboard *********/
//print_r($_POST);
//echo $_POST['car_info']['images'][0];
//die();
$count = count($_FILES["car_info"]["name"]["images"]);
echo $count;

print_r($_FILES);



//var_dump($_FILES);

for($i=0; $i<$count; $i++){
  // $_FILES['car_info']['images']['name'][$i];
   $car_name[$i] = $_FILES["car_info"]["name"]["images"][$i];
   
   if(empty($car_name[$i]))
   {
     $_POST['car_info']['images'][$i] =  $_POST['car_info']['images'][$i];
     break;
   }
   
   
   
   
   $car_type[$i] = $_FILES['car_info']['type']['images'][$i];
   $car_temp[$i] = $_FILES['car_info']['tmp_name']['images'][$i];
   
   
   
   $target_dir = 'uploads/car/';
   $target_file[$i] = $target_dir . basename($car_name[$i]);
   
   move_uploaded_file($car_temp[$i], $target_file[$i]);
   $_POST['car_info']['images'][$i] =  $_FILES["car_info"]["name"]["images"][$i];
  }
  
//var_dump($car_name);
$car_info = json_encode($_POST['car_info']);
$engine_transmission = json_encode($_POST['engine_transmission']);
$tyres_brakes = json_encode($_POST['tyres_brakes']);
$dimensions = json_encode($_POST['dimensions']);
$appearance = json_encode($_POST['appearance']);
$comfort = json_encode($_POST['comfort']);
$safety_security = json_encode($_POST['safety_security']);

$data =  array('car_info' => $car_info,'engine_transmission' => $engine_transmission,'tyres_brakes' => $tyres_brakes,'dimensions' => $dimensions,'appearance' => $appearance,'comfort' => $comfort,'safety_security' =>$safety_security,'price' => $_POST['price']);
$data['query']	= $this->Admin_model->car_update($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/car_list'.'?msg2='.$msg);
}

public function select_car_model(){  /************** Select Model By Car Name **************/ 
	$car = $_POST['car_id'];

    $data = $this->Admin_model->fetch_model($car);

   $count = count($data);
   //var_dump($count);
   var_dump($data);
   //var_dump($data[0]['models']);
    //var_dump($data[1]['models']);
    echo "<option value='' selected='selected' disabled>Select Model</option>";
   foreach ($data as $car_model) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$car_model['id'].">".$car_model['model_name']."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";	  
    
}
 //  echo "</select>";
}

public function select_variant(){

  $model = $_POST['model_name']; 
  $data = $this->Admin_model->fetch_car_variant_by_model($model);
  
    echo "<option value='' selected='selected' disabled>Select Variant</option>";
    foreach ($data as $car_variant) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$car_variant['id'].">".$car_variant['variant'].""; 
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";   
    }
}

public function delete_car(){
  $id = $_GET['id'];
  $this->Admin_model->car_delete($id);
  $msg = "Car deleted Successfully";
  redirect('admin/car_list?msg='.$msg);
}

public function delete_used_car(){
  $id = $_GET['id'];
  $this->Admin_model->used_car_delete($id);
  $msg = "Success";
  redirect('admin/used_car_list?msg='.$msg);
}

public function delete_vehicle_name(){
  $id = $_GET['id'];
  $type = $_GET['type'];
  if($type=='car'){
  $this->Admin_model->car_delete_name($id);
  }else{
	 $this->Admin_model->bike_delete_name($id); 
  }
  $msg = "Success";
  redirect('admin/vehicle_make?msg='.$msg);
}

public function delete_vehicle_model(){
  $id = $_GET['id'];
  $type = $_GET['type'];
  if($type=='car'){
     $this->Admin_model->car_delete_model($id);
  }else{
	 $this->Admin_model->bike_delete_model($id); 
  }
  $msg = "Success";
  redirect('admin/vehicle_model?msg='.$msg);
}

public function delete_vehicle_variant(){
  $id = $_GET['id'];
  $type = $_GET['type'];
  if($type=='car'){
     $this->Admin_model->car_delete_variant($id);
  }else{
	 $this->Admin_model->bike_delete_variant($id); 
  }
  $msg = "Success";
  redirect('admin/vehicle_variant?msg='.$msg);
}

public function edit_car(){
  $id = $_GET['id'];
   $data['query'] = $this->Admin_model->fetch_car_name();
   $data['query_name_model'] = $this->Admin_model->fetch_car_model();
   $data['query_variant'] = $this->Admin_model->fetch_car_variant();
   $data['car'] = $this->Admin_model->edit_car_by_Id($id);
 // var_dump($data['car']);
  //exit;
  $this->load->view('admin/edit_car',$data);
}

public function clone_car(){
  $id = $_GET['id'];
   $data['query'] = $this->Admin_model->fetch_car_name();
   $data['query_name_model'] = $this->Admin_model->fetch_car_model();
   $data['query_variant'] = $this->Admin_model->fetch_car_variant();
   $data['car'] = $this->Admin_model->edit_car_by_Id($id);
 // var_dump($data['car']);
  //exit;
  $this->load->view('admin/clone_car',$data);
}

///////////////////////////////////////BIKES???????????????????????????

public function add_bike(){       /*********** Add car Page ****/      
    $data['query'] = $this->Admin_model->fetch_bike();  
    $data['query_name'] = $this->Admin_model->fetch_bike_names();
    $data['query_model'] = $this->Admin_model->fetch_bike_models2();
    $data['query_variant'] = $this->Admin_model->fetch_bike_variants2();
    $data['query_cities'] = $this->Admin_model->fetch_cities();
    $this->load->view('admin/add_bike',$data);
}


public function fetch_bike_model()
    {
        $bike_id = $_POST['bike_id'];
		$data['query_model'] = $this->Admin_model->fetch_bike_models($bike_id);
		
		//$count = count($data);
		echo "<option value='' selected='selected' disabled>Select Model</option>";
   foreach($data['query_model'] as $model) 
        { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$model->id.">".$model->model_name."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";
        }
    }
    
    public function fetch_bike_variant()
    {
        $model_id = $_POST['model_id'];
		$data['query_variant'] = $this->Admin_model->fetch_bike_variants($model_id);
		
		//$count = count($data);
		echo "<option value='' selected='selected' disabled>Select Variant</option>";
   foreach($data['query_variant'] as $variant) 
        { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$variant->id.">".$variant->variant."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";
        }
    }

public function add_new_bike(){  /**************** Add New BIKE from Dashboard *********/

$count = count($_FILES["bike_info"]["name"]["images"]);


//print_r($_FILES);die;

for($i=0; $i<$count; $i++){
  // $_FILES['car_info']['images']['name'][$i];
   $bike_name[$i] = $_FILES["bike_info"]["name"]["images"][$i];
   $bike_type[$i] = $_FILES['bike_info']['type']['images'][$i];
   $bike_temp[$i] = $_FILES['bike_info']['tmp_name']['images'][$i];
   $target_dir = 'uploads/bikes/';
   $target_file[$i] = $target_dir . basename($bike_name[$i]);
   move_uploaded_file($bike_temp[$i], $target_file[$i]);
   $_POST['bike_info']['images'][$i] =  $_FILES["bike_info"]["name"]["images"][$i];
  }

//var_dump($car_name);  
 $bike_id = $_POST['bike_id'];
$model_id = $_POST['model_id'];
$variant_id = $_POST['variant_id'];
$bike_info = json_encode($_POST['bike_info']);
$price_info = json_encode($_POST['price_info']);
$engine_transmission = json_encode($_POST['engine_transmission']);
$tyres_brakes = json_encode($_POST['tyres_brakes']);
$feature_safety = json_encode($_POST['feature_safety']);
$chasis_suspension = json_encode($_POST['chasis_suspension']);
$dimension = json_encode($_POST['dimension']);
$electricals = json_encode($_POST['electricals']);

$data =  array('bike_id' => $bike_id,'model_id' => $model_id,'variant_id' => $variant_id, 'bike_info' => $bike_info,'price_info' => $price_info,'engine_transmission' => $engine_transmission,'tyres_brakes' => $tyres_brakes,'feature_safety' => $feature_safety,'chasis_suspension' => $chasis_suspension,'dimension' => $dimension,'electricals' => $electricals);
$data['query']	= $this->Admin_model->bike_insert($data);
$data['message'] = "Data Inserted Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/bike_list'.'?msg3='.$msg);
}

public function update_bike(){  /**************** Add New BIKE from Dashboard *********/
//print_r($_POST);

if(($_FILES["bike_info"]["name"]['images'][0])!=NULL){
$count = count($_FILES["bike_info"]["name"]["images"]);
//print_r($_FILES['bike_info']['name']['images']);die;

for($i=0; $i<$count; $i++){
   $bike_name[$i] = $_FILES["bike_info"]["name"]["images"][$i];
   $bike_type[$i] = $_FILES['bike_info']['type']['images'][$i];
   $bike_temp[$i] = $_FILES['bike_info']['tmp_name']['images'][$i];
   $target_dir = 'uploads/bikes/';
   $target_file[$i] = $target_dir . basename($bike_name[$i]); 
   move_uploaded_file($bike_temp[$i], $target_file[$i]);
   $_POST['bike_info']['images'][$i] =  $_FILES["bike_info"]["name"]["images"][$i];
   }
  }else{
  $id = $_POST['id'];
  //var_dump($id);die;
  $img_data = $this->Admin_model->fetch_img_details($id);
  //var_dump($img_data[0]->bike_info);die;

  $j_img = json_decode($img_data[0]->bike_info);
  //var_dump($j_img);die;
  $len = count($j_img->images);
  //var_dump($len);die;
  for($q=0;$q<$len;$q++){
    $_POST['bike_info']['images'][$q]=$j_img->images[$q];  
    }
  }

  $new_len = count($_POST['bike_info']['images']);
  $len = count($_POST['image']);
  echo $len;
  echo $new_len;
  
    $i_max = $len+$new_len;
  
    for($i=0;$i<$len;$i++)
    {
      
      array_push($_POST['bike_info']['images'],$_POST['image'][$i]);
      
      
    }
  

  //print_r($_POST['bike_info']['images']);
  //die();
  //var_dump($_POST['bike_info']['images']);die;

//var_dump($car_name);  
 $bike_id = $_POST['bike_id'];
$model_id = $_POST['model_id'];
$variant_id = $_POST['variant_id'];
$bike_info = json_encode($_POST['bike_info']);
@$price_info = json_encode($_POST['price_info']);
$engine_transmission = json_encode($_POST['engine_transmission']);
$tyres_brakes = json_encode($_POST['tyres_brakes']);
$feature_safety = json_encode($_POST['feature_safety']);
$chasis_suspension = json_encode($_POST['chasis_suspension']);
$dimension = json_encode($_POST['dimension']);
$electricals = json_encode($_POST['electricals']);

$data =  array('bike_id' => $bike_id,'model_id' => $model_id,'variant_id' => $variant_id, 'bike_info' => $bike_info,'price_info' => $price_info,'engine_transmission' => $engine_transmission,'tyres_brakes' => $tyres_brakes,'feature_safety' => $feature_safety,'chasis_suspension' => $chasis_suspension,'dimension' => $dimension,'electricals' => $electricals,'price' => $_POST['price']);
$data['query']	= $this->Admin_model->bike_update($data);
$data['message'] = "Data Updated Successfully";
$msg = "Success";
redirect(base_url().'index.php/admin/bike_list'.'?msg2='.$msg);
}

public function select_bike_model(){  /************** Select Model By Bike Name **************/ 
	$bike = $_POST['bike_name'];

    $data = $this->Admin_model->fetch_bike_model($bike);

   $count = count($data);

    echo "<option value='' selected='selected' disabled>Select Model</option>";
   for ($i=0; $i < $count; $i++) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$data[$i]['models'].">".trim($data[$i]['models'],'"')."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";	  
    
}
 //  echo "</select>";
}

public function select_bike_variant(){

  $bike = $_POST['bike_name'];

  $model = $_POST['model_name']; 

  $data = $this->Admin_model->fetch_bike_variant($bike,$model);
  
  $count = count($data);

   
    echo "<option value='' selected='selected' disabled>Select Variant</option>";
   for ($i=0; $i < $count; $i++) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$data[$i]['variant'].">".trim($data[$i]['variant'],'"')."";
    // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";    
   }

}

public function bike_list(){
    $data['query'] = $this->Admin_model->bike_listing();
	$data['query_name'] = $this->Admin_model->fetch_bike_names();
	$data['query_model'] = $this->Admin_model->fetch_bike_models2();
	$data['query_variant'] = $this->Admin_model->fetch_bike_variants2();
    $this->load->view('admin/bike_list',$data);
}

public function delete_bike(){
  $id = $_GET['id'];
  $this->Admin_model->bike_delete($id);
  $msg = "Success";
  redirect('admin/bike_list?msg='.$msg);
}

public function delete_user(){
  $id = $_GET['id'];
  $this->Admin_model->user_delete($id);
  $msg = "Success";
  redirect('admin/users?msg='.$msg);
}

public function edit_admin()
{
    //$data['query'] = $this->Admin_model->fetch_admin_details();
    $this->load->view('admin/edit_admin');
}

public function update_admin()
{
    $email = $_POST['email'];
    $password = $_POST['password'];
    $data = array('email' => $email,'password' => $password);
    $data2 =  $this->Admin_model->update_admin($data);
    if($data2==1){    
    $this->session->set_userdata('email',$email);
    $msg = "Success";
    redirect('admin/my_account?msg='.$msg);	
    }
    
}

public function delete_buyer(){
  $id = $_GET['id'];
  $this->Admin_model->buyer_delete($id);
  $msg = "Success";
  redirect('admin/users?msg='.$msg);
}

public function edit_bike(){
  $id = $_GET['id'];
  $data['query_name'] = $this->Admin_model->fetch_bike_names();
  $data['query_model'] = $this->Admin_model->fetch_bike_models2();
  $data['query_variant'] = $this->Admin_model->fetch_bike_variants2();
  $data['bike'] = $this->Admin_model->edit_bike_by_Id($id);
  $data['query_cities'] = $this->Admin_model->fetch_cities();
 // var_dump($data['car']);
  //exit;
  $this->load->view('admin/edit_bike',$data);
}

public function clone_bike(){
  $id = $_GET['id'];
  $data['query_name'] = $this->Admin_model->fetch_bike_names();
  $data['query_model'] = $this->Admin_model->fetch_bike_models2();
  $data['query_variant'] = $this->Admin_model->fetch_bike_variants2();
  $data['bike'] = $this->Admin_model->edit_bike_by_Id($id);
  $data['query_cities'] = $this->Admin_model->fetch_cities();
 // var_dump($data['car']);
  //exit;
  $this->load->view('admin/clone_bike',$data);
}

public function users()
{
    $data['query'] = $this->Admin_model->fetch_users();
    $this->load->view('admin/users',$data);
}

public function interested_buyer()
{
    $data['query'] = $this->Admin_model->fetch_buyers();
    $this->load->view('admin/interested_buyer',$data);
}

public function used_car_list()
{
	$data['query_name_model'] = $this->Admin_model->fetch_car_model();
    $data['query_variant'] = $this->Admin_model->fetch_car_variant();
    $data['query'] = $this->Admin_model->fetch_used_cars_list();
    $this->load->view('admin/used_car_list',$data);
}

public function used_car_details()
{
	$id = $_GET['id'];
	$data['query_name_model'] = $this->Admin_model->fetch_car_model();
    $data['query_variant'] = $this->Admin_model->fetch_car_variant();
    $data['query'] = $this->Admin_model->fetch_used_cars_by_id($id);
    $this->load->view('admin/used_car_details',$data);
}

public function update_status_used_car()
{
    $status = $_POST['status'];
    $id = $_POST['id'];
    $this->Admin_model->update_status_used_car($status,$id);
}

public function update_status_car()
{
    $status = $_POST['status'];
    $id = $_POST['id'];
    $this->Admin_model->update_status_car($status,$id);
}

public function used_bikes_list()
{
    $data['query_name'] = $this->Admin_model->fetch_bike_names();
    $data['query_model'] = $this->Admin_model->fetch_bike_models2();
    $data['query'] = $this->Admin_model->fetch_used_bikes_list();
    $this->load->view('admin/used_bikes_list',$data);
}

public function update_status_used_bike()
{
    $status = $_POST['status'];
    $id = $_POST['id'];
    $this->Admin_model->update_status_used_bike($status,$id);
}

public function update_status_bike()
{
    $status = $_POST['status'];
    $id = $_POST['id'];
    $this->Admin_model->update_status_bike($status,$id);
}
public function logout()
{
    unset($_SESSION["email"]);
    //unset($_SESSION["phone"]);
    //unset($_SESSION["name"]);
    header("Location: ". base_url()."index.php/Admin");
}

public function delete_used_ike(){
  $id = $_GET['id'];
  $this->Admin_model->delete_used_ike($id);
  $msg = "Success";
  redirect('admin/used_bikes_list?msg='.$msg);
}

}