<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
public function __construct() 
{   
parent::__construct();
$this->load->database();
$this->load->model('User_model');
$this->load->library('session');
$this->load->helper('global_functions');
}
public function index()   /***  Dashboard Home **/
{
$email = $this->session->userdata('email');
$phone = $this->session->userdata('phone');
$data['query_buyer'] = $this->User_model->get_buyer($phone);
$data['query'] = $this->User_model->get_bike_list($email,$phone);
$data['query_car'] = $this->User_model->get_car_list($email,$phone);
$this->load->view('user/index',$data);
}

public function car_listing(){
$phone = $this->session->userdata('phone');
$data['query'] = $this->User_model->get_car_list($phone);
$data['query_name_model'] = $this->User_model->fetch_car_model();
$data['query_variant'] = $this->User_model->fetch_car_variant();
$this->load->view('user/car_listing',$data);
}

public function bike_listing(){
$email = $this->session->userdata('email');
$phone = $this->session->userdata('phone');
$data['query_buyer'] = $this->User_model->get_buyer($phone);
$data['query_model'] = $this->User_model->fetch_bike_name();
$data['query'] = $this->User_model->get_bike_list($email,$phone);
$this->load->view('user/bike_listing',$data);
}

public function interesred_buyer(){
    $user_info = $_GET['user_p'];
    $data['query'] = $this->User_model->get_buyer($user_info);
$this->load->view('user/interested_buyer',$data);
}

public function edit_used_car(){
$id = $_GET['id'];
$data['query'] = $this->User_model->get_car_detail($id);
$data['query_name'] = $this->User_model->fetch_car_name();
$data['query_cities'] = $this->User_model->fetch_cities();
$data['query_name_model'] = $this->User_model->fetch_car_model();
$data['query_variant'] = $this->User_model->fetch_car_variant();
//var_dump($data['query_name_model']);die;
$this->load->view('user/edit_used_car',$data);
}

public function update_used_car(){
	
	$vehicle_info = json_encode($_POST);
	if(($_FILES['additional_info']['name']['img'][0])!=NULL){
	$count = count($_FILES['additional_info']['name']['img']);
	//var_dump($vehicle_info);die;
	for($i=0;$i<$count;$i++){
		$car_name[$i] = $_FILES['additional_info']['name']['img'][$i];
		$car_type[$i] = $_FILES['additional_info']['type']['img'][$i];
		$car_tmp[$i]  = $_FILES['additional_info']['tmp_name']['img'][$i];
		$target_dir = 'uploads/used_cars/';
		$target_file[$i] = $target_dir.basename($car_name[$i]);	
		move_uploaded_file($car_tmp[$i],$target_file[$i]);
		$_POST['additional_info']['img'][$i] = $car_name[$i];
	}
	$additional_info = json_encode($_POST);
    $data = array('vehicle_info' => $vehicle_info,
				  'price' => $_POST['price'],
				  'additional_info' => $additional_info,
					  'comment' => $_POST['comment']	);
	}else{
		$data = array('vehicle_info' => $vehicle_info,
					  'price' => $_POST['price'],
					  'comment' => $_POST['comment']);
}	
//var_dump($data);die;
    $this->User_model->update_used_car($data);
$msg = 'Success';
header("Location:".base_url()."index.php/User/car_listing");	
}

public function register_user(){
$this->session->set_userdata("user_reg_data",FALSE);
$user_info = json_encode($this->input->post('user_info'));

$otp=rand(9999,99999);
$data = array(
              'name' => $this->input->post('user-name'),
              'email' => $this->input->post('user-email'),
              'password' => $this->input->post('password'),
              'phone' => $this->input->post('user-phone'),
              'user_info' => $user_info,
              'otp' => $otp
            );
  $this->session->set_userdata("user_reg_data",$data);
  
  $this->send_otp();

}

function send_otp()
{
//////////////////////////////////////////
//////   SMS91  API  Credentials    /////
////////////////////////////////////////
 $data=$this->session->get_userdata("user_reg_data");
 $data=$data['user_reg_data'];
 $sms="Your One Time Password (OTP) : {$data['otp']} , Please use this otp to verify your account.\n Thank You \nDreamwheels Pvt. Ltd";
 sendSMS($data['phone'],$sms);
 $this->session->set_userdata("otp_op",TRUE);
  $this->session->set_userdata("otp_op_msg","<b style='color:green'>One Time Password was sent on your entered mobile number</b>");

     $this->otp_page();
}

function otp_page()
{
if($this->session->get_userdata("otp_op")['otp_op'])
{
  $data=$this->session->get_userdata("user_reg_data");
  $data=$data['user_reg_data'];
  $this->load->view('Register_verify_otp',$data);
}
 else {
 
header("Location:".base_url()."index.php/Welcome/Register");	
}
}
 function verify_otp()
{
 
   $data=$this->session->get_userdata("user_reg_data");
   $data=$data['user_reg_data'];
   if($this->input->post('otp')==$data['otp'])
   {
    array_pop($data);
    $this->User_model->insert_user($data);
    $this->session->set_userdata("otp_op",FALSE);
    $this->session->set_userdata("user_reg_data",FALSE);
    $msg = "Success";
    redirect('Welcome/Register?msg='.$msg);
   }
   else
   {
     $this->session->set_userdata("otp_op",TRUE);
    $this->session->set_userdata("otp_op_msg","<b style='color:red'>Wrong OTP !!!</b>");

     $this->otp_page();
   }

}
public function login_user(){
 
          //Log via Phone
 //$phone='';$email='';$pass='';
 $error="";
   
 
	if(!empty($this->input->post('phone')) && !empty($this->input->post('password')))
   {
      $phone = $this->input->post('phone');
	$pass = $this->input->post('password');
   }	

	if(!empty($phone)){

	 $data['query'] = $this->User_model->select_user_by_phone($phone,$pass);
	 
	  if(count($data['query']))
	  {
	 $this->session->set_userdata('email',$data['query'][0]->email);
          $this->session->set_userdata('phone',$data['query'][0]->phone);
          $this->session->set_userdata('name',$data['query'][0]->name);
	  }else{
		$data['query'] = $this->User_model->select_user($phone,$pass);
        if(count($data['query']))
	  {
	 $this->session->set_userdata('email',$data['query'][0]->email);
	 $this->session->set_userdata('phone',$data['query'][0]->phone);
           $this->session->set_userdata('name',$data['query'][0]->name);
	  }
	  
	  }
          $error = "Error(0) : Phone Number/Email Does Not Matched !!!";
	}

     if(count($data['query'])){
	 redirect('User/index');	
}else{
    echo "Please wait....";
    echo "<script>alert('".$error."');window.history.back();</script>";    
}

}

public function logout()
{
    unset($_SESSION["email"]);
    unset($_SESSION["phone"]);
    unset($_SESSION["name"]);
    header("Location: ". base_url());
}

public function email_validate()
{
    $email = $_POST['email'];
    $data = $this->User_model->email_validate($email);
    //var_dump($data);
   if(count($data)>=1){       
//        //$_SESSION['email_validate'] = $data->email;
       //echo $data[0]->email;
       echo "Match";
    }
    else{
        echo "No match";
    }
    
}

public function phone_validate()
{
    $phone = $_POST['phone'];
    $data = $this->User_model->phone_validate($phone);
    //var_dump($data);
   if(count($data)>=1){       
//        //$_SESSION['email_validate'] = $data->email;
       //echo $data[0]->email;
       echo "Match";
    }
    else{
        echo "No match";
    }
    
}

public function my_account(){
$phone = $this->session->userdata('phone');
$data['query'] = $this->User_model->select_user_by_phone($phone);	
$this->load->view('user/my_account',$data);
}

public function password_recovery()
{
    if(!empty($this->input->post('mobile_email')))
   {
     $mobile_email=$this->input->post('mobile_email');
     
       if(strpos($mobile_email,'@')===FALSE)
       {
        
              $data = $this->User_model->phone_validate($mobile_email);
   if(count($data)>=1){       
       $this->send_password($mobile_email,$email=FALSE);
    }
    else{
      echo "<script>alert('Invalid Mobile Number!')</script>";
    }
       }
 else {
  $data = $this->User_model->email_validate($mobile_email);
   if(count($data)>=1){       
      $this->send_password($mobile=FALSE,$mobile_email);
    }
    else{
        echo "<script>alert('Invalid Email-ID!')</script>";
    }
       }
     
   }
   else
   {
       echo "<script>alert('Please enter valid email id or mobile number')</script>";
   }
   
   $this->load->view('recoverpass');   
}

public function send_password($mobile=FALSE,$email=FALSE)
{
     $data=$this->User_model->get_password($mobile,$email);
     $pass=$data[0]->password; 
    if($mobile)
    {
       $sms="Your Dreamwheels Account Password : {$pass}\nThank You\nDreamwheels Pvt. Ltd";
       sendSMS($mobile,$sms);
 
  $this->session->set_userdata("recovery_op",TRUE);
  $this->session->set_userdata("recovery_op_msg","<b style='color:green'>Password was sent on your registered mobile number</b>");
    }
 else {
       echo "<script>alert('Email Recovery feature is not available yet,Please use your registered mobile number to recover your password!')</script>"; 
    }
    
    
}
}