<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
public function __construct() 
{   
parent::__construct();
$this->load->database();
$this->load->model('User_model');
$this->load->model('Welcome_model');
$this->load->library('session');
$this->load->helper('global_functions');
}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
   
		$data['car'] =  $this->Welcome_model->fetch_list_cars();
                $data['query_car'] =  $this->Welcome_model->fetch_car_name();  
                $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
                $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
                       
                //Via Category (NEW) 
                $data['featured'] =     $this->Welcome_model->get_car_list_via_category('featured'); 
		$data['latest'] =    $this->Welcome_model->get_car_list_via_category('latest'); 
		$data['upcoming'] =    $this->Welcome_model->get_car_list_via_category('upcoming'); 
		$data['recommended'] = $this->Welcome_model->get_car_list_via_category('recommended'); 
                
                //Old Vehicle
                $data['used'] = $this->Welcome_model->get_car_list_via_condition_old();
		
		$this->load->view('index',$data);
	}
        
        public function car_finance()
        {
            $this->load->view('car_finance');
        }
        
        public function car_insurance()
        {
            $this->load->view('car_insurance');
        }
        
        public function careers()
        {
            $this->load->view('careers');
        }
        
        public function faq()
        {
            $this->load->view('faq');
        }
        
        public function terms()
        {
            $this->load->view('terms');
        }
        
        public function policy()
        {
            $this->load->view('privacy_policy');
        }
        
        public function Recoverpass()
        {
         $this->load->view('recoverpass');   
        }


        public function cities(){
            $data = $this->Welcome_model->fetch_cities();
            
   
            echo "<option value='' selected='selected'>Select City</option>";
           foreach ($data as $data1) { 
            //$data_car[$i] = $data[$i]['models'];
            echo "<option value=".$data1->city_id.">".trim($data1->city_name,'"')."";
           // echo "<option value='".$data_car[$i]."'>";
            echo "</option>"; 
                }
        }
        public function select_car_model(){  /************** Select Model By Car Name **************/ 
	$car = $_POST['car_id'];

    $data = $this->Welcome_model->fetch_model($car);

    echo "<option value='default'>Select Model</option>";
   foreach ($data as $car_model) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$car_model['id'].">".$car_model['model_name']."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";	  
    
}
 //  echo "</select>";
}

public function select_variant(){

  $model = $_POST['model_name']; 
  $data = $this->Welcome_model->fetch_car_variant_by_model($model);
  
    echo "<option value='' selected='selected' disabled>Select Variant</option>";
    foreach ($data as $car_variant) { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$car_variant['id'].">".$car_variant['variant'].""; 
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";   
    }
}

public function select_car_type(){

  $model = $_POST['model_name']; 
  $data = $this->Welcome_model->fetch_car_type_by_model($model);
  
   // echo "<option value='' selected='selected' disabled>Select Variant</option>";
    foreach ($data as $car_type) { 
      
      $type = $car_type["car_info->>'$.car_type'"];
	  $type_name = str_replace("_"," ",$type);
	  echo ucfirst($type_name);
    //$data_car[$i] = $data[$i]['models'];
    //echo "<option value=".$car_type['id'].">".$car_type['variant'].""; 
   // echo "<option value='".$data_car[$i]."'>";
    //echo "</option>";   
    }
}
        public function session_city(){
            //session_start();
            $city_id = $_POST['city_id'];
            $data = $this->Welcome_model->fetch_city_by_id($city_id);
            //var_dump($data);
            $_SESSION['city_name'] = $data[0]->city_name;
            //var_dump($_SESIION['city_name']);
        }
        
        public function latest_vehicles()
        {    
          $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
          $data['cars'] =  $this->Welcome_model->fetch_latest_cars2();
          $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
          $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
          $data['bikes'] =  $this->Welcome_model->fetch_latest_bikes2();
            $this->load->view('latest_vehicles',$data); 
        }

	public function post_car(){
    $data['query_cities'] = $this->Welcome_model->fetch_cities();
		$data['query_car'] =  $this->Welcome_model->fetch_car_name();
		$this->load->view('post_requirment',$data);

	}

	public function post_user_requirment(){  /***** Post Requirment ****/
		 
    $user_data = json_encode($_POST['user']);
    $requirment = $_POST['msg'];
    $data =  array('user_info' => $user_data,
                 'requirment' => $requirment);
   
    $this->Welcome_model->Insert_post_data($data);
     echo "<div class='alert alert-success'>Your Requirment has been posted.You will be contacted soon</div>";
	 
    }
	public function post_requirment(){
        
		$this->load->view('Requirment');

	}
        
        
        ############ Car Filter ########
      public function vehicle_search()
        {
           
if(isset($_GET['search_flow']))
{
 $data['query'] = $this->Welcome_model->vehicle_search();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('vehicle_search_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("vehicle_search",$data);
 }

}
else
{
    echo "<script>alert('This feature was temporarily unavailable!'); window.location.href='". base_url()."'</script>";
}
 
        }
        
        
        public function car_price_filter2()
        {
            $price = $_GET['price'];
            $car_type = $_GET['type'];
//            $data['query'] = $this->Welcome_model->cars_list_by_price($price,$car_type);
//            $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
//            $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
//	    $this->load->view('new_car',$data);
            
 $data['query'] = $this->Welcome_model->fetch_new_cars_ajax($price,$car_type);
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('new_car_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("new_car",$data);
 }
        }
        
        
        
        public function car_price_filter3()
        {
            $price = $_GET['price'];
            $car_type = $_GET['type'];
           
            $data['query'] = $this->Welcome_model->used_cars_list_by_price($price,$car_type);
            $data['query_car'] =  $this->Welcome_model->fetch_car_name();
            $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
            $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
	    $this->load->view('car_listing',$data);
        }
        
        public function car_model_filter2()
        {
            $car_name = $_GET['car_name'];
            $model_name = $_GET['model_name'];
           
            $data['query'] = $this->Welcome_model->cars_list_by_model($car_name,$model_name);
            $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
            $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
	    $this->load->view('new_car',$data);
        }
        
        public function car_model_filter3()
        {
            $car_name = $_GET['car_name'];
            $model_name = $_GET['model_name'];
            $data['query'] = $this->Welcome_model->used_cars_list_by_model2($car_name,$model_name);
		    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
            $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
            $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
	    $this->load->view('car_listing',$data);
        }

	public function car_compare(){
		$data['query_car'] =  $this->Welcome_model->fetch_car_name();  
        $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
		$data['query'] = $this->Welcome_model->fetch_list_cars();
		$this->load->view('compare',$data);
	}

	public function car_listing(){

       /// $data['query'] = $this->Welcome_model->car_list();
		    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
       // $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        //$data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        //$this->load->view('car_listing', $data);
        
  $data['query'] = $this->Welcome_model->fetch_car_listing_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('car_listing_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("car_listing",$data);
 }
	 

	}

	public function bike_listing(){
		$this->load->view('bike_listing');
	}

	public function upcoming_car(){
   
  $data['query'] = $this->Welcome_model->fetch_upcomming_car_listing_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('upcomming_car_listing_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("upcoming_cars",$data);
 }
 
 }
	public function contacts(){
		$this->load->view('contacts');
	}
    public function Register(){
        $data['query'] = $this->Welcome_model->fetch_cities();
		$this->load->view('Register',$data);
	}
     public function about(){
		$this->load->view('about');
	}

	 public function details(){
	 	$id = $_GET['id'];
	 	$data['query'] = $this->Welcome_model->fetch_car_by_id($id);
		$data['query_model_variants'] = $this->Welcome_model->fetch_model_variants($id);
                $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
                $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
		$this->load->view('detail',$data);
	}
        
        public function used_car_details(){
	 	$id = $_GET['id'];
	 	$data['query'] = $this->Welcome_model->fetch_used_car_by_id($id);
		$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
		$this->load->view('used_car_details',$data);
	}

	public function post_requirement_step2(){	
    $this->session->set_userdata('vehicle_type',$_POST['vehicle_type']); 
	$this->session->set_userdata('vehicle_info',$_POST);
	 //var_dump($_SESSION['vehicle_info']);
    $this->load->view('post_requirment_step2');
}

    public function post_requirment_step3(){

    $this->session->set_userdata('price',$_POST['price']);
    	
    //var_dump($_SESSION['car_info']);

   // var_dump($_SESSION['car_price']);
          
	$this->load->view('post_requirment_step3');
}

  public function submit_car_data(){

		$count = count($_FILES["additional_info"]["name"]["img"]);

for($i=0; $i<$count; $i++){
   $car_name[$i] = $_FILES["additional_info"]["name"]["img"][$i];
   $car_type[$i] = $_FILES['additional_info']['type']['img'][$i];
   $car_temp[$i] = $_FILES['additional_info']['tmp_name']['img'][$i];
   $target_dir = 'uploads/used_cars/';
   $target_file[$i] = $target_dir . basename($car_name[$i]);
   move_uploaded_file($car_temp[$i], $target_file[$i]);
   $_POST['additional_info']['img'][$i] =  $_FILES["additional_info"]["name"]["img"][$i];
  }
  
  	$vehicle_info = json_encode($_SESSION['vehicle_info']);

  	$additional_info = json_encode($_POST);
	
  	$user_info = json_encode($_POST['user_info']);

  	$data = array('vehicle_type' => $_SESSION['vehicle_type'],
                  'vehicle_info' => $vehicle_info,
                  'price' => $_SESSION['price'],
                  'additional_info' => $additional_info,
				  'comment' => $_SESSION['comment'],
  	              'user_info' => $user_info);
     
     $data1 =  array('name' => $_POST['user_info']['name'],
                     'phone' => $_POST['user_info']['phone'],
					 'user_type' => 'car');
//var_dump($data);die;
    // $this->User_model->insert_user3($data1);
  	               
  	 $this->User_model->insert_car($data);
     
     $data['msg'] = "Your Car has been successfully published";
    
  header("Location:".base_url()."index.php/Welcome/success");
 
     } 
	 
 public function fetch_model(){   /***** Fetch all model of car******/
 $car = $_POST['car_name'];
 $car_list = $this->Welcome_model->fetch_all_model($car);
 foreach ($car_list as $cars) {	
 echo '<option value='.trim($cars->model,'"').'>'.trim($cars->model,'"').'</option>';
 }
 }
 
 public function car_sell_login(){   /***** Fetch all model of car******/
	$this->load->view('car_sell_login');
 }
 
  public function bike_sell_login(){   /***** Fetch all model of car******/
	$this->load->view('bike_sell_login');
 }

 public function login_user(){
 
          //Log via Phone
 //$phone='';$email='';$pass='';
 $error="";
   
 
	if(!empty($this->input->post('phone')) && !empty($this->input->post('password')))
   {
      $phone = $this->input->post('phone');
	$pass = $this->input->post('password');
	$page = $_GET['page'];
   }	

	if(!empty($phone)){

	 $data['query'] = $this->Welcome_model->select_user_by_phone($phone,$pass);
	 
	  if(count($data['query']))
	  {
	 $this->session->set_userdata('email',$data['query'][0]->email);
          $this->session->set_userdata('phone',$data['query'][0]->phone);
          $this->session->set_userdata('name',$data['query'][0]->name);
		  $this->session->set_userdata('authority_id',$data['query'][0]->id);
	  }else{
		$data['query'] = $this->Welcome_model->select_user($phone,$pass);
        if(count($data['query']))
	  {
	 $this->session->set_userdata('email',$data['query'][0]->email);
	 $this->session->set_userdata('phone',$data['query'][0]->phone);
           $this->session->set_userdata('name',$data['query'][0]->name);
		   $this->session->set_userdata('authority_id',$data['query'][0]->id);
	  }
	  
	  }
	  //var_dump($data);
          $error = "Error(0) : Phone Number/Email Does Not Matched !!!";
	}

     if(count($data['query'])){
	 redirect('Welcome/'.$page);	
}else{
    echo "Please wait....";
    echo "<script>alert('".$error."');window.history.back();</script>";    
}

}
 
 public function search_cars(){  /****** Car Search ****/
 	$car_name = $_POST['car'];
        $model = $_POST['model'];
        $fuel_type = $_POST['fuel'];
        $price = $_POST['price'];
 	$data['query']  = $this->Welcome_model->fetch_all_cars($car_name,$model,$fuel_type,$price);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('new_car_list', $data);	
 }
 
  public function search_cars_by_price(){  /****** Car Search ****/
 	
        $model = $_POST['car_model'];
        $price = $_POST['price'];
 	$data['query']  = $this->Welcome_model->fetch_all_cars_by_price($model,$price);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('search_cars3', $data);	
 }
 
  public function search_cars2(){  /****** Car Search ****/
 	$car_type = $_GET['car_type'];
 	$data['query']  = $this->Welcome_model->fetch_all_cars2($car_type);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('new_car_list', $data);	
 }
 
 public function search_vehicle(){  /****** Car Search ****/
 	//$car_type = $_GET['car_type'];
	$info = $_POST['search'];
 	$data['query']  = $this->Welcome_model->search_vehicles($info);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('new_car_list', $data);	
 }
  public function search_cars3(){  /****** Car Search ****/
 	$car = $_POST['car'];
 	$data['query']  = $this->Welcome_model->fetch_all_cars3($car);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('search_cars3', $data);	
 }
  public function search_cars4(){  /****** Car Search ****/
 	$car_model = $_POST['car_model'];
 	$data['query']  = $this->Welcome_model->fetch_all_cars4($car_model);
    $data['query_car'] =  $this->Welcome_model->fetch_car_name();
 	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 	$this->load->view('search_cars3', $data);	
 }
public function first_compare(){
$car1 = $_POST['car_name'];
$model1 = $_POST['model_name'];
$variant1 = $_POST['variant_name'];
$data = $this->Welcome_model->compare_car1($car1,$model1,$variant1);

echo trim($data[0]->car_image,'"');

}

public function cars3_compare(){
	$car_data = str_replace('+', ' ',$_POST['car_data']);
	$data = explode("&",$car_data);
    $carNames=[];
	foreach($data as $v)
	{
      $carNames[]=(explode("=",$v));
	}

	$id1 = $this->Welcome_model->get_carId($carNames[0],$carNames[1],$carNames[2]);

	$car_id1 = $id1[0]->id;

	$id2 = $this->Welcome_model->get_carId($carNames[3],$carNames[4],$carNames[5]);

	$car_id2 = $id2[0]->id;

	$array_car =  array($car_id1);

    
	$id3 = $this->Welcome_model->get_carId($carNames[6],$carNames[7],$carNames[8]);
   
	$car_id3 = $id3[0]->id;

	array_push($array_car, $car_id2, $car_id3);
    
     $arr_car = implode(",",$array_car); 

    $data['query'] = $this->Welcome_model->compare($arr_car);  
    var_dump($data);
    echo json_encode($data['query']);
	
}
public function cars2_data()
{
    $carNames1 = $_POST['car1_val'];
    $modelNames1 = $_POST['model1_val'];
    $variantNames1 = $_POST['variant1_val'];
	
    $carNames2 = $_POST['car2_val'];
    $modelNames2 = $_POST['model2_val'];
    $variantNames2 = $_POST['variant2_val'];
	
    $data['query1'] = $this->Welcome_model->compare_all_cars1($carNames1,$modelNames1,$variantNames1);
    $data['query2'] = $this->Welcome_model->compare_all_cars2($carNames2,$modelNames2,$variantNames2);
	$data['query_name_model'] = $this->Welcome_model->fetch_car_model();
    $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
    $this->load->view('car_comparision_details',$data);	
}

public function cars2_data2(){

    $carNames1 = $_POST['car1_val'];
    $modelNames1 = $_POST['model1_val'];
    $variantNames1 = $_POST['variant1_val'];
    
    $carNames2 = $_POST['car2_val'];
    $modelNames2 = $_POST['model2_val'];
    $variantNames2 = $_POST['variant2_val'];
    
    $carNames3 = $_POST['car3_val'];
    $modelNames3 = $_POST['model3_val'];
    $variantNames3 = $_POST['variant3_val'];

        $data['query1'] = $this->Welcome_model->compare_all_cars1($carNames1,$modelNames1,$variantNames1);
        $data['query2'] = $this->Welcome_model->compare_all_cars2($carNames2,$modelNames2,$variantNames2);
       $data['query3'] = $this->Welcome_model->compare_all_cars3($carNames3,$modelNames3,$variantNames3);
	   $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
       $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        $this->load->view('car_comparision_details2',$data);	
}

public function cars2_compare(){
//	$car_data = str_replace('+', ' ',$_POST['car_data']);
//	$data = explode("&",$car_data);
//    $carNames=[];
//    print_r($data);die;
//	foreach($data as $v)
//	{
//      $carNames[]=(explode("=",$v));
//	}
    $carNames1 = $_POST['car_data1'];
    $modelNames1 = $_POST['model1_val'];
    $variantNames1 = $_POST['variant1_val'];
    
    $carNames2 = $_POST['car_data2'];
    $modelNames2 = $_POST['model2_val'];
    $variantNames2 = $_POST['variant2_val'];
	$id1 = $this->Welcome_model->get_carId($carNames1,$modelNames1,$variantNames1);

	$car_id1 = $id1->id;

	$id2 = $this->Welcome_model->get_carId($carNames2,$modelNames2,$variantNames2);

	$car_id2 = $id2->id;

	$array_car =  array($car_id1);

	array_push($array_car, $car_id2);
    
     $arr_car = implode(",",$array_car); 

    $data['query'] = $this->Welcome_model->compare($arr_car);  
	
}

public function emi_calculator(){
$data['car_list']	= $this->Welcome_model->fetch_list_cars();
	$this->load->view('emi_calculate',$data);
}

public function select_price(){
	$car = $_POST['car_id'];
	//echo $car;
    $model = $_POST['model_id'];
    //echo $model;
    $variant = $_POST['variant_name'];
    //echo $variant;

    $data = $this->Welcome_model->get_price($car,$model,$variant);
	echo json_encode($data);
	// if(isset($price[0]->price)){
		// echo trim($price[0]->price,'"');
	// }else{
	    // echo "0";
	// }
    //var_dump($data['query']);
}

public function new_car(){
	 //$data['query'] = $this->Welcome_model->fetch_new_cars();
//   $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
//   $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
 
 $data['query'] = $this->Welcome_model->fetch_new_cars_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('new_car_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("new_car",$data);
 }
	 
}

public function range(){
	$this->load->view('range');
}

public function fetch_vehicle_by_car()
    {
        $car = $_POST['car'];
        $data['query'] = $this->Welcome_model->fetch_vehicle_by_car($car);
        $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
         $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        $this->load->view('used_cars_filter', $data);
        
    }
    
public function fetch_vehicle_by_model()
    {
        $car_model = $_POST['car_model'];
        $data['query'] = $this->Welcome_model->fetch_vehicle_by_model($car_model);
        $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
         $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        $this->load->view('used_cars_filter', $data);
    }   
    
public function fetch_vehicle_by_price()
    {
        $price = $_POST['price'];
        $data['query'] = $this->Welcome_model->fetch_used_vehicle_by_price($price);
        $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
         $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        $this->load->view('used_cars_filter', $data);
    }
public function fetch_vehicle_by_price_model()
    {
		//print_r($_POST);die;
        $car_model = $_POST['car_model'];
        $price = $_POST['price'];
		//var_dump($car_model);
        $data['query'] = $this->Welcome_model->fetch_used_vehicle_by_price_model($price,$car_model);
        $data['query_name_model'] = $this->Welcome_model->fetch_car_model();
        $data['query_variant'] = $this->Welcome_model->fetch_car_variant();
        $this->load->view('used_cars_filter', $data);
    }	

    public function fetch_new_vehicle_by_car()
    {
        $car = $_POST['car'];
        $data['query'] = $this->Welcome_model->fetch_new_vehicle_by_car($car);
        $data['query1'] = $this->Welcome_model->cars_name();
        //var_dump($data['query']);
        $this->load->view('search_car_page', $data);
    }
    
public function fetch_new_vehicle_by_model()
    {
        $car_model = $_POST['car_model'];
        $data['query'] = $this->Welcome_model->fetch_new_vehicle_by_model($car_model);
        $data['query1'] = $this->Welcome_model->cars_name();
        //var_dump($data['query']);
        $this->load->view('search_car_page', $data);
    }   
    
public function fetch_new_vehicle_by_price()
    {
        $car_model = $_POST['car_model'];
        $price = $_POST['price'];
        $data['query'] = $this->Welcome_model->fetch_new_vehicle_by_price($car_model,$price);
        $data['query1'] = $this->Welcome_model->cars_name();
        //var_dump($data['query']);
        $this->load->view('search_car_page', $data);
    } 
	
public function buyers_need()
        {
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query_bike'] = $this->Welcome_model->used_bikes_list();
            $data['query_bike_details'] = $this->Welcome_model->bike_names_models();
            $data['query_car'] = $this->Welcome_model->car_list();
            $data['query1'] = $this->Welcome_model->cars_all();
            $data['query_cities'] = $this->Welcome_model->fetch_cities();
        //var_dump($data['query']);
		$this->load->view('buyers_need', $data);

	}
        
public function fetch_vehicle_by_city_price()
        {
            $price = $_POST['price'];
            $city = $_POST['city'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query_bike'] = $this->Welcome_model->used_bikes_list_by_price_city($price,$city);
            $data['query_bike_details'] = $this->Welcome_model->bike_names_models();
            $data['query_car'] = $this->Welcome_model->car_list();
            $data['query1'] = $this->Welcome_model->cars_all();
        //var_dump($data['query']);
		$this->load->view('used_cars_bikes', $data);

	}        

        ///////////////////////////////////
       ///////////////BIKES///////////////
      ///////////////////////////////////

public function bike()
	{
                $data['query'] = $this->Welcome_model->fetch_featured_bikes();
                $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	        $data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
                $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
                
                //Via Category (NEW) 
                $data['featured'] =     $this->Welcome_model->get_bike_list_via_category('featured'); 
		$data['latest'] =    $this->Welcome_model->get_bike_list_via_category('latest'); 
		$data['upcoming'] =    $this->Welcome_model->get_bike_list_via_category('upcoming'); 
		$data['recommended'] = $this->Welcome_model->get_bike_list_via_category('recommended'); 
                
                //Old Vehicle
                
                $data['used'] = $this->Welcome_model->get_bike_list_via_condition_old();
		$this->load->view('bike',$data);
	}
        
        public function redirect_to_bike(){
            $model_id = $_GET['model_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->bikes_list_by_model_id($model_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
            $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	    $data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
            //var_dump($data['query']);
	    $this->load->view('new_bikes', $data);

	}
        
        public function redirect_to_used_bike(){
            $model_id = $_GET['model_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_model_id($model_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
            //var_dump($data['query']);
	        $this->load->view('used_bike_listing', $data);
	}
	
	public function search_bikes()
	{
		$bike_id = $_POST['select1'];
		$model_id = $_POST['select2'];
		$price = $_POST['select3'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->fetch_bikes_list($bike_id,$model_id,$price);
            $data['query1'] = $this->Welcome_model->bike_names_models();
            $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	        $data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
            //var_dump($data['query']);
	        $this->load->view('new_bikes', $data);
		
	}
	 public function search_bike(){  /****** Car Search ****/
 	//$car_type = $_GET['car_type'];
	$info = $_POST['search'];
 	$data['query']  = $this->Welcome_model->search_vehicles_bike($info);
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
    $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	$data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
 	$this->load->view('new_bikes', $data);	
 }
	
	public function bike_type_filter()
        {
            $bike_type = $_GET['bike_type'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            
            $data['query'] = $this->Welcome_model->bikes_list_by_type($bike_type);
            $data['query1'] = $this->Welcome_model->bike_names_models();
            $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	    $data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
            //var_dump($data['query']);
            $this->load->view('new_bikes', $data);
        }
        
        public function bike_price_filter2()
        {
            $price = $_GET['price'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            if($price!='Above'){
                $data['query'] = $this->Welcome_model->bikes_list_by_price($price);
            }else{
                $data['query'] = $this->Welcome_model->bikes_list_by_price2($price);
            }
            $data['query1'] = $this->Welcome_model->bike_names_models();
            $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	    $data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();
            //var_dump($data['query']);
            $this->load->view('new_bikes', $data);
        }
        
       
        
         public function bike_price_filter3()
        {
         $price = $_GET['price'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_price2($price);
            $data['query1'] = $this->Welcome_model->bike_names_models();
        //var_dump($data['query']);
		$this->load->view('used_bike_listing', $data);

	}

public function new_bike(){
  //$data['query'] = $this->Welcome_model->fetch_new_bikes();
	//$data['query_model'] = $this->Welcome_model->fetch_bike_models2();
	//$data['query_variant'] = $this->Welcome_model->fetch_bike_variants2();

 $data['query'] = $this->Welcome_model->fetch_new_bikes_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('new_bikes_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("new_bikes",$data);
 }
}

public function new_launch_bike(){
    
     $data['query'] = $this->Welcome_model->fetch_new_bikes_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('new_launch_bikes_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
  $this->load->view("new_launch_bike",$data);
 }
}

public function bike_details(){
	 	$id = $_GET['id'];
	 	$data['query'] = $this->Welcome_model->fetch_bike_by_id($id);
		$data['query_model_variants'] = $this->Welcome_model->fetch_bike_model_variants($id);
                foreach($data['query'] as $bike_filter){
                    $bikeinfo = json_decode($bike_filter->bike_info);
                    $engineinfo = json_decode($bike_filter->engine_transmission);
                    $bike_type = $bikeinfo->bike_type;
                    $engine_displacement = $engineinfo->displacement; 
                }
                $data['query_bike_type'] = $this->Welcome_model->fetch_bike_by_type_displacement($bike_type,$engine_displacement,$id);
		$data['query_model'] = $this->Welcome_model->fetch_bike_models2();
		$data['query_variants'] = $this->Welcome_model->fetch_bike_variants2();
		$this->load->view('bike_detail',$data);
                
                
	}
        
    public function bike_detail_used(){
	 	$id = $_GET['id'];
	 	$data['query'] = $this->Welcome_model->fetch_used_bike_by_id($id);
                $data['query1'] = $this->Welcome_model->bike_names_models();
		$this->load->view('bike_detail_used',$data);
	}    
        
    public function post_bike()
    {
    
    $data['query_cities'] = $this->Welcome_model->fetch_cities();
		$data['query_bike'] = $this->Welcome_model->fetch_bike_names();
		
		$this->load->view('post_bike_requirment',$data);
	}
	
	public function post_bike_requirment_step2()
	{	
	
    $this->session->set_userdata('vehicle_type',$_POST['vehicle_type']); 
	$this->session->set_userdata('vehicle_info',$_POST);
	$this->session->set_userdata('price',$_POST['price']);
	
    $this->load->view('post_bike_requirment_step2');
    }
	
	public function post_bike_requirment_step3()
	{
	    
        $count = count($_FILES["additional_info"]["name"]["img"]);

//var_dump($_FILES);

for($i=0; $i<$count; $i++){
   //$_FILES['additional_info']['img']['name'][$i];
   $bike_name[$i] = $_FILES["additional_info"]["name"]["img"][$i];
   $bike_type[$i] = $_FILES['additional_info']['type']['img'][$i];
   $bike_temp[$i] = $_FILES['additional_info']['tmp_name']['img'][$i];
   $target_dir = 'uploads/used_bikes/';
   $target_file[$i] = $target_dir . basename($bike_name[$i]);
   move_uploaded_file($bike_temp[$i], $target_file[$i]);
   $_POST['additional_info']['img'][$i] =  $_FILES["additional_info"]["name"]["img"][$i];
  }
    $this->session->set_userdata('additional_info',$_POST);
    
	$this->load->view('post_bike_requirment_step3');
    }
	
	public function submit_bike_data(){
	
  	$vehicle_info = json_encode($_SESSION['vehicle_info']);

  	$additional_info = json_encode($_SESSION['additional_info']);

  	$user_info = json_encode($_POST['user_info']);

  	$data = array('vehicle_type' => $_SESSION['vehicle_type'],
                  'vehicle_info' => $vehicle_info,
                  'price' => $_SESSION['price'],
                  'additional_info' => $additional_info,
  	              'user_info' => $user_info);
     
     $data1 =  array('name' => $_POST['user_info']['name'],
                     'email' => $_POST['user_info']['email'],
                     'phone' => $_POST['user_info']['phone'],
					 'user_type' => $_POST['user_type']);

     //$this->User_model->insert_user2($data1);
  	               
  	 $this->User_model->insert_bike($data);
     
     $data['msg'] = "Your Bike has been successfully published";
    
       header("Location:".base_url()."index.php/Welcome/success");
 
     } 
	 
	 public function success()
	 {
		 
		$data['msg'] = "Your Ad has been successfully published";
        $this->load->view('success',$data);			
	 }	 

public function fetch_bike_model()
    {
        $bike_id = $_POST['bike_id'];
		$data['query_model'] = $this->Welcome_model->fetch_bike_models($bike_id);
		
		//$count = count($data);
		echo "<option value='default'>Select Model</option>";
                echo "<option value='default'>All Models</option>";
   foreach($data['query_model'] as $model) 
        { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$model->id.">".$model->model_name."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";
        }
    }
    
    public function fetch_bike_name()
    {
        $model_id = $_POST['model_id'];
		$data['query_model'] = $this->Welcome_model->fetch_bike_names2($model_id);
		
		//$count = count($data);
		foreach($data['query_model'] as $model) 
        { 
    //$data_car[$i] = $data[$i]['models'];
    echo $model->bike_name." ".$model->model_name."";
   
        }
        
    }
    
    public function fetch_bike_variant()
    {
        $model_id = $_POST['model_id'];
		$data['query_variant'] = $this->Welcome_model->fetch_bike_variants($model_id);
		
		//$count = count($data);
		echo "<option value=''>Select Variant</option>";
   foreach($data['query_variant'] as $variant) 
        { 
    //$data_car[$i] = $data[$i]['models'];
    echo "<option value=".$variant->id.">".$variant->variant."";
   // echo "<option value='".$data_car[$i]."'>";
    echo "</option>";
        }
    }
    
    public function used_bike_listing()
        {
 
  $data['query'] = $this->Welcome_model->fetch_bike_listing_ajax();
 if(!empty($this->input->get("page"))){
  $result = $this->load->view('bike_listing_ajax_data', $data);
  echo json_encode($result);
 }
 else
 {
$this->load->view('used_bike_listing', $data);
 }
	 

	}
        
     public function bike_filter()
        {
         $bike_id = $_POST['bike_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_bike_id($bike_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
        //var_dump($data['query']);
		$this->load->view('bike_details_after_filter', $data);

	}
        
        public function bike_model_filter()
        {
         $model_id = $_POST['model_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_model_id($model_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
        //var_dump($data['query']);
		$this->load->view('bike_details_after_filter', $data);

	} 
        
         public function bike_price_filter()
        {
         $price = $_POST['price'];
         $model_id = $_POST['model_id'];
         $bike_id = $_POST['bike_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_price($price,$bike_id,$model_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
        //var_dump($data['query']);
		$this->load->view('bike_details_after_filter', $data);

	}
        
        
          public function bike_kms_filter()
        {
            $kms = $_POST['kms'];  
         $price = $_POST['price'];
         $model_id = $_POST['model_id'];
         $bike_id = $_POST['bike_id'];
            $data['query_bike_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->used_bikes_list_by_kms($kms,$price,$bike_id,$model_id);
            $data['query1'] = $this->Welcome_model->bike_names_models();
        //var_dump($data['query']);
		$this->load->view('bike_details_after_filter', $data);

	}
        
        public function bike_compare(){
            $data['query_name'] = $this->Welcome_model->fetch_bike_names();
            $data['query'] = $this->Welcome_model->fetch_list_cars();
	    $this->load->view('compare_bike',$data);
	}
        
        public function first_bike_compare(){
$bike1 = $_POST['bike_id'];
$model1 = $_POST['model_id'];
$variant1 = $_POST['variant_id'];
$data = $this->Welcome_model->compare_bike1($bike1,$model1,$variant1);
//var_dump($data);
//echo $data[0]->car_name;
echo trim($data[0]->bike_image,'"');

}

        public function bike_comarision_details(){
$bike1 = $_POST['bike1_val'];
$model1 = $_POST['model1_val'];
$variant1 = $_POST['variant1_val'];
$bike2 = $_POST['bike2_val'];
$model2 = $_POST['model2_val'];
$variant2 = $_POST['variant2_val'];
$bike3 = $_POST['bike3_val'];
$model3 = $_POST['model3_val'];
$variant3 = $_POST['variant3_val'];
//var_dump($variant3);
if($variant3==""){
    $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
    $data['query1'] = $this->Welcome_model->compare_all_bikes1($bike1,$model1,$variant1);
    $data['query2'] = $this->Welcome_model->compare_all_bikes2($bike2,$model2,$variant2);
    $this->load->view('bike_comarision_details',$data);
    }else{
        $data['query_model'] = $this->Welcome_model->fetch_bike_models2();
        $data['query1'] = $this->Welcome_model->compare_all_bikes1($bike1,$model1,$variant1);
        $data['query2'] = $this->Welcome_model->compare_all_bikes2($bike2,$model2,$variant2);
        $data['query3'] = $this->Welcome_model->compare_all_bikes3($bike3,$model3,$variant3);
        $this->load->view('bike_comparision_details2',$data);
    }
}

public function insert_buyer_details(){
    $buyer_name = $_POST['buyer_name'];
    $buyer_email = $_POST['buyer_email'];
    $buyer_phone = $_POST['buyer_phone'];
    $vehicle_id = $_POST['vehicle_id'];
    $user_info = $_GET['user_name'];
    $user_info = $_GET['user_phone'];
    
//            $data =  array('name' => $buyer_name,
//                            'email' => $buyer_email,
//                            'phone' => $buyer_phone);
            $data1 =  array('name' => $buyer_name,
                            'email' => $buyer_email,
                            'phone' => $buyer_phone,
                            'vehicle_id'=> $vehicle_id,
		            'user_type' => 'bike',
                            'user_info' => $user_info);

     $this->User_model->insert_buyer($data1);   
     //$this->User_model->insert_buyer_details($data);
	}
        
public function emi_calculator_bike(){
      $data['query_name'] = $this->Welcome_model->fetch_bike_names();
	$this->load->view('emi_calculate_bike',$data);
}

public function select_price_bike(){
	$bike_id = $_POST['bike_id'];
	//echo $car;
    $model_id = $_POST['model_id'];
   //echo $model;
    $variant_id = $_POST['variant_id'];
   //echo $variant;

    $price = $this->Welcome_model->get_bike_price($bike_id,$model_id,$variant_id);

     echo trim($price[0]->price,'"');
    //var_dump($data['query']);
}

}
