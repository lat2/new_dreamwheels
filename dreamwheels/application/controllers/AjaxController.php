<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class AjaxController extends CI_Controller {


  private $perPage = 5;


	/**
    * Get All Data from this method.
    *
    * @return Response
   */
    public function index()
    {
     $this->load->database();
     $count = $this->db->get('cars2')->num_rows();


     if(!empty($this->input->get("page"))){


      $start = ceil($this->input->get("page") * $this->perPage);
      $this->db->where('status',1);
			 $this->db->group_by("car_info->'$.model'");
      $query = $this->db->limit($start, $this->perPage)->get("cars2");
      $data['posts'] = $query->result();


      $result = $this->load->view('data', $data);
      echo json_encode($result);


     }else{
      $query = $this->db->limit(5, $this->perPage)->get("cars2");
      $this->db->where('status',1);
			 $this->db->group_by("car_info->'$.model'");
      $data['posts'] = $query->result();


	    $this->load->view('myPost', $data);
     }
    }


}